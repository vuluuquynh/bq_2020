﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class change_v3:danplay
    {
        private string type;
        private string list_via;
        private string id_pc_run;
        private string dang_xuat;
        private string bat_2fa;
        private string del_profile;
        private string them_email;
        public change_v3(string type, string list_via, string id_pc_run, string dang_xuat, string bat_2fa, string del_profile, string them_email)
        {
            this.type = type;
            this.list_via = list_via;
            this.id_pc_run = id_pc_run;
            this.dang_xuat = dang_xuat;
            this.bat_2fa = bat_2fa;
            this.del_profile = del_profile;
            this.them_email = them_email;
        }

        public void start_change_info()
        {
            try
            {
                #region account_id - account_name - account_pass - account_cookie - html_page - id_facebook - ghichu - kq_tao_yh                                               
                int tien_trinh = 0; string html_check_login = ""; string id_facebook = ""; string kq_tao_yh = ""; //Khởi tạo các giá trị ban đầu
                string html_page = ""; string account_id = ""; string account_name = ""; string account_pass = ""; string account_cookie = ""; string ghichu = "auto"; string domain_tao_email = null;  //Các giá trị thường xuyên sữ dụng
                try
                {
                    string profile_via = key_danplay("change", "list_acc_via&list_via=" + list_via + "&type=" + type + "&id_pc=" + id_pc_run); string[] arr_list = profile_via.Split('|');
                    account_id = arr_list[0]; account_name = arr_list[1].Trim(); account_pass = arr_list[2]; account_cookie = arr_list[3];
                    if (account_pass == "no-pass" || account_pass == "danplay" || account_pass == "danplay_2") { tien_trinh = 55; } else { tien_trinh = 1; }  //Rẻ nhánh chương trình đi 2 tác vụ khác nhau
                }
                catch { tien_trinh = 99; }

                if (tien_trinh == 1)
                {
                    string kq_tao = null;
                    try { kq_tao = profile_danplay_mau_pc(account_name); } catch { tien_trinh = 99; ghichu = "Lỗi tạo Profile"; }
                    if (kq_tao == "oke")
                    {
                        try
                        {
                            driver.Url = "https://facebook.com/login"; driver.Navigate(); Thread.Sleep(3000);
                            html_check_login = driver.PageSource;
                            id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                            if (id_facebook.Length > 7) { tien_trinh = 5; } else { tien_trinh = 2; }
                        }
                        catch { tien_trinh = 99; ghichu = "Lỗi URL facebook -> Mạng yếu ->b01"; }
                    }
                    else { tien_trinh = 99; ghichu = "Lỗi tạo Profile v2"; }
                }
                #endregion

                try
                {
                    #region tiến trình 2 - chưa login sẳn ở profile
                    if (tien_trinh == 2)
                    {
                        if (account_cookie != "not" && id_facebook.Length < 7)
                        {
                            var temp = account_cookie.Split(';');
                            foreach (var item in temp)
                            {
                                var temp2 = item.Split('=');
                                if (temp2.Count() > 1)
                                { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                            }
                            try
                            {
                                driver.Navigate().Refresh(); Thread.Sleep(3000);
                                html_check_login = driver.PageSource;
                                id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                                if (driver.Url.Contains("checkpoint")) { tien_trinh = 4; } else { if (id_facebook.Length > 7) { tien_trinh = 5; } else { tien_trinh = 3; } }
                            }
                            catch { tien_trinh = 99; ghichu = "Lỗi load lại cookie -> Mạng yếu"; }
                        }

                        if (tien_trinh == 3 || account_cookie == "not")
                        {
                            string kq_lg_fb = null;
                            if (account_cookie == "not") { kq_lg_fb = login_facebook(account_name, account_pass); } else { kq_lg_fb = login_facebook(account_name, account_pass, "https://facebook.com/login"); }
                            if (kq_lg_fb == "check") { tien_trinh = 4; }
                            if (kq_lg_fb == "login") { tien_trinh = 44; ghichu = "Login name|pass -> Lỗi ->Mạng yếu"; }
                            if (kq_lg_fb.Length > 7) { tien_trinh = 5; id_facebook = kq_lg_fb; }
                        }
                    }
                    #endregion

                    #region nếu đang nhập thành công
                    if (tien_trinh == 5)
                    {
                        key_danplay("change", "update_uid_facebook&id_acc=" + account_id + "&uid=" + id_facebook); //Lưu lại ID facebook lên sever     
                        doi_mat_khau(account_id, account_pass);
                        post_va_xoa(id_facebook);
                        like_dao_v2();
                        doc_thong_bao_pc();
                        key_danplay("change", "change_v3_status_8&id_account=" + account_id);
                        try { string get_cookie_s = GetCoookieSelenium("https://www.facebook.com/"); if (get_cookie_s != "") { key_danplay("change", "add_cookie_sever&id_account=" + account_id + "&cookie=" + get_cookie_s); } } catch { } //Lấy cookie và đưa lên sever!
                    }                                        
                    #endregion

                    #region nếu chọn thêm email
                    if (tien_trinh == 5 && them_email == "1")
                    {
                        string kq_xoa_mail = xoa_email_facebook_v1(account_pass, kq_tao_yh); Thread.Sleep(3000);
                        if (kq_xoa_mail == "check") { tien_trinh = 4; }
                        if (kq_xoa_mail == "oke") { key_danplay("change", "dac_biet&loai=changef&chu_y=oke&id_via=" + account_id); }
                        if (kq_xoa_mail == "error") { key_danplay("change", "dac_biet&loai=changef&chu_y=error&id_via=" + account_id); }

                        //Thêm email - very email
                        if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                        {

                            int them_va_very_email = 7;
                            for (int i = 0; i < them_va_very_email; i++)
                            {
                                string email_them_oke = thay_email_change_v1(account_pass, account_id); int kqaua = 7; try { kqaua = Convert.ToInt32(email_them_oke); } catch {kqaua = 0; kq_tao_yh = email_them_oke; }
                                them_va_very_email = kqaua;
                                if (email_them_oke == "sai_pass") { i = 100; them_va_very_email = 50; }
                            }
                            if (them_va_very_email > 5)
                            {
                                if (them_va_very_email == 50) { ghichu = "Mật khẩu đã bị đổi"; } else { ghichu = "Không xác nhận được email hoặc sai pass"; }
                                tien_trinh = 99;
                            }
                        }

                        //Tiếp tục xóa email củ - số điện thoại
                        if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                        {
                            string kq_del_mail_1 = xoa_email_facebook_v1(account_pass, kq_tao_yh);
                            if (kq_del_mail_1 == "check") { tien_trinh = 4; }
                            string kq_del_phone = del_phone_fb_v1(account_pass);
                            if (kq_del_phone == "check") { tien_trinh = 4; }
                            xoa_email_facebook_v1(account_pass, kq_tao_yh);
                        }

                    }
                    #endregion

                    #region bật 2FA nếu đc yêu cầu
                    if (bat_2fa == "1")
                    {
                        string kq_2fa = bat_2fa_facebook(account_id, account_pass);
                        if (kq_2fa == "check") { tien_trinh = 4; }
                        if (kq_2fa == "oke") { key_danplay("change", "dac_biet&loai=2fa&chu_y=oke&id_via=" + account_id); }
                    }
                    #endregion                    

                    #region check fanpage
                    if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                    {                        
                        try
                        {
                            driver.Url = "https://www.facebook.com/bookmarks/pages"; driver.Navigate(); Thread.Sleep(3000);
                            string html_fanpage_page_11 = driver.PageSource;
                            var list_fanpage_1 = Regex.Matches(html_fanpage_page_11, @"<div class=""buttonWrap""><div class=""uiSideNavEditButton""><a title=""(.*?)ref=bookmarks", RegexOptions.Singleline);
                            string fanpage_data = null;
                            foreach (var fanpage_1 in list_fanpage_1)
                            {
                                string[] chuoi_1 = fanpage_1.ToString().Split('"');  //18 là tên fanpage - 20 là url
                                string fan_ten = chuoi_1[19]; string fan_url = chuoi_1[21].Replace("/?ref=bookmarks", "");
                                fanpage_data = fan_ten + "-" + fan_url + "|" + fanpage_data;
                            }
                            if (fanpage_data != null)
                            {
                                key_danplay("change", "dac_biet&loai=fanpage&chu_y=" + fanpage_data + "&id_via=" + account_id);
                            }
                        } catch { key_danplay("change", "dac_biet&loai=fanpage&chu_y=error&id_via=" + account_id); }
                    }
                    #endregion

                    #region Kiểm tra acc có tài khoản ADS không
                    if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                    {
                        try
                        {
                            driver.Url = "https://www.facebook.com/ads/manager/accounts/"; driver.Navigate(); Thread.Sleep(3000);
                            string html_ads_page_11 = driver.PageSource;
                            string facebook_ads = Regex.Match(html_ads_page_11, @"<li><div class=""_1ls"">").Value;
                            if (facebook_ads.Length > 10) { key_danplay("change", "dac_biet&loai=ip&chu_y=ADS-yes&id_via=" + account_id); } else { key_danplay("change", "dac_biet&loai=ip&chu_y=ADS-no&id_via=" + account_id); }
                        } catch { key_danplay("change", "dac_biet&loai=ip&chu_y=error&id_via=" + account_id); }
                    }
                    #endregion                    

                    #region tiến trình 44 - sai thông tin
                    if (tien_trinh == 44)
                    {
                        key_danplay("change", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);
                        //Xóa thư mục để cho nhẹ máy
                        string xoa_profile_sai = "Profile\\" + account_name;
                        try { driver.Close(); driver.Quit(); } catch { }
                        try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                        tien_trinh = 99;
                    }
                    #endregion

                    #region tiến trình 4 - đăng nhập bị check - Kiểm tra các lỗi vì sao bị check
                    if (tien_trinh == 4)
                    {
                        string url_bi_check = driver.Url;
                        key_danplay("change", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check);
                        //Đổi tên thư mục nếu có giá trị ID facebook
                        try { driver.Close(); driver.Quit(); } catch { }
                        try
                        {
                            if (id_facebook.Length > 7)
                            {
                                string thu_muc_cu = "Profile\\" + account_name;
                                string thu_muc_moi = "Profile\\" + id_facebook;
                                Directory.Move(thu_muc_cu, thu_muc_moi);
                            }
                        }
                        catch { }
                        tien_trinh = 99;
                    }
                    #endregion

                    #region báo cáo lỗi nếu khách vào chế độ recover
                    if (tien_trinh == 55)
                    {
                        MessageBox.Show("Yahoo đang gặp lỗi, không thể tạo được email bí danh", "Thông báo lổi");
                    }
                    #endregion

                    #region tiến trình 99 - Kết thúc luồng và báo cáo tình trạng
                    if (tien_trinh == 99)
                    {
                        key_danplay("change", "dac_biet&loai=note_sys&chu_y=" + ghichu + "&id_via=" + account_id);
                        key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                        for (int i = 0; i < 10; i++) { try { driver.Close(); driver.Quit(); Thread.Sleep(500); } catch { } }
                        if (del_profile == "1") { del_profile_cooke(account_name); }
                    }
                    else
                    {
                        key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                        for (int i = 0; i < 10; i++) { try { driver.Close(); driver.Quit(); Thread.Sleep(500); } catch { } }
                        if (del_profile == "1") { del_profile_cooke(account_name); }
                    }

                    try
                    {
                        if (id_facebook.Length > 7)
                        {
                            for(int i=0; i < 10; i++) { try { driver.Close(); driver.Quit(); Thread.Sleep(500); } catch { } }
                            string thu_muc_cu = "Profile\\" + account_name;
                            string thu_muc_moi = "Profile\\" + id_facebook;
                            Directory.Move(thu_muc_cu, thu_muc_moi);
                        }
                    } catch { }

                    #endregion

                }
                catch
                {
                    #region báo cáo dừng luồng để bắt đầu
                    try
                    {
                        key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                        for (int i = 0; i < 10; i++) { try { driver.Close(); driver.Quit(); Thread.Sleep(500); } catch { } }
                        if (del_profile == "1") { del_profile_cooke(account_name); }

                        if (id_facebook.Length > 7)
                        {
                            for (int i = 0; i < 10; i++) { try { driver.Close(); driver.Quit(); } catch { } }
                            string thu_muc_cu = "Profile\\" + account_name;
                            string thu_muc_moi = "Profile\\" + id_facebook;
                            Directory.Move(thu_muc_cu, thu_muc_moi);
                        }
                    }  catch { }
                    #endregion
                }

            } catch { }
            
            




        }
    }
}
