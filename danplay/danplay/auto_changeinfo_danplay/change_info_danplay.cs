﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{    
    class change_info_danplay:danplay
    {               
        private string type;
        private string list_via;
        private string id_pc_run;
        private string dang_xuat;
        private string bat_2fa;
        public change_info_danplay(string type, string list_via, string id_pc_run, string dang_xuat, string bat_2fa)
        {
            this.type = type;
            this.list_via = list_via;
            this.id_pc_run = id_pc_run;
            this.dang_xuat = dang_xuat;
            this.bat_2fa = bat_2fa;
        }
        
        #region YAHOO: tao_email_yahoo
        string tao_email_yahoo(string tao_email_new = null)
        {
            string kq_tao = "";
            string email_new = null;
            if (tao_email_new != null) { email_new = tao_email_new; } else { email_new = random_email_yahoo(12); } //Truyền vào giá trị email cần tạo            
            try {
                int auto_yahoo = 0;
                driver.Url = "https://mail.yahoo.com/d/settings/1";
                driver.Navigate(); Thread.Sleep(3000);
                if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; }

                #region login yahoo
                while (driver.Url.Contains("login.yahoo.com") && auto_yahoo !=2)
                {

                    string html_yahoo_mail = key_danplay("change", "get_mail_yahoo&id_pc=" + id_pc_run); string[] arr_list_yh = html_yahoo_mail.Split('|');
                    while (html_yahoo_mail.Length < 5)
                    {
                        Thread.Sleep(30000);
                        html_yahoo_mail = key_danplay("change", "get_mail_yahoo&id_pc=" + id_pc_run); arr_list_yh = html_yahoo_mail.Split('|');
                    }
                    string yahoo_id = arr_list_yh[0]; string yahoo_login = arr_list_yh[1]; string yahoo_pass = arr_list_yh[2];                    
                    var yahoo_01 = driver.FindElementByXPath("//*[@id=\"login-username\"]");
                    yahoo_01.SendKeys(yahoo_login); Thread.Sleep(1000);                    
                    yahoo_01.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                    var yahoo_03 = driver.FindElementByXPath("//*[@id=\"login-passwd\"]");
                    yahoo_03.SendKeys(yahoo_pass); Thread.Sleep(1000);
                    yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; } else {
                        Thread.Sleep(5000);
                        if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; } else { auto_yahoo = 4; html_yahoo_mail = key_danplay("change", "get_mail_yahoo&id_yahoo=" + yahoo_id); }
                    }

                    //Load lại nếu login không thành công
                    if (driver.Url.Contains("login.yahoo.com")) { driver.Url = "https://mail.yahoo.com/d/settings/1"; driver.Navigate(); Thread.Sleep(3000); }
                }
                #endregion

                #region login oke tạo email bí danh
                if (auto_yahoo == 2)
                {                    
                    try
                    { //Xóa mail bí danh củ nếu có
                        var yahoo_05 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li");
                        yahoo_05.Click(); Thread.Sleep(2000);
                        var yahoo_06 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[4]/button");
                        yahoo_06.Click(); Thread.Sleep(2000);
                        var yahoo_07 = driver.FindElementByXPath("//*[@id=\"modal-outer\"]/div/div/div[4]/button[1]");
                        yahoo_07.Click(); Thread.Sleep(2000);
                    }
                    catch { }

                    try
                    {
                        var yahoo_08 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/div[3]/button");
                        yahoo_08.Click(); Thread.Sleep(2000);
                        var yahoo_09 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/div/input");
                        yahoo_09.SendKeys(email_new);
                    }
                    catch { }

                    var yahoo_10 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[3]/button[1]/span");
                    yahoo_10.Click(); Thread.Sleep(5000);
                    string yahoo_10_kq = "";
                    try { yahoo_10_kq = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li/div[2]/div/p/span[1]").GetAttribute("<span>"); } catch { }


                    #region kiểm tra xem tạo thành công không, nếu ko thì cho lặp lại
                    if (yahoo_10_kq == email_new) { auto_yahoo = 9; }
                    else
                    {
                        for (int i1 = 0; i1 < 5; i1++)
                        {
                            #region thêm lại lần 2 nếu nó bị lỗi
                            try
                            { //Nếu ko thêm bị lỗi thì thêm lại
                                var yahoo_11 = driver.FindElementsByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/span");
                                if (yahoo_11.Count > 0)
                                {
                                    if (i1 > 3) { email_new = random_email_yahoo(13); }
                                    var yahoo_09_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/div/input");
                                    yahoo_09_1.SendKeys(OpenQA.Selenium.Keys.Control + "a"); Thread.Sleep(500);
                                    yahoo_09_1.SendKeys(email_new);

                                    var yahoo_10_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[3]/button[1]/span");
                                    yahoo_10_1.Click(); Thread.Sleep(2000);
                                    string yahoo_10_kq_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li/div[2]/div/p/span[1]").GetAttribute("<span>");
                                    if (yahoo_10_kq_1 == email_new) { auto_yahoo = 9; i1 = 20; }
                                }
                            }
                            catch { }
                            #endregion
                        }

                    }
                    #endregion

                }
                #endregion
                kq_tao = email_new;
            } catch { kq_tao = "error"; key_danplay("change", "get_mail_yahoo&id_pc=" + id_pc_run); }
                return kq_tao;
        }
        #endregion

        #region YAHOO: yahoo_very_facebook: oke - not_email - error
        string yahoo_very_facebook(string address_email)
        {
            string kq_Check_email_very_facebook = "";
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    driver.Url = "https://mail.yahoo.com/d/folders/1";
                    driver.Navigate(); Thread.Sleep(3000);
                    string data_1 = driver.PageSource;
                    string chuoi_email_very = Regex.Match(data_1, @"/d/folders/1/messages/.*?" + address_email).Value.Replace("/d/folders/1/messages/", "");
                    string[] arry_very_yahoo = chuoi_email_very.Split('"');
                    if (arry_very_yahoo[0] != "")
                    {
                        driver.Url = "https://mail.yahoo.com/d/folders/1/messages/" + arry_very_yahoo[0];
                        Thread.Sleep(3000);
                        data_1 = driver.PageSource;
                        string chuoi_email_url_very = Regex.Match(data_1, @"https://www.facebook.com/confirmcontact.php.*?(?="" style)").Value.Replace("amp;", "");
                        driver.Url = chuoi_email_url_very;
                        driver.Navigate(); Thread.Sleep(3000);
                        i = 100;
                        kq_Check_email_very_facebook = "oke";
                    }
                    else { kq_Check_email_very_facebook = "not_email"; }
                }
                catch { kq_Check_email_very_facebook = "error"; }
            }
            return kq_Check_email_very_facebook;
        }
        #endregion

        #region FACEBOOK: xoa_email_facebook: check - oke - error
        string xoa_email_facebook(string account_pass)
        {
            string ketqua = "";
            try {
                for (int i = 0; i < 10; i++)
                {
                    driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                    driver.Navigate(); Thread.Sleep(2000);
                    var del_email_001 = driver.FindElementsByName("remove_emails[]");
                    if (del_email_001.Count > 0)
                    {
                        try
                        {
                            string html_page = driver.PageSource;
                            string id_element_click = Regex.Match(html_page, @"fbSettingsRemovablesList extended_field.*?(?=""><li class=""fbSettingsRemovablesItem)").Value.Replace("fbSettingsRemovablesList extended_field _4kg _6-h _704 _6-i\" id=\"", "");
                            string id_element_xac_nhan = Regex.Match(html_page, @"submit uiButton uiButtonConfirm.*?(?=""><input value)").Value.Replace("submit uiButton uiButtonConfirm\" id=\"", "");
                            string[] arr_element_xn = id_element_xac_nhan.Split('"');
                            var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/span[2]/span");
                            del_email_002.Click(); Thread.Sleep(2000);
                            var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                            del_email_003.Click(); Thread.Sleep(2000);
                            var del_email_004 = driver.FindElementByName("ajax_password");
                            del_email_004.SendKeys(account_pass); Thread.Sleep(2000);
                            del_email_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                            driver.Navigate(); Thread.Sleep(2000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }
                        catch
                        {
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                            driver.Navigate(); Thread.Sleep(2000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }
                    }
                    else { i = 100; }
                    Thread.Sleep(5000);
                    if (driver.Url.Contains("checkpoint")) { ketqua = "check"; i = 100; } else { ketqua = "oke"; i = 100; }
                }
            } catch { ketqua = "error"; }
            return ketqua;
        }
        #endregion

        #region FACEBOOK: them_email_facebook
        string them_email_facebook(string pass_add_email, string email_yahoo = null)
        {
            string kq_them = ""; string new_email_add_facebook = random_email_yahoo(12); if (email_yahoo != null) { new_email_add_facebook = email_yahoo; }
            try
            {
                driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(3000);

                #region Tìm và xóa đi địa chỉ email đã thêm nhưng chưa very
                var add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                int del_add_email_vl_5 = 5;
                for (int i = 0; i < del_add_email_vl_5; i++)
                {
                    if (add_email_001.Count > 0)
                    {
                        var add_email_002 = driver.FindElementByClassName("SettingsEmailPendingCancel");
                        add_email_002.Click();
                        Thread.Sleep(3000);
                    }
                    add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                    if (add_email_001.Count == 0) { i = 10; }
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 1
                var add_email_003 = driver.FindElementsByClassName("PendingEmailSection");
                if (add_email_003.Count > 0)
                {
                    try
                    {
                        var add_email_004 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/a");
                        add_email_004.Click(); Thread.Sleep(5000);
                    }
                    catch
                    {
                        var add_email_003a_2 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/div[2]/a[3]");
                        add_email_003a_2.Click(); Thread.Sleep(5000);
                        string html_hientai = driver.PageSource;
                        string id_click_add_email_001 = Regex.Match(html_hientai, @"action=""/add_contactpoint/dialog/submit/"" method=""post"" onsubmit.*?(?=""><input type=""hidden"")").Value.Replace("action=\"/add_contactpoint/dialog/submit/\" method=\"post\" onsubmit=\"\" id=\"", "");
                        var add_email_006_2 = driver.FindElementByXPath("//*[@id=\"" + id_click_add_email_001 + "\"]/div[2]/div/table/tbody/tr/td/input");
                        add_email_006_2.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                        add_email_006_2.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    }
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 2
                var add_email_005 = driver.FindElementsByName("new_email");
                if (add_email_005.Count > 0)
                {
                    var add_email_006 = driver.FindElementByName("new_email");
                    add_email_006.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                    add_email_006.SendKeys(OpenQA.Selenium.Keys.Enter);Thread.Sleep(5000);
                }
                #endregion

                #region thêm mật khẩu nếu bị hỏi
                var add_email_007 = driver.FindElementsByName("ajax_password");
                if (add_email_007.Count > 0)
                {
                    var add_email_008 = driver.FindElementByName("ajax_password");
                    add_email_008.SendKeys(pass_add_email); Thread.Sleep(2000);
                    add_email_008.SendKeys(OpenQA.Selenium.Keys.Enter);Thread.Sleep(5000);

                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_them = "check"; } else { kq_them = "oke"; }
            }
            catch { kq_them = "error"; }

            return kq_them;
        }
        #endregion

        #region FACEBOOK: del_phone_fb: check - oke - error
        string del_phone_fb(string pass_facebook)
        {
            string kq_Delete_phone_account_facebook = "";
            try
            {

                for (int i = 0; i < 10; i++)
                {
                    driver.Url = "https://www.facebook.com/settings?tab=mobile";
                    driver.Navigate(); Thread.Sleep(3000);
                    string data_1_1_1 = driver.PageSource;
                    string del_so_phone = Regex.Match(data_1_1_1, @"</span><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</span><br><span id=\"", "");

                    #region trường hợp 1
                    if (del_so_phone == "")
                    {
                        del_so_phone = Regex.Match(data_1_1_1, @"</a><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</a><br><span id=\"", "");

                    }
                    #endregion

                    #region trường hợp 2
                    if (del_so_phone != "")
                    {

                        try
                        {
                            var del_so_phone_001 = driver.FindElementByXPath("//*[@id=\"" + del_so_phone + "\"]/a");
                            del_so_phone_001.Click(); Thread.Sleep(1000);

                            #region xét hết các trường hợp có nhiều số điện thoại
                            for (int ii001 = 0; ii001 < 20; ii001++)
                            {
                                try
                                {
                                    var del_so_phone_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii001 + "]/div[2]/div/div/div/div[2]/div/div/label[2]");
                                    del_so_phone_002.Click(); Thread.Sleep(1000);
                                }
                                catch { }
                            }
                            #endregion

                            Thread.Sleep(3000);
                            string data_1_1_2 = driver.PageSource;
                            string del_so_phone_button = Regex.Match(data_1_1_2, @"layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy"" type=""submit"" id="".*?(?="">)").Value.Replace("layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy\" type=\"submit\" id=\"", "");

                            try
                            {
                                var del_so_phone_003 = driver.FindElementById(del_so_phone_button); Thread.Sleep(1000);
                                del_so_phone_003.Click(); Thread.Sleep(1000);
                            }
                            catch
                            {
                                var del_so_phone_003_1 = driver.FindElementByXPath("//*[@id=\"globalContainer\"]/div[3]/div/div/div/div[2]/button");
                                del_so_phone_003_1.Click(); Thread.Sleep(1000);
                            }

                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }

                    }
                    else { i = 100; }
                    #endregion
                }

                #region xóa số điện thoại mặc định
                for (int imd = 0; imd < 5; imd++)
                {
                    try
                    {
                        string data_444 = driver.PageSource;
                        string id_button = Regex.Match(data_444, @"</span></strong> · <span id="".*?(?="">)").Value.Replace("</span></strong> · <span id=\"", "");
                        var click_001 = driver.FindElementByXPath("//*[@id=\"" + id_button + "\"]/a");
                        click_001.Click(); Thread.Sleep(2000);

                        for (int ii002 = 0; ii002 < 20; ii002++)
                        {
                            try
                            {
                                var click_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii002 + "]/div[2]/div/div/div/div[3]/button");
                                click_002.Click(); Thread.Sleep(2000);
                            }
                            catch { }
                        }

                        try
                        {
                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }
                    }
                    catch { imd++; }

                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_Delete_phone_account_facebook = "check"; } else { kq_Delete_phone_account_facebook = "oke"; }
            }
            catch { kq_Delete_phone_account_facebook = "error"; }
            return kq_Delete_phone_account_facebook;
        }
        #endregion

        #region Change_pass_logout: check - oke - errorr
        string Change_pass_logout(string pass_world_old, string pass_world_new)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string ka_Change_pass_logout = "";
            try
            {
                driver.Url = "https://m.facebook.com/settings/security/password/";
                driver.Navigate();
                //go_check_poit();
                Thread.Sleep(4000);
                var change_pass_01 = driver.FindElementByName("password_old"); Thread.Sleep(1000);
                change_pass_01.SendKeys(pass_world_old);
                var change_pass_02 = driver.FindElementByName("password_new"); Thread.Sleep(1000);
                change_pass_02.SendKeys(pass_world_new);
                var change_pass_03 = driver.FindElementByName("password_confirm"); Thread.Sleep(1000);
                change_pass_03.SendKeys(pass_world_new);
                var change_pass_04 = driver.FindElementByName("save"); Thread.Sleep(1000);
                change_pass_04.Click();
                Thread.Sleep(3000);
                try
                {
                    if (dang_xuat == "1")
                    {
                        var dang_xuat_01 = driver.FindElementByXPath("//*[@id=\"root\"]/form/div[1]/div/section[2]/fieldset/label[1]/div/div[1]/div/div[2]");
                        dang_xuat_01.Click(); Thread.Sleep(2000);
                        var dang_xuat_02 = driver.FindElementByXPath("//*[@id=\"root\"]/form/div[2]/button");
                        dang_xuat_02.Click(); Thread.Sleep(2000);
                        js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                        var dang_xuat_03 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div[2]/a[1]");
                        dang_xuat_03.Click(); Thread.Sleep(2000);
                        js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                        var dang_xuat_04 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div[2]/a[1]");
                        dang_xuat_04.Click(); Thread.Sleep(3000);
                    }
                }
                catch { }
                if (driver.Url.Contains("checkpoint")) { ka_Change_pass_logout = "check"; } else { ka_Change_pass_logout = "oke"; }
            }
            catch { ka_Change_pass_logout = "error"; }
            return ka_Change_pass_logout;
        }
        #endregion

        public void start_change_info()
        {
            
            #region account_id - account_name - account_pass - account_cookie - html_page - id_facebook - ghichu - kq_tao_yh                                               
            int tien_trinh = 0; string html_check_login = ""; string id_facebook = ""; string kq_tao_yh = ""; //Khởi tạo các giá trị ban đầu
            string html_page = "";  string account_id = ""; string account_name = ""; string account_pass = ""; string account_cookie = ""; string ghichu = "auto";//Các giá trị thường xuyên sữ dụng
            try {
                string profile_via = key_danplay("change", "list_acc_via&list_via=" + list_via + "&type=" + type+ "&id_pc="+ id_pc_run); string[] arr_list = profile_via.Split('|');
                account_id = arr_list[0]; account_name = arr_list[1]; account_pass = arr_list[2]; account_cookie = arr_list[3];
                if (account_pass == "no-pass") { tien_trinh = 55; } else { tien_trinh = 1; }                
            } catch { tien_trinh = 99; }
            
            if(tien_trinh == 1)
            {
                string kq_tao = null;
                try { kq_tao = profile_danplay_mau(account_name); } catch { tien_trinh = 99; }
                if (kq_tao == "oke") { tien_trinh = 1; } else { tien_trinh = 99; }
            }
            #endregion                                    

            #region nếu tạo profile oke thì kiểm tra xem có đăng nhập sẳn không
            if (tien_trinh == 1)
            {
                try {
                    driver.Url = "https://facebook.com"; driver.Navigate(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                    if (id_facebook.Length > 7) { tien_trinh = 2; }
                } catch { tien_trinh = 99; }
            }
            #endregion

            try {
                #region sữ dụng cookie để login nếu nó có tồn tại giá trị ở sever
                if (tien_trinh == 1 && account_cookie != "not" && id_facebook.Length < 7)
                {
                    driver.Manage().Cookies.DeleteAllCookies();
                    var temp = account_cookie.Split(';');
                    foreach (var item in temp)
                    {
                        var temp2 = item.Split('=');
                        if (temp2.Count() > 1)
                        { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                    }
                    driver.Navigate().Refresh(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                    if (id_facebook.Length > 7) { tien_trinh = 2; }
                    if (driver.Url.Contains("checkpoint")) { tien_trinh = 4; }
                    ghichu = "login cookie";
                }
                #endregion

                #region nếu cookie không còn login đc thì đăng nhập bằng id và pass
                if (tien_trinh == 1 && id_facebook.Length < 7)
                {
                    string kq_lg_fb = "";
                    if (account_cookie == "not") { kq_lg_fb = login_facebook(account_name, account_pass); } else { kq_lg_fb = login_facebook(account_name, account_pass, "https://www.facebook.com"); }
                    if (kq_lg_fb == "check") { tien_trinh = 4; }
                    if (kq_lg_fb == "login") { tien_trinh = 3; }
                    if (kq_lg_fb.Length > 7) { tien_trinh = 2; id_facebook = kq_lg_fb; }
                    ghichu = "login pass - " + ghichu;
                }
                #endregion

                #region xóa email củ ra khỏi facebook nếu có
                if (tien_trinh == 2)  //Thử thêm 1 email mới xem bị check không - Nếu very kiểu chờ check thì bỏ qua bước này
                {                    
                    key_danplay("change", "update_uid_facebook&id_acc=" + account_id + "&uid=" + id_facebook);

                    //Bật 2FA khi được chọn
                    if (bat_2fa == "1")
                    {
                        string kq_2fa = bat_2fa_facebook(account_id, account_pass);
                        if (kq_2fa == "check") { tien_trinh = 4; }
                    }
                    quyen_rieng_tu();
                    string kq_xoa_mail = xoa_email_facebook(account_pass); Thread.Sleep(3000);
                    if (kq_xoa_mail == "check") { tien_trinh = 4; }
                    if (kq_xoa_mail == "oke") { ghichu = "xóa mail oke - " + ghichu; }
                    if (kq_xoa_mail == "error") { ghichu = "xóa mail error - " + ghichu; }
                    tien_trinh = 5;
                }
                #endregion

                #region xóa, thêm, very email facebook
                if (tien_trinh == 5)
                {
                    
                    if (!driver.Url.Contains("checkpoint"))
                    {
                        kq_tao_yh = tao_email_yahoo(); //Tạo và lấy kết quả.
                        if (kq_tao_yh.Length > 12)
                        {
                            string kq_them_yh2 = them_email_facebook(account_pass, kq_tao_yh);
                            if (kq_them_yh2 == "check") { tien_trinh = 4; }
                            else
                            {
                                string kq_ver_e_fb = yahoo_very_facebook(kq_tao_yh);
                                if (kq_ver_e_fb == "oke") { tien_trinh = 6; } //not_email
                                if (kq_ver_e_fb == "not_email") { tien_trinh = 98; } //
                            }


                        }
                    }
                    else { tien_trinh = 4; }


                }
                #endregion

                #region xóa email - phone - đổi pass - đăng xuất
                if (tien_trinh == 6)
                {
                    try
                    {
                        string get_cookie_s = GetCoookieSelenium();
                        if (get_cookie_s != "") { key_danplay("change", "add_cookie_sever&id_account=" + account_id + "&cookie = " + get_cookie_s); }                        
                    }
                    catch { }

                    string kq_del_mail_1 = xoa_email_facebook(account_pass);
                    if (kq_del_mail_1 == "check") { tien_trinh = 4; }
                    string kq_del_phone = del_phone_fb(account_pass);
                    if (kq_del_phone == "check") { tien_trinh = 4; }
                    string new_pass_dp = random_pass_01(12);
                    string kd_new_pass = Change_pass_logout(account_pass, new_pass_dp);
                    key_danplay("change", "add_new_pass&pass_cu=" + account_pass + "&pass_moi=" + new_pass_dp + "&yahoo=" + kq_tao_yh + "&id_account=" + account_id);

                    #region đổi tên thư mục
                    try { driver.Close(); driver.Quit(); } catch { }
                    try
                    {
                        string thu_muc_cu = "Profile\\" + account_name;
                        string thu_muc_moi = "Profile\\" + id_facebook;
                        Directory.Move(thu_muc_cu, thu_muc_moi);
                    }
                    catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion

                #region login sai thông tin
                if (tien_trinh == 3)
                {
                    key_danplay("change", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);

                    #region xóa thư mục
                    string xoa_profile_sai = "Profile\\" + account_name;
                    try { driver.Close(); driver.Quit(); } catch { }
                    try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion

                #region login bị check
                if (tien_trinh == 4)
                {
                    string url_bi_check = driver.Url;
                    key_danplay("change", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check);

                    #region đổi tên thư mục
                    try { driver.Close(); driver.Quit(); } catch { }
                    try
                    {
                        string thu_muc_cu = "Profile\\" + account_name;
                        string thu_muc_moi = "Profile\\" + id_facebook;
                        Directory.Move(thu_muc_cu, thu_muc_moi);
                    }
                    catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion

                #region ko very đc yahoo
                if (tien_trinh == 4)
                {
                    string url_bi_check = driver.Url;
                    key_danplay("change", "not_very_yahoo&id_account=" + account_id + "&url_check=" + url_bi_check);

                    #region đổi tên thư mục
                    try { driver.Close(); driver.Quit(); } catch { }
                    try
                    {
                        string thu_muc_cu = "Profile\\" + account_name;
                        string thu_muc_moi = "Profile\\" + id_facebook;
                        Directory.Move(thu_muc_cu, thu_muc_moi);
                    }
                    catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion
            }
            catch { tien_trinh = 99; }


            #region VIA VIA VIA
            if (tien_trinh == 55)
            {
                //MessageBox.Show("0055");
                try {
                    string baocaoloi = null;
                    string kq_tao = null;
                    try { kq_tao = profile_danplay_mau(account_name); } catch { tien_trinh = 99; }

                    string kq_0022 = tao_email_yahoo(account_name);
                    if (kq_0022 != account_name) { tien_trinh = 54; } else { tien_trinh = 56; }

                    if (tien_trinh == 56)
                    {
                        driver.Url = "https://m.facebook.com/login/identify"; driver.Navigate(); Thread.Sleep(3000);

                        try
                        {
                            #region kích vào các nút để bắt đầu
                            var via_001 = driver.FindElementByXPath("//*[@id=\"login_identify_search_placeholder\"]");
                            via_001.SendKeys(account_name); Thread.Sleep(500);
                            via_001.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                            var via_002 = driver.FindElementByXPath("//*[@id=\"contact_point_selector_form\"]/div[1]/button");
                            via_002.Click(); Thread.Sleep(5000);
                            #endregion
                        }
                        catch
                        {

                            var via_002_11 = driver.FindElementsByXPath("//*[@id=\"login_identify_search_error_msg\"]");
                            if (via_002_11.Count > 0) {
                                tien_trinh = 54;
                            }

                               
                            
                        }


                        #region tìm email facebook gửi về
                        string url_ck5422 = null;
                        driver.Url = "https://mail.yahoo.com/d/folders/1"; driver.Navigate(); Thread.Sleep(3000);
                        string html_ssks = driver.PageSource;
                        string s2id_facebook = Regex.Match(html_ssks, @"messageGroupList.*?"",""messages""").Value.Replace("message", "");
                        string[] arr_sfsdf = s2id_facebook.Split('"');
                        int id_email = int.Parse(arr_sfsdf[4]);

                        int check_skfk = 0;
                        for (int s = 0; s < 5; s++)
                        {
                            driver.Url = "https://mail.yahoo.com/d/folders/1/messages/" + id_email; driver.Navigate(); Thread.Sleep(3000);
                            html_ssks = driver.PageSource;
                            url_ck5422 = Regex.Match(html_ssks, @"https://www.facebook.com/recover/code/.*?button").Value.Replace("www", "m");
                            if (url_ck5422.Length > 100) { s = 100; check_skfk = 1; }
                        }

                        for (int a = 0; a < 2; a++)
                        {
                            if (check_skfk == 0)
                            {
                                for (int i = 0; i < 10; i++)
                                {
                                    int check_mai = id_email - i;
                                    driver.Url = "https://mail.yahoo.com/d/folders/1/messages/" + check_mai; driver.Navigate(); Thread.Sleep(3000);
                                    html_ssks = driver.PageSource;
                                    url_ck5422 = Regex.Match(html_ssks, @"https://www.facebook.com/recover/code/.*?button").Value.Replace("www", "m");
                                    if (url_ck5422.Length > 100) { i = 100; check_skfk = 1; a = 10; }
                                }

                            }
                        }

                        #endregion

                        #region làm chuẩn xác URL và load xác nhận
                        url_ck5422 = url_ck5422.Replace("amp;", "");
                        driver.Url = url_ck5422; driver.Navigate(); Thread.Sleep(3000);
                        string pass_new_11sd = random_pass_01(15);
                        key_danplay("change", "update_new_pass_facebook&pass=" + pass_new_11sd + "&id_acc=" + account_id);
                        #endregion


                        try
                        { //Trường hợp 1
                            var via_003 = driver.FindElementByXPath("//*[@id=\"u_0_1\"]");
                            via_003.SendKeys(pass_new_11sd); Thread.Sleep(500);
                            via_003.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { baocaoloi = "Lỗi trường hợp nút 1"; }

                        try
                        { //Trường hợp 2
                            var via_003_1 = driver.FindElementByXPath("//*[@id=\"root\"]/div[1]/div/div/form/section[5]/input");
                            via_003_1.SendKeys(pass_new_11sd); Thread.Sleep(500);
                            via_003_1.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { baocaoloi = "Lỗi nút 2"; }


                        if (driver.Url.Contains("checkpoint")) { key_danplay("change", "login_check&id_account=" + account_id); }

                        try { var via_004 = driver.FindElementByXPath("//*[@id=\"root\"]/form/div[2]/button"); via_004.Click(); key_danplay("change", "new_add_notes&notes=Login bước 1 OKE&id_account=" + account_id); } catch { baocaoloi = "lỗi ko có nút login"; }
                        try { var via_005 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div/div[3]/div[2]/form/div/button"); via_005.Click(); } catch { baocaoloi = "lỗi ko có nút oke"; }

                    }                    
                    tien_trinh = 99;
                } catch { }
            }
            #endregion

            #region báo cáo sai thông tin và xóa thư mục
            if (tien_trinh == 54)
            {                
                key_danplay("change", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);
                string xoa_profile_sai = "Profile\\" + account_name;
                try { driver.Close(); driver.Quit(); } catch { }
                try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                tien_trinh = 99;
                
            }
            #endregion

            #region báo kết quả hoàn thành việc
            if (tien_trinh == 99)
            {
                key_danplay("change", "hoanthanh_auto&id_pc="+ id_pc_run);
                try { driver.Close(); driver.Quit(); } catch { }
            } else
            {
                //Thread.Sleep(1800); //Giữ 180 giây (3 phút) sẽ thoát trong bất kỳ trường hợp nào
                key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                try { driver.Close(); driver.Quit(); } catch { }
            }
            #endregion                      

        }
    }
}
