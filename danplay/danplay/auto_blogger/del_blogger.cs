﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class del_blogger:danplay
    {
        public void xoa_blogger()
        {
            try {
                string data_account = key_danplay("change", "del_blogger_run");
                string[] arr_data = data_account.Split('|');
                if (data_account != "")
                {
                    profile_danplay_blogger(arr_data[1]);
                    string kq_login_account = login_account_google(arr_data[1], arr_data[2], arr_data[3]);
                    if (kq_login_account == "not_email") { key_danplay("change", "bao_email_loi&del_blogger_kq=" + arr_data[0] + "&ketqua=3"); driver.Close(); driver.Quit(); } //sai email
                    if (kq_login_account == "very_phone") { key_danplay("change", "bao_email_loi&del_blogger_kq=" + arr_data[0] + "&ketqua=4"); driver.Close(); driver.Quit(); } //Xác nhận số phone
                    if(kq_login_account == "oke")
                    {
                        int vonglap = 2;
                        while (0 < vonglap)
                        {
                            driver.Navigate().GoToUrl("https://www.blogger.com"); Thread.Sleep(3000);
                            try { //Xóa bước 1
                                string link_1 = driver.Url.Replace("allposts", "othersettings");
                                driver.Navigate().GoToUrl(link_1); driver.Navigate();
                                var del_001 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[3]/div[4]/div/div[2]/div[2]/div[2]/div[11]/div/div[2]/table/tbody/tr/td[2]/a");
                                del_001.Click(); Thread.Sleep(1000);
                                var del_002 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[8]/div/div/div[2]/div/div/div[2]/div[2]/button[1]");
                                del_002.Click();
                            } catch { }

                            try { //Xóa bước 2
                                var del_003 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[3]/div[4]/div/div[2]/div[2]/div[2]/div/div/div/div[2]/button[1]");
                                del_003.Click(); Thread.Sleep(2000);
                                var del_004 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[8]/div/div/div[2]/div/div/div[2]/div[2]/button[2]");
                                del_004.Click();
                            }  catch { }
                            try { if (driver.Url.Contains("welcome")) { vonglap = 0; key_danplay("change", "del_blogger_run&id_dell=" + arr_data[0] + "&ketqua=2"); driver.Close(); driver.Quit(); } } catch { } //Đã xóa thành công
                        }
                    }
                }
            } catch { }
        }
    }
}
