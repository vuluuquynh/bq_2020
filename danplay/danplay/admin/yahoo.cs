﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class yahoo:danplay
    {
        #region oke_theme_yahoo
        void oke_theme_yahoo()
        {
            try {
                var th_yahoo_01 = driver.FindElementByXPath("/html/body/div[1]/div/div[1]/div/div[7]/div/div/div/div[3]/div[5]/button/span");
                th_yahoo_01.Click(); Thread.Sleep(3000);
            } catch { }
        }
        #endregion

        #region YAHOO: tao_email_yahoo
        string tao_email_yahoo(string mail_id, string mail_name, string email_pass,  string tao_email_new = null)
        {
            string kq_tao = "";
            string email_new = null;
            
            try
            {
                if (tao_email_new != null) { email_new = tao_email_new; } else { email_new = random_email_yahoo(12); } //Truyền vào giá trị email cần tạo            
                try
                {
                    int auto_yahoo = 0;
                    driver.Url = "https://mail.yahoo.com/d/settings/1"; driver.Navigate(); Thread.Sleep(3000);
                    if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; }

                    #region login yahoo
                    while (driver.Url.Contains("login.yahoo.com") && auto_yahoo != 2)
                    {
                        var yahoo_01 = driver.FindElementByXPath("//*[@id=\"login-username\"]");
                        yahoo_01.SendKeys(mail_name); Thread.Sleep(1000);
                        yahoo_01.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                        try
                        {
                            var yahoo_03 = driver.FindElementByXPath("//*[@id=\"login-passwd\"]");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }

                        try
                        {
                            var yahoo_03 = driver.FindElementByXPath("/html/body/div[2]/div[2]/div[1]/div[2]/div/form/input[6]");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }


                        try
                        {
                            var yahoo_03 = driver.FindElementByXPath("/html/body/div[2]/div[2]/div[1]/div[2]/div/form/input[7]");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }


                        try
                        {
                            var yahoo_03 = driver.FindElementByName("passwordContext");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }



                        if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; }
                        else
                        {
                            Thread.Sleep(5000);
                            if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; } else { auto_yahoo = 4; key_danplay("change", "get_mail_yahoo&id_yahoo=" + mail_id); try { Thread.Sleep(3000); driver.Close(); driver.Quit(); } catch { } }
                        }

                        //Load lại nếu login không thành công
                        if (driver.Url.Contains("login.yahoo.com")) { driver.Url = "https://mail.yahoo.com/d/settings/1"; driver.Navigate(); Thread.Sleep(3000); }
                    }
                    #endregion

                    #region login oke tạo email bí danh
                    if (auto_yahoo == 2)
                    {
                        try
                        { //Xóa mail bí danh củ nếu có
                            var yahoo_05 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li");
                            yahoo_05.Click(); Thread.Sleep(2000);
                            var yahoo_06 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[4]/button");
                            yahoo_06.Click(); Thread.Sleep(2000);
                            var yahoo_07 = driver.FindElementByXPath("//*[@id=\"modal-outer\"]/div/div/div[4]/button[1]");
                            yahoo_07.Click(); Thread.Sleep(2000);
                        }
                        catch { }

                        try
                        {
                            var yahoo_08 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/div[3]/button");
                            yahoo_08.Click(); Thread.Sleep(2000);
                            var yahoo_09 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/div/input");
                            yahoo_09.SendKeys(email_new);
                        }
                        catch { }

                        string yahoo_10_kq = "";
                        try
                        {
                            var yahoo_10 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[3]/button[1]/span");
                            yahoo_10.Click(); Thread.Sleep(5000);
                        }
                        catch { }


                        try { yahoo_10_kq = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li/div[2]/div/p/span[1]").GetAttribute("<span>"); } catch { }


                        #region kiểm tra xem tạo thành công không, nếu ko thì cho lặp lại
                        if (yahoo_10_kq == email_new) { auto_yahoo = 9; }
                        else
                        {
                            for (int i1 = 0; i1 < 5; i1++)
                            {
                                #region thêm lại lần 2 nếu nó bị lỗi
                                try
                                { //Nếu ko thêm bị lỗi thì thêm lại
                                    var yahoo_11 = driver.FindElementsByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/span");
                                    if (yahoo_11.Count > 0)
                                    {
                                        if (i1 > 3) { email_new = random_email_yahoo(13); }
                                        var yahoo_09_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[1]/div/div/input");
                                        yahoo_09_1.SendKeys(OpenQA.Selenium.Keys.Control + "a"); Thread.Sleep(500);
                                        yahoo_09_1.SendKeys(email_new);

                                        var yahoo_10_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div[3]/button[1]/span");
                                        yahoo_10_1.Click(); Thread.Sleep(2000);
                                        string yahoo_10_kq_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[2]/ul/li/div[2]/div/p/span[1]").GetAttribute("<span>");
                                        if (yahoo_10_kq_1 == email_new) { auto_yahoo = 9; i1 = 20; }
                                    }
                                }
                                catch { }
                                #endregion
                            }

                        }
                        #endregion

                    }
                    #endregion

                    kq_tao = email_new;
                } catch { kq_tao = "error"; }
            } catch { kq_tao = "error"; }



            return kq_tao;
        }
        #endregion

        #region YAHOO: tao_email_yahoo_v2
        string tao_email_yahoo_v2(string mail_id, string mail_name, string email_pass, string tao_email_new = null)
        {
            string kq_tao = "";
            string email_new = null;

            try
            {
                if (tao_email_new != null) { email_new = tao_email_new; } else { email_new = random_email_yahoo(12); } //Truyền vào giá trị email cần tạo            
                try
                {
                    int auto_yahoo = 0;
                    driver.Url = "https://mail.yahoo.com/d/settings/1"; driver.Navigate(); Thread.Sleep(3000);
                    if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; }

                    #region login yahoo
                    while (driver.Url.Contains("login.yahoo.com") && auto_yahoo != 2)
                    {
                        var yahoo_01 = driver.FindElementByXPath("//*[@id=\"login-username\"]");
                        yahoo_01.SendKeys(mail_name); Thread.Sleep(1000);
                        yahoo_01.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                        try
                        {
                            var yahoo_03 = driver.FindElementByXPath("//*[@id=\"login-passwd\"]");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }

                        try
                        {
                            var yahoo_03 = driver.FindElementByXPath("/html/body/div[2]/div[2]/div[1]/div[2]/div/form/input[6]");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }


                        try
                        {
                            var yahoo_03 = driver.FindElementByXPath("/html/body/div[2]/div[2]/div[1]/div[2]/div/form/input[7]");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }


                        try
                        {
                            var yahoo_03 = driver.FindElementByName("passwordContext");
                            yahoo_03.SendKeys(email_pass); Thread.Sleep(1000);
                            yahoo_03.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                        catch { }



                        if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; }
                        else
                        {
                            Thread.Sleep(5000);
                            if (driver.Url.Contains("mail.yahoo.com/d/settings")) { auto_yahoo = 2; } else { auto_yahoo = 4; key_danplay("change", "get_mail_yahoo&id_yahoo=" + mail_id); try { Thread.Sleep(3000); driver.Close(); driver.Quit(); } catch { } }
                        }

                        //Load lại nếu login không thành công
                        if (driver.Url.Contains("login.yahoo.com")) { driver.Url = "https://mail.yahoo.com/d/settings/1"; driver.Navigate(); Thread.Sleep(3000); }
                    }
                    #endregion

                    #region login oke tạo email bí danh
                    if (auto_yahoo == 2)
                    {

                        #region Xóa email củ được thêm từ trước nếu có
                        try
                        {
                            var nw_da_01 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[4]/ul/li[1]/div/div");
                            nw_da_01.Click(); Thread.Sleep(1000);
                            var nw_da_02 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[3]/button/span");
                            nw_da_02.Click(); Thread.Sleep(1000);
                            var nw_da_03 = driver.FindElementByXPath("//*[@id=\"modal-outer\"]/div/div/div[4]/button[1]");
                            nw_da_03.Click(); Thread.Sleep(1000);
                        } catch { }
                        #endregion

                        #region Thêm email mới để nhận mã
                        var nw_ya_01 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[4]/div[3]/button");                        
                        nw_ya_01.Click(); Thread.Sleep(1000);

                        try {
                            var nw_ya_02_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[1]/div[1]/div[1]/div/input");
                            string new00ss = random_string(4); Thread.Sleep(1000);
                            nw_ya_02_1.SendKeys("dp"+ new00ss); Thread.Sleep(1000);
                            var nw_ya_02_2 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[2]/button[1]/span");
                            nw_ya_02_2.Click();
                        } catch { }

                        var nw_ya_02 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[1]/div/div[1]/div[2]/div/input");                                                                  
                        string aaasf = random_string(8);
                        nw_ya_02.SendKeys(aaasf); Thread.Sleep(1000);
                        var nw_ya_03 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[4]/button[1]");
                        nw_ya_03.Click(); Thread.Sleep(2000);                        
                        try { email_new = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[4]/ul/li/div/div").Text; } catch { email_new = null; }
                        if (email_new == null)
                        {
                            for (int i2 = 0; i2 < 5; i2++)
                            {
                                var yh_error_1 = driver.FindElementsByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[1]/div/span");
                                if (yh_error_1.Count > 0)
                                {
                                    if (i2 > 3) { aaasf = random_string(8); }
                                    var yahoo_09_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[1]/div/div/div[2]/div/input");
                                    yahoo_09_1.SendKeys(OpenQA.Selenium.Keys.Control + "a"); Thread.Sleep(500);
                                    yahoo_09_1.SendKeys(aaasf);
                                    var yahoo_10_1 = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[2]/div/div/div/div[4]/button[1]");
                                    yahoo_10_1.Click(); Thread.Sleep(2000);
                                    try { email_new = driver.FindElementByXPath("//*[@id=\"mail-app-component\"]/section/div/article/div/div[1]/div/div[4]/ul/li/div/div").Text; i2 = 10; } catch { }
                                } else { i2 = 10; }

                            }
                        }
                        #endregion

                    }
                    #endregion

                    kq_tao = email_new;
                }
                catch { kq_tao = "error"; }
            }
            catch { kq_tao = "error"; }
            return kq_tao;
        }
        #endregion    

        #region YAHOO: doc_email_1
        string doc_email_1(string address_email)
        {
            string kq_Check_email_very_facebook = "";
           
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        driver.Url = "https://mail.yahoo.com/d/folders/1";
                        driver.Navigate(); Thread.Sleep(3000);
                        oke_theme_yahoo();
                        string data_1 = driver.PageSource;
                        string chuoi_email_very = Regex.Match(data_1, @"/d/folders/1/messages/.*?" + address_email).Value.Replace("/d/folders/1/messages/", "");
                        string[] arry_very_yahoo = chuoi_email_very.Split('"');
                        if (arry_very_yahoo[0] != "")
                        {
                            driver.Url = "https://mail.yahoo.com/d/folders/1/messages/" + arry_very_yahoo[0];
                            oke_theme_yahoo();
                            Thread.Sleep(3000);
                            data_1 = driver.PageSource;
                            string chuoi_email_url_very = Regex.Match(data_1, @"https://www.facebook.com/confirmcontact.php.*?(?="" style)").Value.Replace("amp;", "");
                            i = 100;
                            kq_Check_email_very_facebook = chuoi_email_url_very;
                        }
                        else { kq_Check_email_very_facebook = "not_email"; }
                    }
                    catch { kq_Check_email_very_facebook = "error"; }
                }
            } catch { }

            return kq_Check_email_very_facebook;
        }
        #endregion

        #region YAHOO: doc_email_2
        string doc_email_2()
        {
            string url_ck5422 = null;
            try
            {
                #region tìm email facebook gửi về
                driver.Url = "https://mail.yahoo.com/d/folders/1"; driver.Navigate(); Thread.Sleep(3000);
                oke_theme_yahoo();
                string html_ssks = driver.PageSource;
                string s2id_facebook = Regex.Match(html_ssks, @"messageGroupList.*?"",""messages""").Value.Replace("message", "");
                string[] arr_sfsdf = s2id_facebook.Split('"');
                int id_email = int.Parse(arr_sfsdf[4]);

                int check_skfk = 0;
                for (int s = 0; s < 5; s++)
                {
                    driver.Url = "https://mail.yahoo.com/d/folders/1/messages/" + id_email; driver.Navigate(); Thread.Sleep(3000);
                    oke_theme_yahoo();
                    html_ssks = driver.PageSource;
                    url_ck5422 = Regex.Match(html_ssks, @"https://www.facebook.com/recover/code/.*?button").Value.Replace("www", "m");
                    if (url_ck5422.Length > 100) { s = 100; check_skfk = 1; }
                }

                for (int a = 0; a < 2; a++)
                {
                    if (check_skfk == 0)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            int check_mai = id_email - i;
                            driver.Url = "https://mail.yahoo.com/d/folders/1/messages/" + check_mai; driver.Navigate(); Thread.Sleep(3000);
                            oke_theme_yahoo();
                            html_ssks = driver.PageSource;
                            url_ck5422 = Regex.Match(html_ssks, @"https://www.facebook.com/recover/code/.*?button").Value.Replace("www", "m");
                            if (url_ck5422.Length > 100) { i = 100; check_skfk = 1; a = 10; }
                        }

                    }
                }
                #endregion
            } catch { }
            return url_ck5422;
        }
        #endregion

        #region Hệ thống email YAHOO CỦ
        public void yahoo_start_oll()
        {
            try
            {
                string acc_id = null; string acc_name = null; string acc_pass = null; string lam_viec = null; string new_email = null;
                int tien_trinh = 0;

                #region lấy dữ liệu acc xuống
                string yahoo_e1 = GetData("https://danplay.net/?auto=004-yahoo");
                string[] ar_yah = yahoo_e1.Split('|');
                acc_id = ar_yah[0]; acc_name = ar_yah[1]; acc_pass = ar_yah[2];
                if (acc_name.Length > 5)
                {
                    string jshs = profile_danplay_mau_demo_2(acc_name);
                    if (jshs != "oke") { tien_trinh = 44; } else { tien_trinh = 1; tao_email_yahoo_v2(acc_id, acc_name, acc_pass); }                                        
                } else { tien_trinh = 44; }
                #endregion

                if(tien_trinh == 44) {
                    GetData("https://danplay.net/?auto=004-yahoo&id=" + acc_id + "&bao_cao_loi");
                    try { Thread.Sleep(3000); driver.Close(); driver.Quit(); } catch { } }

                while (tien_trinh < 2)
                {
                    #region tìm việc cần làm                    
                    Thread.Sleep(4000);               
                    driver.Url = "https://danplay.net/?auto=004-yahoo&id=" + acc_id + "&lam_viec"; driver.Navigate(); Thread.Sleep(1000);
                    string arr_1 = driver.PageSource;
                    string[] arr_2 = arr_1.Split('|');
                    lam_viec = arr_2[1].Trim();
                    new_email = arr_2[2].Trim();
                    #endregion                    

                    #region Làm việc 1 - tạo email yahoo
                    if (lam_viec == "1")
                    {
                        string ketqua = tao_email_yahoo_v2(acc_id, acc_name, acc_pass);
                        if (ketqua.Length > 9)
                        {
                            driver.Url = "https://danplay.net/?auto=004-yahoo&kq=oke&bi_danh=" + ketqua + "&id=" + acc_id; driver.Navigate(); Thread.Sleep(1000);                            
                        }
                        else
                        {
                            driver.Url = "https://danplay.net/?auto=004-yahoo&kq=error&bi_danh=" + ketqua + "&id=" + acc_id; driver.Navigate(); Thread.Sleep(1000);
                        }
                    }
                    #endregion

                    #region Tìm link very email - lúc thêm email
                    if (lam_viec == "2")
                    {
                        string kq_v2 = doc_email_1(new_email);
                        if (kq_v2.Length > 10)
                        {
                            driver.Url = "https://danplay.net/?auto=004-yahoo&kq=" + kq_v2 + "&doc_email_1&id=" + acc_id; driver.Navigate(); Thread.Sleep(1000);
                        }
                        else
                        {
                            driver.Url = "https://danplay.net/?auto=004-yahoo&kq=error&doc_email_1&id=" + acc_id; driver.Navigate(); Thread.Sleep(1000); 
                        }
                    }
                    #endregion

                    #region Tìm link very email - lúc thêm email
                    if (lam_viec == "3")
                    {
                        string kq_v3 = doc_email_2();
                        if (kq_v3.Length > 10)
                        {
                            string code_49_vp = Regex.Match(kq_v3, @"n=.*?(?=&)").Value.Replace("n=", "");
                            driver.Url = "https://danplay.net/?auto=004-yahoo&kq=" + code_49_vp + "&doc_email_2&id=" + acc_id; driver.Navigate(); Thread.Sleep(1000); 
                        }
                        else
                        {
                            driver.Url = "https://danplay.net/?auto=004-yahoo&kq=error&doc_email_2&id=" + acc_id; driver.Navigate(); Thread.Sleep(1000);
                        }
                    }
                    #endregion

                }

                GetData("https://danplay.net/?auto=004-yahoo&id=" + acc_id + "&bao_cao_loi"); 
                try { Thread.Sleep(3000); driver.Close(); driver.Quit(); } catch { }
            } catch { try { Thread.Sleep(3000); driver.Close(); driver.Quit(); } catch { } }

            for (int ai = 0; ai<10; ai++)
            {
                try { Thread.Sleep(3000); driver.Close(); driver.Quit(); } catch { }
            }
        }
        #endregion

        #region Hệ thống đọc email mới
        public void yahoo_start()
        {

        }
        #endregion
    }
}
