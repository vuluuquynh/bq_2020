﻿using System.Windows.Forms;

namespace danplay
{
    partial class danplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(danplay));
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.process1 = new System.Diagnostics.Process();
            this.KeyAPI = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BLOG = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.IMG = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.Post_link = new System.Windows.Forms.TabPage();
            this.post_link_facebook = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button18 = new System.Windows.Forms.Button();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.Xin_nhom = new System.Windows.Forms.CheckBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button8 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Tuong_tac = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.button22 = new System.Windows.Forms.Button();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.button23 = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Profile = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button24 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button_Profile_mau = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button16 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.KeyAPI.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.BLOG.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.IMG.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.Post_link.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.Tuong_tac.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.Profile.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "";
            this.process1.SynchronizingObject = this;
            // 
            // KeyAPI
            // 
            this.KeyAPI.Controls.Add(this.groupBox9);
            this.KeyAPI.Controls.Add(this.groupBox8);
            this.KeyAPI.Controls.Add(this.groupBox7);
            this.KeyAPI.Location = new System.Drawing.Point(4, 34);
            this.KeyAPI.Name = "KeyAPI";
            this.KeyAPI.Padding = new System.Windows.Forms.Padding(3);
            this.KeyAPI.Size = new System.Drawing.Size(770, 388);
            this.KeyAPI.TabIndex = 4;
            this.KeyAPI.Text = "Key (api )";
            this.KeyAPI.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.button3);
            this.groupBox9.Location = new System.Drawing.Point(6, 305);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(758, 78);
            this.groupBox9.TabIndex = 6;
            this.groupBox9.TabStop = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(10, 25);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(730, 43);
            this.button3.TabIndex = 5;
            this.button3.Text = "Lưu thông tin";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.checkBox25);
            this.groupBox8.Controls.Add(this.checkBox1);
            this.groupBox8.Controls.Add(this.button2);
            this.groupBox8.Controls.Add(this.textBox5);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Location = new System.Drawing.Point(6, 162);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(758, 151);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "KEY - API";
            // 
            // checkBox25
            // 
            this.checkBox25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(395, 95);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(147, 29);
            this.checkBox25.TabIndex = 6;
            this.checkBox25.Text = "Auto Update";
            this.checkBox25.UseVisualStyleBackColor = true;
            this.checkBox25.CheckedChanged += new System.EventHandler(this.CheckBox25_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(576, 95);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(150, 29);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Lưu thông tin";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(10, 88);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(279, 43);
            this.button2.TabIndex = 4;
            this.button2.Text = "Show KEY";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox5.Location = new System.Drawing.Point(392, 42);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(349, 30);
            this.textBox5.TabIndex = 3;
            this.textBox5.UseSystemPasswordChar = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 25);
            this.label9.TabIndex = 0;
            this.label9.Text = "Nhập KEY của bạn:";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.linkLabel2);
            this.groupBox7.Controls.Add(this.linkLabel1);
            this.groupBox7.Controls.Add(this.textBox4);
            this.groupBox7.Controls.Add(this.label8);
            this.groupBox7.Controls.Add(this.label7);
            this.groupBox7.Location = new System.Drawing.Point(6, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(758, 152);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Phiên hoạt động";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(6, 122);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(682, 25);
            this.linkLabel2.TabIndex = 5;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Bấm vào đây để tham gia GR MMO DANPLAY, cộng đồng SUPPORT 24/24!";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(489, 82);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(173, 25);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "https://danplay.net";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
            // 
            // textBox4
            // 
            this.textBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox4.Location = new System.Drawing.Point(392, 38);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(349, 30);
            this.textBox4.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(444, 25);
            this.label8.TabIndex = 1;
            this.label8.Text = "Bạn có thể điều khiển AUTO trực tuyến ở website:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(347, 25);
            this.label7.TabIndex = 0;
            this.label7.Text = "Bạn cần đặt cho AUTO 1 cái tên riêng:";
            // 
            // BLOG
            // 
            this.BLOG.Controls.Add(this.groupBox16);
            this.BLOG.Controls.Add(this.groupBox15);
            this.BLOG.Location = new System.Drawing.Point(4, 34);
            this.BLOG.Name = "BLOG";
            this.BLOG.Size = new System.Drawing.Size(770, 388);
            this.BLOG.TabIndex = 8;
            this.BLOG.Text = "BLOG";
            this.BLOG.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox16.Controls.Add(this.textBox14);
            this.groupBox16.Controls.Add(this.label19);
            this.groupBox16.Controls.Add(this.button14);
            this.groupBox16.Location = new System.Drawing.Point(18, 203);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(736, 157);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Xóa Blogger";
            // 
            // textBox14
            // 
            this.textBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox14.Location = new System.Drawing.Point(172, 58);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(510, 30);
            this.textBox14.TabIndex = 4;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(46, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 25);
            this.label19.TabIndex = 2;
            this.label19.Text = "Số luồng:";
            // 
            // button14
            // 
            this.button14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button14.Location = new System.Drawing.Point(52, 105);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(632, 42);
            this.button14.TabIndex = 1;
            this.button14.Text = "Xóa Blogger";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.Button14_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox15.Controls.Add(this.textBox13);
            this.groupBox15.Controls.Add(this.label18);
            this.groupBox15.Controls.Add(this.button13);
            this.groupBox15.Location = new System.Drawing.Point(18, 20);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(736, 145);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Tạo Blogger";
            // 
            // textBox13
            // 
            this.textBox13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox13.Location = new System.Drawing.Point(172, 38);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(510, 30);
            this.textBox13.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(46, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 25);
            this.label18.TabIndex = 1;
            this.label18.Text = "Số luồng:";
            // 
            // button13
            // 
            this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button13.Location = new System.Drawing.Point(52, 88);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(632, 43);
            this.button13.TabIndex = 0;
            this.button13.Text = "Bắt đầu - Tạo Blogger";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.Button13_Click);
            // 
            // IMG
            // 
            this.IMG.Controls.Add(this.groupBox14);
            this.IMG.Controls.Add(this.groupBox13);
            this.IMG.Location = new System.Drawing.Point(4, 34);
            this.IMG.Name = "IMG";
            this.IMG.Size = new System.Drawing.Size(770, 388);
            this.IMG.TabIndex = 7;
            this.IMG.Text = "IMG";
            this.IMG.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox14.Controls.Add(this.button11);
            this.groupBox14.Controls.Add(this.button12);
            this.groupBox14.Controls.Add(this.textBox12);
            this.groupBox14.Controls.Add(this.textBox11);
            this.groupBox14.Controls.Add(this.label17);
            this.groupBox14.Controls.Add(this.textBox10);
            this.groupBox14.Controls.Add(this.label16);
            this.groupBox14.Controls.Add(this.label15);
            this.groupBox14.Controls.Add(this.textBox9);
            this.groupBox14.Location = new System.Drawing.Point(10, 165);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(750, 212);
            this.groupBox14.TabIndex = 1;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Nhân bản 1 ảnh thành nhiều ảnh";
            // 
            // button11
            // 
            this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button11.Location = new System.Drawing.Point(620, 149);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(114, 46);
            this.button11.TabIndex = 9;
            this.button11.Text = "Mở IMG";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.Location = new System.Drawing.Point(380, 149);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(214, 46);
            this.button12.TabIndex = 8;
            this.button12.Text = "Bắt đầu";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // textBox12
            // 
            this.textBox12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox12.Location = new System.Drawing.Point(16, 154);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(344, 30);
            this.textBox12.TabIndex = 7;
            this.textBox12.Text = "Nơi chứa ảnh cần thêm...";
            this.textBox12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TextBox12_MouseClick);
            // 
            // textBox11
            // 
            this.textBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox11.Location = new System.Drawing.Point(626, 92);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(97, 30);
            this.textBox11.TabIndex = 6;
            this.textBox11.Text = "40";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(374, 98);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(221, 25);
            this.label17.TabIndex = 5;
            this.label17.Text = "Số lượng ảnh nhân bản:";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(232, 95);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(79, 30);
            this.textBox10.TabIndex = 4;
            this.textBox10.Text = "40";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 95);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(147, 25);
            this.label16.TabIndex = 3;
            this.label16.Text = "Kích thước chữ:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(183, 25);
            this.label15.TabIndex = 2;
            this.label15.Text = "Chữ thêm vào ảnh: ";
            // 
            // textBox9
            // 
            this.textBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox9.Location = new System.Drawing.Point(232, 45);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(500, 30);
            this.textBox9.TabIndex = 1;
            this.textBox9.Text = "Clip hot";
            // 
            // groupBox13
            // 
            this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox13.Controls.Add(this.button10);
            this.groupBox13.Controls.Add(this.button9);
            this.groupBox13.Controls.Add(this.textBox8);
            this.groupBox13.Location = new System.Drawing.Point(10, 20);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(750, 115);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Thêm nút PLAY vào hình ảnh";
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button10.Location = new System.Drawing.Point(620, 55);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(112, 46);
            this.button10.TabIndex = 2;
            this.button10.Text = "Mở IMG";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button9.Location = new System.Drawing.Point(380, 55);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(216, 46);
            this.button9.TabIndex = 1;
            this.button9.Text = "Bắt đầu";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // textBox8
            // 
            this.textBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox8.Location = new System.Drawing.Point(15, 60);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(344, 30);
            this.textBox8.TabIndex = 0;
            this.textBox8.Text = "Nơi chứa ảnh cần thêm...";
            this.textBox8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TextBox8_MouseClick);
            // 
            // Post_link
            // 
            this.Post_link.Controls.Add(this.post_link_facebook);
            this.Post_link.Controls.Add(this.groupBox12);
            this.Post_link.Controls.Add(this.groupBox10);
            this.Post_link.Controls.Add(this.groupBox11);
            this.Post_link.Location = new System.Drawing.Point(4, 34);
            this.Post_link.Name = "Post_link";
            this.Post_link.Size = new System.Drawing.Size(770, 388);
            this.Post_link.TabIndex = 6;
            this.Post_link.Text = "POST";
            this.Post_link.UseVisualStyleBackColor = true;
            // 
            // post_link_facebook
            // 
            this.post_link_facebook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.post_link_facebook.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.post_link_facebook.Location = new System.Drawing.Point(6, 331);
            this.post_link_facebook.Name = "post_link_facebook";
            this.post_link_facebook.Size = new System.Drawing.Size(758, 45);
            this.post_link_facebook.TabIndex = 5;
            this.post_link_facebook.Text = "Bắt đầu AUTO POST";
            this.post_link_facebook.UseVisualStyleBackColor = false;
            this.post_link_facebook.Click += new System.EventHandler(this.Post_link_facebook_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox12.Controls.Add(this.button18);
            this.groupBox12.Controls.Add(this.textBox16);
            this.groupBox12.Controls.Add(this.label13);
            this.groupBox12.Controls.Add(this.textBox15);
            this.groupBox12.Controls.Add(this.label12);
            this.groupBox12.Controls.Add(this.checkBox7);
            this.groupBox12.Controls.Add(this.radioButton6);
            this.groupBox12.Controls.Add(this.radioButton5);
            this.groupBox12.Controls.Add(this.checkBox6);
            this.groupBox12.Controls.Add(this.Xin_nhom);
            this.groupBox12.Location = new System.Drawing.Point(6, 162);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(758, 151);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Tùy chỉnh POST";
            // 
            // button18
            // 
            this.button18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button18.Location = new System.Drawing.Point(644, 92);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(68, 38);
            this.button18.TabIndex = 15;
            this.button18.Text = "...";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.Button18_Click);
            // 
            // textBox16
            // 
            this.textBox16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox16.Location = new System.Drawing.Point(644, 48);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(67, 30);
            this.textBox16.TabIndex = 14;
            this.textBox16.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(566, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 25);
            this.label13.TabIndex = 13;
            this.label13.Text = "Số cmt:";
            // 
            // textBox15
            // 
            this.textBox15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox15.Location = new System.Drawing.Point(560, 92);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(67, 30);
            this.textBox15.TabIndex = 12;
            this.textBox15.Text = "1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(480, 95);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 25);
            this.label12.TabIndex = 11;
            this.label12.Text = "Số file:";
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Checked = true;
            this.checkBox7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox7.Location = new System.Drawing.Point(423, 49);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(119, 29);
            this.checkBox7.TabIndex = 10;
            this.checkBox7.Text = "Bình luận";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(206, 92);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(226, 29);
            this.radioButton6.TabIndex = 9;
            this.radioButton6.Text = "Đăng ảnh hoặc Video";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(58, 92);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(125, 29);
            this.radioButton5.TabIndex = 8;
            this.radioButton5.Text = "Đăng Link";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox6.AutoSize = true;
            this.checkBox6.Checked = true;
            this.checkBox6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox6.Location = new System.Drawing.Point(231, 49);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(135, 29);
            this.checkBox6.TabIndex = 7;
            this.checkBox6.Text = "Quét nhóm";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // Xin_nhom
            // 
            this.Xin_nhom.AutoSize = true;
            this.Xin_nhom.Checked = true;
            this.Xin_nhom.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Xin_nhom.Location = new System.Drawing.Point(60, 49);
            this.Xin_nhom.Name = "Xin_nhom";
            this.Xin_nhom.Size = new System.Drawing.Size(121, 29);
            this.Xin_nhom.TabIndex = 6;
            this.Xin_nhom.Text = "Xin nhóm";
            this.Xin_nhom.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.checkBox3);
            this.groupBox10.Controls.Add(this.radioButton3);
            this.groupBox10.Controls.Add(this.radioButton4);
            this.groupBox10.Controls.Add(this.textBox7);
            this.groupBox10.Controls.Add(this.label11);
            this.groupBox10.Location = new System.Drawing.Point(6, 78);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(758, 82);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(613, 34);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(128, 29);
            this.checkBox3.TabIndex = 5;
            this.checkBox3.Text = "Lưu profile";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(510, 32);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(66, 29);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Tốt";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(429, 32);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(69, 29);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.Text = "Mới";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // textBox7
            // 
            this.textBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox7.Location = new System.Drawing.Point(129, 32);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(278, 30);
            this.textBox7.TabIndex = 2;
            this.textBox7.Text = "all";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 25);
            this.label11.TabIndex = 0;
            this.label11.Text = "List VIA:";
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.button8);
            this.groupBox11.Controls.Add(this.textBox6);
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Location = new System.Drawing.Point(6, 2);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(758, 75);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Location = new System.Drawing.Point(580, 22);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(158, 43);
            this.button8.TabIndex = 2;
            this.button8.Text = "Xác nhận";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // textBox6
            // 
            this.textBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox6.Location = new System.Drawing.Point(129, 28);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(428, 30);
            this.textBox6.TabIndex = 1;
            this.textBox6.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 25);
            this.label10.TabIndex = 0;
            this.label10.Text = "Số luồng:";
            // 
            // Tuong_tac
            // 
            this.Tuong_tac.Controls.Add(this.groupBox19);
            this.Tuong_tac.Controls.Add(this.groupBox18);
            this.Tuong_tac.Location = new System.Drawing.Point(4, 34);
            this.Tuong_tac.Name = "Tuong_tac";
            this.Tuong_tac.Size = new System.Drawing.Size(770, 388);
            this.Tuong_tac.TabIndex = 9;
            this.Tuong_tac.Text = "FACEBOOK";
            this.Tuong_tac.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.checkBox29);
            this.groupBox19.Controls.Add(this.checkBox28);
            this.groupBox19.Controls.Add(this.checkBox27);
            this.groupBox19.Controls.Add(this.checkBox20);
            this.groupBox19.Controls.Add(this.checkBox11);
            this.groupBox19.Controls.Add(this.checkBox5);
            this.groupBox19.Controls.Add(this.checkBox4);
            this.groupBox19.Controls.Add(this.checkBox2);
            this.groupBox19.Controls.Add(this.checkBox26);
            this.groupBox19.Controls.Add(this.checkBox21);
            this.groupBox19.Controls.Add(this.button22);
            this.groupBox19.Controls.Add(this.checkBox24);
            this.groupBox19.Controls.Add(this.checkBox23);
            this.groupBox19.Controls.Add(this.checkBox22);
            this.groupBox19.Controls.Add(this.textBox19);
            this.groupBox19.Controls.Add(this.label21);
            this.groupBox19.Controls.Add(this.checkBox16);
            this.groupBox19.Controls.Add(this.checkBox19);
            this.groupBox19.Controls.Add(this.checkBox17);
            this.groupBox19.Controls.Add(this.checkBox15);
            this.groupBox19.Controls.Add(this.checkBox14);
            this.groupBox19.Controls.Add(this.checkBox13);
            this.groupBox19.Controls.Add(this.checkBox12);
            this.groupBox19.Controls.Add(this.button23);
            this.groupBox19.Location = new System.Drawing.Point(4, 91);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(758, 288);
            this.groupBox19.TabIndex = 6;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Tương tác";
            // 
            // checkBox29
            // 
            this.checkBox29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(426, 155);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(56, 29);
            this.checkBox29.TabIndex = 33;
            this.checkBox29.Text = "IP";
            this.checkBox29.UseVisualStyleBackColor = true;
            this.checkBox29.MouseLeave += new System.EventHandler(this.CheckBox29_MouseLeave);
            this.checkBox29.MouseHover += new System.EventHandler(this.CheckBox29_MouseHover);
            // 
            // checkBox28
            // 
            this.checkBox28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(193, 46);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(65, 29);
            this.checkBox28.TabIndex = 32;
            this.checkBox28.Text = "ĐB";
            this.checkBox28.UseVisualStyleBackColor = true;
            this.checkBox28.MouseLeave += new System.EventHandler(this.CheckBox28_MouseLeave);
            this.checkBox28.MouseHover += new System.EventHandler(this.CheckBox28_MouseHover);
            // 
            // checkBox27
            // 
            this.checkBox27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(290, 154);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(65, 29);
            this.checkBox27.TabIndex = 31;
            this.checkBox27.Text = "ĐP";
            this.checkBox27.UseVisualStyleBackColor = true;
            this.checkBox27.MouseLeave += new System.EventHandler(this.CheckBox27_MouseLeave);
            this.checkBox27.MouseHover += new System.EventHandler(this.CheckBox27_MouseHover);
            // 
            // checkBox20
            // 
            this.checkBox20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(284, 46);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(66, 29);
            this.checkBox20.TabIndex = 30;
            this.checkBox20.Text = "KS";
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.MouseLeave += new System.EventHandler(this.CheckBox20_MouseLeave);
            this.checkBox20.MouseHover += new System.EventHandler(this.CheckBox20_MouseHover);
            // 
            // checkBox11
            // 
            this.checkBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(220, 154);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(65, 29);
            this.checkBox11.TabIndex = 29;
            this.checkBox11.Text = "ĐT";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.MouseLeave += new System.EventHandler(this.CheckBox11_MouseLeave);
            this.checkBox11.MouseHover += new System.EventHandler(this.CheckBox11_MouseHover);
            // 
            // checkBox5
            // 
            this.checkBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(149, 154);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(66, 29);
            this.checkBox5.TabIndex = 28;
            this.checkBox5.Text = "XN";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.MouseLeave += new System.EventHandler(this.CheckBox5_MouseLeave);
            this.checkBox5.MouseHover += new System.EventHandler(this.CheckBox5_MouseHover);
            // 
            // checkBox4
            // 
            this.checkBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(7, 154);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(67, 29);
            this.checkBox4.TabIndex = 27;
            this.checkBox4.Text = "CN";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.MouseLeave += new System.EventHandler(this.CheckBox4_MouseLeave);
            this.checkBox4.MouseHover += new System.EventHandler(this.CheckBox4_MouseHover);
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(360, 154);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(66, 29);
            this.checkBox2.TabIndex = 26;
            this.checkBox2.Text = "ĐX";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.MouseLeave += new System.EventHandler(this.CheckBox2_MouseLeave);
            this.checkBox2.MouseHover += new System.EventHandler(this.CheckBox2_MouseHover);
            // 
            // checkBox26
            // 
            this.checkBox26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(489, 98);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(120, 29);
            this.checkBox26.TabIndex = 25;
            this.checkBox26.Text = "Xóa SDT";
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(639, 98);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(109, 29);
            this.checkBox21.TabIndex = 24;
            this.checkBox21.Text = "Bật 2FA";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(664, 146);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(66, 45);
            this.button22.TabIndex = 23;
            this.button22.Text = "OK";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.Button22_Click);
            // 
            // checkBox24
            // 
            this.checkBox24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(645, 46);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(87, 29);
            this.checkBox24.TabIndex = 22;
            this.checkBox24.Text = "Thích";
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(557, 46);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(62, 29);
            this.checkBox23.TabIndex = 21;
            this.checkBox23.Text = "BL";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            this.checkBox22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(12, 98);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(108, 29);
            this.checkBox22.TabIndex = 20;
            this.checkBox22.Text = "Tạo BM";
            this.checkBox22.UseVisualStyleBackColor = true;
            this.checkBox22.MouseLeave += new System.EventHandler(this.CheckBox22_MouseLeave);
            this.checkBox22.MouseHover += new System.EventHandler(this.CheckBox22_MouseHover);
            // 
            // textBox19
            // 
            this.textBox19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox19.Location = new System.Drawing.Point(592, 151);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(62, 30);
            this.textBox19.TabIndex = 19;
            this.textBox19.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(508, 155);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 25);
            this.label21.TabIndex = 18;
            this.label21.Text = "Luồng:";
            // 
            // checkBox16
            // 
            this.checkBox16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(335, 98);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(126, 29);
            this.checkBox16.TabIndex = 16;
            this.checkBox16.Text = "Xóa MAIL";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox19.AutoSize = true;
            this.checkBox19.Enabled = false;
            this.checkBox19.Location = new System.Drawing.Point(466, 46);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(65, 29);
            this.checkBox19.TabIndex = 14;
            this.checkBox19.Text = "TN";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(376, 46);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(64, 29);
            this.checkBox17.TabIndex = 12;
            this.checkBox17.Text = "BB";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.MouseLeave += new System.EventHandler(this.CheckBox17_MouseLeave);
            this.checkBox17.MouseHover += new System.EventHandler(this.CheckBox17_MouseHover);
            // 
            // checkBox15
            // 
            this.checkBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(169, 98);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(140, 29);
            this.checkBox15.TabIndex = 10;
            this.checkBox15.Text = "Thêm email";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(85, 154);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(65, 29);
            this.checkBox14.TabIndex = 9;
            this.checkBox14.Text = "RN";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.MouseLeave += new System.EventHandler(this.CheckBox14_MouseLeave);
            this.checkBox14.MouseHover += new System.EventHandler(this.CheckBox14_MouseHover);
            // 
            // checkBox13
            // 
            this.checkBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(100, 46);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(67, 29);
            this.checkBox13.TabIndex = 8;
            this.checkBox13.Text = "SG";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.MouseLeave += new System.EventHandler(this.CheckBox13_MouseLeave);
            this.checkBox13.MouseHover += new System.EventHandler(this.CheckBox13_MouseHover);
            // 
            // checkBox12
            // 
            this.checkBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(10, 46);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(64, 29);
            this.checkBox12.TabIndex = 7;
            this.checkBox12.Text = "TT";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.MouseLeave += new System.EventHandler(this.CheckBox12_MouseLeave);
            this.checkBox12.MouseHover += new System.EventHandler(this.CheckBox12_MouseHover);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(12, 225);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(718, 45);
            this.button23.TabIndex = 5;
            this.button23.Text = "Bắt đầu";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.Button23_Click);
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox18.Controls.Add(this.checkBox18);
            this.groupBox18.Controls.Add(this.checkBox8);
            this.groupBox18.Controls.Add(this.checkBox10);
            this.groupBox18.Controls.Add(this.checkBox9);
            this.groupBox18.Controls.Add(this.textBox18);
            this.groupBox18.Controls.Add(this.label20);
            this.groupBox18.Location = new System.Drawing.Point(6, 5);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(758, 82);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            // 
            // checkBox18
            // 
            this.checkBox18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(392, 31);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(70, 29);
            this.checkBox18.TabIndex = 11;
            this.checkBox18.Text = "Mới";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(666, 31);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(67, 29);
            this.checkBox8.TabIndex = 10;
            this.checkBox8.Text = "Tốt";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(576, 31);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(78, 29);
            this.checkBox10.TabIndex = 8;
            this.checkBox10.Text = "Nuôi";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(470, 31);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(95, 29);
            this.checkBox9.TabIndex = 7;
            this.checkBox9.Text = "Check";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // textBox18
            // 
            this.textBox18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox18.Location = new System.Drawing.Point(110, 28);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(270, 30);
            this.textBox18.TabIndex = 2;
            this.textBox18.Text = "all";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 32);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 25);
            this.label20.TabIndex = 0;
            this.label20.Text = "List VIA:";
            // 
            // Profile
            // 
            this.Profile.AllowDrop = true;
            this.Profile.Controls.Add(this.panel2);
            this.Profile.Location = new System.Drawing.Point(4, 34);
            this.Profile.Name = "Profile";
            this.Profile.Padding = new System.Windows.Forms.Padding(3);
            this.Profile.Size = new System.Drawing.Size(770, 388);
            this.Profile.TabIndex = 5;
            this.Profile.Text = "PROFILE";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(770, 388);
            this.panel2.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.listView1);
            this.groupBox3.Location = new System.Drawing.Point(6, 111);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(754, 274);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lấy thông tin Profile";
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4});
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(10, 29);
            this.listView1.Name = "listView1";
            this.listView1.RightToLeftLayout = true;
            this.listView1.Size = new System.Drawing.Size(726, 226);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListView1_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            this.columnHeader1.Width = 42;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Profile";
            this.columnHeader2.Width = 254;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Actions";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 155;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.button24);
            this.groupBox2.Controls.Add(this.button19);
            this.groupBox2.Controls.Add(this.button17);
            this.groupBox2.Controls.Add(this.button15);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button_Profile_mau);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Location = new System.Drawing.Point(6, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(754, 65);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.Location = new System.Drawing.Point(140, 22);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(90, 35);
            this.button24.TabIndex = 8;
            this.button24.Text = "Làm mới";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.Button24_Click);
            // 
            // button19
            // 
            this.button19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Location = new System.Drawing.Point(538, 22);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(57, 35);
            this.button19.TabIndex = 7;
            this.button19.Text = "SYS";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.Button19_Click);
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(10, 22);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(58, 35);
            this.button17.TabIndex = 6;
            this.button17.Text = "DR";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.Button17_Click);
            // 
            // button15
            // 
            this.button15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(237, 22);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(294, 35);
            this.button15.TabIndex = 5;
            this.button15.Text = "Quét";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.Button15_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(76, 22);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(56, 35);
            this.button4.TabIndex = 4;
            this.button4.Text = "CH";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // button_Profile_mau
            // 
            this.button_Profile_mau.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Profile_mau.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Profile_mau.Location = new System.Drawing.Point(603, 22);
            this.button_Profile_mau.Name = "button_Profile_mau";
            this.button_Profile_mau.Size = new System.Drawing.Size(60, 35);
            this.button_Profile_mau.TabIndex = 1;
            this.button_Profile_mau.Text = "Mẫu";
            this.button_Profile_mau.UseVisualStyleBackColor = true;
            this.button_Profile_mau.Click += new System.EventHandler(this.Button_Profile_mau_Click);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(670, 22);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(64, 35);
            this.button7.TabIndex = 2;
            this.button7.Text = "Xóa";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button16);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(6, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(754, 57);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // button16
            // 
            this.button16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(510, 15);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(135, 37);
            this.button16.TabIndex = 8;
            this.button16.Text = "Auto Login";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.Button16_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(654, 17);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(81, 35);
            this.button5.TabIndex = 7;
            this.button5.Text = "Xóa";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(396, 15);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(105, 37);
            this.button6.TabIndex = 6;
            this.button6.Text = "Tạo / mở";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tên:";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(58, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(326, 26);
            this.textBox1.TabIndex = 5;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox1_KeyDown);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.Profile);
            this.tabControl1.Controls.Add(this.Tuong_tac);
            this.tabControl1.Controls.Add(this.Post_link);
            this.tabControl1.Controls.Add(this.IMG);
            this.tabControl1.Controls.Add(this.BLOG);
            this.tabControl1.Controls.Add(this.KeyAPI);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(778, 426);
            this.tabControl1.TabIndex = 0;
            // 
            // danplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 449);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "danplay";
            this.Text = "  DANPLAY.NET";
            this.KeyAPI.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.BLOG.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.IMG.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.Post_link.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.Tuong_tac.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.Profile.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Diagnostics.Process process1;
        private TabControl tabControl1;
        private TabPage Profile;
        private Panel panel2;
        private GroupBox groupBox3;
        private ListView listView1;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader4;
        private GroupBox groupBox2;
        private Button button24;
        private Button button19;
        private Button button17;
        private Button button15;
        private Button button4;
        private Button button_Profile_mau;
        private Button button7;
        private GroupBox groupBox1;
        private Button button16;
        private Button button5;
        private Button button6;
        private Label label1;
        private TextBox textBox1;
        private TabPage Tuong_tac;
        private GroupBox groupBox19;
        private CheckBox checkBox20;
        private CheckBox checkBox11;
        private CheckBox checkBox5;
        private CheckBox checkBox4;
        private CheckBox checkBox2;
        private CheckBox checkBox26;
        private CheckBox checkBox21;
        private Button button22;
        private CheckBox checkBox24;
        private CheckBox checkBox23;
        private CheckBox checkBox22;
        private TextBox textBox19;
        private Label label21;
        private CheckBox checkBox16;
        private CheckBox checkBox19;
        private CheckBox checkBox17;
        private CheckBox checkBox15;
        private CheckBox checkBox14;
        private CheckBox checkBox13;
        private CheckBox checkBox12;
        private Button button23;
        private GroupBox groupBox18;
        private CheckBox checkBox18;
        private CheckBox checkBox8;
        private CheckBox checkBox10;
        private CheckBox checkBox9;
        private TextBox textBox18;
        private Label label20;
        private TabPage Post_link;
        private Button post_link_facebook;
        private GroupBox groupBox12;
        private Button button18;
        private TextBox textBox16;
        private Label label13;
        private TextBox textBox15;
        private Label label12;
        private CheckBox checkBox7;
        private RadioButton radioButton6;
        private RadioButton radioButton5;
        private CheckBox checkBox6;
        private CheckBox Xin_nhom;
        private GroupBox groupBox10;
        private CheckBox checkBox3;
        private RadioButton radioButton3;
        private RadioButton radioButton4;
        private TextBox textBox7;
        private Label label11;
        private GroupBox groupBox11;
        private Button button8;
        private TextBox textBox6;
        private Label label10;
        private TabPage IMG;
        private GroupBox groupBox14;
        private Button button11;
        private Button button12;
        private TextBox textBox12;
        private TextBox textBox11;
        private Label label17;
        private TextBox textBox10;
        private Label label16;
        private Label label15;
        private TextBox textBox9;
        private GroupBox groupBox13;
        private Button button10;
        private Button button9;
        private TextBox textBox8;
        private TabPage BLOG;
        private GroupBox groupBox16;
        private TextBox textBox14;
        private Label label19;
        private Button button14;
        private GroupBox groupBox15;
        private TextBox textBox13;
        private Label label18;
        private Button button13;
        private TabPage KeyAPI;
        private GroupBox groupBox9;
        private Button button3;
        private GroupBox groupBox8;
        private CheckBox checkBox25;
        private CheckBox checkBox1;
        private Button button2;
        private TextBox textBox5;
        private Label label9;
        private GroupBox groupBox7;
        private LinkLabel linkLabel2;
        private LinkLabel linkLabel1;
        private TextBox textBox4;
        private Label label8;
        private Label label7;
        private CheckBox checkBox27;
        private CheckBox checkBox28;
        private CheckBox checkBox29;
    } 
 }

