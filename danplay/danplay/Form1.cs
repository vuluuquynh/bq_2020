﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using xNet;
using Cookie = OpenQA.Selenium.Cookie;

namespace danplay
{
    public partial class danplay : Form
    {        
        BackgroundWorker post_facebook;
        BackgroundWorker tao_profile_11;
        BackgroundWorker tao_profile_12;
        BackgroundWorker danplay_addplay;
        BackgroundWorker danplay_add_text;
        BackgroundWorker del_blogger;
        BackgroundWorker auto_tao_blogger;
        BackgroundWorker fb_login_profile;
        BackgroundWorker auto_fix_profile;
        BackgroundWorker auto_sys_profile;
        BackgroundWorker load_profile;
        BackgroundWorker backup_fb;
        BackgroundWorker tim_profile;
        BackgroundWorker thoat_nhom_fb;
        BackgroundWorker all_facebook;
        BackgroundWorker auto_update_dp;
        private Random rnd = new Random();
        string ProfileFolderPath = "Profile";
        string version = "067";
        public ChromeDriver driver;
        public danplay()
        {
            InitializeComponent();
            LoadlistView1();                                               
            this.Text = "DANPLAY.NET v" + version;
            Control.CheckForIllegalCrossThreadCalls = false;

            #region trả lại giá trị đã ghi nhớ
            try
            {
                string filePath = "data\\key_api.txt"; // Kiểm tra sự tồn tại của tập tin            
                if (System.IO.File.Exists(filePath))
                {
                    string text_api = File.ReadAllText(filePath, Encoding.UTF8);
                    string[] artext_api = text_api.Split('|');
                    this.textBox4.Text = artext_api[0].Trim();
                    this.textBox5.Text = artext_api[1].Trim();
                    if (artext_api[2].Trim() == "1") { checkBox1.CheckState = CheckState.Checked; }
                    this.textBox6.Text = artext_api[3].Trim();
                    this.textBox7.Text = artext_api[4].Trim();
                    this.textBox13.Text = artext_api[5].Trim();
                    this.textBox14.Text = artext_api[6].Trim();
                    if (artext_api[7].Trim() == "0") { Xin_nhom.CheckState = CheckState.Unchecked; }
                    if (artext_api[8].Trim() == "0") { checkBox6.CheckState = CheckState.Unchecked; }
                    if (artext_api[9].Trim() == "0") { checkBox7.CheckState = CheckState.Unchecked; }
                    this.textBox16.Text = artext_api[10].Trim();
                    if (artext_api[11].Trim() == "1") { radioButton5.Checked = true; }
                    if (artext_api[12].Trim() == "1") { radioButton6.Checked = true; }
                    this.textBox15.Text = artext_api[13].Trim();
                    if (artext_api[14].Trim() == "1") { checkBox25.CheckState = CheckState.Checked; }
                }

                string filePath_1 = "data\\auto_facebook.txt"; // Kiểm tra sự tồn tại của tập tin            
                if (System.IO.File.Exists(filePath_1))
                {
                    string text_01_1 = File.ReadAllText(filePath_1, Encoding.UTF8);
                    string[] text_01 = text_01_1.Split('|');
                    this.textBox19.Text = text_01[0].Trim();
                    if (text_01[2].Trim() == "1") { checkBox18.CheckState = CheckState.Checked; }
                    if (text_01[3].Trim() == "1") { checkBox9.CheckState = CheckState.Checked; }
                    if (text_01[4].Trim() == "1") { checkBox10.CheckState = CheckState.Checked; }
                    if (text_01[5].Trim() == "1") { checkBox8.CheckState = CheckState.Checked; }
                    if (text_01[6].Trim() == "1") { checkBox12.CheckState = CheckState.Checked; }
                    if (text_01[7].Trim() == "1") { checkBox13.CheckState = CheckState.Checked; }
                    if (text_01[8].Trim() == "1") { checkBox17.CheckState = CheckState.Checked; }
                    if (text_01[9].Trim() == "1") { checkBox19.CheckState = CheckState.Checked; }
                    if (text_01[10].Trim() == "1") { checkBox23.CheckState = CheckState.Checked; }
                    if (text_01[11].Trim() == "1") { checkBox24.CheckState = CheckState.Checked; }
                    if (text_01[12].Trim() == "1") { checkBox14.CheckState = CheckState.Checked; }
                    if (text_01[13].Trim() == "1") { checkBox15.CheckState = CheckState.Checked; }
                    if (text_01[14].Trim() == "1") { checkBox16.CheckState = CheckState.Checked; }
                    if (text_01[15].Trim() == "1") { checkBox26.CheckState = CheckState.Checked; }
                    if (text_01[16].Trim() == "1") { checkBox21.CheckState = CheckState.Checked; }
                    if (text_01[17].Trim() == "1") { checkBox22.CheckState = CheckState.Checked; }
                    if (text_01[18].Trim() == "1") { checkBox2.CheckState = CheckState.Checked; }
                    if (text_01[19].Trim() == "1") { checkBox4.CheckState = CheckState.Checked; }
                    if (text_01[20].Trim() == "1") { checkBox5.CheckState = CheckState.Checked; }
                    if (text_01[21].Trim() == "1") { checkBox11.CheckState = CheckState.Checked; }
                    if (text_01[22].Trim() == "1") { checkBox20.CheckState = CheckState.Checked; }                    
                    
                }
            }
            catch { }
            #endregion            

            try { File.Delete("data\\update.txt"); } catch { } //Xóa file tạm giữ lúc update
            post_facebook = new BackgroundWorker(); post_facebook.WorkerReportsProgress = true; post_facebook.WorkerSupportsCancellation = true; post_facebook.DoWork += post_facebook_run;
            tao_profile_11 = new BackgroundWorker(); tao_profile_11.WorkerReportsProgress = true; tao_profile_11.WorkerSupportsCancellation = true; tao_profile_11.DoWork += tao_profile_11_run;
            tao_profile_12 = new BackgroundWorker(); tao_profile_12.WorkerReportsProgress = true; tao_profile_12.WorkerSupportsCancellation = true; tao_profile_12.DoWork += tao_profile_12_run;
            danplay_addplay = new BackgroundWorker(); danplay_addplay.WorkerReportsProgress = true; danplay_addplay.WorkerSupportsCancellation = true; danplay_addplay.DoWork += danplay_addplay_run;
            danplay_add_text = new BackgroundWorker(); danplay_add_text.WorkerReportsProgress = true; danplay_add_text.WorkerSupportsCancellation = true; danplay_add_text.DoWork += danplay_add_text_run;
            auto_tao_blogger = new BackgroundWorker(); auto_tao_blogger.WorkerReportsProgress = true; auto_tao_blogger.WorkerSupportsCancellation = true; auto_tao_blogger.DoWork += auto_tao_blogger_run;
            del_blogger = new BackgroundWorker(); del_blogger.WorkerReportsProgress = true; del_blogger.WorkerSupportsCancellation = true; del_blogger.DoWork += del_blogger_run;
            fb_login_profile = new BackgroundWorker(); fb_login_profile.WorkerReportsProgress = true; fb_login_profile.WorkerSupportsCancellation = true; fb_login_profile.DoWork += fb_login_profile_run;
            auto_fix_profile = new BackgroundWorker(); auto_fix_profile.WorkerReportsProgress = true; auto_fix_profile.WorkerSupportsCancellation = true; auto_fix_profile.DoWork += auto_fix_profile_run;
            auto_sys_profile = new BackgroundWorker(); auto_sys_profile.WorkerReportsProgress = true; auto_sys_profile.WorkerSupportsCancellation = true; auto_sys_profile.DoWork += auto_sys_profile_run;            
            load_profile = new BackgroundWorker(); load_profile.WorkerReportsProgress = true; load_profile.WorkerSupportsCancellation = true; load_profile.DoWork += load_profile_run;
            backup_fb = new BackgroundWorker(); backup_fb.WorkerReportsProgress = true; backup_fb.WorkerSupportsCancellation = true; backup_fb.DoWork += backup_fb_run;
            tim_profile = new BackgroundWorker(); tim_profile.WorkerReportsProgress = true; tim_profile.WorkerSupportsCancellation = true; tim_profile.DoWork += tim_profile_run;
            thoat_nhom_fb = new BackgroundWorker(); thoat_nhom_fb.WorkerReportsProgress = true; thoat_nhom_fb.WorkerSupportsCancellation = true; thoat_nhom_fb.DoWork += thoat_nhom_fb_run;
            all_facebook = new BackgroundWorker(); all_facebook.WorkerReportsProgress = true; all_facebook.WorkerSupportsCancellation = true; all_facebook.DoWork += all_facebook_run;
            auto_update_dp = new BackgroundWorker(); auto_update_dp.WorkerReportsProgress = true; auto_update_dp.WorkerSupportsCancellation = true; auto_update_dp.DoWork += auto_update_dp_run;            
        }

        #region share_gr_m_fb_1
        public void share_gr_m_fb_1()
        {
            var post_09 = driver.FindElementsByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
            if (post_09.Count > 0)
            {
                var post_11 = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                post_11.Click(); Thread.Sleep(1000);
            }
            else
            {
                string html_home1 = driver.PageSource;
                string id_click_post = Regex.Match(html_home1, @"style.display = 'none';"".*?(?="">)").Value.Replace("style.display = 'none';\" id=\"", "");
                if (id_click_post == "")
                {
                    id_click_post = Regex.Match(html_home1, @"textbox.*?(?="">)").Value.Replace("textbox\" id=\"", "");
                }
                var post_10 = driver.FindElementByXPath("//*[@id=\"" + id_click_post + "\"]/div");
                post_10.Click(); Thread.Sleep(1000);

            } Thread.Sleep(4000);
        }
        #endregion

        #region share_gr_m_fb_2
        public void share_gr_m_fb_2(string noi_dung_text, string noi_dung_link = null)
        {
            for (int ia = 0; ia < 10; ia++)
            {
                try
                {
                    try
                    {
                        var post_22_226 = driver.FindElementByXPath("//*[@id=\"composer-placeholder-textarea\"]");
                        if (noi_dung_link != null) { post_22_226.SendKeys(noi_dung_link+" . ."); Thread.Sleep(5000); }
                        post_22_226.SendKeys(OpenQA.Selenium.Keys.Control + "a"); post_22_226.SendKeys(noi_dung_text); Thread.Sleep(2000); ia = 99;
                    }
                    catch { }

                    try
                    {
                        if (ia < 10)
                        {
                            var post_22_225 = driver.FindElementByXPath("//*[@id=\"uniqid_1\"]");
                            if (noi_dung_link != null) { post_22_225.SendKeys(noi_dung_link + " . ."); Thread.Sleep(2000); }
                            post_22_225.SendKeys(OpenQA.Selenium.Keys.Control + "a");  post_22_225.SendKeys(noi_dung_text); Thread.Sleep(5000); ia = 99;
                        }
                    }
                    catch { }

                }
                catch { Thread.Sleep(2000); }
            }
        }
        #endregion

        #region share_gr_m_fb_3
        public string share_gr_m_fb_3()
        {
            string ka_1 = "e";
            try
            {
                for (int ip = 0; ip < 10; ip++)
                {
                    try
                    {

                        try
                        {
                            var post_13_1 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[3]/div/button");
                            post_13_1.Click(); Thread.Sleep(2000); ip = 10;
                        }
                        catch { }

                        try
                        {
                            var post_13 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[1]/div/div[3]/div/button");
                            post_13.Click(); Thread.Sleep(2000); ip = 10;
                        }
                        catch { }


                    }
                    catch { Thread.Sleep(2000); }
                }
                ka_1 = "oke";
            } catch { ka_1 = "error"; }
            return ka_1;
        }
        #endregion

        #region share_gr_m_fb
        public string share_gr_m_fb(string id_nhom, string conten_1, string conten_2 = null)
        {
            string ket_qua = "oke";
            driver.Url = "https://m.facebook.com/groups/"+ id_nhom; driver.Navigate(); Thread.Sleep(5000);
            share_gr_m_fb_1();
            if (conten_2 == null) { share_gr_m_fb_2(conten_1); } else { share_gr_m_fb_2(conten_1, conten_2); }
            ket_qua = share_gr_m_fb_3();
            return ket_qua;
        }
        #endregion

        #region khang_spam
        public string khang_spam()
        {
            string ket_qua = "oke";
            try {
                driver.Url = "https://www.facebook.com/support/"; driver.Navigate(); Thread.Sleep(5000);
                string html_01 = Regex.Match(driver.PageSource, @"Facebook © 2019.*?<div><div><div class=").Value.Replace("id=\"con", "");
                string html_02 = Regex.Match(html_01, @"id=""(.*?)(?="")").Value.Replace("id=\"", "");
                var cl01 = driver.FindElementByXPath("//*[@id=\"" + html_02 + "\"]/div/div[1]/div[1]/div[2]/div/div/a/div/table/tbody/tr/td[2]/div/span"); cl01.Click(); Thread.Sleep(5000);
                for (int i1 = 7; i1 < 13; i1++)
                {
                    try
                    {
                        var cl02 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + i1 + "]/div[2]/div/div/div/div/div/div/div[2]/span[2]/div/div/button"); cl02.Click();
                    }
                    catch { }

                }
                Thread.Sleep(5000);


                for (int i2 = 7; i2 < 13; i2++)
                {
                    try
                    {
                        var cl03 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + i2 + "]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); cl03.Click();
                    }
                    catch { }

                }
                Thread.Sleep(5000);

                string html_03 = Regex.Match(driver.PageSource, @"</div><label class=.*?style").Value.Replace("id=\"con", "");
                string html_04 = Regex.Match(html_03, @"id=""(.*?)(?="")").Value.Replace("id=\"", "");

                var cl04 = driver.FindElementById(html_04); cl04.Click(); Thread.Sleep(1000);

                for (int i3 = 7; i3 < 13; i3++)
                {
                    try
                    {
                        var cl05 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + i3 + "]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button"); cl05.Click();
                    }
                    catch { }

                }
            } catch { ket_qua = "error"; }
            return ket_qua;
        }
        #endregion
        
        #region TEST AUTO ĐỂ ĐƯA VÀO CHÍNH THỨC ------------------------------------------------------------------------------------------------------------------------------------>
        public void test_auto1()
        {
           
            //profile_danplay_mau_pc("thu_nghiem"); //  - 100041007664697 - 100034072201187 -100033012742458 - 100033719207972     
            
          
            
        }
        #endregion

        #region RECOVER EMAIL
        public string recover_email(string id_acc, string acc_login)
        {
            string pass_moi = "login";
            try
            {                
                string email = GetData("https://vip.danplay.net/?auto=adp_facebook&recover_passs=" + id_acc + "&key=" + textBox5.Text);
                if (email.Length > 5)
                {
                    driver.Url = "https://www.facebook.com/login/identify/"; driver.Navigate();
                    var r001 = driver.FindElementById("identify_email"); r001.SendKeys(email);
                    var r002 = driver.FindElementByName("did_submit"); r002.Click(); Thread.Sleep(5000);                                            
                    try { var r002_1 = driver.FindElementByXPath("//*[@id=\"initiate_interstitial\"]/div[3]/div/div[1]/button"); r002_1.Click(); Thread.Sleep(5000); } catch { }
                    if (driver.Url.Contains("send_email") && !driver.Url.Contains("gmail.com") && !driver.Url.Contains("yahoo.com"))
                    {
                        try
                        {
                            int lap_tt = 0;
                            while (lap_tt < 20)
                            {
                                lap_tt++; Thread.Sleep(10000); string check_email = GetData("https://email.danplay.net/?e=email&email_recover=" + email); // MessageBox.Show("Lặp lần: " + lap_tt, "kq check: " + check_email);
                                try
                                {
                                    if (check_email.Length > 4)
                                    {
                                        try
                                        {
                                            var r004 = driver.FindElementById("recovery_code_entry"); r004.SendKeys(check_email);
                                            var r005 = driver.FindElementByName("reset_action"); r005.Click(); Thread.Sleep(5000);
                                        }
                                        catch { }

                                        try
                                        {
                                            pass_moi = random_pass_01(12);
                                            var r006 = driver.FindElementById("password_new"); r006.SendKeys(pass_moi); Thread.Sleep(2000);
                                            var r007 = driver.FindElementById("btn_continue"); r007.Click();
                                            key_danplay("change", "add_new_pass_v1&pass_cu=khong_nho&pass_moi=" + pass_moi + "&id_account=" + id_acc); Thread.Sleep(1000);

                                            try
                                            {
                                                string code_2fa_get = GetData("https://vip.danplay.net/2fa/res.php?get_2fa=" + acc_login);
                                                var r008 = driver.FindElementById("approvals_code"); r008.SendKeys(code_2fa_get);
                                            }
                                            catch { }                                            
                                            try { var r009 = driver.FindElementById("checkpointSubmitButton"); r009.Click(); } catch { }
                                            try { var r009 = driver.FindElementById("checkpointSubmitButton"); r009.Click(); } catch { }
                                            try { var r009 = driver.FindElementById("checkpointSubmitButton"); r009.Click(); } catch { }
                                            try { var r009 = driver.FindElementById("checkpointSubmitButton"); r009.Click(); } catch { }
                                            try { var r009 = driver.FindElementById("checkpointSubmitButton"); r009.Click(); } catch { }
                                            pass_moi = Regex.Match(driver.PageSource, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                                        }
                                        catch { }
                                        lap_tt = 100;
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { GetData("https://vip.danplay.net/?auto=adp_facebook&recover_not_email=" + id_acc + "&key=" + textBox5.Text); }
                    } else
                    {
                        string capcha_v2 = Regex.Match(driver.PageSource, @"ReCaptchav2Captcha.*?captcha_persist_data").Value.Replace("vubaodan", "");
                        if (capcha_v2.Length > 2) {
                            if(checkBox18.Checked == true)
                            {
                                try { driver.Close(); driver.Quit(); } catch { }
                                reboot_modem(); ket_noi_sever();
                            }
                        }
                        pass_moi = "login";
                    }
                }             
            } catch { }
            if (pass_moi.Length < 7) { pass_moi = "login"; }
            return pass_moi;
        }
        #endregion

        #region cac_truong_hop_check
        public string cac_truong_hop_check(string kq_login = null)
        {
            string truong_hop = "|";
            if (driver.Url.Contains("checkpoint"))
            {
                Thread.Sleep(3000);
                try
                {
                    string htmls = driver.PageSource;
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"id_upload\"")) { truong_hop = truong_hop + "Upload ảnh"; }
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"3\"")) { truong_hop = truong_hop + "|Xác minh ảnh bạn bè"; }
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"14\"")) { truong_hop = truong_hop + "|Phê duyệt đăng nhập"; }
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"18\"")) { truong_hop = truong_hop + "|Xác minh bình luận"; }
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"26\"")) { truong_hop = truong_hop + "|Nhờ bạn bè tin cậy"; }
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"34\"")) { truong_hop = truong_hop + "|Gửi mã về điện thoại"; }
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"35\"")) { truong_hop = truong_hop + "|Đăng nhập bằng GOOGLE"; }
                    if (htmls.Contains("<input type=\"radio\" name=\"verification_method\" value=\"37\"")) { truong_hop = truong_hop + "|Nhận mã ở email"; }
                    if (htmls.Contains("<input type=\"file\"")) { truong_hop = truong_hop + "|Upload file"; }
                    if (htmls.Contains("name=\"upload_meta\"")) { truong_hop = truong_hop + "|Upload ảnh"; }
                    if (htmls.Contains("<u>Warning</u>")) { truong_hop = truong_hop + "|Dừng vòng lặp"; }
                }
                catch { }
            }
                                         
            return truong_hop;
        }
        #endregion

        #region ĐỔI TÊN FACEBOOK
        public string doi_ten_fb(string id_via, string uid, string pass)
        {            
            string uias = GetData("https://vip.danplay.net/?auto=adp_facebook&tim_via_theo_id=" + id_via + "&key=" + textBox5.Text + "&gia_tri=id_clone");         
            string ketqua = "Đổi tên VN";
            if(uias.Length < 10)
            {
                try {
                    string name_doi_ten = GetData("https://vip.danplay.net/?auto=adp_facebook&id_clone_name=" + uid); string name1 = ""; string name2 = "";
                    try { string[] arrta = name_doi_ten.Split('|'); name1 = arrta[0]; name2 = arrta[1]; } catch { }
                    driver.Navigate().GoToUrl("https://www.facebook.com/settings?tab=account&section=name&view"); Thread.Sleep(3000);
                    var cl001 = driver.FindElementByName("primary_last_name"); cl001.SendKeys(OpenQA.Selenium.Keys.Control + "a"); cl001.SendKeys(name1); Thread.Sleep(100);
                    try { var cl001_1 = driver.FindElementByName("primary_middle_name"); cl001_1.SendKeys(OpenQA.Selenium.Keys.Control + "a"); cl001_1.SendKeys(OpenQA.Selenium.Keys.Delete); } catch { }Thread.Sleep(100);  
                    var cl002 = driver.FindElementByName("primary_first_name"); cl002.SendKeys(OpenQA.Selenium.Keys.Control + "a"); cl002.SendKeys(name2); Thread.Sleep(100);
                    string id_click = Regex.Match(driver.PageSource, @"type=""submit"" id="".*?(?="")").Value.Replace("type=\"submit\" id=\"", "");
                    var n001 = driver.FindElementById(id_click); n001.Click(); Thread.Sleep(5000);
                    var n002 = driver.FindElementById("save_password"); n002.SendKeys(pass);
                    n002.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    try { var n003 = driver.FindElementById("save_password"); n003.SendKeys("that_bai"); ketqua = "SAI_PASS"; } catch { }
                } catch { ketqua = "ERROR đổi tên"; }
                GetData("https://vip.danplay.net/?auto=adp_facebook&ghi_chu_moi="+ ketqua + "&id_account="+ id_via + "&key="+ textBox5.Text);
                //<-- Chưa lưu database để TEST code -->
            }            
            return ketqua;
        }
        #endregion     

        #region LIKE bài viết --------------------------------------------------------- HAY SỮA NHẤT
        public string like_post_wall_account_v2(string id_post)
        {
            string ketqua = "error_tt";
            try {
                string url_chay = "https://m.facebook.com/"+ id_post;
                if (id_post.Length > 20) { url_chay = id_post;} url_chay = url_chay.Replace("www", "m");
                driver.Url = url_chay; driver.Navigate(); Thread.Sleep(2000);
                string st001 = Regex.Match(driver.PageSource, @"{&quot;tn&quot;:&quot;>&quot;}"" id="".*?(?="")").Value.Replace("{&quot;tn&quot;:&quot;>&quot;}\" id=\"", "");
                string st002 = driver.FindElementById(st001).GetAttribute("aria-pressed");
                if (st002 == "false")
                {
                    var st003 = driver.FindElementById(st001); st003.Click(); ketqua = "oke";
                }
                else { ketqua = "like"; }

            } catch { ketqua = "error_tt"; }
            return ketqua;
        }        
        #endregion        

        #region CMT + LIKE AUTO --------------------------------------------------------- HAY SỮA NHẤT
        public string like_cmt_post_v2(string id_post, string noidung = null)
        {
            string ketqua = "error_v2"; 
            if (noidung == null) { noidung = "<3"; }
            try
            {
                string url_chay = "https://m.facebook.com/" + id_post;
                if (id_post.Length > 20) { url_chay = id_post; }
                url_chay = url_chay.Replace("www", "m");
                driver.Url = url_chay; driver.Navigate(); Thread.Sleep(2000);
                string st001 = Regex.Match(driver.PageSource, @"{&quot;tn&quot;:&quot;>&quot;}"" id="".*?(?="")").Value.Replace("{&quot;tn&quot;:&quot;>&quot;}\" id=\"", "");
                string st002 = driver.FindElementById(st001).GetAttribute("aria-pressed");
                if (st002 == "false")
                {
                    var st003 = driver.FindElementById(st001); st003.Click(); ketqua = "oke";
                    var st004 = driver.FindElementById("composerInput"); st004.SendKeys(noidung); Thread.Sleep(5000);
                    var st005 = driver.FindElementByName("submit"); st005.Click(); ketqua = "cmt_oke";
                }
                else { ketqua = "like"; }

            }
            catch { ketqua = "error_tt"; }
            return ketqua;
        }       
        #endregion        

        #region XIN NHÓM FACEBOOK
        public string xin_nhom_facebook(string id_nhom)
        {
            string ketqua = "no";
            driver.Navigate().GoToUrl("https://m.facebook.com/groups/" + id_nhom); Thread.Sleep(5000);
            string button_add = "0";
            try { if (button_add == "0") { button_add = driver.FindElementByXPath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div/button").GetAttribute("use"); } } catch { }
            try { if (button_add == "0") { button_add = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[2]/div/div[1]/div/button").GetAttribute("use"); } } catch { }
            if (button_add == "special")
            {
                try { var button_add_1 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div/button"); button_add_1.Click(); } catch { }
                try { var button_add_1 = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[2]/div/div[1]/div/button"); button_add_1.Click(); } catch { }
                ketqua = "ok";
            }
            return ketqua;
        }
        #endregion

        #region Khởi động lại modem lấy IP mới
        public void reboot_modem()
        {
            string profile_tam = "vubaodan_" + random_pass_01(10);
            profile_danplay_blogger(profile_tam);            
            driver.Url = "http://192.168.0.1/"; driver.Navigate(); //Thread.Sleep(1000);


            try {
                var e01 = driver.FindElementById("details-button"); e01.Click(); Thread.Sleep(3000);
                var e02 = driver.FindElementById("proceed-link"); e02.Click(); Thread.Sleep(3000);
            } catch { }
            try {
                var a01 = driver.FindElementById("userName"); a01.SendKeys("admin"); Thread.Sleep(100);
                var a02 = driver.FindElementById("pcPassword"); a02.SendKeys("admin"); Thread.Sleep(100);
                var a03 = driver.FindElementById("loginBtn"); a03.Click(); Thread.Sleep(5000);
            } catch {

                SendKeys.SendWait("admin");
                SendKeys.SendWait("{TAB}");
                SendKeys.SendWait("admin");
                SendKeys.SendWait("{ENTER}"); Thread.Sleep(2000);
                for (int i = 0; i < 17; i++)
                {
                    Thread.Sleep(100);
                    SendKeys.SendWait("{TAB}");
                }
                SendKeys.SendWait("{ENTER}"); Thread.Sleep(1000);
                for (int i = 0; i < 6; i++)
                {
                    Thread.Sleep(100);
                    SendKeys.SendWait("{TAB}");
                }
                SendKeys.SendWait("{ENTER}"); Thread.Sleep(1000);
                for (int i = 0; i < 6; i++)
                {
                    Thread.Sleep(100);
                    SendKeys.SendWait("{TAB}");
                }
                SendKeys.SendWait("{ENTER}"); Thread.Sleep(100); SendKeys.SendWait("{ENTER}"); Thread.Sleep(100); SendKeys.SendWait("{ENTER}"); Thread.Sleep(100); SendKeys.SendWait("{ENTER}"); Thread.Sleep(5000);
                try { driver.Close(); driver.Quit(); } catch { }
                Thread.Sleep(5000);
                string filePath_mau_tam1 = "Profile\\" + profile_tam;
                if (Directory.Exists(filePath_mau_tam1))
                {
                    Directory.Delete(filePath_mau_tam1, true);
                }

            }
            
            try
            {
                string url = driver.Url.Replace("https", "http"); driver.Url = url; driver.Navigate();

                for (int i = 0; i < 19; i++)
                {
                    Thread.Sleep(100);
                    SendKeys.SendWait("{TAB}");
                }
                SendKeys.SendWait("{ENTER}"); Thread.Sleep(1000);
                for (int i = 0; i < 6; i++)
                {
                    Thread.Sleep(100);
                    SendKeys.SendWait("{TAB}");
                }
                SendKeys.SendWait("{ENTER}"); Thread.Sleep(1000);
                for (int i = 0; i < 7; i++)
                {
                    Thread.Sleep(100);
                    SendKeys.SendWait("{TAB}");
                }
                SendKeys.SendWait("{ENTER}"); Thread.Sleep(100); SendKeys.SendWait("{ENTER}"); Thread.Sleep(100); SendKeys.SendWait("{ENTER}"); Thread.Sleep(100); SendKeys.SendWait("{ENTER}"); Thread.Sleep(5000);
                try { driver.Close(); driver.Quit(); } catch { }
                Thread.Sleep(5000);
                string filePath_mau_tam = "Profile\\" + profile_tam;
                if (Directory.Exists(filePath_mau_tam))
                {
                    Directory.Delete(filePath_mau_tam, true);
                }
            } catch { }
        }
        #endregion     

        #region Update version DANPLAY
        void update_version_dp()
        {
            try
            {
                //Đóng update củ chưa thoát
                try { EndTask("update_auto"); } catch { } 
                //Xóa file update củ
                if (File.Exists(Path.Combine("update_auto.exe")))
                {
                    File.Delete(Path.Combine("update_auto.exe"));
                }
                //Tải file update mới
                if (!File.Exists(Path.Combine("update_auto.exe")))
                {
                    HttpRequest http = new HttpRequest();
                    http.ConnectTimeout = 99999999;
                    http.KeepAliveTimeout = 99999999;
                    http.ReadWriteTimeout = 99999999;
                    var binImg = http.Get("http://cloud.danplay.net/kfkskasdasfksdaasd1/auto_danplay/update_auto.exe").ToMemoryStream().ToArray();
                    File.WriteAllBytes("update_auto.exe", binImg);
                }
                //Kiểm tra lại lần nữa, nếu có thì khởi động update
                if (File.Exists(Path.Combine("update_auto.exe")))
                {
                    Process.Start("update_auto");
                }
            } catch { MessageBox.Show("Gặp lỗi trong lúc Update version mới, xin thử lại!", "Thông báo"); }
        }
        #endregion

        #region PROFILE - xóa profile nhưng để lại cookie
        public void del_profile_cooke(string nameprofile)
        {
            try
            {
                string new_profile = nameprofile + "_newdanplay";

                string file_goc = "Profile\\" + nameprofile + "\\Default\\Cookies";
                string file_moi = "Profile\\" + nameprofile + "_newdanplay\\Default\\Cookies";
                string thu_muc_moi = "Profile\\" + new_profile;
                string thu_muc_goc = "Profile\\" + nameprofile;
                thu_muc_goc = thu_muc_goc.Replace("_newdanplay", "");
                if (File.Exists(file_goc))
                {
                    if (!Directory.Exists("Profile\\" + new_profile)) { Directory.CreateDirectory("Profile\\" + new_profile + "\\Default"); } //Tạo thư mục mới
                    System.IO.File.Copy(file_goc, file_moi, true);
                    //Xóa thư mục gốc                                                    
                    if (Directory.Exists(thu_muc_goc)) { Directory.Delete(thu_muc_goc, true); }
                    //Đổi tên thư mục mới thành tên thư mục góc                                                                        
                }
                try { Directory.Move(thu_muc_moi, thu_muc_goc); } catch { }
            }
            catch { }
        }
        #endregion

        #region THEME: các nút button cơ bản
        int iii = 1;
        private void Button2_Click(object sender, EventArgs e)
        {
            iii = iii + 1;
            iii = iii % 2;
            if (iii == 0) { this.textBox5.UseSystemPasswordChar = false; button2.Text = "Che KEY"; }
            else { this.textBox5.UseSystemPasswordChar = true; button2.Text = "Show KEY"; }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string html = key_danplay("change", "check_key");
            if (html != "") { button3.Text = html; } else { button3.Text = "KEY không đúng"; }
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            int luong = 0;
            try { luong = Convert.ToInt32(textBox6.Text); } catch { luong = 1; }
            key_danplay("post", "xac_nhan_luong&input=" + luong);
            MessageBox.Show("Auto POST: sẽ chạy " + luong + " luồng cùng lúc", "Cấu hình luồng");
        }
        #endregion

        #region THEME: listview
        void LoadlistView1()
        {            
            try
            {
                listView1.Items.Clear();
                listView1.View = View.Details;
                listView1.FullRowSelect = true;
                listView1.GridLines = true;


                string filePath_pr = "Profile\\data_profile.txt"; // Kiểm tra sự tồn tại của tập tin                   
                if (System.IO.File.Exists(filePath_pr)) {                    
                    string[] text_profire = File.ReadAllText(filePath_pr, Encoding.UTF8).Split('|'); int tong_pr = text_profire.Count();                    
                    for (int i=0; i< tong_pr; i++)
                    {
                        ListViewItem item1 = new ListViewItem();
                        string[] skaj = text_profire[i].Split('>');
                        item1.Text = "" + (i+1);                        
                        item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = skaj[0] });
                        item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = skaj[1] }); 
                        listView1.Items.Add(item1);
                    }
                    
                } else
                {
                    string pathFolder = "Profile";
                    string[] DirectoryList = Directory.GetDirectories(pathFolder, "*", SearchOption.TopDirectoryOnly);
                    int i = 0;
                    string html_note_profile = GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&xuat_note_profile");
                    string data_new = null;
                    foreach (var item in DirectoryList)
                    {
                        i++;
                        ListViewItem item1 = new ListViewItem();
                        string nameprofile = item.Replace("Profile\\", "");
                        item1.Text = "" + i;
                        string ngay_sks = "......";
                        string name_profile_s = item.ToString().Replace("Profile\\", "");
                        string ghi_chu = Regex.Match(html_note_profile, @"-" + name_profile_s + "-.*?}").ToString();                                                    
                        if (ghi_chu.Length > 1) { string ghi_chu_1 = Regex.Match(ghi_chu, @"--.*?(?=})").Value.Replace("--", ""); if (ghi_chu_1.Length > 1) { ngay_sks = ghi_chu_1; } else { ngay_sks = "auto"; } }
                        if (name_profile_s == "profile_danplay") { ngay_sks = "Profile HỆ THỐNG"; }
                        item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = item.Replace("Profile\\", "") });
                        item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = ngay_sks });
                        listView1.Items.Add(item1);                        
                        data_new = data_new + name_profile_s + ">" + ngay_sks + "|";
                    }
                    File.WriteAllText("Profile\\data_profile.txt", data_new);
                }


                

                
            } catch { }
        }
        #endregion

        #region THEME: link về web
        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string link = @"https://vip.danplay.net/?act=018";
            Process.Start(link);
        }
        #endregion

        #region TẮT CHƯƠNG TRÌNH - EndTask
        public void EndTask(string taskname)
        {
            string processName = taskname.Replace(".exe", "");
            foreach (Process process in Process.GetProcessesByName(processName))
            {
                process.Kill();
            }
        }
        #endregion    

        #region FACEBOOK - LOGIN: login_facebook: login - check - ID facebook - error
        public string login_facebook(string login_name, string login_pass, string url_login = null, string dell_cookie = null)
        {
            int iia = 0;
            try { if (dell_cookie != null) { driver.Manage().Cookies.DeleteAllCookies(); } } catch { }
            string kq_login = "";
            try { if (url_login != null) { driver.Url = url_login; driver.Navigate(); Thread.Sleep(5000); } } catch { }
            try
            {
                try
                {
                    try {
                        var login_001 = driver.FindElementById("email");
                        login_001.SendKeys(OpenQA.Selenium.Keys.Control + "a"); Thread.Sleep(500);
                        login_001.SendKeys(login_name); Thread.Sleep(1000);
                    } catch { }
                    var login_002 = driver.FindElementById("pass");
                    login_002.SendKeys(login_pass); Thread.Sleep(1000);
                    var login_003 = driver.FindElementById("loginbutton");
                    login_003.Click(); Thread.Sleep(5000);
                } catch {
                    driver.Url = "https://www.facebook.com/login"; driver.Navigate(); Thread.Sleep(5000);
                    var login_001 = driver.FindElementById("email");
                    login_001.SendKeys(OpenQA.Selenium.Keys.Control + "a"); Thread.Sleep(500);
                    login_001.SendKeys(login_name); Thread.Sleep(1000);                    
                    var login_002 = driver.FindElementById("pass");
                    login_002.SendKeys(login_pass); Thread.Sleep(1000);
                    var login_003 = driver.FindElementById("loginbutton");
                    login_003.Click(); Thread.Sleep(5000);
                }                
                //Thông báo kết quả sau LOGIN
                try
                {
                    var login_004 = driver.FindElementById("approvals_code");
                    string ma_2fa_get = GetData("https://vip.danplay.net/2fa/res.php?get_2fa=" + login_name);
                    if (ma_2fa_get.Length > 4)
                    {
                        login_004.SendKeys(ma_2fa_get); Thread.Sleep(1000);
                        login_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        for (int i = 0; i < 10; i++)
                        {
                            try
                            {
                                //try { string id_dung_luu = Regex.Match(driver.PageSource, @"dont_save"" class=""uiInputLabelInput uiInputLabelRadio"" id=""(.*?)(?="")").Value.Replace("dont_save\" class=\"uiInputLabelInput uiInputLabelRadio\" id=\"", "");
                                //var click_ko_luu = driver.FindElementById(id_dung_luu); click_ko_luu.Click(); Thread.Sleep(2000); } catch { }                               
                                var login_005 = driver.FindElementById("checkpointSubmitButton");
                                login_005.Click(); Thread.Sleep(2000);                                
                                if (driver.Url.Contains("checkpoint")) { if (cac_truong_hop_check(login_name) != "|") { i = 100; iia++; } }
                                
                            }
                            catch { i = 20; }
                        }
                    }
                }
                catch { }                
                go_bac_sy(login_name, login_pass);                
                if (driver.Url.Contains("login_attempt")) { kq_login = "login"; }
                if (driver.Url.Contains("checkpoint")) { kq_login = "check"; }
                string html_id_facebook = driver.PageSource;
                string id_facebook = Regex.Match(html_id_facebook, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                if (!driver.Url.Contains("login_attempt") && !driver.Url.Contains("checkpoint") && id_facebook.Length > 7) { kq_login = id_facebook;                    
                    ket_noi_sever();                    
                    GetData("https://vip.danplay.net/?auto=adp_facebook&update_nick_tot&key=" + textBox5.Text + "login_name=" + kq_login);                    
                }
            }
            catch { kq_login = "error"; }
            return kq_login;
        }
        #endregion

        #region gỡ bác sỹ nếu gặp
        public void go_bac_sy(string login_name = null, string login_pass = null)
        {
            int doi_pass_bat_buoc = 0; int iia = 0;
            if (iia == 0)
            {
                #region Vòng lặp checkpointSubmitButton                     
                try
                {
                    for (int i = 0; i < 5; i++)
                    {
                        int y = 0;
                        if (driver.Url.Contains("checkpoint")) { if (cac_truong_hop_check(login_name) != "|") { i = 100; iia++; } }
                        
                        else
                        {
                            int sokytu = driver.PageSource.Length;
                            try
                            {
                                var login_005 = driver.FindElementById("checkpointSubmitButton");
                                login_005.Click(); Thread.Sleep(2000);
                            }
                            catch { y++; }
                            try
                            {
                                var login_005 = driver.FindElementById("checkpointButtonContinue");
                                login_005.Click(); Thread.Sleep(2000);
                            }
                            catch { y++; }
                            var pa001 = driver.FindElementsByName("password_new");
                            if (y == 2 || pa001.Count > 0) { i = 20; }
                            //if (sokytu == driver.PageSource.Length) { i = i + 4; }
                            doi_pass_bat_buoc = i;
                        }
                    }
                }
                catch { }
                #endregion
            }

            if (iia == 0)
            {                
                #region Đổi pass mới nếu bị yêu cầu
                string pass_moi = random_string(10);
                //Đổi pass mới nếu bị bắt buộc
                try
                {
                    Thread.Sleep(2000);
                    var pa001 = driver.FindElementByName("password_new"); pa001.SendKeys(pass_moi); Thread.Sleep(2000);
                    doi_pass_bat_buoc++;
                    if (doi_pass_bat_buoc > 5) { ket_noi_sever(); GetData("https://vip.danplay.net/?auto=adp_facebook&go_check_new_pass&key=" + textBox5.Text + "&pass_moi=" + pass_moi + "&pass_cu=" + login_pass + "&login_name=" + login_name); }

                }
                catch { }

                try
                {
                    var pa002 = driver.FindElementByName("password_confirm"); pa002.SendKeys(pass_moi); Thread.Sleep(2000); doi_pass_bat_buoc++;
                }
                catch { }
                #endregion
            }

            if (iia == 0)
            {                
                #region Vòng lặp checkpointSubmitButton
                try
                {
                    for (int i = 0; i < 7; i++)
                    {
                        if (driver.Url.Contains("checkpoint")) { if (cac_truong_hop_check(login_name) != "|") { i = 100; iia++; } }
                        
                        else
                        {
                            int y = 0; int sokytu = driver.PageSource.Length;
                            try
                            {
                                var login_005 = driver.FindElementById("checkpointSubmitButton");
                                login_005.Click(); Thread.Sleep(5000);
                            }
                            catch { y++; }
                            try
                            {
                                var login_005 = driver.FindElementById("checkpointButtonContinue");
                                login_005.Click(); Thread.Sleep(5000);
                            }
                            catch { y++; }

                            try
                            {
                                var login_005 = driver.FindElementByName("submit[Go to News Feed]");
                                login_005.Click(); Thread.Sleep(5000);
                            }
                            catch { }

                            try
                            {
                                var login_005 = driver.FindElementByName("submit[Continue]");
                                login_005.Click(); Thread.Sleep(5000);
                            }
                            catch { }
                            if (y == 2) { i = 20; }
                            //if (sokytu == driver.PageSource.Length) { i = i + 4; }
                        }
                    }
                }
                catch { }
                #endregion
            }
        }
        #endregion

        #region FACEBOOK: add_groups_facebook
        public void add_groups_facebook(string cookie_gr, string id_account, string url_goc = null)
        {
            try
            {
                string arr_nhom = GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&xin_nhom_facebook=" + id_account + "&title=" + textBox1.Text + "|Auto xin nhóm");
                string[] arr_nhom1 = arr_nhom.Split('|');
                string id_gr = arr_nhom1[0];
                string id_nhom = arr_nhom1[1];
                if (id_nhom.Length > 5)
                {
                    string info_groups = GetData("https://www.facebook.com/groups/" + id_nhom + "/members/", cookie_gr);
                    string thanh_vien = Regex.Match(info_groups, @"<span class=""_grt _50f8"">.*?(?=</span>)").Value.Replace("<span class=\"_grt _50f8\">", "");
                    thanh_vien = thanh_vien.Replace(@".", "").Trim();
                    thanh_vien = thanh_vien.Replace(@",", "").Trim();
                    var html_id_gr = GetData("https://www.facebook.com/groups/" + id_nhom + "/requests/", cookie_gr);
                    var chuoi_duyet = Regex.Matches(html_id_gr, @"queue=pending", RegexOptions.Singleline);
                    int so_chuoi = chuoi_duyet.Count;

                    if (so_chuoi == 1)
                    {
                        GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&ketqua_nhom_facebook=" + so_chuoi + "&thanhvien=" + thanh_vien + "&id_nhom=" + id_gr + "&title=" + textBox1.Text + "|Nhóm: " + id_nhom + " bị duyệt");
                    }
                    else
                    {
                        GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&ketqua_nhom_facebook=" + so_chuoi + "&thanhvien=" + thanh_vien + "&id_nhom=" + id_gr + "&title=" + textBox1.Text + "|Nhóm: " + id_nhom + " tuyệt vời");
                        try
                        {
                            driver.Navigate().GoToUrl("https://m.facebook.com/groups/" + id_nhom); Thread.Sleep(5000);
                            string html_gr = driver.PageSource;

                            try
                            {
                                var button_add = driver.FindElementByXPath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div/button");
                                button_add.Click(); Thread.Sleep(2000);
                            }
                            catch { }

                            try
                            {
                                var button_add = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[2]/div/div[1]/div/div/button");
                                button_add.Click(); Thread.Sleep(2000);
                            }
                            catch { }
                            if (url_goc != null) { driver.Navigate().GoToUrl(url_goc); Thread.Sleep(3000); }

                        }
                        catch
                        {
                            if (url_goc != null) { driver.Navigate().GoToUrl(url_goc); Thread.Sleep(3000); }
                        }
                    }
                }
            }
            catch { }
        }

        #endregion

        #region FACEBOOK: go_check_poit
        public void go_check_poit()
        {

            try
            {
                var get_theme_old = driver.FindElementByXPath("//*[@id=\"viewport\"]/div[3]/div/a");
                get_theme_old.Click(); Thread.Sleep(2000);
            }
            catch { }


            for (int c1 = 0; c1 < 20; c1++)
            {
                try
                {
                    var get_chekpoit = driver.FindElementByXPath("//*[@id=\"checkpointSubmitButton-actual-button\"]");
                    get_chekpoit.Click(); Thread.Sleep(2000);
                }
                catch
                {
                    try {
                        Thread.Sleep(1000);
                        var get_chekpoit_1 = driver.FindElementsByXPath("//*[@id=\"checkpointSubmitButton-actual-button\"]");
                        if (get_chekpoit_1.Count < 1) { c1 = 20; }
                    } catch { }
                }
            }
        }
        #endregion

        #region CHECK_KEY: key_danplay
        public string key_danplay(string file_check, string active = null, string data = null, string notes = null)
        {
            string html = "";
            if (textBox4.Text != "" && textBox5.Text != "")
            {
                #region lưu file txt giá trị auto đang chạy
                if (checkBox1.CheckState == CheckState.Checked && (active == "xac_nhan_luong" || active == "check_key"))
                {
                    if (!Directory.Exists("data")) { Directory.CreateDirectory("data"); }
                    string name_auto = textBox4.Text; //Ghi chú phiên hoạt động
                    string key_auto = textBox5.Text; //Key cấp trên Danplay
                    string checkbox2_change_auto = checkBox1.CheckState == CheckState.Checked ? "1" : "0"; //Đăng xuất toàn bộ thiết bị sau khi changeinfo
                    string luong_post = textBox6.Text; //Giá trị luồng post
                    string list_via_post = textBox7.Text; //Giá trị xuất tài khoản VIA - quốc gia, profile, all
                    string luong_tao_blog = textBox13.Text; //luồng tạo blogger
                    string luong_xoa_blog = textBox14.Text; //luồng xóa blogger
                    string giai_doan_1 = name_auto + "|" + key_auto + "|" + checkbox2_change_auto + "|" + luong_post + "|" + list_via_post + "|" + luong_tao_blog + "|" + luong_xoa_blog;

                    string Xin_nhom_change_auto = Xin_nhom.CheckState == CheckState.Checked ? "1" : "0"; //Xin_nhom
                    string checkBox6_change_auto = checkBox6.CheckState == CheckState.Checked ? "1" : "0"; //Quét nhóm
                    string checkBox7_change_auto = checkBox7.CheckState == CheckState.Checked ? "1" : "0"; //Bình Luận
                    string textBox16_t = textBox16.Text; //Số bình luận
                    string radio5_change_auto = radioButton5.Checked == true ? "1" : "0"; // Đăng link
                    string radio6_change_auto = radioButton6.Checked == true ? "1" : "0"; // Đăng file
                    string textBox15_t = textBox15.Text; //Số file đăng
                    string checkBox25_auto_update = checkBox25.CheckState == CheckState.Checked ? "1" : "0";
                    string form_postv2 = Xin_nhom_change_auto + "|" + checkBox6_change_auto + "|" + checkBox7_change_auto + "|" + textBox16_t + "|" + radio5_change_auto + "|" + radio6_change_auto + "|" + textBox15_t+"|"+ checkBox25_auto_update;
                    string luu_gia_tri_key = giai_doan_1 + "|" + form_postv2;

                    String filepath = Path.Combine(Environment.CurrentDirectory, "data\\key_api.txt");
                    try
                    {
                        Invoke(new Action(() =>
                        {
                            FileStream fs = new FileStream(filepath, FileMode.Create);//Tạo file mới
                                StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);//fs là 1 FileStream              
                                sWriter.WriteLine(luu_gia_tri_key.Trim());
                            sWriter.Flush();
                            fs.Close();
                        }));
                    }
                    catch { }
                }
                #endregion

                #region gửi thông tin lên sever

                file_check = "adp_" + file_check;
                try
                {
                    string key = textBox5.Text; string name_auto = textBox4.Text;
                    string url = "https://vip.danplay.net/?auto=" + file_check + "&version=" + version + "&key=" + key + "&name_auto=" + name_auto + "&active=" + active + "&data=" + data + "&notes=" + notes;
                    //if(active== "id_name_pc&input=5") { MessageBox.Show(url);}                         
                    HttpRequest http = new HttpRequest();
                    html = http.Get(url).ToString();
                } catch { html = "error"; }

                #endregion
            }
            if (active == "check_key")
            {
                try
                {
                    string kiem_tra_update = GetData("http://cloud.danplay.net/kfkskasdasfksdaasd1/danplay.php?version=" + version);
                    string[] html_ar = kiem_tra_update.Split('|');
                    if (html_ar[0] == "update")
                    {

                        try
                        {
                            Invoke((Action)(() => this.Text = "UPDATE --> " + html_ar[1]));
                            DialogResult dialogResult = MessageBox.Show("Cho phép DANPLAY tự động cập nhật version mới?\n\n" + html_ar[1], "Thông báo", MessageBoxButtons.YesNo);
                            if (dialogResult == DialogResult.Yes)
                            { update_version_dp(); Thread.Sleep(30000); }


                            while (1 < 2)
                            {
                                DialogResult dialogResult1 = MessageBox.Show("Phiên bản củ của bạn không còn hoạt động được nữa.\n\n Xin vui lòng nâng cấp, để được phục vụ!", "Thông báo", MessageBoxButtons.YesNo);
                                if (dialogResult1 == DialogResult.Yes) { update_version_dp(); }
                                Thread.Sleep(10000);
                            }

                        } catch { }

                    }
                    else
                    {
                        try { Invoke((Action)(() => this.Text = "DANPLAY.NET v"+ version + " --> " + html)); } catch { }
                    }
                } catch { }
            }

            return html;
        }

        #endregion       

        #region SELENIUM: GetData
        public string GetData(string url, string cookie = null)
        {
            string html = "";
            try
            {
                HttpRequest http = new HttpRequest();
                http.Cookies = new CookieDictionary();
                if (!string.IsNullOrEmpty(cookie))
                {
                    AddCookie(http, cookie);
                }
                http.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
                html = http.Get(url).ToString();
            }
            catch { html = "error"; }

            return html;
        }
        #endregion

        #region SELENIUM: AddCookie
        void AddCookie(HttpRequest http, string cookie)
        {
            //driver.Manage().Cookies.DeleteAllCookies();
            var temp = cookie.Split(';');
            foreach (var item in temp)
            {
                var temp2 = item.Split('=');
                if (temp2.Count() > 1)
                {
                    http.Cookies.Add(temp2[0], temp2[1]);
                }
            }
        }
        #endregion

        #region SELENIUM: GetCoookieSelenium: error - string.cookie
        public string GetCoookieSelenium(string get_cookie_domain = null)
        {
            string kq_get_cookie = "";
            try
            {
                if (get_cookie_domain != null)
                {
                    this.driver.Url = get_cookie_domain;
                    driver.Navigate();
                }
                var cookies = driver.Manage().Cookies.AllCookies;
                var strCookieList = new List<string>();
                foreach (var item in cookies)
                    strCookieList.Add($"{item.Name}={item.Value}");
                var export_cookies = string.Join(";", strCookieList.ToArray());
                kq_get_cookie = export_cookies;
            }
            catch { kq_get_cookie = "error"; }
            return kq_get_cookie;
        }
        #endregion

        #region FILE: UploadFile
        public string UploadData(HttpRequest http, string url, MultipartContent data = null, string contentType = null, string userArgent = "", string cookie = null)
        {
            if (http == null)
            {
                http = new HttpRequest();
                http.Cookies = new CookieDictionary();
            }

            if (!string.IsNullOrEmpty(cookie))
            {
                AddCookie(http, cookie);
            }

            if (!string.IsNullOrEmpty(userArgent))
            {
                http.UserAgent = userArgent;
            }

            string html = http.Post(url, data).ToString();
            return html;
        }


        public string UploadFile(string url, string path)
        {
            MultipartContent data = new MultipartContent() {
                { new StringContent("name"), "up"},
                { new FileContent(path), "fileUpload", Path.GetFileName(path)}
            };
            string html = UploadData(null, url, data, "multipart/form-data");
            return html;
        }
        #endregion

        #region PROFILE: profile_danplay_mau: oke - error
        public string profile_danplay_mau(string profile_name)
        {
            string profile_add = "Profile";
            string profile_new = "Profile\\" + profile_name;
            string profile_old = "Profile\\profile_danplay";
            string profile_old_zip = "Profile\\profile_danplay.zip";
            string kq_profile_nem = "";
            try
            {
                if (!Directory.Exists(profile_old)) { MessageBox.Show("Bạn chưa tạo Profile mẫu", "Lổi hệ thống"); }
                else
                {
                    if (!Directory.Exists(profile_add)) { Directory.CreateDirectory(profile_add); }
                    if (!Directory.Exists(profile_new))
                    {
                        if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                        if (!Directory.Exists(profile_new)) { Directory.CreateDirectory(profile_new); }
                        try { ZipFile.ExtractToDirectory(profile_old_zip, profile_new); } catch { }
                    }
                    else
                    {
                        try
                        {
                            string file_moi2 = profile_new + "\\chrome_debug.log";  //Nếu PROFILE chưa bị xóa thì ko cần làm bước này
                            if (!File.Exists(file_moi2))
                            {
                                if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                                string file_goc1 = profile_new + "\\Default\\Cookies";
                                string file_moi1 = profile_new + "\\Default\\VBD-Cookies";
                                System.IO.File.Copy(file_goc1, file_moi1, true); // Copy qua cái mới                            
                                if (File.Exists(file_goc1)) { File.Delete(file_goc1); } //Xóa cái củ    
                                ZipFile.ExtractToDirectory(profile_old_zip, profile_new); //Giải nén
                                System.IO.File.Copy(file_moi1, file_goc1, true); //Chuyển cái bản sao thành bản chính
                                if (File.Exists(file_moi1)) { File.Delete(file_moi1); } //Xóa bản sao
                            }
                        }
                        catch { }
                    }

                    ChromeOptions options = new ChromeOptions();
                    options.AddArgument("user-data-dir=" + ProfileFolderPath + "\\" + profile_name);
                    options.AddArgument("--window-size=600,700");
                    options.AddArgument("--disable-infobars");
                    options.AddArgument("--disable-notifications");
                    var driverService = ChromeDriverService.CreateDefaultService();
                    driverService.HideCommandPromptWindow = true;
                    driver = new ChromeDriver(driverService, options);
                    kq_profile_nem = "oke";
                }
            }
            catch { string filePath_rog = "Profile\\profile_danplay.zip"; if (File.Exists(filePath_rog)) { File.Delete(filePath_rog); } kq_profile_nem = "error"; }
            return kq_profile_nem;
        }
        #endregion

        #region PROFILE PC: profile_danplay_mau: oke - error
        public string profile_danplay_mau_pc(string profile_name)
        {
            string profile_add = "Profile";
            string profile_new = "Profile\\" + profile_name;
            string profile_old = "Profile\\profile_danplay";
            string profile_old_zip = "Profile\\profile_danplay.zip";
            string kq_profile_nem = "";            
            try
            {
                if (!Directory.Exists(profile_old)) { MessageBox.Show("Bạn chưa tạo Profile mẫu", "Lổi hệ thống"); }
                else
                {
                    if (!Directory.Exists(profile_add)) { Directory.CreateDirectory(profile_add); }
                    if (!Directory.Exists(profile_new))
                    {
                        if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                        if (!Directory.Exists(profile_new)) { Directory.CreateDirectory(profile_new); }
                        try { ZipFile.ExtractToDirectory(profile_old_zip, profile_new); } catch { }
                    }
                    else
                    {
                        try
                        {
                            string file_moi2 = profile_new + "\\chrome_debug.log";  //Nếu PROFILE chưa bị xóa thì ko cần làm bước này
                            if (!File.Exists(file_moi2))
                            {
                                if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                                string file_goc1 = profile_new + "\\Default\\Cookies";
                                string file_moi1 = profile_new + "\\Default\\VBD-Cookies";
                                System.IO.File.Copy(file_goc1, file_moi1, true); // Copy qua cái mới                            
                                if (File.Exists(file_goc1)) { File.Delete(file_goc1); } //Xóa cái củ    
                                ZipFile.ExtractToDirectory(profile_old_zip, profile_new); //Giải nén
                                System.IO.File.Copy(file_moi1, file_goc1, true); //Chuyển cái bản sao thành bản chính
                                if (File.Exists(file_moi1)) { File.Delete(file_moi1); } //Xóa bản sao
                            }
                        }
                        catch { }
                    }
                    
                    ChromeOptions options = new ChromeOptions();
                    options.AddArgument("user-data-dir=" + ProfileFolderPath + "\\" + profile_name);
                    options.AddArgument("--window-size=1100,1000");
                    options.AddArgument("--disable-infobars");
                    options.AddArgument("--disable-notifications");
                    var driverService = ChromeDriverService.CreateDefaultService();
                    driverService.HideCommandPromptWindow = true;
                    driver = new ChromeDriver(driverService, options);
                    kq_profile_nem = "oke";
                    
                }
            }
            catch { string filePath_rog = "Profile\\profile_danplay.zip"; if (File.Exists(filePath_rog)) { File.Delete(filePath_rog); } kq_profile_nem = "error"; }
            return kq_profile_nem;
        }
        #endregion

        #region PROFILE DEMO: profile_danplay_mau: oke - error
        public string profile_danplay_mau_demo(string profile_name)
        {
            string profile_add = "Profile";
            string profile_new = "Profile\\" + profile_name;
            try
            {

                if (!Directory.Exists(profile_add)) { Directory.CreateDirectory(profile_add); }
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("user-data-dir=" + ProfileFolderPath + "\\" + profile_name);
                options.AddArgument("--window-size=1500,900");
                options.AddArgument("--disable-infobars");
                options.AddArgument("--disable-notifications");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                driver = new ChromeDriver(driverService, options);

            }
            catch { MessageBox.Show("Có lỗi, bạn tạo 2 profile trùng tên...", "Thông báo"); }
            return "oke";
        }
        #endregion

        #region PROFILE DEMO_2: profile_danplay_mau: oke - error
        public string profile_danplay_mau_demo_2(string profile_name)
        {
            string profile_add = "Profile";
            string profile_new = "Profile\\" + profile_name;
            string kq = "oke";
            try
            {

                if (!Directory.Exists(profile_add)) { Directory.CreateDirectory(profile_add); }
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("user-data-dir=" + ProfileFolderPath + "\\" + profile_name);
                options.AddArgument("--window-size=400,600");
                options.AddArgument("--disable-infobars");
                options.AddArgument("--disable-notifications");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                driver = new ChromeDriver(driverService, options);

            }
            catch
            {
                kq = "error"; Thread.Sleep(10000);
                try { driver.Close(); driver.Quit(); } catch { }
            }
            return kq;
        }
        #endregion

        #region PROFILE: profile_danplay_blogger: oke - error
        public string profile_danplay_blogger(string profile_name)
        {            
            string profile_new = "Profile\\" + profile_name;
            try
            {              
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("user-data-dir=" + ProfileFolderPath + "\\" + profile_name);
                options.AddArgument("--window-size=1200,700");
                options.AddArgument("--disable-infobars");
                options.AddArgument("--disable-notifications");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                driver = new ChromeDriver(driverService, options);
                
            }catch { }
            return "oke";
        }
        #endregion

        #region PROFILE: tạo profile rổng
        //private void Button_profile_rong_Click(object sender, EventArgs e)
        //{
        //    #region xóa profile rỗng đi tạo lại
        //    string filePath_mau = "Profile\\profile_rong.zip";
        //    string filePath_mau1 = "Profile\\profile_rong";
        //    if (File.Exists(filePath_mau))
        //    {
        //        File.Delete(filePath_mau);
        //    }

        //    if (Directory.Exists(filePath_mau1))
        //    {
        //        Directory.Delete(filePath_mau1, true);
        //    }
        //    #endregion
        //    add_profile profile_rong = new add_profile("profile_rong");
        //    Thread profile_rong_1 = new Thread(profile_rong.profile_rong);
        //    profile_rong_1.Start();
        //}
        #endregion

        #region PROFILE: tạo profile mẫu
        private void Button_Profile_mau_Click(object sender, EventArgs e)
        {
            add_profile profile_danplay = new add_profile("profile_danplay");
            Thread profile_danplay_1 = new Thread(profile_danplay.profile_danplay);
            profile_danplay_1.Start();
        }
        #endregion

        #region PROFILE: xóa profile mẫu
        private void Button7_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn xóa dữ liệu Profile mẫu", "Thông báo", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string filePath_mau = "Profile\\profile_danplay.zip";
                string filePath_mau1 = "Profile\\profile_danplay";
                if (File.Exists(filePath_mau))
                {
                    File.Delete(filePath_mau);
                }

                if (Directory.Exists(filePath_mau1))
                {
                    Directory.Delete(filePath_mau1, true);
                }

                MessageBox.Show("Đã xóa Profile mẫu", "Thông báo");
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Tất cả các profile và cửa sổ Chrome hiện đang mở sẽ bị tắt\nAuto sẽ tiếp tục mở chrome để làm việc lại từ đầu!", "Thông báo hệ thống", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try { EndTask("chromedriver"); } catch { }
                try { EndTask("chrome"); } catch { }
                //MessageBox.Show("Đã tắt tất cả Chrome", "Thông báo hoàn thành");
            }

        }

        private void Button17_Click(object sender, EventArgs e)
        {            
            DialogResult dialogResult = MessageBox.Show("Tất cả Chromedriver sẽ tắt,\nChrome vẫn mở nhưng auto mất quyền điều khiển!\nBạn chắc chắn muốn tắt Chromedriver?", "Thông báo hệ thống", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                for (int i = 0; i < 10; i++)
                {
                    try { EndTask("chromedriver"); } catch { }
                }

            }
        }

        #endregion

        #region PROFILE: mở profile ở danh sách
        private void ListView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (tao_profile_12.IsBusy) { tao_profile_12.CancelAsync(); } else { tao_profile_12.RunWorkerAsync(); }
        }
        private void tao_profile_12_run(object sender, EventArgs e)
        {
            try
            {
                string productName = null;
                string price = null;
                string quantity = null;
                if (listView1.SelectedItems.Count > 0)
                {
                    productName = listView1.SelectedItems[0].SubItems[0].Text;                    
                    price = listView1.SelectedItems[0].SubItems[1].Text;                    
                    quantity = listView1.SelectedItems[0].SubItems[2].Text;                    
                    try {                        
                        list_view_pr auto_blog = new list_view_pr(price);
                        Thread auto_b3 = new Thread(() => { auto_blog.start_profile();});
                        auto_b3.IsBackground = true; auto_b3.Start();} catch { }
                    Invoke((Action)(() => textBox1.Text = price));
                }                
            }
            catch { }
        }
        #endregion

        #region PROFILE: tạo profile theo mẫu
        private void tao_profile_11_run(object sender, EventArgs e)
        {
            if (textBox1.Text == "ip") { reboot_modem(); }

            if (textBox1.Text.Length > 1 && textBox1.Text!= "ip") {
                try
                {
                    list_view_pr auto_blog = new list_view_pr(textBox1.Text);
                    Thread auto_b3 = new Thread(() => { auto_blog.start_profile(); });
                    auto_b3.IsBackground = true; auto_b3.Start();
                }
                catch { }
            }
            
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            if (tao_profile_11.IsBusy) { tao_profile_11.CancelAsync(); } else { tao_profile_11.RunWorkerAsync(); }
        }
        #endregion

        #region FACEBOOK: them_email_facebook
        string them_email_facebook(string pass_add_email, string new_email_add_facebook)
        {
            string kq_them = "";
            try
            {
                driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(3000);

                #region Tìm và xóa đi địa chỉ email đã thêm nhưng chưa very
                var add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                int del_add_email_vl_5 = 5;
                for (int i = 0; i < del_add_email_vl_5; i++)
                {
                    if (add_email_001.Count > 0)
                    {
                        var add_email_002 = driver.FindElementByClassName("SettingsEmailPendingCancel");
                        add_email_002.Click();
                        Thread.Sleep(3000);
                    }
                    add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                    if (add_email_001.Count == 0) { i = 10; }
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 1
                var add_email_003 = driver.FindElementsByClassName("PendingEmailSection");
                if (add_email_003.Count > 0)
                {
                    try
                    {
                        var add_email_004 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/a");
                        add_email_004.Click(); Thread.Sleep(5000);
                    }
                    catch { }

                    try {
                        var add_email_003a_2 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/div[2]/a[3]");
                        add_email_003a_2.Click(); Thread.Sleep(5000);
                    } catch { }

                    try {
                        string html_hientai = driver.PageSource;
                        string id_click_add_email_001 = Regex.Match(html_hientai, @"action=""/add_contactpoint/dialog/submit/"" method=""post"" onsubmit.*?(?=""><input type=""hidden"")").Value.Replace("action=\"/add_contactpoint/dialog/submit/\" method=\"post\" onsubmit=\"\" id=\"", "");
                        var add_email_006_2 = driver.FindElementByXPath("//*[@id=\"" + id_click_add_email_001 + "\"]/div[2]/div/table/tbody/tr/td/input");
                        add_email_006_2.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                        add_email_006_2.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    } catch { }

                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 2
                var add_email_005 = driver.FindElementsByName("new_email");
                if (add_email_005.Count > 0)
                {
                    var add_email_006 = driver.FindElementByName("new_email");
                    add_email_006.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                    add_email_006.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                }
                #endregion

                #region Tìm nút thêm email mới để thêm - trường hợp 3
                string id_click_add_email_002 = Regex.Match(driver.PageSource, @"action=""/add_contactpoint/dialog/submit/"" method=""post"" onsubmit.*?(?=""><input type=""hidden"")").Value.Replace("action=\"/add_contactpoint/dialog/submit/\" method=\"post\" onsubmit=\"\" id=\"", "");                               
                var add_email_005_3 = driver.FindElementsById(id_click_add_email_002);
                if (add_email_005_3.Count > 0)
                {
                    var add_email_006_3 = driver.FindElementById(id_click_add_email_002);
                    add_email_006_3.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                    add_email_006_3.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                }
                #endregion

                #region thêm mật khẩu nếu bị hỏi
                var add_email_007 = driver.FindElementsByName("ajax_password");
                if (add_email_007.Count > 0)
                {
                    them_pass_ajax(pass_add_email);
                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_them = "check"; } else { kq_them = "oke"; }
            }
            catch { kq_them = "error"; }

            return kq_them;
        }
        #endregion

        #region PROFILE: xóa profile ở danh sách listview
        private void Button5_Click(object sender, EventArgs e)
        {
            try
            {
                string productName = null;
                string price = null;
                string quantity = null;

                if (listView1.SelectedItems.Count > 0)
                {
                    productName = listView1.SelectedItems[0].SubItems[0].Text;
                    price = listView1.SelectedItems[0].SubItems[1].Text;
                    quantity = listView1.SelectedItems[0].SubItems[2].Text;

                    DialogResult dialogResult = MessageBox.Show("Xóa: " + price, "Xóa profile", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string filePath_xoa = "Profile\\" + price;
                        if (Directory.Exists(filePath_xoa))
                        {
                            Directory.Delete(filePath_xoa, true);
                        }
                        foreach (ListViewItem it in listView1.Items) { it.Remove(); } 
                        
                    }
                }
                else
                {
                    MessageBox.Show("Hãy chọn 1 Profile cần xóa");
                }
                string filePath_smau = "Profile\\data_profile.txt";
                if (File.Exists(filePath_smau)) { File.Delete(filePath_smau); }
                LoadlistView1();
            }
            catch { }
        }
        #endregion

        #region PROFILE: sự kiện textBox1 lúc thêm profile

        private void tim_profile_run(object sender, EventArgs e)
        {

            
            try
            {
                listView1.Items.Clear();
                listView1.View = View.Details;
                listView1.FullRowSelect = true;
                listView1.GridLines = true;                

                string filePath_pr = "Profile\\data_profile.txt"; // Kiểm tra sự tồn tại của tập tin 
                if (System.IO.File.Exists(filePath_pr))
                {
                    string[] text_profire = File.ReadAllText(filePath_pr, Encoding.UTF8).Split('|'); int tong_pr = text_profire.Count();
                    int ii = 0;
                    for (int i = 0; i < tong_pr; i++)
                    {
                        string[] skaj = text_profire[i].Split('>');
                        if (textBox1.Text == "") {
                            ListViewItem item1 = new ListViewItem();
                            item1.Text = "" + (i + 1);
                            item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = skaj[0] });
                            item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = skaj[1] });
                            listView1.Items.Add(item1);
                        } else {
                            if (skaj[0].Contains(textBox1.Text) || skaj[1].Contains(textBox1.Text))
                            {
                                ListViewItem item1 = new ListViewItem();
                                ii++;
                                item1.Text = "" + (ii);
                                item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = skaj[0] });
                                item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = skaj[1] });
                                listView1.Items.Add(item1);
                            }
                        }
                        
                    }
                }
                else
                {
                    string pathFolder = "Profile";
                    string[] DirectoryList = Directory.GetDirectories(pathFolder, "*", SearchOption.TopDirectoryOnly);
                    int i = 0;
                    foreach (var item in DirectoryList)
                    {
                        i++;                       
                        string nameprofile = item.Replace("Profile\\", "");
                        if (nameprofile.Contains(textBox1.Text))
                        {
                            ListViewItem item1 = new ListViewItem();
                            item1.Text = "" + i;
                            FileInfo fileInfo = new FileInfo("Profile\\" + item.Replace("Profile\\", "") + "\\chrome_debug.log");
                            item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = item.Replace("Profile\\", "") });
                            item1.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = fileInfo.CreationTime.ToString() });
                            listView1.Items.Add(item1);
                        }
                    }
                }
            } catch { }

        }

        private void TextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter)
            {
                if (tim_profile.IsBusy) { tim_profile.CancelAsync(); } else { tim_profile.RunWorkerAsync(); }
            }
        }
        #endregion        

        #region PROFILE: Làm mới Profile    
        private void load_profile_run(object sender, EventArgs e)
        {            
            string filePath_smau = "Profile\\data_profile.txt";
            if (File.Exists(filePath_smau)) { File.Delete(filePath_smau); }         
            LoadlistView1();
        }

        private void Button24_Click(object sender, EventArgs e)
        {
            if (load_profile.IsBusy) { load_profile.CancelAsync(); } else { load_profile.RunWorkerAsync(); }
        }


        #endregion

        #region PROFILE: quet
        private void auto_fix_profile_run(object sender, DoWorkEventArgs e)
        {
            string kq_check_key = key_danplay("change", "check_key");
            if (kq_check_key != "error" && kq_check_key != "")
            {
                try
                {
                    string html_check_profile = GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&xuat_profile_ok");
                    if (html_check_profile != "")
                    {
                        string pathFolder = "Profile";
                        string[] DirectoryList = Directory.GetDirectories(pathFolder, "*", SearchOption.TopDirectoryOnly);
                        foreach (var item in DirectoryList)
                        {
                            Button btn = new Button();
                            btn.Text = item.Replace("Profile\\", "");
                            btn.AutoSize = true;
                            string nameprofile = btn.Text;
                            Invoke((Action)(() => button15.Text = nameprofile));

                            string id_facebook_profile = Regex.Match(html_check_profile, @"-" + nameprofile + "-.*?(?=})").Value.Replace("-" + nameprofile + "-", "");
                            if (id_facebook_profile != "" && nameprofile != "danplay.net")
                            {

                                Invoke((Action)(() => button15.Text = "Profile: " + nameprofile + " - " + id_facebook_profile));
                                if (id_facebook_profile == "not")
                                {
                                    Invoke((Action)(() => button15.Text = "Profile: " + nameprofile + " không cần đổi tên"));
                                }
                                else
                                {
                                    //Phải đổi tên mới dùng được
                                    Invoke((Action)(() => button15.Text = nameprofile));
                                    string thu_muc_cu = "Profile\\" + nameprofile;
                                    string thu_muc_moi = "Profile\\" + id_facebook_profile;
                                    try
                                    {
                                        Directory.Move(thu_muc_cu, thu_muc_moi);

                                        Invoke((Action)(() => button15.Text = nameprofile + " - " + id_facebook_profile + "  đổi tên"));

                                    } catch { }

                                }
                            }
                            else
                            {

                                if (nameprofile == "danplay.net" || nameprofile == "profile_danplay" || nameprofile == "profile_rong")
                                {
                                    Invoke((Action)(() => button15.Text = nameprofile));
                                }
                                else
                                {
                                    string check_id_profiele_id = Regex.Match(html_check_profile, @"" + nameprofile + "").Value.Replace("-" + nameprofile + "-", "");                                    
                                    if (check_id_profiele_id == "")
                                    {
                                        try
                                        {
                                            string filePath_mau1 = "Profile\\" + nameprofile;
                                            if (Directory.Exists(filePath_mau1))
                                            {
                                                Directory.Delete(filePath_mau1, true);
                                            }
                                        }
                                        catch { }
                                        Invoke((Action)(() => button15.Text = nameprofile));
                                    }
                                }
                            }
                        }

                    }
                }
                catch { }
            }
            else { MessageBox.Show("Key không đúng, xin kiểm tra lại!", "Hệ thống"); }
            Invoke((Action)(() => button15.Text = "Quét xong Profile"));
            LoadlistView1();
        }
        private void Button15_Click(object sender, EventArgs e)
        {
            if (auto_fix_profile.IsBusy) { auto_fix_profile.CancelAsync(); } else { auto_fix_profile.RunWorkerAsync(); }
        }
        #endregion

        #region RANDOM: danh sách lệnh - random_email_yahoo - random_pass_01 - random_pass_02 - random_string
        public static int rand_num(int length)
        {
            const string alphanumericCharacters = "0123456789";
            string email_ngau_nhien = GetRandomString(length, alphanumericCharacters);
            int aa = Convert.ToInt32(email_ngau_nhien);
            return aa;
        }
        public static string GetRandomString(int length, IEnumerable<char> characterSet)
        {
            if (length < 0)
                throw new ArgumentException("length must not be negative", "length");
            if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
                throw new ArgumentException("length is too big", "length");
            if (characterSet == null)
                throw new ArgumentNullException("characterSet");
            var characterArray = characterSet.Distinct().ToArray();
            if (characterArray.Length == 0)
                throw new ArgumentException("characterSet must not be empty", "characterSet");

            var bytes = new byte[length * 8];
            var result = new char[length];
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                cryptoProvider.GetBytes(bytes);
            }
            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }
            return new string(result);
        }

        public static string random_email_yahoo(int length)
        {
            const string alphanumericCharacters = "abcdefghijklmnopqrstuvwxyz" + "0123456789";
            string email_ngau_nhien = GetRandomString(length, alphanumericCharacters);
            return "dp" + email_ngau_nhien + "@yahoo.com";
        }

        public static string random_string(int length)
        {
            const string alphanumericCharacters = "abcdefghijklmnopqrstuvwxyz" + "0123456789";
            string email_ngau_nhien = GetRandomString(length, alphanumericCharacters);
            return "dp" + email_ngau_nhien;
        }

        public static string random_pass_01(int length)
        {
            const string alphanumericCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" +
                "0123456789";
            return GetRandomString(length, alphanumericCharacters);
        }

        public static string random_pass_02(int length)
        {
            const string alphanumericCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" +
                "0123456789" +
                "~!@#$%^&*()_+";
            return GetRandomString(length, alphanumericCharacters);
        }
        #endregion       

        #region NOTE: auto_facebook
        internal void auto_facebook(string conten, int giay)
        {
            if (giay == 0)
            {
                Invoke((Action)(() => this.button23.Text = conten));
            }

            if (giay > 0)
            {
                for (int i = 0; i < giay; i++)
                {
                    int cho = giay - i;
                    Invoke((Action)(() => this.button23.Text = conten + " - đợi: " + cho + " giây")); Thread.Sleep(1000);
                }
                Invoke((Action)(() => this.button23.Text = conten + " - đang đợi"));
            }
        }
        #endregion

        #region NOTE: add_blog_note
        internal void add_blog_note(string conten, int giay)
        {
            if (giay == 0)
            {
                Invoke((Action)(() => this.button13.Text = conten));
            }

            if (giay > 0)
            {
                for (int i = 0; i < giay; i++)
                {
                    int cho = giay - i;
                    Invoke((Action)(() => this.button13.Text = conten + " - đợi: " + cho + " giây")); Thread.Sleep(1000);
                }
                Invoke((Action)(() => this.button13.Text = conten + " - đang đợi"));
            }
        }
        #endregion

        #region NOTE: note_post_fb
        internal void note_post_fb(string conten, int giay)
        {
            if (giay == 0)
            {
                Invoke((Action)(() => this.post_link_facebook.Text = conten));
            }

            if (giay > 0)
            {
                for (int i = 0; i < giay; i++)
                {
                    int cho = giay - i;
                    Invoke((Action)(() => this.post_link_facebook.Text = conten + " - đợi: " + cho + " giây")); Thread.Sleep(1000);
                }
                Invoke((Action)(() => this.post_link_facebook.Text = conten + " - đang đợi"));
            }
        }
        #endregion       

        #region AUTO_POST_FACEBOOK: post_facebook_run
        private void post_facebook_run(object sender, EventArgs e)
        {

            //string profile_name = "100008426167401";
            //string profile_id = "116157";
            //profile_danplay_mau(profile_name);

            ThreadPool.SetMaxThreads(50, 50);
            Invoke((Action)(() => post_link_facebook.BackColor = Color.SkyBlue));
            try
            {
                note_post_fb("Auto POST: kiểm tra KEY", 0);
                int status = 0; string kq_check_key = key_danplay("change", "check_key");
                if (kq_check_key != "error" && kq_check_key != "") { status = 1; }
                else
                {
                    if (kq_check_key == "") { MessageBox.Show("Bạn chưa điền đầy đủ thông tin cần thiết", "Kiểm tra kết nối"); }
                    if (kq_check_key == "error") { MessageBox.Show("KEY bạn nhập không chính xác", "Xác nhận KEY"); }
                }
                if (status == 1)
                {
                    #region update số luồng lên sever
                    int luong = 0;
                    try { luong = Convert.ToInt32(textBox6.Text); } catch { luong = 1; }
                    key_danplay("post", "xac_nhan_luong&dell=ok&input=" + luong);
                    #endregion
                    int lap_while = 0;
                    while (lap_while < 1)
                    {
                        if (textBox7.Text.Length > 4) { lap_while = 9; note_post_fb("Auto POST: Khởi động đơn luồng", 0); } else { note_post_fb("Auto POST: tính số luồng cần khởi động", 0); }
                        string id_pc_run = key_danplay("post", "id_name_pc&input=" + textBox6.Text);
                        string luong_run_input = key_danplay("post", "luong_run_auto&id_pc=" + id_pc_run);
                        int luong_run = 0; int luong_input = 0;
                        try { string[] arr_luong = luong_run_input.Split('|'); luong_run = Convert.ToInt32(arr_luong[0]); luong_input = Convert.ToInt32(arr_luong[1]); } catch { luong_run = 0; luong_input = 0; }
                        Invoke((Action)(() => this.textBox6.Text = luong_input.ToString()));
                        int curentTaskCount = 0;
                        int maxTask = luong_input;  //int maxTask = 20;
                        while (luong_run < luong_input)
                        {
                            luong_run++;
                            note_post_fb("Auto POST: Khởi động luồng: " + luong_run, 0);
                            string type_via = radioButton3.Checked == true ? "5" : "0";
                            string luu_profile = checkBox3.Checked == true ? "1" : "4";
                            string xin_nhom = Xin_nhom.Checked == true ? "1" : "4";
                            string quet_nhom = checkBox6.Checked == true ? "1" : "4";
                            string binh_luan = checkBox7.Checked == true ? "1" : "4";
                            string post_link = radioButton5.Checked == true ? "1" : "4";
                            string post_file = radioButton6.Checked == true ? "1" : "4";
                            string so_file = textBox15.Text;
                            string so_cmt = textBox16.Text;
                            post_v2 auto_post = new post_v2(type_via, textBox7.Text, id_pc_run, luu_profile, xin_nhom, quet_nhom, binh_luan, post_link, post_file, so_file, so_cmt);
                            while (curentTaskCount >= maxTask)
                            {
                                Thread.Sleep(TimeSpan.FromSeconds(1));
                            }
                            curentTaskCount++;
                            Thread auto_2 = new Thread(() =>
                            {
                                auto_post.start_auto_post_facebook();
                                curentTaskCount--;
                            });
                            auto_2.IsBackground = true;
                            auto_2.Start();

                            if (textBox7.Text.Length > 4) { luong_run = 9999; }
                            else
                            {
                                note_post_fb("Auto POST: chuẩn bị khởi luồng thứ: " + luong_run + "/" + luong_input, 3);
                            }
                        }
                        if (textBox7.Text.Length < 4) { note_post_fb("Auto POST: Đang chạy: " + luong_run + "/" + luong_input + " làm mới luồng", 10); }
                    }
                }

            }
            catch { }
            note_post_fb("Bắt đầu AUTO POST", 0);
            Invoke((Action)(() => post_link_facebook.BackColor = Color.LightGray));
        }
        private void Post_link_facebook_Click(object sender, EventArgs e)
        {
            if (post_facebook.IsBusy) { post_facebook.CancelAsync(); } else { post_facebook.RunWorkerAsync(); }
        }

        #endregion

        #region resizeImage: điều chỉnh kích thước ảnh
        public Image resizeImage(Image img, int width)
        {

            int originalW = img.Width;
            int originalH = img.Height;

            int height = width * originalH / originalW;

            Bitmap b = new Bitmap(width, height);
            Graphics g = Graphics.FromImage((Image)b);

            g.DrawImage(img, 0, 0, width, height);
            g.Dispose();

            return (Image)b;
        }
        #endregion

        #region ADD-PLAY: chọn thư mục thêm icon play
        private void TextBox8_MouseClick(object sender, MouseEventArgs e)
        {
            FolderBrowserDialog fl = new FolderBrowserDialog();
            fl.SelectedPath = "";//co the lay duong dan tu file 
            fl.ShowNewFolderButton = true;
            if (fl.ShowDialog() == DialogResult.OK)
            {
                string thu_muc = fl.SelectedPath;
                textBox8.Text = "" + thu_muc;

            }
        }
        #endregion

        #region ADD-PLAY: mở thư mục đã thêm icon play
        private void Button10_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists("Data_image\\image_add_play")) { Directory.CreateDirectory("Data_image\\image_add_play"); }
            if (!Directory.Exists("Data_image\\icon_play")) { Directory.CreateDirectory("Data_image\\icon_play"); }
            Process.Start("Data_image\\image_add_play");
        }
        #endregion

        #region ADD-PLAY: danplay_addplay_run
        private void danplay_addplay_run(object sender, DoWorkEventArgs e)
        {
            try
            {
                string path = textBox8.Text;
                Image img = null;
                string fullPath = String.Empty;
                string[] imgExtension = { "*.png", "*.jpg", "*.jpeg", ".gif", "*.bmp" };
                List<FileInfo> files = new List<FileInfo>();
                DirectoryInfo dir = new DirectoryInfo(path);
                foreach (string ext in imgExtension)
                {
                    FileInfo[] folder = dir.GetFiles(ext, SearchOption.AllDirectories);
                    int i = 0;
                    foreach (FileInfo file in folder)
                    {
                        FileStream fs = file.OpenRead();
                        fullPath = path + @"\" + file.Name;
                        try
                        {
                            i++;
                            Invoke((Action)(() => button9.Text = "IMG: " + i));
                            using (Image image = resizeImage(Image.FromFile(fullPath), 640))
                            using (Image watermarkImage = Image.FromFile("Data_image\\icon_play\\play.png"))
                            using (Graphics imageGraphics = Graphics.FromImage(image))
                            using (TextureBrush watermarkBrush = new TextureBrush(watermarkImage))
                            {
                                int x = (image.Width / 2 - watermarkImage.Width / 2);
                                int y = (image.Height / 2 - watermarkImage.Height / 2);
                                watermarkBrush.TranslateTransform(x, y);
                                imageGraphics.FillRectangle(watermarkBrush, new Rectangle(new Point(x, y), new Size(watermarkImage.Width + 1, watermarkImage.Height)));
                                image.Save("Data_image\\image_add_play\\" + file);
                            }

                        }
                        catch { }
                    }
                    Invoke((Action)(() => button9.Text = "Hoàn thành"));
                }
            }
            catch { }

        }


        private void Button9_Click(object sender, EventArgs e)
        {
            if (danplay_addplay.IsBusy)
            {
                danplay_addplay.CancelAsync();
            }
            else
            {
                danplay_addplay.RunWorkerAsync();
            }
        }
        #endregion

        #region ADD-TEXT: danplay_add_text_run
        private void Button12_Click(object sender, EventArgs e)
        {
            if (danplay_add_text.IsBusy)
            {
                danplay_add_text.CancelAsync();
            }
            else
            {
                danplay_add_text.RunWorkerAsync();
            }
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists("Data_image\\add_img_text")) { Directory.CreateDirectory("Data_image\\add_img_text"); }
            Process.Start("Data_image\\add_img_text");
        }

        private void danplay_add_text_run(object sender, EventArgs e)
        {
            int size = 0;
            try
            {
                size = Int32.Parse(textBox10.Text);
            }
            catch { size = 50; }

            int lap = 0;
            try
            {
                lap = Int32.Parse(textBox11.Text);
            }
            catch { lap = 10; }


            try
            {
                string path = textBox12.Text;
                Image img = null;
                string fullPath = String.Empty;
                string[] imgExtension = { "*.png", "*.jpg", "*.jpeg", ".gif", "*.bmp" };
                List<FileInfo> files = new List<FileInfo>();
                DirectoryInfo dir = new DirectoryInfo(path);
                foreach (string ext in imgExtension)
                {
                    FileInfo[] folder = dir.GetFiles(ext, SearchOption.AllDirectories);
                    int i = 0;
                    foreach (FileInfo file in folder)
                    {
                        for (int i_l = 0; i_l < lap; i_l++)
                        {

                            #region xữ lý ảnh
                            FileStream fs = file.OpenRead();
                            string rand = random_string(15);
                            fullPath = path + @"\" + file.Name;
                            string file_save = rand + ".jpg";
                            i++;
                            Invoke((Action)(() => button12.Text = file_save));

                            try
                            {
                                string firstText = textBox9.Text;
                                string imageFilePath = fullPath;
                                SolidBrush drawBrush = new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)));
                                //Random rd = new Random();

                                string Numrd_str;
                                int Num_size = rnd.Next(640, 900);
                                Image img_0 = resizeImage(Image.FromFile(imageFilePath), Num_size);
                                int originalW = img_0.Width;
                                int originalH = img_0.Height;

                                int Numrd1 = rnd.Next(0, originalW - 200);
                                int Numrd2 = rnd.Next(0, originalH - 100);
                                PointF firstLocation = new PointF(Numrd1, Numrd2);
                                Bitmap newBitmap;

                                #region lần 1 để lấy kích thước mới
                                using (var bitmap = (Bitmap)img_0)
                                {
                                    newBitmap = new Bitmap(bitmap);
                                }
                                string imageFilePath_1 = file_save;

                                newBitmap.Save("Data_image\\add_img_text\\" + imageFilePath_1);
                                newBitmap.Dispose();
                                #endregion

                                string imageFilePath_2 = "Data_image\\add_img_text\\" + imageFilePath_1;
                                Image img_1 = Image.FromFile(imageFilePath_2);

                                using (var bitmap = (Bitmap)img_1)//load the image file
                                {
                                    using (Graphics graphics = Graphics.FromImage(bitmap))
                                    {
                                        using (Font arialFont = new Font("Arial", size))
                                        {
                                            graphics.DrawString(firstText, arialFont, drawBrush, firstLocation);

                                        }
                                    }
                                    newBitmap = new Bitmap(bitmap);
                                }
                                newBitmap.Save("Data_image\\add_img_text\\" + imageFilePath_1);//save the image file
                                newBitmap.Dispose();
                            }
                            catch { }
                            #endregion
                        }

                    }
                    Invoke((Action)(() => button12.Text = "Tiếp tục"));
                }

            }
            catch { }

        }

        private void TextBox12_MouseClick(object sender, MouseEventArgs e)
        {
            FolderBrowserDialog fl = new FolderBrowserDialog();
            fl.SelectedPath = "";//co the lay duong dan tu file 
            fl.ShowNewFolderButton = true;
            if (fl.ShowDialog() == DialogResult.OK)
            {
                string thu_muc = fl.SelectedPath;
                textBox12.Text = "" + thu_muc;
            }
        }
        #endregion

        #region BLOGGER: login_google_tao_blog: loi_element - oke - not_email - loi_post
        public string login_google_tao_blog(string email_id, string email, string pass, string mail_kp)
        {
            string[] ar_email = email.Split('@');
            string kq_login = "";
            try
            {
                driver.Navigate().GoToUrl("https://www.blogger.com/go/createyourblog?hl=vi");
                for (int i21 = 0; i21 < 6; i21++)
                {
                    int i11 = 6 - i21; Thread.Sleep(1000);
                }

                #region login account goolge
                if (driver.Url.Contains("signin/v2"))
                {
                    try
                    {
                        var login_email = driver.FindElementById("identifierId");
                        login_email.SendKeys(email);
                        var button_email = driver.FindElementByXPath("//*[@id=\"identifierNext\"]/span/span");
                        button_email.Click();
                        Thread.Sleep(3000);
                        if (driver.Url.Contains("pwd"))
                        {
                            var pass_email = driver.FindElementByName("password");
                            Thread.Sleep(3000);
                            pass_email.SendKeys(pass);
                            var button_pass = driver.FindElementByXPath("//*[@id=\"passwordNext\"]/span/span");
                            button_pass.Click(); Thread.Sleep(3000);

                        }
                        Thread.Sleep(3000);
                        if (driver.Url.Contains("signin/v2"))
                        {
                            Thread.Sleep(3000);

                            try
                            {
                                var button_pass = driver.FindElementByXPath("//*[@id=\"view_container\"]/div/div/div[2]/div/div/div/form/span/section/div/span/div/ul/li[1]/div/div[2]");
                                button_pass.Click();
                            }
                            catch { }

                            try
                            {
                                var button_pass = driver.FindElementByXPath("//*[@id=\"view_container\"]/div/div/div[2]/div/div/div/form/span/section/div/div/div/ul/li[1]/div/div[2]");
                                button_pass.Click();
                            }
                            catch { }
                            Thread.Sleep(3000);
                            var pass_email = driver.FindElementById("identifierId");
                            Thread.Sleep(3000);
                            pass_email.SendKeys(mail_kp);
                            var button_1 = driver.FindElementByXPath("//*[@id=\"view_container\"]/div/div/div[2]/div/div[2]/div/div[1]/div/span/span");
                            button_1.Click();
                            Thread.Sleep(3000);
                        }
                    }
                    catch { }
                }
                #endregion
                if (driver.Url.Contains("signinoptions") && !driver.Url.Contains("signin/v2")) { driver.Navigate().GoToUrl("https://www.blogger.com/go/createyourblog?hl=vi"); Thread.Sleep(3000); }
                if ((driver.Url.Contains("switch-profile") || driver.Url.Contains("AccountChooser") || driver.Url.Contains("blogID") || driver.Url.Contains("blogger") || driver.Url.Contains("signinchooser") || driver.Url.Contains("createyourblog") || driver.Url.Contains("blogID")) && !driver.Url.Contains("signin/v2"))
                {

                    #region kiểm tra xem nếu lần đầu tạo blog thì phải đặt tên hồ sơ    
                    try
                    {
                        var click2 = driver.FindElementById("profileIdentifier");
                        click2.Click(); Thread.Sleep(3000);

                        var blog_001 = driver.FindElementsByName("bloggerDisplayName");
                        if (blog_001.Count > 0)
                        {
                            var blog_002 = driver.FindElementByName("bloggerDisplayName");
                            string ho_so_blog = random_string(10);
                            blog_002.SendKeys(ho_so_blog);
                            var blog_003 = driver.FindElementById("switchButton");
                            blog_003.Click(); Thread.Sleep(3000);
                        }
                    }
                    catch { }
                    #endregion

                    #region tạo blogger
                    try
                    {
                        string blogger_conten_post = GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&blogger_conten&title=" + textBox1.Text + "|Blogger: chuẩn bị nội dung bài viết");

                        if (blogger_conten_post != "error")
                        {
                            string data_blogger_add = GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&blogger_title&title=" + textBox1.Text + "|Blogger: đặt tên cho blogger");
                            string[] arr_data_blog = data_blogger_add.Split('|');
                            string dia_chi_blogger = arr_data_blog[1] + "-" + random_string(6);
                            var ten_blog_1 = driver.FindElementById("newBlog-title");
                            ten_blog_1.SendKeys(arr_data_blog[0]);
                            var ten_blog_2 = driver.FindElementById("newBlog-address");
                            ten_blog_2.SendKeys(dia_chi_blogger);
                            var button_2 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[8]/div/div/div[2]/div/div/div[2]/div[1]/div/div/fieldset/div[1]/div[1]/div[1]/div[1]/img");
                            button_2.Click(); Thread.Sleep(3000);
                            var button_3 = driver.FindElementById("newBlog-button");
                            button_3.Click(); Thread.Sleep(3000);
                            for (int i2 = 0; i2 < 10; i2++)
                            {
                                int i1 = 10 - i2; Thread.Sleep(1000);
                            }
                            var button_full = driver.FindElementsByXPath("//*[@id=\"blogger-app\"]/div[9]/div/div/div/a");
                            if (button_full.Count > 0)
                            {
                                GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&bao_email_loi=" + email_id + "&kq_1=6&title=" + textBox1.Text + "|Blogger: " + email_id + " bị lổi"); driver.Close(); driver.Quit();
                            }
                            string link_1 = driver.Url.Replace("allposts", "editor");
                            driver.Navigate().GoToUrl(link_1); driver.Navigate();
                            Thread.Sleep(6000);
                            var button_55 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[3]/div[3]/div/div/div/form/div[1]/input");
                            button_55.SendKeys(arr_data_blog[0]); Thread.Sleep(1000);
                            var button_5 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[3]/div[3]/div/div/div/form/div[5]/div/div[1]/div/span/span/button[2]");
                            button_5.Click(); Thread.Sleep(1000);
                            var button_6 = driver.FindElementByXPath("//*[@id=\"postingHtmlBox\"]");
                            button_6.SendKeys(blogger_conten_post); Thread.Sleep(1000);
                            var button_7 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[3]/div[3]/div/div/div/form/div[1]/span/button[1]");
                            button_7.Click(); Thread.Sleep(3000);
                            var button_8 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[3]/div[4]/div/div[2]/div[2]/div[2]/div[1]/table/tbody[1]/tr[1]/td[1]/div/input");
                            button_8.Click(); Thread.Sleep(1000);
                            string button_10 = driver.FindElementByXPath("//*[@id=\"blogger-app\"]/div[3]/div[4]/div/div[2]/div[2]/div[2]/div[1]/table/tbody[1]/tr[1]/td[2]/div/div[2]/a[2]").GetAttribute("href");
                            string notes_1 = email + "|" + pass + "|" + mail_kp;
                            kq_login = GetData("https://vip.danplay.net/?auto=auto_danplay&key=" + textBox5.Text + "&add_url_blogger=" + button_10 + "&notes=" + notes_1 + "&title=" + textBox1.Text + "|Blogger: " + button_10);
                        }
                        kq_login = "oke";
                        if (blogger_conten_post == "error") { kq_login = "loi_post"; }
                        if (driver.Url.Contains("signin/v2")) { kq_login = "not_email"; }
                    }
                    catch { }
                    #endregion
                }
                if (driver.Url.Contains("signin/v2")) { kq_login = "not_email"; }
            }
            catch { kq_login = "loi_element"; }
            try { if (driver.Url.Contains("speedbump")) { kq_login = "very_phone"; } } catch { }
            return kq_login;
        }
        #endregion

        #region BLOGGER: auto tạo blogger
        private void auto_tao_blogger_run(object sender, DoWorkEventArgs e)
        {

            Invoke((Action)(() => button13.BackColor = Color.Cyan));
            int status = 0; string kq_check_key = key_danplay("change", "check_key");
            if (kq_check_key != "error" && kq_check_key != "") { status = 1; }
            if (status == 1)
            {
                int luong_max = 0;
                int luong = 0;
                try { luong_max = Convert.ToInt32(textBox13.Text); } catch { luong_max = 1; }
                for (int luong_i = 0; luong_i < luong_max; luong_i++)
                {
                    add_blogger auto_blog = new add_blogger();
                    Thread auto_b2 = new Thread(() =>
                    {
                        auto_blog.tao_blogger();
                    });
                    auto_b2.IsBackground = true;
                    auto_b2.Start();
                    add_blog_note("Khởi động luồng mới: " + luong_i + "/" + luong_max, 3); Thread.Sleep(1000);
                }
            }
            add_blog_note("Bạn có thể mở thêm luồng", 0);
            Invoke((Action)(() => button13.BackColor = Color.LightGray));
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            if (auto_tao_blogger.IsBusy) { auto_tao_blogger.CancelAsync(); } else { auto_tao_blogger.RunWorkerAsync(); }
        }

        #endregion

        #region BLOGGER: login_account_google

        public string login_account_google(string email, string pass, string email_kp)
        {
            string kq_login = "oke";
            driver.Navigate().GoToUrl("https://www.blogger.com/go/createyourblog?hl=vi");
            Thread.Sleep(6000);
            #region login account goolge
            if (driver.Url.Contains("signin/v2"))
            {
                try
                {
                    var login_email = driver.FindElementById("identifierId");
                    login_email.SendKeys(email);
                    var button_email = driver.FindElementByXPath("//*[@id=\"identifierNext\"]/span/span");
                    button_email.Click();
                    Thread.Sleep(3000);
                    if (driver.Url.Contains("pwd"))
                    {
                        var pass_email = driver.FindElementByName("password");
                        Thread.Sleep(3000);
                        pass_email.SendKeys(pass);
                        var button_pass = driver.FindElementByXPath("//*[@id=\"passwordNext\"]/span/span");
                        button_pass.Click(); Thread.Sleep(3000);

                    }
                    Thread.Sleep(3000);
                    if (driver.Url.Contains("signin/v2"))
                    {
                        Thread.Sleep(3000);

                        try
                        {
                            var button_pass = driver.FindElementByXPath("//*[@id=\"view_container\"]/div/div/div[2]/div/div/div/form/span/section/div/span/div/ul/li[1]/div/div[2]");
                            button_pass.Click();
                        }
                        catch { }

                        try
                        {
                            var button_pass = driver.FindElementByXPath("//*[@id=\"view_container\"]/div/div/div[2]/div/div/div/form/span/section/div/div/div/ul/li[1]/div/div[2]");
                            button_pass.Click();
                        }
                        catch { }

                        Thread.Sleep(3000);
                        var pass_email = driver.FindElementById("identifierId");
                        pass_email.SendKeys(email_kp);
                        var button_1 = driver.FindElementByXPath("//*[@id=\"view_container\"]/div/div/div[2]/div/div[2]/div/div[1]/div/span/span");
                        button_1.Click();
                        Thread.Sleep(3000);

                    }
                }
                catch { }
            }
            #endregion
            if (driver.Url.Contains("signin/v2")) { kq_login = "not_email"; }
            if (driver.Url.Contains("speedbump")) { kq_login = "very_phone"; }
            return kq_login;
        }
        #endregion

        #region BLOGGER: del_blogger_run
        private void Button14_Click(object sender, EventArgs e)
        {
            if (del_blogger.IsBusy) { del_blogger.CancelAsync(); } else { del_blogger.RunWorkerAsync(); }
        }

        private void del_blogger_run(object sender, EventArgs e)
        {
            Invoke((Action)(() => button14.BackColor = Color.Cyan));
            int status = 0; string kq_check_key = key_danplay("change", "check_key");
            if (kq_check_key != "error" && kq_check_key != "") { status = 1; }
            if (status == 1)
            {
                int luong_max = 0;
                int luong = 0;
                try { luong_max = Convert.ToInt32(textBox14.Text); } catch { luong_max = 1; }
                for (int luong_i = 0; luong_i < luong_max; luong_i++)
                {
                    del_blogger auto_blog = new del_blogger();
                    Thread auto_b3 = new Thread(() =>
                    {
                        auto_blog.xoa_blogger();
                    });
                    auto_b3.IsBackground = true;
                    auto_b3.Start();
                    Invoke((Action)(() => button14.Text = "Khởi động luồng mới: " + luong_i + "/" + luong_max)); Thread.Sleep(1000);
                }
            }
            Invoke((Action)(() => button14.Text = "Bạn có thể mở thêm luồng để xóa blogger")); Thread.Sleep(1000);
            Invoke((Action)(() => button14.BackColor = Color.LightGray));
        }
        #endregion

        #region FACEBOOK 2FA
        public string bat_2fa_facebook(string id, string pass, string name = null)
        {
            string kq = null;
            try
            {
                string check_oke_2fa = GetData("https://vip.danplay.net/2fa/res.php?status_fa=" + id);
                if (check_oke_2fa != "10")
                {
                    if (name != null) { profile_danplay_mau(name); }
                    driver.Navigate().GoToUrl("https://m.facebook.com/security/2fac/setup/intro/"); Thread.Sleep(5000);

                    try
                    {
                        var code_2fa_002_1 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div[1]/div/input");
                        code_2fa_002_1.SendKeys(pass); Thread.Sleep(3000);
                        code_2fa_002_1.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    }
                    catch { }

                    try
                    {
                        var code_2fa_001 = driver.FindElementByXPath("//*[@id=\"intro_button\"]");
                        code_2fa_001.Click(); Thread.Sleep(5000);
                    } catch { }


                    try
                    {
                        var code_2fa_002 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div[1]/div/input");
                        code_2fa_002.SendKeys(pass); Thread.Sleep(3000);
                        code_2fa_002.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    }
                    catch { }

                    try
                    {
                        var code_2fa_003 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div/div/div[2]/div/div[1]/div/a/div/div[2]/div/div/div[2]");
                        code_2fa_003.Click(); Thread.Sleep(6000);
                    }
                    catch { }

                    try
                    {
                        var code_2fa_003 = driver.FindElementByXPath("//*[@id=\"root\"]/div[1]/div/div/div/div[2]/div/div[1]/div/div/div[2]/div/div/div[3]/a");
                        code_2fa_003.Click(); Thread.Sleep(6000);
                    }
                    catch { }
                    string code_2fa_004_2fa = "error";

                    try { code_2fa_004_2fa = driver.FindElementByXPath("//*[@id=\"root\"]/div[1]/div/form/div[2]/div/div/div/div/div[2]/div[2]").Text; } catch { }

                    try { if (code_2fa_004_2fa.Length < 10) { code_2fa_004_2fa = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/form/div[2]/div/div/div/div/div[2]/div[2]").Text; } } catch { }


                    if (code_2fa_004_2fa.Length > 10)
                    {
                        var code_2fa_005 = driver.FindElementByXPath("//*[@id=\"qr_confirm_button\"]");
                        code_2fa_005.Click();
                    } // else { MessageBox.Show("ID: " + id + " bị lỗi bật 2FA\n\n" + code_2fa_004_2fa + ", kiểm tra lại giao diện", "Lỗi bật 2FA"); }


                    if (code_2fa_004_2fa.Length > 10)
                    {
                        string code_2fa = GetData("https://vip.danplay.net/2fa/res.php?face=" + id + "&code=" + code_2fa_004_2fa);
                        var code_2fa_006 = driver.FindElementByXPath("//*[@id=\"type_code_container\"]");
                        code_2fa_006.SendKeys(code_2fa); Thread.Sleep(2000);
                        code_2fa_006.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    }               
                    

                    try
                    {
                        Thread.Sleep(5000);
                        var code_2fa_007 = driver.FindElementByXPath("//*[@id=\"TwoFactButton\"]/form/button");
                        code_2fa_007.Click();
                        GetData("https://vip.danplay.net/2fa/res.php?up_status_fa=" + id);
                    } catch { }

                    kq = "oke";
                    if (driver.Url.Contains("checkpoint")) { kq = "check"; }
                }
                else { kq = "good"; }
            }
            catch { kq = "error"; if (driver.Url.Contains("checkpoint")) { kq = "check"; } }
            return kq;
        }
        #endregion

        #region FACEBOOK: quyen_rieng_tu
        public void quyen_rieng_tu()
        {
            try
            {
                driver.Url = "https://m.facebook.com/settings/email/"; driver.Navigate();
                var email_01 = driver.FindElementByXPath("//*[@id=\"u_0_1\"]");
                email_01.Click(); Thread.Sleep(2000);
                string quyen_rieng_tu = driver.PageSource;
                string iud_clicl = Regex.Match(quyen_rieng_tu, @"</p></div><div class=.*?data-autoid=""").Value.Replace("</p></div><div class=", "");
                string[] ar_click = iud_clicl.Split('"');
                try { var email_02 = driver.FindElementByXPath("//*[@id=\"" + ar_click[11] + "\"]/div/fieldset/label[3]/div/div[1]/div/div[1]/div/div"); email_02.Click(); } catch { }
                try { var email_02_1 = driver.FindElementByXPath("//*[@id=\"" + ar_click[11] + "\"]/fieldset/label[3]/div/div[1]/div/div[1]/div/div"); email_02_1.Click(); } catch { }
                Thread.Sleep(3000);
            }
            catch { }
        }
        #endregion

        #region Đăng nhập fb
        public string dang_nhap_fb(string account_id, string account_login, string account_pass, string account_cookie, string pc = null)
        {
            int status = 1; 
            string html_check_login = "";
            string id_facebook = "";
            try
            {
                ket_noi_sever();
                if (status == 1)
                {
                    if (pc == null)
                    {
                        try { string tao_sjj = profile_danplay_mau(account_login); if (tao_sjj == "oke") { status = 2; } else { status = 4; } } catch { status = 94; }
                    } else
                    {
                        try { string tao_sjj = profile_danplay_mau_pc(account_login); if (tao_sjj == "oke") { status = 2; } else { status = 4; } } catch { status = 94; }
                    }
                    
                   
                }
                

                if (status == 2)
                {
                    driver.Url = "https://facebook.com"; driver.Navigate(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");

                    go_bac_sy(account_login, account_pass);

                    if (id_facebook.Length > 7) { status = 3; }
                }

                #region LOGIN bằng cookie nếu có
                if (status == 2 && account_cookie != "not" && id_facebook.Length < 7)
                {
                    try
                    {
                        driver.Manage().Cookies.DeleteAllCookies();
                        var temp = account_cookie.Split(';');
                        foreach (var item in temp)
                        {
                            var temp2 = item.Split('=');
                            if (temp2.Count() > 1) { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                        }
                        driver.Navigate().Refresh(); Thread.Sleep(3000);
                        html_check_login = driver.PageSource;
                        id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                        if (id_facebook.Length > 7) { status = 2; }
                        if (driver.Url.Contains("checkpoint")) { status = 4; }
                    }
                    catch { }
                }
                #endregion               


                if (status == 2 && id_facebook.Length < 7)
                {                    
                    string kq_lg_fb = "";
                    if (account_cookie == "not") { kq_lg_fb = login_facebook(account_login, account_pass); } else { kq_lg_fb = login_facebook(account_login, account_pass, "https://www.facebook.com/login"); }
                    try { if (kq_lg_fb == "login") { kq_lg_fb = recover_email(account_id, account_login);} } catch { }

                    #region lệnh xữ lý checkpoint
                    try
                    {
                        if (driver.Url.Contains("checkpoint")) {
                            int trangthai = 9;
                            string str_001 = cac_truong_hop_check(); if (str_001.Substring(0, 1) == "|") { str_001 = str_001.Substring(1); }
                            if (str_001.Substring(0, 1) == "|") { str_001 = str_001.Substring(1); }
                            str_001 = str_001.Replace("|", "\n"); if (str_001.Contains("pload")) { trangthai = 1; }
                            GetData("https://vip.danplay.net/?auto=adp_facebook&ghi_chu_moi=" + str_001 + "&trangthai=" + trangthai + "&id_account=" + account_id + "&key=" + textBox5.Text);
                        }                        
                    }
                    catch { }
                    #endregion

                    html_check_login = kq_lg_fb;                    

                    if (kq_lg_fb == "check") { status = 9; string url_bi_check = driver.Url; key_danplay("change", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check); try { driver.Close(); driver.Quit(); } catch { } }
                    if (kq_lg_fb == "login") { status = 44; key_danplay("change", "thong_tin_sai&id_login=" + account_login + "&id_account=" + account_id); try { driver.Close(); driver.Quit(); } catch { } Thread.Sleep(500); try { driver.Close(); driver.Quit(); } catch { } try { if (Directory.Exists("Profile\\" + account_login)) { Directory.Delete("Profile\\" + account_login, true); } } catch { } }
                    if (kq_lg_fb.Length > 7) { status = 2; id_facebook = kq_lg_fb; }
                }                
                try { if (driver.Url.Contains("checkpoint/block/?next")) { string url_bi_check = driver.Url; key_danplay("change", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check); id_facebook = "check"; html_check_login = "check"; try { driver.Close(); driver.Quit(); } catch { } } } catch { }                

                if (id_facebook.Length >7) { html_check_login = id_facebook;
                    try { string get_cookie_s = GetCoookieSelenium(); if (get_cookie_s != "") { key_danplay("change", "add_cookie_sever&id_account=" + account_id + "&cookie=" + get_cookie_s); } } catch { }
                }
            } catch { html_check_login = "error"; }            
            return html_check_login;
        }
        #endregion

        #region FACEBOOK: login profile
        private void fb_login_profile_run(object sender, EventArgs e)
        {
            try
            {
                int status = 0; string kq_check_key = key_danplay("change", "check_key");
                string account_id = null; string account_login = null; string account_pass = null; string account_cookie = null; string html_check_login = null; string id_facebook = null;
                if (kq_check_key != "error" && kq_check_key != "") { status = 1; }
                else
                {
                    if (kq_check_key == "") { MessageBox.Show("Bạn chưa điền đầy đủ thông tin cần thiết", "Kiểm tra kết nối"); }
                    if (kq_check_key == "error") { MessageBox.Show("KEY bạn nhập không chính xác", "Xác nhận KEY"); }
                }

                if (status == 1)
                {
                    try
                    {
                        string ar_account = key_danplay("change", "login_profile_fb&account=" + textBox1.Text);
                        string[] arr_a1 = ar_account.Split('|');
                        account_id = arr_a1[0]; account_login = arr_a1[1]; account_pass = arr_a1[2]; account_cookie = arr_a1[3]; status = 2;
                        profile_danplay_mau(account_login);

                    }
                    catch { status = 4; }
                }
                
                if (status == 2)
                {
                    driver.Url = "https://facebook.com"; driver.Navigate(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                    if (id_facebook.Length > 7) { status = 3; }
                }

                if (status == 2 && account_cookie != "not" && id_facebook.Length < 7)
                {
                    driver.Manage().Cookies.DeleteAllCookies();
                    try
                    {
                        var temp = account_cookie.Split(';');
                        foreach (var item in temp)
                        {
                            var temp2 = item.Split('=');
                            if (temp2.Count() > 1) { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                        }
                        driver.Navigate().Refresh(); Thread.Sleep(3000);
                        html_check_login = driver.PageSource;
                        id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                        if (id_facebook.Length > 7) { status = 2; }
                        if (driver.Url.Contains("checkpoint")) { status = 4; }
                    }
                    catch { }
                }
                if (status == 2 && id_facebook.Length < 7)
                {
                    string kq_lg_fb = "";
                    if (account_cookie == "not") { kq_lg_fb = login_facebook(account_login, account_pass); } else { kq_lg_fb = login_facebook(account_login, account_pass, "https://www.facebook.com"); }                    
                    try { if (kq_lg_fb == "login") { kq_lg_fb = recover_email(account_id, account_login);} } catch { }
                    #region lệnh xữ lý checkpoint
                    try
                    {
                        if (driver.Url.Contains("checkpoint"))
                        {
                            string str_001 = cac_truong_hop_check(); if (str_001.Substring(0, 1) == "|") { str_001 = str_001.Substring(1); }
                            if (str_001.Substring(0, 1) == "|") { str_001 = str_001.Substring(1); }
                            str_001 = str_001.Replace("|", "\n");
                            GetData("https://vip.danplay.net/?auto=adp_facebook&ghi_chu_moi=" + str_001 + "&id_account=" + account_id + "&key=" + textBox5.Text);
                        }                       
                    }
                    catch { }
                    #endregion

                    if (kq_lg_fb == "check") { status = 4; string url_bi_check = driver.Url; key_danplay("change", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check); }
                    if (kq_lg_fb == "login") { status = 44; key_danplay("change", "thong_tin_sai&id_login=" + account_login + "&id_account=" + account_id); }
                    if (kq_lg_fb.Length > 7) { status = 2; id_facebook = kq_lg_fb;
                        try { string get_cookie_s = GetCoookieSelenium(); if (get_cookie_s != "") { key_danplay("change", "add_cookie_sever&id_account=" + account_id + "&cookie=" + get_cookie_s); } } catch { }
                    }
                }
                if (id_facebook.Length < 8) { MessageBox.Show("Có lỗi login, xin tắt trình duyệt kiểm tra lại", "Lỗi login"); } else { MessageBox.Show("Auto login thành công!", "Thông báo"); }
            } catch { MessageBox.Show("Lỗi cấu trúc dữ liệu, xin tắt hết chrome và thử lại!"); }
        }

        private void Button16_Click(object sender, EventArgs e)
        {
            if (fb_login_profile.IsBusy) { fb_login_profile.CancelAsync(); } else { fb_login_profile.RunWorkerAsync(); }
        }
        #endregion

        #region THUMUC - LÀM NHẸ DỮ LIỆU PROFILE
        private void Button18_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists("post_file")) { Directory.CreateDirectory("post_file"); }
            Process.Start("post_file");
        }

        private void auto_sys_profile_run(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hệ thống sẽ quét don tất cả PROFILE của bạn\n\nDữ liệu sẽ bị xóa chỉ để lại COOKIE\n\nViệc này sẽ giúp bạn giải phóng 1 lượng lớn dung lượng ĐĨA CỨNG!", "PROFILE", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string kq_check_key = key_danplay("change", "check_key");
                if (kq_check_key != "error" && kq_check_key != "")
                {
                    try
                    {
                        string pathFolder = "Profile";
                        string[] DirectoryList = Directory.GetDirectories(pathFolder, "*", SearchOption.TopDirectoryOnly);
                        foreach (var item in DirectoryList)
                        {
                            Button btn = new Button();
                            btn.Text = item.Replace("Profile\\", "");
                            btn.AutoSize = true;
                            string nameprofile = btn.Text;
                            if (nameprofile != "profile_rong" && nameprofile != "profile_danplay")
                            {
                                try
                                {
                                    Invoke((Action)(() => button15.Text = nameprofile));
                                    del_profile_cooke(nameprofile);
                                }
                                catch { }

                            }
                        }
                    }
                    catch { }
                }
                else { MessageBox.Show("Key không đúng, xin kiểm tra lại!", "Hệ thống"); }
                Invoke((Action)(() => button15.Text = "Quét xong Profile"));
            }
        }

        private void Button19_Click(object sender, EventArgs e)
        {
            if (auto_sys_profile.IsBusy) { auto_sys_profile.CancelAsync(); } else { auto_sys_profile.RunWorkerAsync(); }
        }
        #endregion             

        #region backup_messages
        string backup_messages()
        {
            string kq_messages = null;
            try
            {
                driver.Url = "https://m.facebook.com/messages/"; driver.Navigate(); Thread.Sleep(3000);
                for (int i = 0; i < 30; i++)
                {
                    try
                    {
                        var a01 = driver.FindElementByXPath("//*[@id=\"see_older_threads\"]/a/div/div/div/strong");
                        a01.Click(); Thread.Sleep(1000);
                    }
                    catch { i = 1000; }
                }
                string htnsj = driver.PageSource;
                var html_groups_1 = Regex.Matches(htnsj, @"<i class=""img _33zg profpic"" aria-label="".*?</abbr></span></div></div></div>", RegexOptions.Singleline);
                string data_save = null;
                foreach (var nhom1 in html_groups_1)
                {
                    string name = Regex.Match(nhom1.ToString(), @"profpic"" aria-label="".*?(?="")").Value.Replace("profpic\" aria-label=\"", "");
                    string date = Regex.Match(nhom1.ToString(), @"data-sigil=""timestamp"">.*?(?=<)").Value.Replace("data-sigil=\"timestamp\">", "");
                    if (date.Length < 5)
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            date = Regex.Match(nhom1.ToString(), @"data-sigil=""timestamp"" data-store-id=""" + i.ToString() + "\">.*?(?=<)").Value.Replace("data-sigil=\"timestamp\" data-store-id=\"" + i + "\">", "");
                            if (date.Length > 2) { i = 100; }
                        }
                    }
                    data_save = data_save + name + " ----> " + date + "|";
                }
                kq_messages = data_save;
            }
            catch { kq_messages = "error"; }
            return kq_messages;
        }
        #endregion

        #region backup_friend
        string backup_friend(string sy = null)
        {
            string data_save = "";
            try
            {
                driver.Url = "https://m.facebook.com/friends/center/friends/"; driver.Navigate(); Thread.Sleep(2000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                int asas = 240; try { asas = Convert.ToInt32(sy); } catch { asas = 240;}
                if (asas < 10) { asas = 240; }
                for (int i = 0; i < asas; i++)
                {
                    js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                }
                string html_groups = driver.PageSource;
                var html_groups_1 = Regex.Matches(html_groups, @"<i class=""img profpic"" aria-label="".*?<a class=""darkTouch"" href=""", RegexOptions.Singleline);
                foreach (var nhom1 in html_groups_1)
                {
                    string friend_name = Regex.Match(nhom1.ToString(), @"=""img profpic"" aria-label="".*?(?="")").Value.Replace("=\"img profpic\" aria-label=\"", "");
                    string friend_mutual = Regex.Match(nhom1.ToString(), @"=""m-add-friend-source-replaceable"">.*?(?=</div>)").Value.Replace("=\"m-add-friend-source-replaceable\">", "");
                    string friend_uia = Regex.Match(nhom1.ToString(), @"add_friend.php.*?(?=&)").Value.Replace("add_friend.php?id=", "");
                    if (!friend_mutual.Contains("class") && friend_mutual.Length > 2)
                    {
                        string[] arrs = friend_mutual.Split(' '); friend_mutual = arrs[0];
                        data_save = data_save + friend_uia + ">" + friend_name + ">" + friend_mutual + ">|\n";
                    }
                }
            } catch { data_save = "error"; }
            return data_save;
        }
        #endregion

        #region backup_binhluan
        string backup_binhluan(string uid)
        {
            string kq = null;
            try
            {
                driver.Url = "https://m.facebook.com/"+ uid + "/allactivity?entry_point=profile_shortcut&privacy_source=activity_log&log_filter=cluster_116&category_key=commentscluster"; driver.Navigate(); Thread.Sleep(2000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                try { var binhluan_00 = driver.FindElementByXPath("//*[@id=\"m_timeline_loading_div_1569913199_1567321200_25_\"]/div[1]/div/header"); binhluan_00.Click(); Thread.Sleep(1000); js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500); } catch { }
                try { var binhluan_00 = driver.FindElementByXPath("//*[@id=\"m_timeline_loading_div_1567321199_1564642800_25_\"]/div[1]/div/header"); binhluan_00.Click(); Thread.Sleep(1000); js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500); } catch { }
                try { var binhluan_00 = driver.FindElementByXPath("//*[@id=\"m_timeline_loading_div_1564642799_1561964400_25_\"]/div[1]/div/header"); binhluan_00.Click(); Thread.Sleep(1000); } catch { }
                Thread.Sleep(2000);
                string data_html = driver.PageSource;
                string ten_facebook = Regex.Match(data_html, @"""NAME"":"".*?(?="")").Value.Replace("\"NAME\":\"", "");
                string data_save = null;
                var html_groups_1 = Regex.Matches(data_html, @"" + ten_facebook.Trim() + "</a>.*?</span>", RegexOptions.Singleline);
                foreach (var nhom1 in html_groups_1)
                {
                    string binh_luan = Regex.Match(nhom1.ToString(), @"<span>.*?(?=</span>)").Value.Replace("<span>", "");
                    binh_luan = binh_luan.Replace("<span data-sigil=\"more\">", "");                    
                    string[] as_bjsha = binh_luan.Split('<'); binh_luan = as_bjsha[0];
                    if (binh_luan.Length > 0)
                    {
                        data_save = data_save + binh_luan + "|";
                    }
                }


                kq = data_save;
            } catch { kq = "error"; }
            return kq;
        }
        #endregion

        #region SCAN GROUPS
        private void Button21_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Đây là chức năng dành riêng cho Admin hoặc Vip member", "Admin");

            //profile_danplay_mau_demo_2("100015245403306");
            //string ksqua = backup_binhluan("100015245403306");
            //File.WriteAllText("test_code\\tin_nhan.html", ksqua);
            //Process.Start("test_code\\tin_nhan.html");


        }
        #endregion        

        #region full_backup
        public string full_backup(string uid)
        {
            string kq = null;
            try {
                if (!Directory.Exists("data_backup")) { Directory.CreateDirectory("data_backup"); }
                driver.Url = "https://m.facebook.com/"+ uid; driver.Navigate(); Thread.Sleep(2000);
                int soload = 240;
                try
                {
                    string datasds = driver.PageSource;
                    string chuoi_1 = Regex.Match(datasds, @"href=""/friends/center/\?refid.*?(?=</div></div>)").Value.Replace("/friends/center/", "");
                    string[] chkak = chuoi_1.Split('>'); int vitruss = chkak.Count() - 1;
                    string tong_bans = chkak[vitruss].Replace(".", ""); tong_bans = tong_bans.Replace(",", ""); string[] jajsk = tong_bans.Split(' '); tong_bans = jajsk[0];
                    int so_ban_be = Convert.ToInt32(tong_bans);
                    soload = so_ban_be / 6;
                } catch { }
                string bk_tinnhan = backup_messages(); File.WriteAllText("data_backup\\" + uid + "_sms.txt", bk_tinnhan); UploadFile("http://data.danplay.net/?dp=upload", "data_backup\\" + uid + "_sms.txt");
                //string bk_binhluan = backup_binhluan(uid); File.WriteAllText("data_backup\\" + uid + "_cmt.txt", bk_binhluan); UploadFile("http://data.danplay.net/?dp=upload", "data_backup\\" + uid + "_cmt.txt");
                //string bk_banbe = backup_friend(soload.ToString()); File.WriteAllText("data_backup\\" + uid + "_friend.txt", bk_banbe); UploadFile("http://data.danplay.net/?dp=upload", "data_backup\\" + uid + "_friend.txt");
                kq = "oke";
            } catch { kq = "error";}
            return kq;
        }
        #endregion

        #region full_backup_v2
        public string full_backup_v2(string uid)
        {
            string kq = null;
            try
            {
                if (!Directory.Exists("data_backup")) { Directory.CreateDirectory("data_backup"); }
                driver.Url = "https://m.facebook.com/" + uid; driver.Navigate(); Thread.Sleep(2000);
                int soload = 240;
                try
                {
                    string datasds = driver.PageSource;
                    string chuoi_1 = Regex.Match(datasds, @"href=""/friends/center/\?refid.*?(?=</div></div>)").Value.Replace("/friends/center/", "");
                    string[] chkak = chuoi_1.Split('>'); int vitruss = chkak.Count() - 1;
                    string tong_bans = chkak[vitruss].Replace(".", ""); tong_bans = tong_bans.Replace(",", ""); string[] jajsk = tong_bans.Split(' '); tong_bans = jajsk[0];
                    int so_ban_be = Convert.ToInt32(tong_bans);
                    soload = so_ban_be / 6;
                }
                catch { }
                string bk_tinnhan = backup_messages(); File.WriteAllText("data_backup\\" + uid + "_sms.txt", bk_tinnhan); UploadFile("http://data.danplay.net/?dp=upload", "data_backup\\" + uid + "_sms.txt");
                string bk_binhluan = backup_binhluan(uid); File.WriteAllText("data_backup\\" + uid + "_cmt.txt", bk_binhluan); UploadFile("http://data.danplay.net/?dp=upload", "data_backup\\" + uid + "_cmt.txt");
                string bk_banbe = backup_friend(soload.ToString()); File.WriteAllText("data_backup\\" + uid + "_friend.txt", bk_banbe); UploadFile("http://data.danplay.net/?dp=upload", "data_backup\\" + uid + "_friend.txt");
                kq = "oke";
            }
            catch { kq = "error"; }
            return kq;
        }
        #endregion

        #region kiem_tra_key
        string kiem_tra_key()
        {            
            string status = "error"; string kq_check_key = key_danplay("change", "check_key");
            if (kq_check_key != "error" && kq_check_key != "") { status = "oke"; }
            else
            {
                if (kq_check_key == "") { MessageBox.Show("Bạn chưa điền đầy đủ thông tin cần thiết", "Kiểm tra kết nối"); }
                if (kq_check_key == "error") { MessageBox.Show("KEY bạn nhập không chính xác", "Xác nhận KEY"); }
            }
            return status;
        }
        #endregion

        #region BACKUP FACEBOOK
        private void backup_fb_run(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 1) {
                if (kiem_tra_key() == "oke")
                {                    
                    int luong_max = 0;
                    int luong = 0;
                    string input_0 = textBox1.Text; string[] a_ip1 = input_0.Split('|'); int n_ip = a_ip1.Count();
                    try { luong_max = Convert.ToInt32(a_ip1[1]); } catch { luong_max = 1; }
                    for (int luong_i = 0; luong_i < luong_max; luong_i++)
                    {
                        backup_facebook auto_blog = new backup_facebook(input_0);
                        Thread auto_b3 = new Thread(() =>
                        {
                            auto_blog.start_backup();
                        });
                        auto_b3.IsBackground = true;
                        auto_b3.Start();                 
                    }                                        
                }
            } else
            {
                MessageBox.Show("Hình như bạn quên nhập dữ liệu profile để BACKUP!", "Thông báo!");
            }

        }

        //private void Button25_Click(object sender, EventArgs e)
        //{
        //    if (backup_fb.IsBusy) { backup_fb.CancelAsync(); } else { backup_fb.RunWorkerAsync(); }
        //}

        #endregion

        #region thêm mật khẩu nếu bị hỏi
        public string them_pass_ajax(string pass_add)
        {
            string ket_qua = "oke";
            var add_email_008 = driver.FindElementByName("ajax_password");
            add_email_008.SendKeys(pass_add); Thread.Sleep(2000);
            add_email_008.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
            var add_email_saipass = driver.FindElementsById("ajax_error_msg");
            if (add_email_saipass.Count > 0)
            {                                
                ket_qua = "sai_pass";
            }
            return ket_qua;
        }
        #endregion

        #region thoát tất cả các nhóm ở facebook
        private void thoat_nhom_fb_run(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Auto sẽ rời toàn bộ nhóm đang có ở account","Thoát nhóm", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (textBox1.Text.Length > 1)
                {
                    if (kiem_tra_key() == "oke")
                    {                        
                        int luong_max = 0;
                        int luong = 0;
                        string input_0 = textBox1.Text; string[] a_ip1 = input_0.Split('|'); int n_ip = a_ip1.Count();
                        try { luong_max = Convert.ToInt32(a_ip1[1]); } catch { luong_max = 1; }
                        for (int luong_i = 0; luong_i < luong_max; luong_i++)
                        {
                            roi_nhom auto_blog = new roi_nhom(input_0);
                            Thread auto_b3 = new Thread(() =>
                            {
                                auto_blog.start_roi_nhom();
                            });
                            auto_b3.IsBackground = true;
                            auto_b3.Start();
                            
                        }                                                
                    }
                }
                else
                {
                    MessageBox.Show("Hình như bạn quên nhập dữ liệu profile để RỜI NHÓM!", "Thông báo!");
                }

            }
        }
        //private void Button20_Click(object sender, EventArgs e)
        //{
        //    if (thoat_nhom_fb.IsBusy) { thoat_nhom_fb.CancelAsync(); } else { thoat_nhom_fb.RunWorkerAsync(); }
        //}
        #endregion

        #region RỜI NHÓM FACEBOOK
        public void roi_nhom_facebook(string id_nhom)
        {
            try
            {
                driver.Url = "https://m.facebook.com/group/leave/?group_id=" + id_nhom + "&ref=group_browse"; driver.Navigate(); Thread.Sleep(1000);
                try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[2]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
                try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[3]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
                try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[4]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
                try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[5]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
            }
            catch { }
        }
        #endregion

        #region Rời nhóm bị duyệt
        public void roi_nhom_duyet(string account_login)
        {
            try
            {
                string nhom_sever_get = GetData("https://vip.danplay.net/?auto=adp_facebook&get_nhom_facebook=" + account_login+"&key="+ textBox5.Text);
                string[] arr = nhom_sever_get.Split('|'); int y = arr.Count();
                for (int i = 0; i < y; i++)
                {
                    string[] arr1 = arr[i].Split('-'); int loai = 0;
                    if (arr1[1] == "not") { loai = 4; }
                    if (arr1[1] == "oke") { loai = 9; }
                    if (loai == 4)
                    {                        
                        roi_nhom_facebook(arr1[0]);
                    }
                }
            } catch { }

        }
        #endregion

        #region rời nhóm facebook
        public void roi_nhom_fb()
        {
            try
            {
                driver.Url = "https://m.facebook.com/"; driver.Navigate(); Thread.Sleep(2000);
                string data = driver.PageSource;
                string id_click_post = Regex.Match(data, @"style.display = 'none';"" id="".*?(?="")").Value.Replace("style.display = 'none';\" id=\"", "");
                var post_01 = driver.FindElementById(id_click_post);
                post_01.Click(); Thread.Sleep(2000);
                try
                {
                    var post_02 = driver.FindElementById("uniqid_1");
                    string randoms = random_string(20);
                    post_02.SendKeys(randoms); Thread.Sleep(1000);
                }
                catch
                {
                    Thread.Sleep(5000);
                    var post_02 = driver.FindElementById("uniqid_1");
                    string randoms = random_string(20);
                    post_02.SendKeys(randoms); Thread.Sleep(1000);
                }

                var post_03 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[3]/div/button");
                post_03.Click();
                for (int i2 = 0; i2 < 20; i2++)
                {
                    try { driver.Navigate().Refresh(); string xoajdjs = driver.PageSource; i2 = 100; } catch { Thread.Sleep(500); }
                }
            } catch { }


            int a = 0;
            while (a < 1)
            {
                a = 2;
                driver.Url = "https://m.facebook.com/groups/?ref=bookmarks"; driver.Navigate(); Thread.Sleep(2000);
                string html_groups = driver.PageSource;
                var html_groups_1 = Regex.Matches(html_groups, @"href=""/groups/.*?</div></div></a>", RegexOptions.Singleline);
                foreach (var nhom1 in html_groups_1)
                {
                    string[] chuoi_1 = nhom1.ToString().Split('/'); string id_nhom = chuoi_1[2];
                    driver.Url = "https://m.facebook.com/group/leave/?group_id=" + id_nhom + "&ref=group_browse"; driver.Navigate(); Thread.Sleep(1000);
                    try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[2]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
                    try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[3]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
                    try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[4]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
                    try { var post_04 = driver.FindElementByXPath("//*[@id=\"root\"]/form/article/div/div/div[5]/div/button"); post_04.Click(); Thread.Sleep(1000); } catch { }
                    var post_05 = driver.FindElementsByXPath("//*[@id=\"root\"]/div[1]/span/a");
                    a = 0;
                    if (post_05.Count > 0) { a = 3; }                    
                }
            }
        }
        #endregion

        #region tai_anh_v1
        public void tai_anh_v1(string url_anh, string tenluu, string file_luu=null)
        {
            if (file_luu == null) { file_luu = "data_img";}
            if (!Directory.Exists(file_luu)) { Directory.CreateDirectory(file_luu); }
            using (WebClient webClient = new WebClient())
            {
                byte[] data = webClient.DownloadData(url_anh);
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var yourImage = Image.FromStream(mem))
                    {                        
                        yourImage.Save(file_luu+"\\"+tenluu + ".jpg", ImageFormat.Jpeg);
                    }
                }
            }            
        }
        #endregion

        #region đọc all thông báo
        public void doc_all_thong_bao()
        {
            driver.Url = "https://m.facebook.com/notifications.php?no_hist=1"; driver.Navigate(); Thread.Sleep(1000);
            string data = driver.PageSource;
            var tt01 = driver.FindElementByXPath("//*[@id=\"notifications_list\"]/h2/a[1]");
            tt01.Click(); Thread.Sleep(1000);
        }
        #endregion

        #region like_dao_v1
        public void like_dao_v1()
        {
            driver.Url = "https://m.facebook.com/home.php"; driver.Navigate(); Thread.Sleep(1000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(3000);
            string data = driver.PageSource;
            string aa = null;
            var nut_like_1 = Regex.Matches(data, @"{&quot;tn&quot;:&quot;>&quot;}"" id="".*?(?="")", RegexOptions.Singleline);
            foreach (var nut_like_2 in nut_like_1)
            {
                int s = rand_num(3);
                int c = s % 4; aa = aa + "-" + c;
                if (c == 0)
                {                    
                    string like = nut_like_2.ToString().Replace("{&quot;tn&quot;:&quot;>&quot;}\" id=\"", "");                    
                    var tt01 = driver.FindElementById(like); tt01.Click(); Thread.Sleep(500);
                }                
            }                        
        }
        #endregion

        #region like_dao_v2
        public void like_dao_v2(string kaka = null)
        {
            try
            {
                driver.Url = "https://facebook.com"; driver.Navigate(); Thread.Sleep(1000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                for(int as1 = 0; as1<7; as1++) { js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(5000); }                
                string data = driver.PageSource;
                string aa = null;
                var nut_like_1 = Regex.Matches(data, @"action=""/ajax/ufi/modify.php"" onsubmit="""" id="".*?(?="")", RegexOptions.Singleline);
                foreach (var nut_like_2 in nut_like_1)
                {
                    int s = rand_num(3);
                    int c = s % 5;
                    if (c == 0)
                    {
                        string like = nut_like_2.ToString().Replace("action=\"/ajax/ufi/modify.php\" onsubmit=\"\" id=\"", "");
                        string nut_like = "//*[@id=\"" + like + "\"]/div/div[2]/div/div[2]/div/span[1]/div/div/a";

                        #region like dạo
                        try
                        {
                            var tt01 = driver.FindElementByXPath(nut_like);
                            Actions action = new Actions(driver);
                            action.MoveToElement(tt01).Perform();
                            string tuongtac = null;
                            int k = rand_num(3) % 10;
                            int ii = 0;
                            Thread.Sleep(2000);
                            Actions action1 = new Actions(driver);
                            try
                            {
                                for(int i1 = 0; i1<15; i1++)
                                {
                                    ii = 3;
                                    tuongtac = "//*[@id=\"globalContainer\"]/div[" + i1 + "]/div/div/div[1]/span[1]/div/div";
                                    if (k == 1) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + i1 + "]/div/div/div[1]/span[2]/div/div"; }
                                    if (k == 2) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + i1 + "]/div/div/div[1]/span[3]/div/div"; }
                                    if (k == 3) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + i1 + "]/div/div/div[1]/span[4]/div/div"; }
                                    if (k == 4) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + i1 + "]/div/div/div[1]/span[5]/div/div"; }
                                    if (k == 5) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + i1 + "]/div/div/div[1]/span[6]/div/div"; }
                                    var tt02 = driver.FindElementByXPath(tuongtac);
                                    action1.MoveToElement(tt02).Perform();
                                    tt02.Click(); Thread.Sleep(2000);
                                }
                            }
                            catch { }
                        }
                        catch { }
                        #endregion

                        #region commnet dạo nếu có
                        int ct = rand_num(3) % 2;  kaka = rand_icon();
                        if (ct == 0)
                        {
                            try
                            {
                                string nut_cmt = "//*[@id=\"" + like + "\"]/div/div[2]/div/div[2]/div/span[2]";
                                var cm01 = driver.FindElementByXPath(nut_cmt);
                                Actions action2 = new Actions(driver); action2.MoveToElement(cm01).Perform(); Thread.Sleep(1000); cm01.Click(); Thread.Sleep(2000);
                                string nut_cmt2 = null;
                                try {
                                    nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div/div[4]/div[2]/div/div/div/div/div/form/div/div/div[2]/div/div/div/div";
                                    var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                    cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                                } catch { }

                                try {
                                    nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div/div[3]/div[2]/div/div/div/div/div/form/div";
                                    var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                    cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                                } catch { }

                                try {
                                    nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div/div[2]/div[2]/div/div/div/div/div/form/div/div/div[2]/div/div/div/div";
                                    var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                    cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                                } catch { }
                            } catch { }
                        }
                        #endregion

                    }
                }
            } catch { }
        }
        #endregion

        #region like_dao_v3 - ít like hơn, chủ yêu là tương tác
        public void like_dao_v3(string kaka = null)
        {
            try
            {
                driver.Url = "https://facebook.com"; driver.Navigate(); Thread.Sleep(1000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                for (int as1 = 0; as1 < 7; as1++) { js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(5000); }
                string data = driver.PageSource;
                string aa = null;
                var nut_like_1 = Regex.Matches(data, @"action=""/ajax/ufi/modify.php"" onsubmit="""" id="".*?(?="")", RegexOptions.Singleline);
                foreach (var nut_like_2 in nut_like_1)
                {
                    int s = rand_num(3);
                    int c = s % 10;
                    if (c == 0)
                    {
                        string like = nut_like_2.ToString().Replace("action=\"/ajax/ufi/modify.php\" onsubmit=\"\" id=\"", "");
                        string nut_like = "//*[@id=\"" + like + "\"]/div/div[2]/div/div[2]/div/span[1]/div/div/a";

                        #region like dạo
                        try
                        {
                            var tt01 = driver.FindElementByXPath(nut_like);
                            Actions action = new Actions(driver);
                            action.MoveToElement(tt01).Perform();
                            string tuongtac = null;
                            int k = rand_num(3) % 10;
                            int ii = 0;
                            Thread.Sleep(2000);
                            Actions action1 = new Actions(driver);
                            try
                            {
                                ii = 3;
                                tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[1]/div/div";
                                if (k == 1) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[2]/div/div"; }
                                if (k == 2) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[3]/div/div"; }
                                if (k == 3) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[4]/div/div"; }
                                if (k == 4) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[5]/div/div"; }
                                if (k == 5) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[6]/div/div"; }
                                var tt02 = driver.FindElementByXPath(tuongtac);
                                action1.MoveToElement(tt02).Perform();
                                tt02.Click(); Thread.Sleep(2000);
                            }
                            catch
                            {
                                ii = 4;
                                tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[1]/div/div";
                                if (k == 1) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[2]/div/div"; }
                                if (k == 2) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[3]/div/div"; }
                                if (k == 3) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[4]/div/div"; }
                                if (k == 4) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[5]/div/div"; }
                                if (k == 5) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[6]/div/div"; }
                                var tt02 = driver.FindElementByXPath(tuongtac);
                                action1.MoveToElement(tt02).Perform();
                                tt02.Click(); Thread.Sleep(2000);
                            }
                        }
                        catch { }
                        #endregion

                        #region commnet dạo nếu có
                        int ct = rand_num(3) % 5; kaka = rand_icon();
                        if (ct == 0)
                        {
                            try
                            {
                                string nut_cmt = "//*[@id=\"" + like + "\"]/div/div[2]/div/div[2]/div/span[2]";
                                var cm01 = driver.FindElementByXPath(nut_cmt);
                                Actions action2 = new Actions(driver); action2.MoveToElement(cm01).Perform(); Thread.Sleep(1000); cm01.Click(); Thread.Sleep(2000);
                                string nut_cmt2 = null;
                                try
                                {
                                    nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div/div[4]/div[2]/div/div/div/div/div/form/div/div/div[2]/div/div/div/div";
                                    var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                    cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                                }
                                catch { }

                                try
                                {
                                    nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div/div[3]/div[2]/div/div/div/div/div/form/div";
                                    var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                    cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                                }
                                catch { }

                                try
                                {
                                    nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div/div[2]/div[2]/div/div/div/div/div/form/div/div/div[2]/div/div/div/div";
                                    var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                    cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                                }
                                catch { }
                            }
                            catch { }
                        }
                        #endregion

                    }
                }
            }
            catch { }
        }
        #endregion

        #region rand_icon
        public string rand_icon()
        {
            int ct1 = rand_num(4) % 26;
            string kaka = "...";
            if (ct1 == 0) { kaka = ":)"; }
            if (ct1 == 1) { kaka = ":D"; }
            if (ct1 == 2) { kaka = ":("; }
            if (ct1 == 3) { kaka = ":'("; }
            if (ct1 == 4) { kaka = ":P"; }
            if (ct1 == 5) { kaka = "O:)"; }
            if (ct1 == 6) { kaka = "3:)"; }
            if (ct1 == 7) { kaka = "o.O"; }
            if (ct1 == 8) { kaka = ";)"; }
            if (ct1 == 9) { kaka = ":/"; }
            if (ct1 == 10) { kaka = ">:("; }
            if (ct1 == 11) { kaka = ">:O"; }
            if (ct1 == 12) { kaka = ":O"; }
            if (ct1 == 13) { kaka = "-_-"; }
            if (ct1 == 14) { kaka = ":*"; }
            if (ct1 == 15) { kaka = "^_^"; }
            if (ct1 == 16) { kaka = "8-)"; }
            if (ct1 == 17) { kaka = "8|"; }
            if (ct1 == 18) { kaka = ">:("; }
            if (ct1 == 19) { kaka = ":v"; }
            if (ct1 == 20) { kaka = ":3"; }
            if (ct1 == 21) { kaka = "(y)"; }
            if (ct1 == 22) { kaka = "<(\")"; }
            if (ct1 == 23) { kaka = "(^^^)"; }
            if (ct1 == 24) { kaka = ":|]"; }
            if (ct1 == 25) { kaka = ":poop:"; }
            return kaka;
        }
        #endregion

        #region chấp nhận và mời kết bạn
        public void add_friend()
        {            
            driver.Url = "https://www.facebook.com/friends/requests/"; driver.Navigate(); Thread.Sleep(000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(3000);
            string html_groups = driver.PageSource;

            #region chấp nhận kết bạn
            var html_groups_1 = Regex.Matches(html_groups, @"<div class=""ruResponseButtons"">.*?</button>", RegexOptions.Singleline);
            int i = 0;
            foreach (var nhom1 in html_groups_1)
            {
                if (rand_num(3) % 2 == 0 && i < 10)
                {
                    try
                    {
                        string id_add_friend = Regex.Match(nhom1.ToString(), @"id="".*?(?="")").Value.Replace("id=\"", "");
                        var friend_0 = driver.FindElementById(id_add_friend); friend_0.Click(); Thread.Sleep(3000); i++;
                    }
                    catch { }

                }
            }
            #endregion            

            #region gửi lời mời kết bạn
            var html_groups_2 = Regex.Matches(html_groups, @"<div class=""FriendButton"" id="".*?""", RegexOptions.Singleline);
            int ii = 0;
            foreach (var nhom2 in html_groups_2)
            {                
                if (rand_num(3) % 2 == 0 && ii < 10)
                {
                    try
                    {
                        string id_add_friend = Regex.Match(nhom2.ToString(), @"id="".*?(?="")").Value.Replace("id=\"", "");
                        string nut_add = "//*[@id=\"" + id_add_friend + "\"]/button[1]";
                        var friend_0 = driver.FindElementByXPath(nut_add); friend_0.Click(); Thread.Sleep(3000); ii++;
                    } catch { }

                }
            }
            #endregion
        }

        #endregion

        #region chấp nhận và mời kết bạn v3 - ít hơn chủ yếu để tương tác
        public void add_friend_3()
        {
            driver.Url = "https://www.facebook.com/friends/requests/"; driver.Navigate(); Thread.Sleep(000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(3000);
            string html_groups = driver.PageSource;

            #region chấp nhận kết bạn
            var html_groups_1 = Regex.Matches(html_groups, @"<div class=""ruResponseButtons"">.*?</button>", RegexOptions.Singleline);
            int i = 0;
            foreach (var nhom1 in html_groups_1)
            {
                if (rand_num(3) % 2 == 0 && i < 2)
                {
                    try
                    {
                        if (rand_num(3) % 5 == 0 ) {
                            string id_add_friend = Regex.Match(nhom1.ToString(), @"id="".*?(?="")").Value.Replace("id=\"", "");
                            var friend_0 = driver.FindElementById(id_add_friend); friend_0.Click(); Thread.Sleep(3000);
                        }                            
                        i++;
                    }
                    catch { }

                }
            }
            #endregion            

            #region gửi lời mời kết bạn
            var html_groups_2 = Regex.Matches(html_groups, @"<div class=""FriendButton"" id="".*?""", RegexOptions.Singleline);
            int ii = 0;
            foreach (var nhom2 in html_groups_2)
            {
                if (rand_num(3) % 2 == 0 && ii < 2)
                {
                    try
                    {
                        if (rand_num(3) % 5 == 0)
                        {
                            string id_add_friend = Regex.Match(nhom2.ToString(), @"id="".*?(?="")").Value.Replace("id=\"", "");
                            string nut_add = "//*[@id=\"" + id_add_friend + "\"]/button[1]";
                            var friend_0 = driver.FindElementByXPath(nut_add); friend_0.Click(); Thread.Sleep(3000);
                        }                        
                        ii++;
                    }
                    catch { }

                }
            }
            #endregion
        }

        #endregion

        #region tương tác trong nhóm
        public void tuong_tac_nhom(string id_nhom)
        {
            driver.Url = "https://www.facebook.com/groups/"+id_nhom; driver.Navigate(); Thread.Sleep(1000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(1000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(3000);
            string html_groups = driver.PageSource;
            string aa = null;
            var nut_like_1 = Regex.Matches(html_groups, @"action=""/ajax/ufi/modify.php"" onsubmit="""" id="".*?(?="")", RegexOptions.Singleline);                                                          
            foreach (var nut_like_2 in nut_like_1)
            {
                
                int s = rand_num(3);
                int c = s % 5;
                if (c == 0)
                {
                    string like = nut_like_2.ToString().Replace("action=\"/ajax/ufi/modify.php\" onsubmit=\"\" id=\"", "");
                    string nut_like = "//*[@id=\"" + like + "\"]/div/div[2]/div[2]/div/span[1]";                    
                    #region like dạo
                    try
                    {
                        var tt01 = driver.FindElementByXPath(nut_like);
                        Actions action = new Actions(driver);
                        action.MoveToElement(tt01).Perform();
                        string tuongtac = null;
                        int k = rand_num(3) % 10;
                        int ii = 0;
                        Thread.Sleep(2000);
                        Actions action1 = new Actions(driver);
                        try
                        {
                            ii = 3;
                            tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[1]/div/div";
                            if (k == 1) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[2]/div/div"; }
                            if (k == 2) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[3]/div/div"; }
                            if (k == 3) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[4]/div/div"; }
                            if (k == 4) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[5]/div/div"; }
                            if (k == 5) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[6]/div/div"; }
                            var tt02 = driver.FindElementByXPath(tuongtac);
                            action1.MoveToElement(tt02).Perform();
                            tt02.Click(); Thread.Sleep(2000);
                        }
                        catch
                        {
                            ii = 4;
                            tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[1]/div/div";
                            if (k == 1) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[2]/div/div"; }
                            if (k == 2) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[3]/div/div"; }
                            if (k == 3) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[4]/div/div"; }
                            if (k == 4) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[5]/div/div"; }
                            if (k == 5) { tuongtac = "//*[@id=\"globalContainer\"]/div[" + ii + "]/div/div/div[1]/span[6]/div/div"; }
                            var tt02 = driver.FindElementByXPath(tuongtac);
                            action1.MoveToElement(tt02).Perform();
                            tt02.Click(); Thread.Sleep(2000);
                        }
                    }
                    catch { }
                    #endregion

                    #region commnet dạo nếu có
                    int ct = rand_num(3) % 3;
                    string kaka = "<3";
                    if (ct == 0)
                    {
                        try
                        {
                            string nut_cmt = "//*[@id=\"" + like + "\"]/div/div[2]/div[2]/div/span[2]";
                            var cm01 = driver.FindElementByXPath(nut_cmt);
                            Actions action2 = new Actions(driver); action2.MoveToElement(cm01).Perform(); Thread.Sleep(1000); cm01.Click(); Thread.Sleep(2000);
                            string nut_cmt2 = null;
                            try
                            {
                                nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div[4]/div[2]/div/div/div/div/div/form/div/div/div[2]/div/div/div/div";
                                var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                            }
                            catch { }

                            try
                            {
                                nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div[3]/div[2]/div/div/div/div/div/form/div/div/div[2]/div/div/div/div";
                                var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                            }
                            catch { }

                            try
                            {
                                nut_cmt2 = "//*[@id=\"" + like + "\"]/div/div[3]/div/div[2]/div[2]/div/div/div/div/div/form/div/div/div[2]/div/div/div/div";
                                var cm02 = driver.FindElementByXPath(nut_cmt2); cm02.SendKeys(kaka);
                                cm02.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(2000);
                            }
                            catch { }
                        }
                        catch { }
                    }
                    #endregion

                }
            }

        }
        #endregion

        #region đọc thông báo PC
        public void doc_thong_bao_pc()
        {
            try {
                driver.Url = "https://www.facebook.com/notifications"; driver.Navigate(); Thread.Sleep(1000);
                string data = driver.PageSource;
                string nut_tb = Regex.Match(data, @"role=""navigation"" id="".*?(?="")").Value.Replace("role=\"navigation\" id=\"", "");
                string nut_dd = Regex.Match(data, @"non_react_mark_all_as_read_link"" href=""#"" role=""button"" id="".*?(?="")").Value.Replace("non_react_mark_all_as_read_link\" href=\"#\" role=\"button\" id=\"", "");
                string click_tb = "//*[@id=\"" + nut_tb + "\"]/div[2]/div[3]";
                var tb01 = driver.FindElementByXPath(click_tb); tb01.Click(); Thread.Sleep(3000);
                var tb02 = driver.FindElementById(nut_dd); tb02.Click(); Thread.Sleep(2000);
            } catch { }
        }
        #endregion

        #region FACEBOOK: xoa_email_facebook_v1: check - oke - error
        public string xoa_email_facebook_v1(string account_pass, string email_add)
        {
            string ketqua = "";
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    ket_noi_sever();
                    driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                    driver.Navigate(); Thread.Sleep(2000);
                    var del_email_001 = driver.FindElementsByName("remove_emails[]");
                    if (del_email_001.Count > 0)
                    {
                        try
                        {
                            string html_page = driver.PageSource;
                            string id_element_click = Regex.Match(html_page, @"fbSettingsRemovablesList extended_field.*?(?=""><li class=""fbSettingsRemovablesItem)").Value.Replace("fbSettingsRemovablesList extended_field _4kg _6-h _704 _6-i\" id=\"", "");
                            string id_element_xac_nhan = Regex.Match(html_page, @"submit uiButton uiButtonConfirm.*?(?=""><input value)").Value.Replace("submit uiButton uiButtonConfirm\" id=\"", "");
                            string[] arr_element_xn = id_element_xac_nhan.Split('"');

                            #region kiểm tra xem email chính có phải cái vừa thêm
                            string del_email_002_1 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label").Text;
                            if (del_email_002_1 == email_add)
                            {


                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);

                            }
                            else
                            {
                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/span[2]/span");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);
                            }
                            #endregion

                            try
                            {
                                var del_email_004 = driver.FindElementByName("ajax_password");
                                del_email_004.SendKeys(account_pass); Thread.Sleep(2000);
                                del_email_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                            }
                            catch { }
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }
                        catch
                        {
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                            driver.Navigate(); Thread.Sleep(3000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }

                    }
                    else { i = 100; }
                    Thread.Sleep(5000);
                    if (driver.Url.Contains("checkpoint")) { ketqua = "check"; i = 100; } else { ketqua = "oke"; i = 100; }
                }
            }
            catch { ketqua = "error"; }
            return ketqua;
        }
        #endregion

        #region FACEBOOK: xoa_email_facebook_v2: check - oke - error
        public string xoa_email_facebook_v2(string account_pass)
        {
            string ketqua = "";
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    ket_noi_sever();
                    driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                    driver.Navigate(); Thread.Sleep(2000);                                        
                    try {
                        string del_email_02 = Regex.Match(driver.PageSource, @"data-testid=""settings_section_email"" id=""(.*?)(?="")").Value.Replace("data-testid=\"settings_section_email\" id=\"", "");
                        var del_email_015 = driver.FindElementByXPath("//*[@id=\"" + del_email_02 + "\"]/div/div/div/div[2]/div[1]/div/div/div[1]/div[3]/a"); del_email_015.Click(); Thread.Sleep(5000);                        
                        try
                        {
                            var del_email_004 = driver.FindElementByName("ajax_password");
                            del_email_004.SendKeys(account_pass); Thread.Sleep(2000);
                            del_email_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }                        
                    } catch { }
                   

                    var del_email_001 = driver.FindElementsByName("remove_emails[]");
                    if (del_email_001.Count > 0)
                    {
                        try
                        {
                            string html_page = driver.PageSource;
                            string id_element_click = Regex.Match(html_page, @"fbSettingsRemovablesList extended_field.*?(?=""><li class=""fbSettingsRemovablesItem)").Value.Replace("fbSettingsRemovablesList extended_field _4kg _6-h _704 _6-i\" id=\"", "");
                            string id_element_xac_nhan = Regex.Match(html_page, @"submit uiButton uiButtonConfirm.*?(?=""><input value)").Value.Replace("submit uiButton uiButtonConfirm\" id=\"", "");
                            string[] arr_element_xn = id_element_xac_nhan.Split('"');

                            #region kiểm tra xem email chính có phải cái vừa thêm
                            string del_email_002_1 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label").Text;
                            string email_02 = GetData("https://vip.danplay.net/?auto=adp_facebook&email_he_thong=" + del_email_002_1);
                            if (email_02 == "co")
                            {
                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/div/label");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);
                            }
                            else
                            {
                                var del_email_002 = driver.FindElementByXPath("//*[@id=\"" + id_element_click + "\"]/li[2]/div/span[2]/span");
                                del_email_002.Click(); Thread.Sleep(2000);
                                var del_email_003 = driver.FindElementById(arr_element_xn[2]);
                                del_email_003.Click(); Thread.Sleep(2000);
                            }
                            #endregion

                            try
                            {
                                var del_email_004 = driver.FindElementByName("ajax_password");
                                del_email_004.SendKeys(account_pass); Thread.Sleep(2000);
                                del_email_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                            }
                            catch { }
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }
                        catch
                        {
                            driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view";
                            driver.Navigate(); Thread.Sleep(3000);
                            del_email_001 = driver.FindElementsByName("remove_emails[]");
                        }

                    }
                    else { i = 100; }
                    Thread.Sleep(5000);
                    if (driver.Url.Contains("checkpoint")) { ketqua = "check"; i = 100; } else { ketqua = "oke"; i = 100; }
                }
            }
            catch { ketqua = "error"; }
            return ketqua;
        }
        #endregion

        #region FACEBOOK: del_phone_fb: check - oke - error
        public string del_phone_fb_v1(string pass_facebook)
        {
            string kq_Delete_phone_account_facebook = "";
            try
            {

                for (int i = 0; i < 10; i++)
                {
                    ket_noi_sever();
                    driver.Url = "https://www.facebook.com/settings?tab=mobile";
                    driver.Navigate(); Thread.Sleep(3000);
                    string data_1_1_1 = driver.PageSource;
                    string del_so_phone = Regex.Match(data_1_1_1, @"</span><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</span><br><span id=\"", "");

                    #region trường hợp 1
                    if (del_so_phone == "")
                    {
                        del_so_phone = Regex.Match(data_1_1_1, @"</a><br><span id="".*?(?=""><a href=""#"" role=""button"">)").Value.Replace("</a><br><span id=\"", "");

                    }
                    #endregion

                    #region trường hợp 1.1
                    if (del_so_phone == "")
                    {
                        del_so_phone = Regex.Match(data_1_1_1, @"</span>; <br><span id=.*?(?="">)").Value.Replace("</span>; <br><span id=\"", "");
                    }
                    #endregion

                    #region trường hợp 2
                    if (del_so_phone != "")
                    {

                        try
                        {
                            var del_so_phone_001 = driver.FindElementByXPath("//*[@id=\"" + del_so_phone + "\"]/a");
                            del_so_phone_001.Click(); Thread.Sleep(1000);

                            #region xét hết các trường hợp có nhiều số điện thoại
                            for (int ii001 = 0; ii001 < 20; ii001++)
                            {
                                try
                                {
                                    var del_so_phone_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii001 + "]/div[2]/div/div/div/div[2]/div/div/label[2]");
                                    del_so_phone_002.Click(); Thread.Sleep(1000);
                                }
                                catch { }
                            }
                            #endregion

                            Thread.Sleep(3000);
                            string data_1_1_2 = driver.PageSource;
                            string del_so_phone_button = Regex.Match(data_1_1_2, @"layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy"" type=""submit"" id="".*?(?="">)").Value.Replace("layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy\" type=\"submit\" id=\"", "");

                            try
                            {
                                var del_so_phone_003 = driver.FindElementById(del_so_phone_button); Thread.Sleep(1000);
                                del_so_phone_003.Click(); Thread.Sleep(1000);
                            }
                            catch
                            {
                                var del_so_phone_003_1 = driver.FindElementByXPath("//*[@id=\"globalContainer\"]/div[3]/div/div/div/div[2]/button");
                                del_so_phone_003_1.Click(); Thread.Sleep(1000);
                            }

                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }

                    }
                    else { i = 100; }
                    #endregion
                }

                #region xóa số điện thoại mặc định
                for (int imd = 0; imd < 5; imd++)
                {
                    try
                    {
                        string data_444 = driver.PageSource;
                        string id_button = Regex.Match(data_444, @"</span></strong> · <span id="".*?(?="">)").Value.Replace("</span></strong> · <span id=\"", "");
                        var click_001 = driver.FindElementByXPath("//*[@id=\"" + id_button + "\"]/a");
                        click_001.Click(); Thread.Sleep(2000);

                        for (int ii002 = 0; ii002 < 20; ii002++)
                        {
                            try
                            {
                                var click_002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[" + ii002 + "]/div[2]/div/div/div/div[3]/button");
                                click_002.Click(); Thread.Sleep(2000);
                            }
                            catch { }
                        }

                        try
                        {
                            var del_phone_004 = driver.FindElementByName("ajax_password");
                            del_phone_004.SendKeys(pass_facebook); Thread.Sleep(2000);
                            del_phone_004.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(3000);
                        }
                        catch { }
                    }
                    catch { imd++; }

                }
                #endregion

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_Delete_phone_account_facebook = "check"; } else { kq_Delete_phone_account_facebook = "oke"; }
            }
            catch { kq_Delete_phone_account_facebook = "error"; }
            return kq_Delete_phone_account_facebook;
        }
        #endregion

        #region FACEBOOK: them_email_facebook
        public string them_email_facebook_v1(string pass_add_email, string new_email_add_facebook)
        {            
            string kq_them = "oke";
            try
            {
                ket_noi_sever();
                driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);
                if (driver.Url.Contains("settings")) {

                    #region Tìm và xóa đi địa chỉ email đã thêm nhưng chưa very
                    try
                    {
                        var add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                        int del_add_email_vl_5 = 5;
                        for (int i = 0; i < del_add_email_vl_5; i++)
                        {
                            if (add_email_001.Count > 0)
                            {
                                var add_email_002 = driver.FindElementByClassName("SettingsEmailPendingCancel");
                                add_email_002.Click();
                                Thread.Sleep(3000);
                            }
                            add_email_001 = driver.FindElementsByClassName("SettingsEmailPendingCancel");
                            if (add_email_001.Count == 0) { i = 10; }
                        }
                    } catch { }                    
                    driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);
                    #endregion

                    #region Tìm nút thêm email mới để thêm - trường hợp 1    
                    try
                    {
                        #region 001 - các trường hợp tìm nút click vào mục thêm email
                        try
                        {
                            if (driver.Url.Contains("settings")) {
                                var add_email_004_01 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div");
                                add_email_004_01.Click(); Thread.Sleep(5000);
                            }
                            
                        }
                        catch { }
                        #endregion
                        
                        #region 002 - các trường hợp tìm nút click vào mục thêm email
                        try
                        {
                            if (driver.Url.Contains("settings"))
                            {
                                var add_email_004 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/a");
                                add_email_004.Click(); Thread.Sleep(5000);
                            }                           
                        }
                        catch { }
                        #endregion
                        
                        #region 003 - các trường hợp tìm nút click vào mục thêm email
                        try
                        {
                            if (driver.Url.Contains("settings"))
                            {
                                var add_email_003a_2 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/div[2]/a[3]");
                                add_email_003a_2.Click(); Thread.Sleep(5000);
                            }
                            string id_click_add_email_001 = Regex.Match(driver.PageSource, @"action=""/add_contactpoint/dialog/submit/"" method=""post"" onsubmit.*?(?=""><input type=""hidden"")").Value.Replace("action=\"/add_contactpoint/dialog/submit/\" method=\"post\" onsubmit=\"\" id=\"", "");


                            try
                            {
                                if (driver.Url.Contains("settings"))
                                {
                                    var add_email_006_2 = driver.FindElementByXPath("//*[@id=\"" + id_click_add_email_001 + "\"]/div[2]/div/table/tbody/tr/td/input");
                                    add_email_006_2.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                                    add_email_006_2.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                                }
                            }
                            catch { }


                            try
                            {
                                if (driver.Url.Contains("settings"))
                                {
                                    string id_click_add_email_002 = Regex.Match(driver.PageSource, @"<input type=""text"" class=""inputtext.*?(?=placeholder)").Value.Replace("<input type", "");
                                    string id_click_add_email_003 = Regex.Match(id_click_add_email_002, @"id="".*?(?="")").Value.Replace("id=\"", "");
                                    var add_email_006_2 = driver.FindElementByXPath("//*[@id=\"" + id_click_add_email_003 + "\"]");
                                    add_email_006_2.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                                    add_email_006_2.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                                }
                            }
                            catch { }
                        } catch { }
                        #endregion

                    }
                    catch { }
                    #endregion
                    
                    #region Tìm nút thêm email mới để thêm - trường hợp 2
                    try
                    {
                        var add_email_005 = driver.FindElementsByName("new_email");
                        if (add_email_005.Count > 0)
                        {
                            if (driver.Url.Contains("settings")) { }
                            var add_email_006 = driver.FindElementByName("new_email");
                            add_email_006.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                            add_email_006.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                        }
                    }
                    catch { }
                    #endregion


                    #region Tìm nút thêm email mới để thêm - trường hợp 3
                    try
                    {
                        var add_email_005_3 = driver.FindElementByXPath("//*[@id=\"SettingsPage_Content\"]/ul/li[3]/div/div/div/div[2]/div[1]/div/div/a"); add_email_005_3.Click(); Thread.Sleep(5000);
                        var add_email_006_3 = driver.FindElementByName("new_email");
                        add_email_006_3.SendKeys(new_email_add_facebook); Thread.Sleep(2000);
                        add_email_006_3.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(5000);
                    }
                    catch { }
                    #endregion



                    #region thêm mật khẩu nếu bị hỏi
                    var add_email_007 = driver.FindElementsByName("ajax_password");
                    if (add_email_007.Count > 0)
                    {
                        kq_them = them_pass_ajax(pass_add_email);
                    }
                    #endregion

                }

                Thread.Sleep(5000);
                if (driver.Url.Contains("checkpoint")) { kq_them = "check"; }
            } catch { kq_them = "error";}

            return kq_them;
        }
        #endregion

        #region thay_email_change
        public string thay_email_change_v1(string account_pass, string account_id)
        {
            string tien_trinh = "4";            
            try
            {                
                driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);                
                    string email_01 = null;
                    string email_03 = null;

                    #region Tìm code từ email củ đã gửi
                    try { email_03 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/div[1]/b").Text; } catch { }
                    try { email_01 = driver.FindElementByName("primary_email").GetAttribute("value"); } catch { email_01 = ""; }

                   
                    if (email_03 != null)
                    {
                        try {
                            string code_very_new = GetData("https://email.danplay.net/?e=email&email_code_very="+ email_03);
                            if (code_very_new.Length > 5)
                            {
                                string kq_ver_525 = "https://www.facebook.com/confirmcontact.php?c=" + code_very_new;
                                tien_trinh = email_03;
                                driver.Url = kq_ver_525; driver.Navigate(); Thread.Sleep(3000);
                                key_danplay("change", "change_email_oke&yahoo=" + email_03 + "&id_account=" + account_id);
                            }
                        } catch { }
                    }
                    #endregion  

                    try { email_03 = driver.FindElementByXPath("//*[@id=\"settings_account_email\"]/tbody[2]/tr[1]/td/div/div[1]/b").Text; } catch { }
                    try { email_01 = driver.FindElementByName("primary_email").GetAttribute("value"); } catch { email_01 = ""; }

                string email_02 = GetData("https://vip.danplay.net/?auto=adp_facebook&email_he_thong=" + email_01);
                    if (email_02 != "co" || email_01==email_03)
                    {
                        string domain_tao_email = null; string kq_tao_yh = null;
                        domain_tao_email = GetData("https://vip.danplay.net/?auto=adp_facebook&facebook_email_add_domain_v2&key="+ textBox5.Text);
                        while (domain_tao_email.Length < 8)
                        {
                            domain_tao_email = GetData("https://vip.danplay.net/?auto=adp_facebook&facebook_email_add_domain_v2&key="+ textBox5.Text);
                        }

                        kq_tao_yh = random_string(13) + "@" + domain_tao_email; 
                        if (kq_tao_yh.Length > 15)
                        {
                            string kq_them_yh2 = them_email_facebook_v1(account_pass, kq_tao_yh);
                            if (kq_them_yh2 != "check" && kq_them_yh2 != "sai_pass")
                            {
                                #region lấy kết quả và very cách mới email domain
                                string code_5 = GetData("https://email.danplay.net/?e=email&email_code_very=" + kq_tao_yh);
                                if (code_5.Length < 5)
                                {
                                    for (int i = 0; i < 20; i++)
                                    {
                                        code_5 = GetData("https://email.danplay.net/?e=email&email_code_very=" + kq_tao_yh);
                                        Thread.Sleep(10000); if (code_5.Length > 5) { i = 100; }
                                    }
                                }

                                if (code_5.Length > 5)
                                {
                                int aa = 0;
                                while (aa < 5) { if (code_5.Length < 5) { code_5 = GetData("https://email.danplay.net/?e=email&email_code_very=" + kq_tao_yh); } else { aa = 100; } aa++; Thread.Sleep(3000); }                                
                                string kq_ver_525 = "https://www.facebook.com/confirmcontact.php?c=" + code_5;
                                tien_trinh = kq_tao_yh;
                                driver.Url = kq_ver_525; driver.Navigate();
                                Thread.Sleep(3000);
                                key_danplay("change", "change_email_oke&yahoo=" + kq_tao_yh + "&id_account=" + account_id);
                                }
                                #endregion
                            }
                            else { tien_trinh = kq_them_yh2; }
                        }
                    } else { tien_trinh = "0"; }                              
                
            } catch { }
            return tien_trinh;
        }
        #endregion

        #region post bài ở GROUP
        public string post_bai_nhom(string id_nhom, string noi_dung)
        {
            string kq = "error";
            driver.Url = "https://www.facebook.com/groups/"+ id_nhom; driver.Navigate(); Thread.Sleep(2000);
            string id_vietbai = Regex.Match(driver.PageSource, @"composerID:"".*?(?="")").Value.Replace("composerID:\"", "");
            var click_001 = driver.FindElementById(id_vietbai); click_001.Click(); Thread.Sleep(3000);
            string id_vietbai_1 = Regex.Match(driver.PageSource, @"<div></div></div></div></div><div class=.*?(?=<span>)").Value.Replace("<div></div></div></div></div><div class=", "");
            string id_vietbai_2 = Regex.Match(id_vietbai_1, @"<div id="".*?(?="")").Value.Replace("<div id=\"", "");
            string XPath = null;
            try
            {
                XPath = "//*[@id=\"" + id_vietbai_2 + "\"]/div[1]/div/div[1]/div[1]/div[2]/div/div/div/div/div/div/div[2]/div/div/div/div";
                var click_002 = driver.FindElementByXPath(XPath); click_002.SendKeys(noi_dung); Thread.Sleep(5000);
                try { XPath = "//*[@id=\"" + id_vietbai_2 + "\"]/div[3]/div[3]/div/div[2]/div/div[2]/button/span"; var click_003 = driver.FindElementByXPath(XPath); click_003.Click(); Thread.Sleep(2000); } catch { }
                try { XPath = "//*[@id=\"" + id_vietbai_2 + "\"]/div[2]/div[3]/div/div[2]/div/div[2]/button/span"; var click_003 = driver.FindElementByXPath(XPath); click_003.Click(); Thread.Sleep(2000); } catch { }
                kq = "oke";
            } catch { kq = "error"; }
            return kq;
        }
        #endregion

        #region Change_pass_logout: check - oke - errorr
        public string Change_pass_logout(string pass_world_old, string account_id, string pass_world_new = null)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string ka_Change_pass_logout = "";
            if (pass_world_new == null) { pass_world_new = random_pass_01(12); }
            try
            {
                driver.Url = "https://www.facebook.com/settings?tab=account&section=email&view"; driver.Navigate(); Thread.Sleep(5000);
                string email_01 = null;                                
                try { email_01 = driver.FindElementByName("primary_email").GetAttribute("value"); } catch { email_01 = ""; }
                if(email_01 == "") { email_01 = driver.FindElementByXPath("//*[@id=\"SettingsPage_Content\"]/ul/li[4]/a/span[3]/strong").Text; }
                string email_02 = GetData("https://vip.danplay.net/?auto=adp_facebook&email_he_thong=" + email_01);
                if ((email_02 == "co") || checkBox8.Checked == true)
                {
                    try
                    {
                        driver.Url = "https://m.facebook.com/settings/security/password/"; driver.Navigate(); Thread.Sleep(4000);
                        var change_pass_01 = driver.FindElementByName("password_old"); Thread.Sleep(1000);
                        change_pass_01.SendKeys(pass_world_old);
                        var change_pass_02 = driver.FindElementByName("password_new"); Thread.Sleep(1000);
                        change_pass_02.SendKeys(pass_world_new);
                        var change_pass_03 = driver.FindElementByName("password_confirm"); Thread.Sleep(1000);
                        change_pass_03.SendKeys(pass_world_new);
                        var change_pass_04 = driver.FindElementByName("save"); Thread.Sleep(1000);
                        change_pass_04.Click();
                        ket_noi_sever(); 
                        Thread.Sleep(5000);                       
                        if (driver.Url.Contains("checkpoint")) { ka_Change_pass_logout = "check"; } else { ka_Change_pass_logout = "oke"; }
                        if (driver.Url.Contains("survey")) { key_danplay("change", "add_new_pass_v1&pass_cu=" + pass_world_old + "&pass_moi=" + pass_world_new + "&id_account=" + account_id); } else {
                            GetData("https://vip.danplay.net/?auto=adp_facebook&ghi_chu_moi=ERROR_PASS&id_account=260879&key="+textBox5.Text+"&id_account=" + account_id);                            
                        }
                    }
                    catch { ka_Change_pass_logout = "error"; }
                }                                                                
            } catch { ka_Change_pass_logout = "error"; }            
            return ka_Change_pass_logout;
        }
        #endregion

        #region ĐĂNG XUẤT TẤT CẢ THIẾT BỊ
        public void dangxuat_thietbi()
        {
            driver.Url = "https://m.facebook.com/settings/security_login/sessions/log_out_all/confirm/"; driver.Navigate(); Thread.Sleep(2000);
            try { var d001 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div[2]/a[1]"); Thread.Sleep(500); d001.Click(); Thread.Sleep(2000); } catch { }
            try { var d001 = driver.FindElementByXPath("//*[@id=\"root\"]/div/div/div[2]/a[1]"); Thread.Sleep(500); d001.Click(); Thread.Sleep(2000); } catch { }
        }
        #endregion

        #region post bài và xóa để lấy tương tác trình duyệt
        public void post_va_xoa(string id_facebook)
        {
            try {
                driver.Url = "https://www.facebook.com/" + id_facebook; driver.Navigate(); Thread.Sleep(1000);
                string html_viet = driver.PageSource;
                string id_vietbai = Regex.Match(html_viet, @"role=""presentation"" style="""" id="".*?(?="")").Value.Replace("role=\"presentation\" style=\"\" id=\"", "");
                string dang_vietbai_1 = Regex.Match(html_viet, @"</span></span></a></span></span></div><div class.*?<span>").Value.Replace("</span></span></a></span></span></div><div class", "");
                string dang_vietbai = Regex.Match(dang_vietbai_1, @"id="".*?(?="")").Value.Replace("id=\"", "");
                var viet_01 = driver.FindElementByXPath("//*[@id=\"" + id_vietbai + "\"]/div/div/div[2]/div/div/div/div"); viet_01.Click(); Thread.Sleep(2000); viet_01.SendKeys(rand_num(6).ToString()); Thread.Sleep(2000);
                try { var viet_02 = driver.FindElementByXPath("//*[@id=\"" + dang_vietbai + "\"]/div[2]/div[3]/div[2]/button"); viet_02.Click(); Thread.Sleep(5000); } catch { }
                try { var viet_02 = driver.FindElementByXPath("//*[@id=\"" + dang_vietbai + "\"]/div[2]/div[3]/div/div[2]/div/button/span"); viet_02.Click(); Thread.Sleep(5000); } catch { }                
                driver.Url = "https://www.facebook.com/" + id_facebook; driver.Navigate(); Thread.Sleep(1000);
                var html_groups_1 = Regex.Matches(driver.PageSource, @"aria-haspopup=""true"" aria-expanded=""false"" rel=""toggle"" href=""#"" role=""button"" id="".*?""", RegexOptions.Singleline);
                int lan = 0;
                foreach (var nhom1 in html_groups_1)
                {
                    lan++;
                    if (lan == 2)
                    {
                        string id_click = Regex.Match(nhom1.ToString(), @"id="".*?(?="")").Value.Replace("id=\"", "");
                        var via_002 = driver.FindElementById(id_click); via_002.Click(); Thread.Sleep(3000);
                        var via_003 = driver.FindElementByXPath("//*[@id=\"feed_post_menu\"]/div/ul/li[9]/a/span/span"); via_003.Click(); Thread.Sleep(4000);
                    }
                }
                string xoa_bv = Regex.Match(driver.PageSource, @"is_notification_preview=0"" method=""post"" onsubmit="""" id="".*?(?="")").Value.Replace("is_notification_preview=0\" method=\"post\" onsubmit=\"\" id=\"", "");
                var xoa_bv_1 = driver.FindElementByXPath("//*[@id=\"" + xoa_bv + "\"]/div[3]/button"); xoa_bv_1.Click(); Thread.Sleep(2000);
            } catch { }
        }
        #endregion

        #region đổi mật khẩu
        public void doi_mat_khau(string account_id, string pass_cu)
        {
            string new_pass_dp = random_pass_01(12);
            try {
                driver.Url = "https://www.facebook.com/settings?tab=security&section=password&view"; driver.Navigate(); Thread.Sleep(1000);
                
                for (int i = 0; i < 30; i++)
                {
                    try { var via_006 = driver.FindElementByName("password_old"); via_006.SendKeys(pass_cu); Thread.Sleep(2000); i = 100; } catch { Thread.Sleep(2000); }
                }
                var via_007 = driver.FindElementByName("password_new"); via_007.SendKeys(new_pass_dp); Thread.Sleep(1000);
                var via_008 = driver.FindElementByName("password_confirm"); via_008.SendKeys(new_pass_dp); Thread.Sleep(5000);
                string id_luu_pass_moi = Regex.Match(driver.PageSource, @"type=""submit"" id="".*?(?="")").Value.Replace("type=\"submit\" id=\"", "");
                var via_009 = driver.FindElementById(id_luu_pass_moi);                
                via_009.Click(); Thread.Sleep(5000);
                ket_noi_sever();
                key_danplay("change", "add_new_pass_v1&pass_cu=" + pass_cu + "&pass_moi=" + new_pass_dp + "&id_account=" + account_id);
            } catch {
                //string id_luu_pass_moi1 = Regex.Match(driver.PageSource, @"type=""submit"" id="".*?(?="")").Value.Replace("type=\"submit\" id=\"", "");
                //File.WriteAllText("test_code\\res00.html", account_id+"\n\n"+ id_luu_pass_moi1+"\n\n" +driver.PageSource); Process.Start("test_code\\res00.html"); MessageBox.Show(id_luu_pass_moi1+"\n\n"+ account_id);
                ket_noi_sever();
                key_danplay("change", "add_new_pass_v2&pass_cu=" + pass_cu + "&pass_moi=" + new_pass_dp + "&id_account=" + account_id);
            }
        }
        #endregion

        #region đổi tên PROFILE
        public void doi_ten_profile(string account_login, string id_facebook)
        {
            for (int i = 0; i < 5; i++) { try { driver.Close(); driver.Quit(); Thread.Sleep(500); } catch { } }
            try { if (account_login != id_facebook && id_facebook.Length>7) { string thu_muc_cu = "Profile\\" + account_login; string thu_muc_moi = "Profile\\" + id_facebook; Directory.Move(thu_muc_cu, thu_muc_moi); } } catch { }
        }
        #endregion

        #region phân tích nhóm
        public string phan_tich_nhom(string id_nhom, string face_cookies)
        {
            string kq = "error";
            try {
                ket_noi_sever();
                string html_id_gr = GetData("https://www.facebook.com/groups/" + id_nhom + "/requests/", face_cookies);
                if (html_id_gr != "error")
                {
                    var chuoi_duyet = Regex.Matches(html_id_gr, @"queue=pending", RegexOptions.Singleline);
                    int so_chuoi = chuoi_duyet.Count;
                    if (so_chuoi != 1) { kq = "1"; } else { kq = "0"; }
                }                
            } catch {}
            return kq;
        }
        #endregion

        #region get_groups_facebook
        public string get_groups_facebook(string account_id, string account_login, string face_cookies = null)
        {
            if (face_cookies == null) {
                face_cookies = GetData("https://vip.danplay.net/?auto=adp_facebook&key=" + textBox5.Text + "&get_cookie_mem_via=" + account_id);
            }
            string kq_Post_facebook = "";
            try
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                driver.Url = "https://m.facebook.com/groups/?ref=bookmarks";
                driver.Navigate(); Thread.Sleep(4000);
                go_check_poit();
                for (int i = 0; i < 25; i++)
                {
                    js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                    js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                }
                string html_groups = driver.PageSource;
                var html_groups_1 = Regex.Matches(html_groups, @"href=""/groups/.*?</div></div></a>", RegexOptions.Singleline);
                string data_1 = "";
                string save_data_nhom = "";
                string save_data_post = "-not";
                string nhom_khong_duyet = "";
                string nhom_sever_get = GetData("https://vip.danplay.net/?auto=adp_facebook&get_nhom_facebook="+ account_login);
                int nhom_de_lai = 1000;   try { nhom_de_lai = Convert.ToInt32(GetData("https://vip.danplay.net/?auto=adp_facebook&key=" + textBox5.Text + "&so_nhom_de_lai")); } catch { nhom_de_lai = 1000; }
                if (nhom_de_lai < 1) { nhom_de_lai = 1000; }
                foreach (var nhom1 in html_groups_1)
                {
                    string[] chuoi_1 = nhom1.ToString().Split('/');
                    string id_nhom = chuoi_1[2];
                    string nhom_ko_post_duoc = Regex.Match(nhom_sever_get, @"" + chuoi_1[2] + "-not").Value.Replace("-not", "");
                    if (nhom_ko_post_duoc != chuoi_1[2])
                    {
                        int thanh_vien = 0; string ten_nhom = ""; 
                        string thong_tin_001 = GetData("https://vip.danplay.net/?fb=get_member_groups&uid="+ id_nhom);
                        try { string[] arr1 = thong_tin_001.Split('|'); thanh_vien = Convert.ToInt32(arr1[0]); ten_nhom = arr1[1];  } catch { }                        
                        if(thanh_vien>0 && thanh_vien < nhom_de_lai) { roi_nhom_facebook(id_nhom); } else
                        {
                            string nhom = phan_tich_nhom(id_nhom, face_cookies);
                            if (nhom == "1") { save_data_post = "-oke"; nhom_khong_duyet = nhom_khong_duyet + "|" + id_nhom+"--"+ thanh_vien+"--"+ ten_nhom; } else { save_data_post = "-not"; }
                        }
                        
                    } else { save_data_post = "-not"; }
                    save_data_nhom = save_data_nhom + id_nhom + save_data_post + "|";
                }
                GetData("https://vip.danplay.net/?auto=adp_facebook&key="+textBox5.Text+"&up_list_groups=" + account_id + "&data=" + nhom_khong_duyet);
                if (!Directory.Exists("data_groups")) { Directory.CreateDirectory("data_groups"); }
                String filepath = Path.Combine(Environment.CurrentDirectory, "data_groups/" + account_login + ".txt");
                FileStream fs = new FileStream(filepath, FileMode.Create);//Tạo file mới
                StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);//fs là 1 FileStream              
                sWriter.WriteLine(save_data_nhom.Trim());
                sWriter.Flush();
                fs.Close();
                UploadFile("https://vip.danplay.net/file_data/upload.php", "data_groups/" + account_login + ".txt");
                kq_Post_facebook = "oke";
            } catch { kq_Post_facebook = "error"; }
            return kq_Post_facebook;
        }
        #endregion

        #region KẾT NỐI SEVER DANPLAY - KHỞI ĐỘNG CÙNG WIN - AUTO UPDATE
        public void ket_noi_sever()
        {            
            string danplay = GetData("https://vip.danplay.net/auto.html");
            if (danplay != "danplay")
            {
                while (danplay != "danplay")
                {
                    Thread.Sleep(10000); danplay = GetData("https://vip.danplay.net/auto.html");
                }
            }
            int aaass = 0;  //Tạm dừng các auto đang chạy trước khi update
            try { aaass = Convert.ToInt32(File.ReadAllText("data\\update.txt", Encoding.UTF8).Trim()); } catch { aaass = 0; }
            while (aaass > 0)
            {
                Thread.Sleep(10000);  try { aaass = Convert.ToInt32(File.ReadAllText("data\\update.txt", Encoding.UTF8).Trim()); } catch { aaass = 0; }                
            }            
        }

        private void auto_update_dp_run(object sender, EventArgs e)
        {            
            string kiem_tra_update = null;            
            while (1 < 2)
            {
                Thread.Sleep(30000);
                kiem_tra_update = GetData("http://cloud.danplay.net/kfkskasdasfksdaasd1/danplay.php?version=" + version);
                if (kiem_tra_update != "" && kiem_tra_update != "error") {
                    int a41s4 = 1; int aaass = 0; int auto_jsjss = 0;
                    try { aaass = Convert.ToInt32(File.ReadAllText("data\\update.txt", Encoding.UTF8).Trim()); } catch { }
                    while (a41s4 > 0)
                    {
                        a41s4++;
                        File.WriteAllText("data\\update.txt", a41s4.ToString()); Thread.Sleep(1000);
                        try { aaass = Convert.ToInt32(File.ReadAllText("data\\update.txt", Encoding.UTF8).Trim()); } catch { }
                        auto_jsjss = 60 - aaass;
                        Invoke((Action)(() => this.Text = "Hệ thống tự update sau: " + auto_jsjss + " giây"));
                        if(auto_jsjss < 10) {
                            try { EndTask("chromedriver"); } catch { } //Tắt chromedriver
                            try { EndTask("update_auto"); } catch { } //Cửa sổ update củ chưa thoát
                            try { EndTask("chrome"); } catch { } //Đóng toàn bộ cửa sổ chrome đang hoạt động
                        }
                        if (auto_jsjss < 1) { if (File.Exists("data\\update.txt")) { File.Delete("data\\update.txt"); } update_version_dp(); }
                    }                    
                }
            }
        }

        protected override void OnShown(EventArgs e)
        {            
            string kiem_tra_update = GetData("http://cloud.danplay.net/kfkskasdasfksdaasd1/danplay.php?version=" + version);
            if (kiem_tra_update != "" && kiem_tra_update != "error") { update_version_dp(); }


            string filePath = "data\\win.txt";
            if (System.IO.File.Exists(filePath))
            {
                string text_api = File.ReadAllText(filePath, Encoding.UTF8);
                if (text_api.Trim() == "oke")
                {
                    if (all_facebook.IsBusy) { all_facebook.CancelAsync(); } else { all_facebook.RunWorkerAsync(); }
                    if (auto_update_dp.IsBusy) { auto_update_dp.CancelAsync(); } else { auto_update_dp.RunWorkerAsync(); }
                }
            }
        }
        #endregion

        #region KHỞI ĐỘNG AUTO FACEBOOK

        public void start_auto_facebook()
        {
            Invoke((Action)(() => button23.BackColor = Color.SkyBlue));
            int luong_kd = 0; try { luong_kd = Convert.ToInt32(textBox19.Text); } catch { }
            int list_via_chay = textBox18.Text.Length; if (list_via_chay > 4) { luong_kd = 1; };
            if (kiem_tra_key() == "oke")
            {
                auto_facebook("AUTO FACEBOOK đang kiểm tra KEY", 0);                
                string cb_moi = checkBox18.Checked == true ? "1" : "0"; string cb_check = checkBox9.Checked == true ? "1" : "0"; string cb_nuoi = checkBox10.Checked == true ? "1" : "0"; string cb_tot = checkBox8.Checked == true ? "1" : "0";
                string cb_tuong = checkBox12.Checked == true ? "1" : "0"; string cb_nhom = checkBox13.Checked == true ? "1" : "0"; string cb_ban_be = checkBox17.Checked == true ? "1" : "0"; string cb_tin_nhan = checkBox19.Checked == true ? "1" : "0";
                string cb_binh_luan = checkBox23.Checked == true ? "1" : "0"; string cb_thich = checkBox24.Checked == true ? "1" : "0"; string cb_doi_pass = checkBox14.Checked == true ? "1" : "0"; string cb_them_email = checkBox15.Checked == true ? "1" : "0";
                string cb_xoa_email = checkBox16.Checked == true ? "1" : "0"; string cb_xoa_sdt = checkBox26.Checked == true ? "1" : "0"; string cb_2fa = checkBox21.Checked == true ? "1" : "0"; string cb_backup = checkBox22.Checked == true ? "1" : "0";
                string cb_dang_xuat = checkBox2.Checked == true ? "1" : "0"; string cb_check_nhom = checkBox4.Checked == true ? "1" : "0"; string cb_xin_nhom = checkBox5.Checked == true ? "1" : "0"; string cb_doi_ten = checkBox11.Checked == true ? "1" : "0";
                string cb_khang_spam = checkBox20.Checked == true ? "1" : "0"; string cb_doi_pass1 = checkBox27.Checked == true ? "1" : "0"; string cb_spam_tuong = checkBox28.Checked == true ? "1" : "0"; string cb_reset_ip = checkBox29.Checked == true ? "1" : "0";
                string che_do_auto = cb_moi + "|" + cb_check + "|" + cb_nuoi + "|" + cb_tot + "|" + cb_tuong + "|" + cb_nhom + "|" + cb_ban_be + "|" + cb_tin_nhan + "|" + cb_binh_luan + "|" + cb_thich + "|" + cb_doi_pass + "|" + cb_them_email + "|" + cb_xoa_email + "|" + cb_xoa_sdt + "|" + cb_2fa + "|" 
                    + cb_backup + "|" + cb_dang_xuat+"|"+ cb_check_nhom+"|"+ cb_xin_nhom+"|"+ cb_doi_ten+"|"+ cb_khang_spam+"|"+ cb_doi_pass1+"|"+ cb_spam_tuong+"|"+ cb_reset_ip;
                string arr_luong = GetData("https://vip.danplay.net/?auto=adp_facebook&check_luong=" + textBox4.Text + "&luong=" + luong_kd + "&key=" + textBox5.Text + "&che_do_auto=" + che_do_auto);

                auto_facebook("AUTO FACEBOOK phân tích yêu cầu", 0); int luong_run = 0;
                string id_luong_chay = GetData("https://vip.danplay.net/?auto=adp_facebook&id_luong=" + textBox4.Text + "&key=" + textBox5.Text);
                int chay = 9;
                while (5 < chay)
                {
                    
                    try {
                        for (int i = luong_run; i < luong_kd; i++)
                        {
                            ket_noi_sever();
                            cb_moi = checkBox18.Checked == true ? "1" : "0"; cb_check = checkBox9.Checked == true ? "1" : "0"; cb_nuoi = checkBox10.Checked == true ? "1" : "0"; cb_tot = checkBox8.Checked == true ? "1" : "0";
                            cb_tuong = checkBox12.Checked == true ? "1" : "0"; cb_nhom = checkBox13.Checked == true ? "1" : "0"; cb_ban_be = checkBox17.Checked == true ? "1" : "0"; cb_tin_nhan = checkBox19.Checked == true ? "1" : "0";
                            cb_binh_luan = checkBox23.Checked == true ? "1" : "0"; cb_thich = checkBox24.Checked == true ? "1" : "0"; cb_doi_pass = checkBox14.Checked == true ? "1" : "0"; cb_them_email = checkBox15.Checked == true ? "1" : "0";
                            cb_xoa_email = checkBox16.Checked == true ? "1" : "0"; cb_xoa_sdt = checkBox26.Checked == true ? "1" : "0"; cb_2fa = checkBox21.Checked == true ? "1" : "0"; cb_backup = checkBox22.Checked == true ? "1" : "0";
                            cb_dang_xuat = checkBox2.Checked == true ? "1" : "0"; cb_check_nhom = checkBox4.Checked == true ? "1" : "0"; cb_xin_nhom = checkBox5.Checked == true ? "1" : "0"; cb_doi_ten = checkBox11.Checked == true ? "1" : "0";
                            cb_khang_spam = checkBox20.Checked == true ? "1" : "0"; cb_doi_pass1 = checkBox27.Checked == true ? "1" : "0"; cb_spam_tuong = checkBox28.Checked == true ? "1" : "0"; cb_reset_ip = checkBox29.Checked == true ? "1" : "0";
                            facebook auto_blog = new facebook(textBox18.Text, cb_moi, cb_check, cb_nuoi, cb_tot, cb_tuong, cb_nhom, cb_ban_be, cb_tin_nhan, cb_binh_luan, cb_thich, cb_doi_pass, cb_them_email, cb_xoa_email, cb_xoa_sdt, cb_2fa, cb_backup, textBox5.Text, id_luong_chay, cb_dang_xuat, cb_check_nhom, cb_xin_nhom, cb_doi_ten, cb_khang_spam, cb_doi_pass1, cb_spam_tuong, cb_reset_ip);
                            Thread auto_b3 = new Thread(() => { auto_blog.start_facebook(); });
                            auto_b3.IsBackground = true; auto_b3.Start();
                            if (textBox18.Text.Length > 4) { chay = 0; luong_kd = 0; }
                            auto_facebook("AUTO FACEBOOK: khởi động luồng: " + i + "/" + luong_kd, 5);
                        }
                        if (list_via_chay > 4) { chay = 0; luong_kd = 0; }
                        else
                        {
                            auto_facebook("AUTO FACEBOOK đang chạy " + luong_kd + "/" + luong_kd, 5);
                            if (luong_kd == 0) { auto_facebook("Hệ thống đang kiểm tra luồng", 30); }
                            ket_noi_sever();
                            arr_luong = GetData("https://vip.danplay.net/?auto=adp_facebook&check_luong_v2=" + id_luong_chay); string[] arr1 = arr_luong.Split('|'); try { luong_kd = Convert.ToInt32(arr1[0]); luong_run = Convert.ToInt32(arr1[1]); } catch { luong_kd = 0; luong_run = 999; }                            

                            #region lưu file txt giá trị auto FACEBOOK đang chạy
                            if (!Directory.Exists("data")) { Directory.CreateDirectory("data"); }
                            String filepath = Path.Combine(Environment.CurrentDirectory, "data\\auto_facebook.txt");
                            try
                            {
                                Invoke(new Action(() =>
                                {
                                    FileStream fs = new FileStream(filepath, FileMode.Create);//Tạo file mới
                                    StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);//fs là 1 FileStream              
                                    sWriter.WriteLine(arr_luong.Trim());
                                    sWriter.Flush();
                                    fs.Close();
                                }));
                            }
                            catch { }
                            #endregion

                            Invoke((Action)(() => this.textBox19.Text = luong_kd.ToString()));
                            if (arr1[2] == "1") { Invoke((Action)(() => checkBox18.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox18.CheckState = CheckState.Unchecked)); }
                            if (arr1[3] == "1") { Invoke((Action)(() => checkBox9.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox9.CheckState = CheckState.Unchecked)); }
                            if (arr1[4] == "1") { Invoke((Action)(() => checkBox10.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox10.CheckState = CheckState.Unchecked)); }
                            if (arr1[5] == "1") { Invoke((Action)(() => checkBox8.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox8.CheckState = CheckState.Unchecked)); }
                            if (arr1[6] == "1") { Invoke((Action)(() => checkBox12.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox12.CheckState = CheckState.Unchecked)); }
                            if (arr1[7] == "1") { Invoke((Action)(() => checkBox13.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox13.CheckState = CheckState.Unchecked)); }
                            if (arr1[8] == "1") { Invoke((Action)(() => checkBox17.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox17.CheckState = CheckState.Unchecked)); }
                            if (arr1[9] == "1") { Invoke((Action)(() => checkBox19.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox19.CheckState = CheckState.Unchecked)); }
                            if (arr1[10] == "1") { Invoke((Action)(() => checkBox23.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox23.CheckState = CheckState.Unchecked)); }
                            if (arr1[11] == "1") { Invoke((Action)(() => checkBox24.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox24.CheckState = CheckState.Unchecked)); }
                            if (arr1[12] == "1") { Invoke((Action)(() => checkBox14.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox14.CheckState = CheckState.Unchecked)); }
                            if (arr1[13] == "1") { Invoke((Action)(() => checkBox15.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox15.CheckState = CheckState.Unchecked)); }
                            if (arr1[14] == "1") { Invoke((Action)(() => checkBox16.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox16.CheckState = CheckState.Unchecked)); }
                            if (arr1[15] == "1") { Invoke((Action)(() => checkBox26.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox26.CheckState = CheckState.Unchecked)); }
                            if (arr1[16] == "1") { Invoke((Action)(() => checkBox21.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox21.CheckState = CheckState.Unchecked)); }
                            if (arr1[17] == "1") { Invoke((Action)(() => checkBox22.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox22.CheckState = CheckState.Unchecked)); }
                            if (arr1[18] == "1") { Invoke((Action)(() => checkBox2.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox2.CheckState = CheckState.Unchecked)); }
                            if (arr1[19] == "1") { Invoke((Action)(() => checkBox4.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox4.CheckState = CheckState.Unchecked)); }
                            if (arr1[20] == "1") { Invoke((Action)(() => checkBox5.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox5.CheckState = CheckState.Unchecked)); }
                            if (arr1[21] == "1") { Invoke((Action)(() => checkBox11.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox11.CheckState = CheckState.Unchecked)); }
                            if (arr1[22] == "1") { Invoke((Action)(() => checkBox20.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox20.CheckState = CheckState.Unchecked)); }
                            if (arr1[23] == "1") { Invoke((Action)(() => checkBox27.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox27.CheckState = CheckState.Unchecked)); }
                            if (arr1[24] == "1") { Invoke((Action)(() => checkBox28.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox28.CheckState = CheckState.Unchecked)); }
                            if (arr1[25] == "1") { Invoke((Action)(() => checkBox29.CheckState = CheckState.Checked)); } else { Invoke((Action)(() => checkBox29.CheckState = CheckState.Unchecked)); }
                            try { chay = Convert.ToInt32(GetData("https://vip.danplay.net/?auto=adp_facebook&trang_thai=" + id_luong_chay)); } catch { }
                            while (chay == 6) { auto_facebook("AUTO FACEBOOK tạm dừng theo lệnh từ sever", 30); try { chay = Convert.ToInt32(GetData("https://vip.danplay.net/?auto=adp_facebook&trang_thai=" + id_luong_chay)); } catch { } }
                            auto_facebook("AUTO FACEBOOK đang tính số luồng khởi động", 0);
                        }
                    } catch { luong_run = 999; auto_facebook("AUTO FACEBOOK mất kết nối đến SEVER", 10); }
                }
            }            
            Invoke((Action)(() => button23.BackColor = Color.LightGray)); auto_facebook("Bắt đầu", 0);
        }
        #endregion

        #region AUTO FACEBOOK
        private void all_facebook_run(object sender, EventArgs e)
        {
            start_auto_facebook();
        }        

        private void Button23_Click(object sender, EventArgs e)
        {
            if (all_facebook.IsBusy) { all_facebook.CancelAsync(); } else { all_facebook.RunWorkerAsync(); }
        }
        private void LinkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string link = @"https://www.facebook.com/groups/danplay.net";
            Process.Start(link);
        }
        #endregion

        #region Cấu hình các luồng cần chạy AUTO FACEBOOK
        private void Button22_Click(object sender, EventArgs e)
        {
            int luong = 0;            
            try { luong = Convert.ToInt32(textBox19.Text); } catch { luong = 0; }
            string cb_moi = checkBox18.Checked == true ? "1" : "0"; string cb_check = checkBox9.Checked == true ? "1" : "0"; string cb_nuoi = checkBox10.Checked == true ? "1" : "0"; string cb_tot = checkBox8.Checked == true ? "1" : "0";
            string cb_tuong = checkBox12.Checked == true ? "1" : "0"; string cb_nhom = checkBox13.Checked == true ? "1" : "0"; string cb_ban_be = checkBox17.Checked == true ? "1" : "0"; string cb_tin_nhan = checkBox19.Checked == true ? "1" : "0";
            string cb_binh_luan = checkBox23.Checked == true ? "1" : "0"; string cb_thich = checkBox24.Checked == true ? "1" : "0"; string cb_doi_pass = checkBox14.Checked == true ? "1" : "0"; string cb_them_email = checkBox15.Checked == true ? "1" : "0";
            string cb_xoa_email = checkBox16.Checked == true ? "1" : "0"; string cb_xoa_sdt = checkBox26.Checked == true ? "1" : "0"; string cb_2fa = checkBox21.Checked == true ? "1" : "0"; string cb_backup = checkBox22.Checked == true ? "1" : "0";
            string cb_dang_xuat = checkBox2.Checked == true ? "1" : "0"; string cb_check_nhom = checkBox4.Checked == true ? "1" : "0"; string cb_xin_nhom = checkBox5.Checked == true ? "1" : "0"; string cb_doi_ten = checkBox11.Checked == true ? "1" : "0";
            string cb_khang_spam = checkBox20.Checked == true ? "1" : "0"; string cb_doi_pass1 = checkBox27.Checked == true ? "1" : "0"; string cb_spam_tuong = checkBox28.Checked == true ? "1" : "0"; string cb_reset_ip = checkBox29.Checked == true ? "1" : "0";
            string che_do_auto = cb_moi + "|" + cb_check + "|" + cb_nuoi + "|" + cb_tot + "|" + cb_tuong + "|" + cb_nhom + "|" + cb_ban_be + "|" + cb_tin_nhan + "|" + cb_binh_luan + "|" + cb_thich + "|" + cb_doi_pass + "|" + cb_them_email + "|" + cb_xoa_email + "|" + cb_xoa_sdt + "|"
                + cb_2fa + "|" + cb_backup + "|" + cb_dang_xuat+"|"+ cb_check_nhom+"|"+ cb_xin_nhom+"|"+ cb_doi_ten+"|"+ cb_khang_spam+"|"+ cb_doi_pass1+"|"+ cb_spam_tuong+"|"+ cb_reset_ip;            
            GetData("https://vip.danplay.net/?auto=adp_facebook&thay_doi_so_luong="+ luong+"&key=" + textBox5.Text+"&name_pc="+ textBox4.Text + "&che_do_auto=" + che_do_auto);
            if (luong == 0) { MessageBox.Show("Auto FACEBOOK: sẽ dừng khi chạy xong các luồng đang hoạt động", "Cấu hình luồng"); } else { MessageBox.Show("Đã gửi các giá trị mới lên SEVER!\n\nAuto FACEBOOK: sẽ chạy " + luong + " luồng cùng lúc", "Cấu hình luồng"); }            
        }

        #endregion        

        #region AUTO TẠO BM
        public string tao_bm()
        {
            string ketqua = "error";
            try
            {
                driver.Url = "https://business.facebook.com/create"; driver.Navigate(); Thread.Sleep(2000);                
                string id_001 = Regex.Match(driver.PageSource, @"business-create-account-button"" id=""(.*?)(?="")").Value.Replace("business-create-account-button\" id=\"", "");
                var bm001 = driver.FindElementById(id_001); bm001.Click(); Thread.Sleep(1000);
                string data_sever = GetData("https://vip.danplay.net/?auto=adp_facebook&cau_hinh_bm&key=" + textBox5.Text);
                string bm01 = null; string bm02 = null; string bm03 = null; string bm04 = null; string bm05 = null; string bm06 = null; string bm07 = null; string bm08 = null;
                if (data_sever.Length > 5)
                {
                    #region tạo Bước 1
                    string bm09 = null; string bm10 = null; string bm11 = null;
                    try { string[] arr = data_sever.Split('|'); bm01 = arr[1]; bm02 = arr[2]; bm03 = arr[3]; bm04 = arr[4]; bm05 = arr[5]; bm06 = arr[6]; bm07 = arr[7]; bm08 = arr[8]; bm09 = arr[9]; bm10 = arr[10]; bm11 = arr[11]; } catch { }
                    if (bm01 == "auto") { bm01 = GetData("https://vip.danplay.net/?auto=adp_facebook&get_ten_vn"); }
                    var bm002 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[4]/div[2]/div/div/div/div/div/div/div[2]/div[2]/label/input"); bm002.SendKeys(bm01);
                    if (bm02 != "" && bm02 != "auto") { var bm002_1 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[4]/div[2]/div/div/div/div/div/div/div[2]/div[3]/label/input"); bm002_1.SendKeys(bm02); }
                    var bm003 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[4]/div[2]/div/div/div/div/div/div/div[2]/div[4]/label/input"); bm003.SendKeys(bm03);
                    var bm004 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[4]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div/button/div/div"); bm004.Click(); Thread.Sleep(2000);
                    var bm005 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/button/div/div/div"); bm005.Click(); Thread.Sleep(1000);
                    var bm006 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[6]/div/div/div/div/div/span/label/input"); bm006.SendKeys(bm04); bm006.SendKeys(OpenQA.Selenium.Keys.Enter); Thread.Sleep(1000);
                    var bm007 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[2]/div/div[3]/div/label/input"); bm007.SendKeys(bm05); Thread.Sleep(500);
                    var bm008 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[2]/div/div[5]/div[1]/label/input"); bm008.SendKeys(bm06); Thread.Sleep(500);
                    var bm009 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[2]/div/div[5]/div[2]/label/input"); bm009.SendKeys(bm07); Thread.Sleep(500);
                    var bm010 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[2]/div/div[6]/div[1]/label/input"); bm010.SendKeys(bm08); Thread.Sleep(500);
                    var bm011 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[2]/div/div[6]/div[2]/label/input"); bm011.SendKeys(bm09); Thread.Sleep(500);
                    var bm012 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[2]/div/div[7]/div/label/input"); bm012.SendKeys(bm10); Thread.Sleep(500);
                    var bm013 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div/button/div/div"); bm013.Click(); Thread.Sleep(15000);
                    var bm014 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[4]/div[2]/div/div/div/div/div/div[3]/span[2]/div/div/button/div/div"); bm014.Click(); Thread.Sleep(5000);
                    string url_app_id = driver.Url;
                    #endregion

                    #region tạo Bước 2                                        
                    for (int a_tien_trinh_2 =0; a_tien_trinh_2<8; a_tien_trinh_2++)
                    {
                        try
                        {
                            driver.Url = url_app_id; driver.Navigate(); Thread.Sleep(5000);
                            string id_002 = Regex.Match(driver.PageSource, @"uiContextualLayerParent""><div><div id=""(.*?)(?="")").Value.Replace("uiContextualLayerParent\"><div><div id=\"", "");
                            var bm015 = driver.FindElementByXPath("//*[@id=\"" + id_002 + "\"]/div/div/div/div[3]/div/div[1]/div/div/div/div/div[2]/div[2]/div[6]/div/div/div[3]/div[3]/div/div[2]/div/div/div[2]/button/div/div"); bm015.Click(); Thread.Sleep(5000);
                            string id_003_1 = Regex.Match(driver.PageSource, @"autocorrect=""off""><label class=(.*?)"">").Value.Replace("autocorrect=", "");
                            string id_003 = Regex.Match(id_003_1, @"id=""(.*?)(?="">)").Value.Replace("id=\"", "");
                            var bm016 = driver.FindElementByXPath("//*[@id=\"" + id_003 + "\"]/input"); bm016.SendKeys(bm11);
                            string id_004 = Regex.Match(driver.PageSource, @"input aria-checked=""false"" class="""" id=""(.*?)(?="")").Value.Replace("input aria-checked=\"false\" class=\"\" id=\"", "");
                            var bm017 = driver.FindElementById(id_004); bm017.Click(); Thread.Sleep(5000);
                            var b018 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b018.Click(); Thread.Sleep(5000);
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[6]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[8]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[9]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            a_tien_trinh_2 = 9;
                        }
                        catch { }
                    }
                    #endregion

                    #region tạo Bước 3
                    string b023_url = "error";
                    for (int a_tien_trinh_3=0; a_tien_trinh_3<8; a_tien_trinh_3++)
                    {
                        try {
                            driver.Url = url_app_id; driver.Navigate(); Thread.Sleep(5000);
                            string id_005 = Regex.Match(driver.PageSource, @"uiContextualLayerParent""><div><div id=""(.*?)(?="")").Value.Replace("uiContextualLayerParent\"><div><div id=\"", "");
                            string id_006_url = driver.FindElementByXPath("//*[@id=\"" + id_005 + "\"]/div/div/div/div[3]/div/div[1]/div/div/div/div/div[2]/div[2]/div[6]/div/div/div[4]/a").GetAttribute("href");
                            driver.Url = id_006_url; driver.Navigate(); Thread.Sleep(5000);

                            try
                            {
                                try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[2]/div/div/div/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                                try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div/div[3]/div/div[2]/div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/span[2]/div/div[1]/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                                b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value");
                            }
                            catch
                            {

                                try
                                {
                                    try
                                    {
                                        string id_007 = Regex.Match(driver.PageSource, @"uiScrollableAreaWrap scrollable"" id=""(.*?)(?="")").Value.Replace("uiScrollableAreaWrap scrollable\" id=\"", "");
                                        var id_007_1 = driver.FindElementByXPath("//*[@id=\"" + id_007 + "\"]/div/div/div/div/div[2]/div/div"); id_007_1.Click(); Thread.Sleep(2000);
                                    }
                                    catch { }


                                    try
                                    {
                                        string id_007 = Regex.Match(driver.PageSource, @"100%;""><div class=""uiScrollableAreaWrap scrollable"" id=""(.*?)(?="")").Value.Replace("100%;\"><div class=\"uiScrollableAreaWrap scrollable\" id=\"", "");
                                        var id_007_1 = driver.FindElementByXPath("//*[@id=\"" + id_007 + "\"]/div/div/div/div/div[2]/div/div"); id_007_1.Click(); Thread.Sleep(2000);
                                    }
                                    catch { }

                                    try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[2]/div/div/div/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                                    try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div/div[3]/div/div[2]/div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/span[2]/div/div[1]/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }

                                    b023_url = "buoc_cuoi";
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[6]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[8]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[9]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi") { MessageBox.Show("Kiểm tra lỗi không thêm được URL vào SEVER"); }
                                }
                                catch { }
                            }
                            a_tien_trinh_3 = 9;
                        } catch { }
                    }                    
                    #endregion

                    string[] id_bm = url_app_id.Split('=');
                    ketqua = b023_url + "|" + bm01 + "|" + id_bm[1] + "|" + bm04;
                }                
            }
            catch { }

            return ketqua;
        }

        #endregion
        
        #region AUTO TẠO BM2
        public string tao_bm2()
        {
            string ketqua = "error";
            try
            {
                
                string data_sever = GetData("https://vip.danplay.net/?auto=adp_facebook&cau_hinh_bm&key=" + textBox5.Text);
                string bm01 = null; string bm02 = null; string bm03 = null; string bm04 = null; string bm05 = null; string bm06 = null; string bm07 = null; string bm08 = null;
                string bm09 = null; string bm10 = null; string bm11 = null;
                if (data_sever.Length > 5)
                {
                    #region tạo bước 1
                    driver.Url = "https://business.facebook.com/pub/start"; driver.Navigate(); Thread.Sleep(3000);
                    string id_001_1 = Regex.Match(driver.PageSource, @"uiContextualLayerParent""><div id=""(.*?)(?="")").Value.Replace("uiContextualLayerParent\"><div id=\"", "");
                    var bm001 = driver.FindElementByXPath("//*[@id=\"" + id_001_1 + "\"]/div/div/div/div/div/div/div/div[3]/button/div/div"); bm001.Click(); Thread.Sleep(5000);
                    try { var tao_moi_001 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[4]/div[2]/div/div/div/div/div/div/div[2]/span[2]/div/div[1]/button/div/div"); tao_moi_001.Click(); Thread.Sleep(2000); } catch { }
                    string html_001 = driver.PageSource;
                    string bm_name = Regex.Match(html_001, @"business-creation-user-name"" id=""(.*?)(?="")").Value.Replace("business-creation-user-name\" id=\"", "");
                    string bm_email = Regex.Match(html_001, @"business-creation-email"" id=""(.*?)(?="")").Value.Replace("business-creation-email\" id=\"", "");
                    string bm_name_dn_1 = Regex.Match(html_001, @"business-creation-business-name(.*?)(?=value)").Value.Replace("business-creation-business-name", "");
                    string bm_name_dn = Regex.Match(bm_name_dn_1, @"id=""(.*?)(?="")").Value.Replace("id=\"", "");
                    string bm_chon_qg = Regex.Match(html_001, @"role=""button"" tabindex=""0"" id=""(.*?)(?="")").Value.Replace("role=\"button\" tabindex=\"0\" id=\"", "");
                    try { string[] arr = data_sever.Split('|'); bm01 = arr[1]; bm02 = arr[2]; bm03 = arr[3]; bm04 = arr[4]; bm05 = arr[5]; bm06 = arr[6]; bm07 = arr[7]; bm08 = arr[8]; bm09 = arr[9]; bm10 = arr[10]; bm11 = arr[11]; } catch { }
                    if (bm02 != "" && bm02 != "auto") { var bm002_1 = driver.FindElementById(bm_name); bm002_1.SendKeys(bm02); Thread.Sleep(500); }
                    var bm003 = driver.FindElementById(bm_email); bm003.SendKeys(bm03); Thread.Sleep(500);
                    if (bm01 == "auto") { bm01 = GetData("https://vip.danplay.net/?auto=adp_facebook&get_ten_vn"); }
                    var bm004 = driver.FindElementById(bm_name_dn); bm004.SendKeys(bm01); Thread.Sleep(5000);
                    string fix_error_001 = Regex.Match(driver.PageSource, @"" + bm01 + "(.*?)role=\"button\"").Value.Replace(" role=\"button\"", "");
                    bm_chon_qg = Regex.Match(fix_error_001, @"id=""(.*?)(?="")").Value.Replace("id=\"", "");
                    var bm_qg_chon = driver.FindElementById(bm_chon_qg); bm_qg_chon.Click(); Thread.Sleep(1000);
                    var bm_001 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div/div/div/div/span/label/input"); bm_001.SendKeys(bm04); Thread.Sleep(5000);
                    string bm_chon_qg_1 = Regex.Match(driver.PageSource, @"uiScrollableAreaWrap scrollable"" id=""(.*?)(?="")").Value.Replace("uiScrollableAreaWrap scrollable\" id=\"", "");
                    var dm_chon_di = driver.FindElementByXPath("//*[@id=\"" + bm_chon_qg_1 + "\"]/div/div/div/div/div/div/li/div/div[1]/div/input"); dm_chon_di.Click(); Thread.Sleep(1000);
                    var bm_tiep_01 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[4]/div[2]/div/div/div/div/div/div/div[2]/span[2]/div/div[2]/button/div/div"); bm_tiep_01.Click(); Thread.Sleep(15000);
                    try {
                        string elk_01 = Regex.Match(driver.PageSource, @"</div></div></span></div></div></div></div></div></div></div></div></div><div class=(.*?)(?=style)").Value.Replace("business-creation-business-name", "");
                        string elk_02 = Regex.Match(elk_01, @"id=""(.*?)(?="")").Value.Replace("id=\"", "");
                        var click_tiep_001 = driver.FindElementByXPath("//*[@id=\"" + elk_02 + "\"]/div[2]/div/div/div/div[3]/button"); click_tiep_001.Click(); Thread.Sleep(10000);
                    } catch { }

                    //File.WriteAllText("res00.html", driver.PageSource);  //100044968365006
                    //MessageBox.Show("Đứng đây để bắt lôi ");
                    string[] url_app_id_1 = driver.Url.Split('='); string url_app_id = "https://business.facebook.com/home/accounts?business_id=" + url_app_id_1[1];
                    #endregion

                    #region tạo Bước 2                                        
                    for (int a_tien_trinh_2 = 0; a_tien_trinh_2 < 8; a_tien_trinh_2++)
                    {
                        try
                        {
                            driver.Url = url_app_id; driver.Navigate(); Thread.Sleep(5000);
                            string id_002 = Regex.Match(driver.PageSource, @"uiContextualLayerParent""><div><div id=""(.*?)(?="")").Value.Replace("uiContextualLayerParent\"><div><div id=\"", "");
                            var bm015 = driver.FindElementByXPath("//*[@id=\"" + id_002 + "\"]/div/div/div/div[3]/div/div[1]/div/div/div/div/div[2]/div[2]/div[6]/div/div/div[3]/div[3]/div/div[2]/div/div/div[2]/button/div/div"); bm015.Click(); Thread.Sleep(5000);
                            string id_003_1 = Regex.Match(driver.PageSource, @"autocorrect=""off""><label class=(.*?)"">").Value.Replace("autocorrect=", "");
                            string id_003 = Regex.Match(id_003_1, @"id=""(.*?)(?="">)").Value.Replace("id=\"", "");
                            var bm016 = driver.FindElementByXPath("//*[@id=\"" + id_003 + "\"]/input"); bm016.SendKeys(bm11);
                            string id_004 = Regex.Match(driver.PageSource, @"input aria-checked=""false"" class="""" id=""(.*?)(?="")").Value.Replace("input aria-checked=\"false\" class=\"\" id=\"", "");
                            var bm017 = driver.FindElementById(id_004); bm017.Click(); Thread.Sleep(5000);
                            var b018 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[5]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b018.Click(); Thread.Sleep(5000);
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[6]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[8]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            try { var b019 = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[9]/div[2]/div/div/div/div/div/div/div[3]/span[2]/div/div[2]/button/div/div"); b019.Click(); Thread.Sleep(5000); } catch { }
                            a_tien_trinh_2 = 9;
                        }
                        catch { }
                    }
                    #endregion

                    #region tạo Bước 3
                    string b023_url = "error";
                    for (int a_tien_trinh_3 = 0; a_tien_trinh_3 < 8; a_tien_trinh_3++)
                    {
                        try
                        {
                            driver.Url = url_app_id; driver.Navigate(); Thread.Sleep(5000);
                            string id_005 = Regex.Match(driver.PageSource, @"uiContextualLayerParent""><div><div id=""(.*?)(?="")").Value.Replace("uiContextualLayerParent\"><div><div id=\"", "");
                            string id_006_url = driver.FindElementByXPath("//*[@id=\"" + id_005 + "\"]/div/div/div/div[3]/div/div[1]/div/div/div/div/div[2]/div[2]/div[6]/div/div/div[4]/a").GetAttribute("href");
                            driver.Url = id_006_url; driver.Navigate(); Thread.Sleep(5000);

                            try
                            {
                                try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[2]/div/div/div/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                                try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div/div[3]/div/div[2]/div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/span[2]/div/div[1]/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                                b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value");
                            }
                            catch
                            {

                                try
                                {
                                    try
                                    {
                                        string id_007 = Regex.Match(driver.PageSource, @"uiScrollableAreaWrap scrollable"" id=""(.*?)(?="")").Value.Replace("uiScrollableAreaWrap scrollable\" id=\"", "");
                                        var id_007_1 = driver.FindElementByXPath("//*[@id=\"" + id_007 + "\"]/div/div/div/div/div[2]/div/div"); id_007_1.Click(); Thread.Sleep(2000);
                                    }
                                    catch { }


                                    try
                                    {
                                        string id_007 = Regex.Match(driver.PageSource, @"100%;""><div class=""uiScrollableAreaWrap scrollable"" id=""(.*?)(?="")").Value.Replace("100%;\"><div class=\"uiScrollableAreaWrap scrollable\" id=\"", "");
                                        var id_007_1 = driver.FindElementByXPath("//*[@id=\"" + id_007 + "\"]/div/div/div/div/div[2]/div/div"); id_007_1.Click(); Thread.Sleep(2000);
                                    }
                                    catch { }

                                    try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[2]/div/div/div/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                                    try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div/div[3]/div/div[2]/div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/span[2]/div/div[1]/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }

                                    b023_url = "buoc_cuoi";
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[6]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[8]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi")
                                    {
                                        try { b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[9]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value"); } catch { }
                                    }
                                    if (b023_url == "buoc_cuoi") { MessageBox.Show("Kiểm tra lỗi không thêm được URL vào SEVER"); }
                                }
                                catch { }
                            }
                            a_tien_trinh_3 = 9;
                        }
                        catch { }
                    }
                    #endregion

                    string[] id_bm = url_app_id.Split('=');
                    ketqua = b023_url + "|" + bm01 + "|" + id_bm[1] + "|" + bm04;
                }

                
            }
            catch { }

            return ketqua;
        }
        #endregion

        #region test bm
        public string test_bm(string url_app_id)
        {            
            driver.Url = url_app_id; driver.Navigate(); Thread.Sleep(5000); string b023_url = "error";
            string id_005 = Regex.Match(driver.PageSource, @"uiContextualLayerParent""><div><div id=""(.*?)(?="")").Value.Replace("uiContextualLayerParent\"><div><div id=\"", "");
            string id_006_url = driver.FindElementByXPath("//*[@id=\"" + id_005 + "\"]/div/div/div/div[3]/div/div[1]/div/div/div/div/div[2]/div[2]/div[6]/div/div/div[4]/a").GetAttribute("href");
            driver.Url = id_006_url; driver.Navigate(); Thread.Sleep(5000);

            try
            {
                try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[2]/div/div/div/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div/div[3]/div/div[2]/div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/span[2]/div/div[1]/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value");
            }
            catch
            {

                try
                {
                    try
                    {
                        string id_007 = Regex.Match(driver.PageSource, @"uiScrollableAreaWrap scrollable"" id=""(.*?)(?="")").Value.Replace("uiScrollableAreaWrap scrollable\" id=\"", "");
                        var id_007_1 = driver.FindElementByXPath("//*[@id=\"" + id_007 + "\"]/div/div/div/div/div[2]/div/div"); id_007_1.Click(); Thread.Sleep(2000);
                    }
                    catch { }


                    try
                    {
                        string id_007 = Regex.Match(driver.PageSource, @"100%;""><div class=""uiScrollableAreaWrap scrollable"" id=""(.*?)(?="")").Value.Replace("100%;\"><div class=\"uiScrollableAreaWrap scrollable\" id=\"", "");
                        var id_007_1 = driver.FindElementByXPath("//*[@id=\"" + id_007 + "\"]/div/div/div/div/div[2]/div/div"); id_007_1.Click(); Thread.Sleep(2000);
                    }
                    catch { }
                    try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[2]/div/div/div/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                    try { var b022 = driver.FindElementByXPath("//*[@id=\"biz_settings_content_view\"]/div/div[3]/div[2]/div/div[3]/div/div[2]/div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/span[2]/div/div[1]/span/button/div/div"); b022.Click(); Thread.Sleep(5000); } catch { }
                    b023_url = driver.FindElementByXPath("//*[@id=\"facebook\"]/body/div[7]/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/label/input").GetAttribute("value");
                }
                catch { }
            }
            string[] id_bm = url_app_id.Split('=');
            string ketqua = b023_url + "|" + id_bm[1];
            return "oke";
        }
        #endregion

        #region Đổi ngôn ngữ VIA
        public void ngon_ngu_via(string ngon_ngu)
        {
            try
            {
                driver.Url = "https://m.facebook.com/language.php?n=%2Fhome.php"; driver.Navigate(); Thread.Sleep(5000);
                string st001 = Regex.Match(driver.PageSource, @"context-layer-root content-pane.*?<div><div class").Value.Replace("<div><div class", "");
                string st002 = Regex.Match(st001, @"id="".*?(?="")").Value.Replace("id=\"", "");
                var n001 = driver.FindElementByXPath("//*[@id=\"" + st002 + "\"]/div/div/div[1]/div/input"); n001.SendKeys(ngon_ngu); Thread.Sleep(5000);
                var n002 = driver.FindElementByXPath("//*[@id=\"" + st002 + "\"]/div/div/div[2]/div[2]/span[1]"); n002.Click();
            }
            catch { }
        }
        #endregion

        #region CÁC GHI CHÚ cho công việc
        private void CheckBox20_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Kháng các bài viết bị spam";
        }
        private void CheckBox20_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox13_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Đăng bài (share link) lên các nhóm";
        }

        private void CheckBox13_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox17_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Chấp nhận kết bạn và gửi lời mời theo gợi ý";
        }

        private void CheckBox17_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox11_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox11_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Đổi tên VIA thành tên TIẾNG VIỆT";
        }

        private void CheckBox5_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Xin nhóm theo list ở thư viện nhóm cá nhân";
        }

        private void CheckBox5_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox12_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Đọc thông báo, like, kết bạn tạo tương tác";
        }

        private void CheckBox12_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }
        private void CheckBox25_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox25.CheckState == CheckState.Checked) { File.WriteAllText("data\\win.txt", "oke"); } else { if (File.Exists("data\\win.txt")) { File.Delete("data\\win.txt"); } }
        }

        private void CheckBox4_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Check thông tin nhóm trong VIA";
        }

        private void CheckBox4_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox14_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Rời nhóm theo yêu cầu";
        }

        private void CheckBox14_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox22_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Auto tạo BM";
        }

        private void CheckBox22_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }
        
        private void CheckBox27_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox27_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Đổi mật khẩu cho VIA";
        }

        private void CheckBox2_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox2_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Đăng xuất tất cả thiết bị";
        }

        private void CheckBox28_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Đăng bài lên tường và comment";
        }

        private void CheckBox28_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }

        private void CheckBox29_MouseHover(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Reset modem trước khi bắt đầu";
        }

        private void CheckBox29_MouseLeave(object sender, EventArgs e)
        {
            this.groupBox19.Text = "Tương tác";
        }
        #endregion

        #region AUTO đăng bài lên tường
        public void dang_bai_spam_len_tuong(string id_facebook)
        {
            try
            {                
                string[] a001 = GetData("https://vip.danplay.net/?auto=adp_facebook&noi_dung_tuong=1&key="+textBox5.Text).Split('|');
                string tieu_de = a001[0]; string link_hinh_anh = a001[1]; int vong_lap = Convert.ToInt32(a001[2]); 
                try
                {
                    driver.Url = "https://m.facebook.com/" + id_facebook + "/allactivity?category_key=statuscluster"; driver.Navigate();
                    driver.Url = "https://m.facebook.com/" + id_facebook + "/allactivity?category_key=statuscluster"; driver.Navigate();
                    string id_post_0 = Regex.Match(driver.PageSource, @"/story\.php\?story_fbid=(.*?)(?="")").Value.Replace("amp;", "");
                    driver.Url = "https://m.facebook.com" + id_post_0; driver.Navigate();
                    string id_a01 = Regex.Match(driver.PageSource, @"&quot;:false}&quot;}"" id=""(.*?)(?="")").Value.Replace("&quot;:false}&quot;}\" id=\"", "");
                    var xoa_1 = driver.FindElementByXPath("//*[@id=\"" + id_a01 + "\"]/a"); xoa_1.Click(); Thread.Sleep(5000);
                    var xoa_2 = driver.FindElementByXPath("//*[@id=\"root\"]/div[4]/div/div/div[2]/a[25]"); xoa_2.Click(); Thread.Sleep(5000);
                    string id_a02 = Regex.Match(driver.PageSource, @"role=""dialog"" id=""(.*?)(?="")").Value.Replace("role=\"dialog\" id=\"", "");
                    var xoa_3 = driver.FindElementByXPath("//*[@id=\"" + id_a02 + "\"]/div/div/div[2]/div[1]/a"); xoa_3.Click();
                }
                catch { }

                driver.Url = "https://m.facebook.com/" + id_facebook; driver.Navigate();
                string id_post = Regex.Match(driver.PageSource, @"style.display = 'none';"" id=""(.*?)(?="")").Value.Replace("style.display = 'none';\" id=\"", "");
                var c001 = driver.FindElementById(id_post); c001.Click(); Thread.Sleep(3000);
                string ten_hinh_anh = link_hinh_anh.Replace("/", ""); ten_hinh_anh = ten_hinh_anh.Replace(".", ""); ten_hinh_anh = ten_hinh_anh.Replace(":", "");
                string xoa_anh_di = "data_img\\" + ten_hinh_anh + ".jpg"; if (!File.Exists(xoa_anh_di)) { tai_anh_v1(link_hinh_anh, ten_hinh_anh); }
                string link_file = "data_img\\" + ten_hinh_anh + ".jpg";                
                String path_img = Path.Combine(Environment.CurrentDirectory, link_file);
                driver.FindElementByCssSelector("input[type='file']").SendKeys(path_img);
                var c002 = driver.FindElementById("uniqid_1"); c002.SendKeys(tieu_de); Thread.Sleep(3000);
                var nut_ne = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[3]/div/button"); nut_ne.Click();
                for (int i = 0; i < 20; i++)
                {
                    try { driver.Url = "https://m.facebook.com/" + id_facebook + "/allactivity?category_key=statuscluster"; driver.Navigate(); i = 200; } catch { Thread.Sleep(2000); }
                }
                string url_dang_cmt = null;

                for (int i = 0; i < 20; i++)
                {
                    try
                    {
                        driver.Url = "https://m.facebook.com/" + id_facebook + "/allactivity?category_key=statuscluster"; driver.Navigate();
                        string id_post_1 = Regex.Match(driver.PageSource, @"/story\.php\?story_fbid=(.*?)(?="")").Value.Replace("amp;", "");
                        url_dang_cmt = "https://m.facebook.com" + id_post_1; i = 200;
                    }
                    catch { Thread.Sleep(2000); }
                }


                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        driver.Url = url_dang_cmt; driver.Navigate();
                        string noi_dung_cmt = GetData("https://vip.danplay.net/?auto=adp_facebook&noi_dung_tuong=2&key=" + textBox5.Text);
                        var link_ne = driver.FindElementById("composerInput"); link_ne.SendKeys(noi_dung_cmt); Thread.Sleep(3000);
                        var dang_di = driver.FindElementByName("submit"); dang_di.Click(); Thread.Sleep(5000);
                    }
                    catch { }
                }

            }
            catch { }

        }
        #endregion

        #region checkpointButtonGetStarted
        public void checkpointButtonGetStarted()
        {
           for(int i = 0; i<10; i++)
            {
                try { var a1 = driver.FindElementById("checkpointButtonGetStarted"); a1.Click(); Thread.Sleep(5000); } catch { }
                try { var a2 = driver.FindElementById("checkpointButtonContinue"); a2.Click(); Thread.Sleep(5000);} catch { }
            }            
        }
        #endregion

        #region COPY selenium
        //driver.Url = "https://email.danplay.net/?e=email&key=5236658255"; driver.Navigate();
        //var a1 = driver.FindElementByName("uid"); a1.SendKeys(id_check);
        //var a2 = driver.FindElementByName("email"); a2.SendKeys(mail_mailto);
        //var a3 = driver.FindElementByName("title"); a3.SendKeys(mail_voice);                           
        //Invoke((Action)(() => Clipboard.SetText(mail_conten))); MessageBox.Show(mail_conten);
        //var a4 = driver.FindElementByName("conten"); a4.SendKeys(OpenQA.Selenium.Keys.Control+"v");
        //var a5 = driver.FindElementByXPath("/html/body/form/button"); a5.Click();
        #endregion
    }
}
