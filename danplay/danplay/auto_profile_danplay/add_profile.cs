﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{     
    class add_profile:danplay
    {
        private string profile_name;
        public add_profile(string profile_name)
        {
            this.profile_name = profile_name;            
        }

        string ProfileFolderPath = "Profile";

        #region EndTask
        public void EndTask(string taskname)
        {
            string processName = taskname.Replace(".exe", "");
            foreach (Process process in Process.GetProcessesByName(processName))
            {
               try { process.Kill(); } catch { }
            }
        }
        #endregion

        #region profile_rong
        public void profile_rong()
        {                                  
            if (!Directory.Exists(ProfileFolderPath)) { Directory.CreateDirectory(ProfileFolderPath); }
            string filePath_rog = "Profile\\profile_rong.zip"; if (File.Exists(filePath_rog)) { File.Delete(filePath_rog); }
            try {
                string profile_mau = "Profile\\profile_rong";
                string profile_mau_zip = "Profile\\profile_rong.zip";
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("user-data-dir="+ profile_mau);
                options.AddArgument("--window-size=600,700");
                options.AddArgument("--disable-infobars");
                options.AddArgument("--disable-notifications");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                driver = new ChromeDriver(driverService, options);
                Thread.Sleep(5000);
                try { driver.Quit(); driver.Close(); } catch { }

                #region bản nháp
                //if (!File.Exists(profile_mau_zip))
                //{
                //    for (int i = 0; i < 10; i++)
                //    {
                //        EndTask("chromedriver");
                //        EndTask("chrome");
                //        Thread.Sleep(500);
                //    }
                //    ZipFile.CreateFromDirectory(profile_mau, profile_mau_zip);
                //}
                #endregion
            }
            catch {  }                      
        }
        #endregion

        #region profile_danplay
        public void profile_danplay()
        {            
            if (!Directory.Exists(ProfileFolderPath)) { Directory.CreateDirectory(ProfileFolderPath); }
            string filePath_rog = "Profile\\profile_danplay.zip"; if (File.Exists(filePath_rog)) { File.Delete(filePath_rog); }
            try
            {
                string profile_mau = "Profile\\profile_danplay";
                string profile_mau_zip = "Profile\\profile_danplay.zip";
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("user-data-dir=" + profile_mau);
                options.AddArgument("--window-size=600,700");
                options.AddArgument("--disable-infobars");
                options.AddArgument("--disable-notifications");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                driver = new ChromeDriver(driverService, options);               
            }
            catch { }
        }
        #endregion
        


    }
}
