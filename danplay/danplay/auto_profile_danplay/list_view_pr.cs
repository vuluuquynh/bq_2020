﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class list_view_pr : danplay
    {
        private string profile_name;
        public list_view_pr(string profile_name)
        {
            this.profile_name = profile_name;
        }
        #region mở profile PC
        public void start_profile_pc()
        {            
            string profile_add = "Profile";
            string profile_new = "Profile\\" + profile_name;
            string profile_old = "Profile\\profile_danplay";
            string profile_old_zip = "Profile\\profile_danplay.zip";
            string kq_profile_nem = "";
            try
            {
                if (!Directory.Exists(profile_old)) { MessageBox.Show("Bạn chưa tạo Profile mẫu", "Lổi hệ thống"); }
                else
                {
                    if (!Directory.Exists(profile_add)) { Directory.CreateDirectory(profile_add); }
                    if (!Directory.Exists(profile_new))
                    {
                        if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                        if (!Directory.Exists(profile_new)) { Directory.CreateDirectory(profile_new); }
                        try { ZipFile.ExtractToDirectory(profile_old_zip, profile_new); } catch { }
                    }
                    else
                    {
                        try
                        {
                            string file_moi2 = profile_new + "\\chrome_debug.log";  //Nếu PROFILE chưa bị xóa thì ko cần làm bước này
                            if (!File.Exists(file_moi2))
                            {
                                if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                                string file_goc1 = profile_new + "\\Default\\Cookies";
                                string file_moi1 = profile_new + "\\Default\\VBD-Cookies";
                                System.IO.File.Copy(file_goc1, file_moi1, true); // Copy qua cái mới                            
                                if (File.Exists(file_goc1)) { File.Delete(file_goc1); } //Xóa cái củ    
                                ZipFile.ExtractToDirectory(profile_old_zip, profile_new); //Giải nén
                                System.IO.File.Copy(file_moi1, file_goc1, true); //Chuyển cái bản sao thành bản chính
                                if (File.Exists(file_moi1)) { File.Delete(file_moi1); } //Xóa bản sao
                            }
                        }
                        catch { }
                    }
                    ChromeOptions options = new ChromeOptions();
                    options.AddArgument("user-data-dir=Profile\\" + profile_name);
                    options.AddArgument("--window-size=1100,1000");
                    options.AddArgument("--disable-infobars");
                    options.AddArgument("--disable-notifications");
                    var driverService = ChromeDriverService.CreateDefaultService();
                    driverService.HideCommandPromptWindow = true;
                    driver = new ChromeDriver(driverService, options);
                    kq_profile_nem = "oke";

                }
            }
            catch { string filePath_rog = "Profile\\profile_danplay.zip"; if (File.Exists(filePath_rog)) { File.Delete(filePath_rog); } kq_profile_nem = "error"; }            
        }
        #endregion

        #region mở profile mẫu thường
        public void start_profile()
        {
            string profile_add = "Profile";
            string profile_new = "Profile\\" + profile_name;
            string profile_old = "Profile\\profile_danplay";
            string profile_old_zip = "Profile\\profile_danplay.zip";
            string kq_profile_nem = "";
            try
            {
                if (!Directory.Exists(profile_old)) { MessageBox.Show("Bạn chưa tạo Profile mẫu", "Lổi hệ thống"); }
                else
                {
                    if (!Directory.Exists(profile_add)) { Directory.CreateDirectory(profile_add); }
                    if (!Directory.Exists(profile_new))
                    {
                        if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                        if (!Directory.Exists(profile_new)) { Directory.CreateDirectory(profile_new); }
                        try { ZipFile.ExtractToDirectory(profile_old_zip, profile_new); } catch { }
                    }
                    else
                    {
                        try
                        {
                            string file_moi2 = profile_new + "\\chrome_debug.log";  //Nếu PROFILE chưa bị xóa thì ko cần làm bước này
                            if (!File.Exists(file_moi2))
                            {
                                if (!File.Exists(profile_old_zip)) { ZipFile.CreateFromDirectory(profile_old, profile_old_zip); }
                                string file_goc1 = profile_new + "\\Default\\Cookies";
                                string file_moi1 = profile_new + "\\Default\\VBD-Cookies";
                                System.IO.File.Copy(file_goc1, file_moi1, true); // Copy qua cái mới                            
                                if (File.Exists(file_goc1)) { File.Delete(file_goc1); } //Xóa cái củ    
                                ZipFile.ExtractToDirectory(profile_old_zip, profile_new); //Giải nén
                                System.IO.File.Copy(file_moi1, file_goc1, true); //Chuyển cái bản sao thành bản chính
                                if (File.Exists(file_moi1)) { File.Delete(file_moi1); } //Xóa bản sao
                            }
                        }
                        catch { }
                    }
                    ChromeOptions options = new ChromeOptions();
                    options.AddArgument("user-data-dir=Profile\\" + profile_name);
                    options.AddArgument("--window-size=600,700");
                    options.AddArgument("--disable-infobars");
                    options.AddArgument("--disable-notifications");
                    var driverService = ChromeDriverService.CreateDefaultService();
                    driverService.HideCommandPromptWindow = true;
                    driver = new ChromeDriver(driverService, options);
                    kq_profile_nem = "oke";

                }
            }
            catch { string filePath_rog = "Profile\\profile_danplay.zip"; if (File.Exists(filePath_rog)) { File.Delete(filePath_rog); } kq_profile_nem = "error"; }
        }
        #endregion
    }
}
