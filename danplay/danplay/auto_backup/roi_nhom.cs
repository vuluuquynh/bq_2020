﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class roi_nhom : danplay
    {
        private string input_0;
        public roi_nhom(string input_0)
        {
            this.input_0 = input_0;
        }
        public void start_roi_nhom()
        {
            try
            {
                string[] a_ip1 = input_0.Split('|'); int n_ip = a_ip1.Count(); //Xét đầu vào của input

                if (n_ip > 1)
                {
                    int a = 1;
                    while (a < 2)
                    {
                        try
                        {
                            string ar_account = key_danplay("change", "list_acc_backup");
                            string[] arr_a1 = ar_account.Split('|');
                            string account_id = arr_a1[0]; string account_login = arr_a1[1]; string account_pass = arr_a1[2]; string account_cookie = arr_a1[3];
                            string kq_dp = dang_nhap_fb(account_id, account_login, account_pass, account_cookie);
                            if (kq_dp.Length > 7)
                            {
                                roi_nhom_fb();

                            }
                            Thread.Sleep(5000); try { driver.Close(); driver.Quit(); } catch { } //Tắt cửa sổ chrome
                        }
                        catch { a = 10; }
                        try { driver.Close(); driver.Quit(); } catch { }
                    }

                }

                if (n_ip == 1)
                {
                    string ar_account = key_danplay("change", "login_profile_fb&account=" + input_0);
                    string[] arr_a1 = ar_account.Split('|');
                    string account_id = arr_a1[0]; string account_login = arr_a1[1]; string account_pass = arr_a1[2]; string account_cookie = arr_a1[3];
                    string kq_dp = dang_nhap_fb(account_id, account_login, account_pass, account_cookie);
                    if (kq_dp.Length > 7)
                    {
                        roi_nhom_fb();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Hệ thống không hoạt động được\n\nCó thể lỗi do dữ liệu bạn nhập vào không tồn tại\n\nHoặc đã có bản mới hơn từ sever!", "Lỗi hệ thống");
            }
        }
    }
}
