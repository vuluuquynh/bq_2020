﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace danplay
{
    class post_v2:danplay
    {
        private string type;
        private string list_via;
        private string id_pc_run;
        private string luu_profile;
        private string xin_nhom;
        private string quet_nhom;
        private string binh_luan;
        private string post_link;
        private string post_file;
        private string so_file;
        private string so_cmt;
        public post_v2(string type, string list_via, string id_pc_run, string luu_profile, string xin_nhom, string quet_nhom, string binh_luan, string post_link, string post_file, string so_file, string so_cmt)
        {
            this.type = type;
            this.list_via = list_via;
            this.id_pc_run = id_pc_run;
            this.luu_profile = luu_profile;
            this.xin_nhom = xin_nhom;
            this.quet_nhom = quet_nhom;
            this.binh_luan = binh_luan;
            this.post_link = post_link;
            this.post_file = post_file;
            this.so_file = so_file;
            this.so_cmt = so_cmt;
        }       

        #region Post_groups_facebook
        string Post_groups_facebook(string id_nhom, string post_spam_data, string cookie, string account_id)
        {
            string kq_post = "";
            try
            {
                #region post lần 1 index ảnh                
                driver.Url = "https://m.facebook.com/groups/" + id_nhom;
                driver.Navigate(); Thread.Sleep(5000);
                go_check_poit();
                var post_09 = driver.FindElementsByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                if (post_09.Count > 0)
                {
                    var post_11 = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                    post_11.Click(); Thread.Sleep(1000);
                }
                else
                {
                    string html_home1 = driver.PageSource;
                    string id_click_post = Regex.Match(html_home1, @"style.display = 'none';"".*?(?="">)").Value.Replace("style.display = 'none';\" id=\"", "");
                    if (id_click_post == "")
                    {
                        id_click_post = Regex.Match(html_home1, @"textbox.*?(?="">)").Value.Replace("textbox\" id=\"", "");
                    }
                    var post_10 = driver.FindElementByXPath("//*[@id=\"" + id_click_post + "\"]/div");
                    post_10.Click(); Thread.Sleep(1000);

                }
                Thread.Sleep(3000);
                for (int ia = 0; ia < 10; ia++)
                {
                    try
                    {

                        try
                        {
                            var post_22_226 = driver.FindElementByXPath("//*[@id=\"composer-placeholder-textarea\"]");
                            post_22_226.SendKeys(post_spam_data); Thread.Sleep(5000); ia = 10;
                        }
                        catch { }

                        try
                        {
                            var post_22_225 = driver.FindElementByXPath("//*[@id=\"uniqid_1\"]");
                            post_22_225.SendKeys(post_spam_data); Thread.Sleep(5000); ia = 10;
                        }
                        catch { }

                    }
                    catch { Thread.Sleep(2000); }
                }

                #endregion                
                #region lặp lại lần 2 để lấy ảnh                      
                driver.Url = "https://m.facebook.com/groups/" + id_nhom;
                driver.Navigate(); Thread.Sleep(5000);

                for (int l2 = 0; l2 < 10; l2++)
                {
                    try
                    {


                        try
                        {
                            var post_11_001 = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                            post_11_001.Click(); Thread.Sleep(3000); l2 = 10;
                        }
                        catch { }


                        try
                        {
                            string html_home1_001 = driver.PageSource;
                            string id_click_post_001 = Regex.Match(html_home1_001, @"style.display = 'none';"".*?(?="">)").Value.Replace("style.display = 'none';\" id=\"", "");
                            if (id_click_post_001 == "")
                            {
                                id_click_post_001 = Regex.Match(html_home1_001, @"textbox.*?(?="">)").Value.Replace("textbox\" id=\"", "");
                            }
                            var post_10_001 = driver.FindElementByXPath("//*[@id=\"" + id_click_post_001 + "\"]/div");
                            post_10_001.Click(); Thread.Sleep(1000); l2 = 10;
                        }
                        catch { }



                    }
                    catch
                    {
                        Thread.Sleep(2000);
                    }

                }


                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        try
                        {
                            var post_22_225_002 = driver.FindElementByXPath("//*[@id=\"composer-placeholder-textarea\"]");
                            post_22_225_002.SendKeys(post_spam_data); Thread.Sleep(5000); i = 10;
                        }
                        catch { }

                        try
                        {
                            var post_22_225_001 = driver.FindElementByXPath("//*[@id=\"uniqid_1\"]");
                            post_22_225_001.SendKeys(post_spam_data); Thread.Sleep(5000); i = 10;
                        }
                        catch { }
                    }
                    catch
                    {
                        Thread.Sleep(2000);
                    }
                }

                #endregion

                for (int ip = 0; ip < 10; ip++)
                {
                    try
                    {

                        try
                        {
                            var post_13_1 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[3]/div/button");
                            post_13_1.Click(); Thread.Sleep(2000); ip = 10;
                        }
                        catch { }

                        try
                        {
                            var post_13 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[1]/div/div[3]/div/button");
                            post_13.Click(); Thread.Sleep(2000); ip = 10;
                        }
                        catch { }


                    }
                    catch { Thread.Sleep(2000); }
                }


                kq_post = "oke";
            }
            catch { kq_post = "error"; }

            return kq_post;
        }
        #endregion

        #region click_de_viet_bai
        void click_de_viet_bai(string id_nhom_post)
        {
            driver.Url = "https://m.facebook.com/groups/" + id_nhom_post; driver.Navigate(); Thread.Sleep(5000);
            var post_09 = driver.FindElementsByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
            if (post_09.Count > 0)
            {
                var post_11 = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                post_11.Click(); Thread.Sleep(1000);
            }
            else
            {
                string html_home1 = driver.PageSource;
                string id_click_post = Regex.Match(html_home1, @"style.display = 'none';"".*?(?="">)").Value.Replace("style.display = 'none';\" id=\"", "");
                if (id_click_post == "")
                {
                    id_click_post = Regex.Match(html_home1, @"textbox.*?(?="">)").Value.Replace("textbox\" id=\"", "");
                }
                var post_10 = driver.FindElementByXPath("//*[@id=\"" + id_click_post + "\"]/div");
                post_10.Click(); Thread.Sleep(1000);

            }
            Thread.Sleep(4000);
        }
        #endregion

        #region nut_tai_file
        void nut_tai_file(string duong_dan)
        {
            String path_img = Path.Combine(Environment.CurrentDirectory, duong_dan);
            driver.FindElementByCssSelector("input[type='file']").SendKeys(path_img);
        }
        #endregion

        #region viet_bai_facebook - nút viết test
        void viet_bai_facebook(string noi_dung_text)
        {
            for (int ia = 0; ia < 10; ia++)
            {
                try
                {
                    try
                    {
                        var post_22_226 = driver.FindElementByXPath("//*[@id=\"composer-placeholder-textarea\"]");
                        post_22_226.SendKeys(noi_dung_text); Thread.Sleep(5000); ia = 99;
                    }
                    catch { }

                    try
                    {
                        if (ia < 10)
                        {
                            var post_22_225 = driver.FindElementByXPath("//*[@id=\"uniqid_1\"]");
                            post_22_225.SendKeys(noi_dung_text); Thread.Sleep(5000); ia = 99;
                        }
                    }
                    catch { }

                }
                catch { Thread.Sleep(2000); }
            }
        }
        #endregion

        #region nut_dang_bai - nút đăng bài
        void nut_dang_bai()
        {
            for (int ip = 0; ip < 10; ip++)
            {
                try
                {

                    try
                    {
                        var post_13_1 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[3]/div/button");
                        post_13_1.Click(); Thread.Sleep(2000); ip = 10;
                    }
                    catch { }

                    try
                    {
                        var post_13 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[1]/div/div[3]/div/button");
                        post_13.Click(); Thread.Sleep(2000); ip = 10;
                    }
                    catch { }


                }
                catch { Thread.Sleep(2000); }
            }
        }
        #endregion

        #region post_file_v2
        string post_file_v2()
        {
            string list_file_can_tai = null;
            try
            {
                if (!Directory.Exists("post_file")) { Directory.CreateDirectory("post_file"); }
                String path = Path.Combine(Environment.CurrentDirectory, "post_file");
                string fullPath = null;
                int int_file = 1;
                int leng_st_file = 0;
                string[] imgExtension = { "*.png", "*.jpg", "*.jpeg", ".gif", "*.bmp", "*.mp4", "*.avi", "*.mov", "*.m4v", "*.mpe", "*.mpeg", "*.mpeg4" };
                DirectoryInfo dir = new DirectoryInfo(path);
                string list_file = null;
                foreach (string ext in imgExtension)
                {
                    FileInfo[] folder = dir.GetFiles(ext, SearchOption.AllDirectories);
                    foreach (FileInfo file in folder)
                    {
                        FileStream fs = file.OpenRead();
                        fullPath = @"post_file\" + file.Name;
                        list_file = list_file + "|" + fullPath;
                    }
                }
                leng_st_file = list_file.Split('|').Length-1;
                try
                {
                    string list_file_v1 = list_file;
                    Random ran_v2 = new Random();
                     try { int_file = Int32.Parse(so_file); } catch { int_file = 1; }
                    string[] ar_list_file = list_file_v1.Split('|'); int leng_st = 0;
                    for (int i1 = 0; i1 < int_file; i1++)
                    {
                        ar_list_file = list_file_v1.Split('|'); leng_st = ar_list_file.Length;
                        int y = ran_v2.Next(1, leng_st);
                        list_file_v1 = list_file_v1.Replace("|" + ar_list_file[y], "");
                        list_file_can_tai = list_file_can_tai + "|" + ar_list_file[y];
                    }
                }
                catch { MessageBox.Show("Bạn yêu cầu POST: "+ int_file + " file \n\nBạn hiện có: "+ leng_st_file + " chứa ở thư mục\n\nAUTO chưa đủ level tự tạo ra file để POST", "Thông báo lỗi"); }
            }
            catch { MessageBox.Show("Lỗi hệ thống, có gì đó không đúng theo yêu cầu của AUTO", "Thông báo lỗi"); }
            return list_file_can_tai;
        }
        #endregion

        #region post_groups_v2
        string post_groups_v2(string id_account, string id_nhom, string noidung, string cookie, string file_post = null)
        {
            string kq = null;
            try {

                click_de_viet_bai(id_nhom);
                if (file_post != null)
                {
                    string[] ap_v2 = file_post.Split('|'); int i_v2 = ap_v2.Length;
                    for (int i = 1; i < i_v2; i++)
                    {
                        nut_tai_file(ap_v2[i]);
                    }
                }                                 
                viet_bai_facebook(noidung);

                if (file_post == null)
                {
                    click_de_viet_bai(id_nhom);
                    viet_bai_facebook(noidung);                    
                } 

                Thread.Sleep(5000);
                nut_dang_bai(); kq = "oke";
                key_danplay("post", "them_ghi_chu&account_id=" + id_account + "&ghi_chu_moi=POST bài thành công");
            } catch { kq = "error"; }
            return kq;
        }
        #endregion

        #region chuyên comment
        string chuyen_cmt_spam(string kq_post_url, string account_id, string account_country, string id_group)
        {
            string kq = null;
            driver.Url = kq_post_url; driver.Navigate(); Thread.Sleep(5000);
            string[] apsas = kq_post_url.Split('/');
            int n_cts = 0;
            try { n_cts = Int32.Parse(so_cmt); } catch { n_cts = 1; }
            for (int icm = 0; icm < n_cts; icm++)
            {

                try
                {                    
                    var cmt_001 = driver.FindElementByXPath("//*[@id=\"comment_form_" + account_id + "_" + apsas[6] + "\"]/div[1]/div[2]");
                    cmt_001.Click(); Thread.Sleep(1000);
                    var cmt_002 = driver.FindElementByXPath("//*[@id=\"composerInput\"]");
                    string noi_dung_post_vcmt = key_danplay("post", "conten_post_groups&country_post_facebook=" + account_country + "&id_nhom_post=" + id_group + "&loai_post=conten_cmt");
                    cmt_002.SendKeys(noi_dung_post_vcmt);
                    var cmt_003 = driver.FindElementByXPath("//*[@id=\"comment_form_" + account_id + "_" + apsas[6] + "\"]/div[1]/div[3]/button");
                    try { cmt_003.Click(); Thread.Sleep(500); cmt_003.Click(); Thread.Sleep(500); cmt_003.Click(); Thread.Sleep(500); cmt_003.Click(); Thread.Sleep(500); } catch { }
                    Thread.Sleep(2000);
                    kq = "oke";
                }
                catch { driver.Url = kq_post_url; driver.Navigate(); Thread.Sleep(5000); }
            }

            return kq;
        }
        #endregion

        #region binh_luan_cmt
        void binh_luan_cmt(string datts, string account_country, string account_id)
        {            
            driver.Url = "https://www.facebook.com/profile.php?id="+ account_id + "&sk=allactivity&entry_point=profile_shortcut"; driver.Navigate(); Thread.Sleep(5000);
            string data_binh_luan = driver.PageSource;
            string text_nhoms = null;
            var ar_2323 = datts.Split('|');
            foreach (var vl_ss2 in ar_2323)
            {
                try {
                    if (vl_ss2 != "") {                        
                        string link1 = Regex.Match(data_binh_luan, @"/groups/" + vl_ss2 + "/permalink/.*?/").Value;
                        if (link1.Length < 5)
                        {
                            text_nhoms = Regex.Match(data_binh_luan, @"<a class=""profileLink"" href=""/groups/.*?(?=/)").Value.Replace("<a class=\"profileLink\" href=\"/groups/","");
                            link1 = Regex.Match(data_binh_luan, @"/groups/" + text_nhoms + "/permalink/.*?/").Value;
                        }                        
                        #region không tìm thấy link lưu lại html để tìm lỗi
                        if (link1.Length < 25) {
                            //Ghi lại các lỗi để sữa                        
                            string data_erreo = driver.PageSource;
                            data_erreo = link1 + "\n\n" + vl_ss2 + " - " + text_nhoms + "\n\nID: " + account_id+ "\n\n" + data_erreo;
                            File.WriteAllText("error_" + vl_ss2 + ".html", data_erreo);                            
                        }
                        #endregion

                        #region đi bình luận nếu tìm thấy link
                        if (link1.Length > 20){
                            driver.Url = "https://m.facebook.com" + link1; driver.Navigate(); Thread.Sleep(5000);
                            string[] apsas = link1.Split('/');
                            int n_cts = 0;
                            try { n_cts = Int32.Parse(so_cmt); } catch { n_cts = 1; }
                            for (int icm = 0; icm < n_cts; icm++)
                            {
                                try
                                {
                                    var cmt_001 = driver.FindElementByXPath("//*[@id=\"comment_form_" + account_id + "_" + apsas[4] + "\"]/div[1]/div[2]");
                                    cmt_001.Click(); Thread.Sleep(1000);
                                    var cmt_002 = driver.FindElementByXPath("//*[@id=\"composerInput\"]");
                                    string noi_dung_post_vcmt = key_danplay("post", "conten_post_groups&country_post_facebook=" + account_country + "&id_nhom_post=" + vl_ss2 + "&loai_post=conten_cmt");
                                    cmt_002.SendKeys(noi_dung_post_vcmt);
                                    var cmt_003 = driver.FindElementByXPath("//*[@id=\"comment_form_" + account_id + "_" + apsas[4] + "\"]/div[1]/div[3]/button");
                                    try { cmt_003.Click(); Thread.Sleep(500); cmt_003.Click(); Thread.Sleep(500); cmt_003.Click(); Thread.Sleep(500); cmt_003.Click(); Thread.Sleep(500); } catch { }
                                    Thread.Sleep(2000);
                                }
                                catch { driver.Url = "https://m.facebook.com" + link1; driver.Navigate(); Thread.Sleep(5000); }
                            }

                            link1 = null;
                        }
                        #endregion
                    }
                } catch { MessageBox.Show("Lỗi giao diện, HTML","HTML");  }
            }            
        }
        #endregion

        public void start_auto_post_facebook()
        {

            #region đoạn cần mở ra lại mà test                
            //profile_danplay_mau("100008426167401");
            //binh_luan_cmt("1007381475962397|430085597607444|","VN");
            //Thread.Sleep(500000);
            #endregion            

            #region account_id - account_name - account_pass - account_cookie - html_page - id_facebook - ghichu - kq_tao_yh                                               
            int tien_trinh = 0; string html_check_login = ""; string id_facebook = ""; string kq_tao_yh = ""; string danh_sach_nhom = null;//Khởi tạo các giá trị ban đầu
            string html_page = ""; string account_id = ""; string account_name = ""; string account_pass = ""; string ghichu = "auto"; string account_limit = ""; string account_country = ""; string account_cookie = ""; int cookie_cu_ok = 0; //Các giá trị thường xuyên sữ dụng
            try
            {
                string profile_via = key_danplay("post", "list_acc_via&list_via=" + list_via + "&type=" + type + "&id_pc=" + id_pc_run); string[] arr_list = profile_via.Split('|');
                //MessageBox.Show(profile_via+"\nID PC: "+id_pc_run+"\nVIA: "+ list_via+"\ntype: "+type, "Tiến key trình: " + tien_trinh);
                account_id = arr_list[0]; account_name = arr_list[1]; account_pass = arr_list[2]; account_limit = arr_list[3]; account_country = arr_list[4]; account_cookie = arr_list[5];
                tien_trinh = 1;
            }
            catch { tien_trinh = 99; }

            if (tien_trinh == 1)
            {
                string kq_tao = null;
                try { kq_tao = profile_danplay_mau(account_name); } catch { tien_trinh = 99; }
                if (kq_tao == "oke") { tien_trinh = 1; } else { tien_trinh = 99; }
            }
            #endregion

            #region nếu tạo profile oke thì kiểm tra xem có đăng nhập sẳn không
            if (tien_trinh == 1)
            {
                try
                {
                    driver.Url = "https://facebook.com"; driver.Navigate(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                    if (id_facebook.Length > 7) { tien_trinh = 2; }
                }
                catch { tien_trinh = 99; }
            }
            #endregion

            try
            {
                #region sữ dụng cookie để login nếu nó có tồn tại giá trị ở sever
                if (tien_trinh == 1 && account_cookie != "not" && id_facebook.Length < 7)
                {
                    var temp = account_cookie.Split(';');
                    foreach (var item in temp)
                    {
                        var temp2 = item.Split('=');
                        if (temp2.Count() > 1)
                        { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                    }
                    driver.Navigate().Refresh(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                    if (id_facebook.Length > 7) { tien_trinh = 2; cookie_cu_ok = 1; }
                    if (driver.Url.Contains("checkpoint")) { tien_trinh = 4; }
                    ghichu = "login cookie";
                }
                #endregion

                #region nếu cookie không còn login đc thì đăng nhập bằng id và pass
                if (tien_trinh == 1 && id_facebook.Length < 7)
                {
                    string kq_lg_fb = "";
                    if (account_cookie == "not") { kq_lg_fb = login_facebook(account_name, account_pass); } else { kq_lg_fb = login_facebook(account_name, account_pass, "https://www.facebook.com"); }
                    if (kq_lg_fb == "check") { tien_trinh = 4; }
                    if (kq_lg_fb == "login") { tien_trinh = 3; }
                    if (kq_lg_fb.Length > 7) { tien_trinh = 2; id_facebook = kq_lg_fb; }
                    ghichu = "login pass - " + ghichu;
                }
                #endregion

                #region login oke thì kiểm tra nhóm
                if (tien_trinh == 2)
                {
                    string get_cookie_s = GetCoookieSelenium("https://www.facebook.com/");
                    string data_cookie = key_danplay("change", "add_cookie_sever&id_account=" + account_id + "&cookie=" + get_cookie_s);
                    string kq_nhom_1 = key_danplay("post", "check_add_nhom&id_acc=" + account_id);
                    if (kq_nhom_1 != "0" && quet_nhom=="1")
                    {
                        get_groups_facebook(account_id, id_facebook, data_cookie); //Lấy danh sách nhóm và tải lên sever
                    }
                    if (xin_nhom == "1") {add_groups_facebook(data_cookie, account_id); } 
                    danh_sach_nhom = key_danplay("post", "get_list_groups&id_face=" + account_id + "&limit=" + account_limit);

                    #region Post bài khi có nhóm không duyệt
                    if (danh_sach_nhom.Length > 10)
                    {
                        var arr_nhom = danh_sach_nhom.Split('|');
                        string xoajdjs = null;
                        string link_post_ok = null;
                        foreach (var value_nhom in arr_nhom)
                        {
                            if (value_nhom != "")
                            {
                                string loai_post_v2 = null;
                                if (post_link == "1") { loai_post_v2 = "conten_url"; }
                                if (post_file == "1") { loai_post_v2 = "conten_file"; }
                                if (post_link == "1" || post_file == "1")
                                {
                                    string pos22252245 = null;
                                    string noi_dung_post_v255 = key_danplay("post", "conten_post_groups&country_post_facebook=" + account_country + "&id_nhom_post="+ value_nhom + "&loai_post=" + loai_post_v2);
                                    if (post_file == "1")
                                    {
                                        string file_ossv2 = post_file_v2();
                                        pos22252245 = post_groups_v2(account_id, value_nhom, noi_dung_post_v255, data_cookie, file_ossv2);
                                    }
                                    else { pos22252245 = post_groups_v2(account_id, value_nhom, noi_dung_post_v255, data_cookie); }

                                    if (pos22252245 != "oke") { danh_sach_nhom = danh_sach_nhom.Replace(value_nhom + "|", ""); } else
                                    {
                                        #region tìm giá trị URL sau post
                                        for (int i = 0; i < 100; i++)
                                        {
                                            try
                                            {
                                                driver.Navigate().Refresh();
                                                xoajdjs = driver.PageSource; i = 100;
                                            }
                                            catch { Thread.Sleep(6000); }
                                        }

                                        #region cắt HTML để tìm URL
                                        int lap_while = 0;
                                        while (link_post_ok == null && lap_while < 10)
                                        {

                                            try
                                            {
                                                string ip_post_facebook = Regex.Match(xoajdjs, @":content_owner_id_new." + list_via + ".*?(?=,&quot;feedback_target&quot;:)").Value.Replace("content_owner_id_new", "");
                                                string[] arr_id = ip_post_facebook.Split(':'); link_post_ok = arr_id[7];
                                            }
                                            catch { }

                                            if (link_post_ok == null)
                                            {
                                                try
                                                {
                                                    string ip_post_facebook_2 = Regex.Match(xoajdjs, @"<span><p>.*?(?=amp;refid)").Value;
                                                    link_post_ok = Regex.Match(ip_post_facebook_2, @";id=.*?(?=&)").Value.Replace(";id=", "");
                                                }
                                                catch { }
                                            }

                                            if (link_post_ok == null)
                                            {
                                                try
                                                {
                                                    string ip_post_facebook_1 = Regex.Match(xoajdjs, @"<span><p>.*?(?=refid)").Value;
                                                    link_post_ok = Regex.Match(ip_post_facebook_1, @"id=.*?(?=&amp;)").Value.Replace("id=", "");
                                                }
                                                catch { }
                                            }


                                            if (link_post_ok == null)
                                            {
                                                lap_while++;
                                                try
                                                {
                                                    driver.Url = "https://m.facebook.com/groups/" + value_nhom; driver.Navigate(); Thread.Sleep(6000);
                                                    xoajdjs = driver.PageSource;
                                                }
                                                catch { }
                                            }
                                        }
                                        #endregion

                                        try { string[] skdskkaskka = link_post_ok.Split('&'); link_post_ok = skdskkaskka[0]; } catch { } //Cắt gọn link POST nếu nó bị dư thừa
                                        string kq_post_url = "https://m.facebook.com/groups/"+ value_nhom + "/permalink/" + link_post_ok;
                                        #endregion

                                        if (link_post_ok.Length > 9)
                                        {
                                            string kshqas = chuyen_cmt_spam(kq_post_url, id_facebook, account_country, value_nhom);
                                            if (kshqas == "oke") { danh_sach_nhom = danh_sach_nhom.Replace(value_nhom + "|", ""); }
                                        }
                                    }                                   
                                }

                            }
                        }
                        
                        #region bình luận vào bài viết
                        if (binh_luan == "1" && id_facebook.Length > 7 && danh_sach_nhom.Length>7)
                        {
                            binh_luan_cmt(danh_sach_nhom, account_country, id_facebook);
                        }
                        #endregion

                    }
                    else { key_danplay("post", "them_ghi_chu&account_id=" + account_id + "&ghi_chu_moi=Không có nhóm POST bài"); }


                    tien_trinh = 99;
                }
                #endregion

                #endregion          

                #region login sai thông tin
                if (tien_trinh == 3)
                {
                    key_danplay("post", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);

                    #region xóa thư mục
                    string xoa_profile_sai = "Profile\\" + account_name;
                    try { driver.Close(); driver.Quit(); } catch { }
                    try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion

                #region login bị check
                if (tien_trinh == 4)
                {
                    string url_bi_check = driver.Url;
                    key_danplay("post", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check);

                    #region đổi tên thư mục
                    try { driver.Close(); driver.Quit(); } catch { }
                    try
                    {
                        string thu_muc_cu = "Profile\\" + account_name;
                        string thu_muc_moi = "Profile\\" + id_facebook;
                        Directory.Move(thu_muc_cu, thu_muc_moi);
                    }
                    catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion

            } catch { tien_trinh = 99; }

            #region báo kết quả hoàn thành việc
            if (tien_trinh == 99)
            {
                key_danplay("post", "hoanthanh_auto&id_pc=" + id_pc_run);
                try { if (luu_profile == "4") { string filePath_xoa = "Profile\\" + account_name; if (Directory.Exists(filePath_xoa)) { Directory.Delete(filePath_xoa, true); } } } catch { }  //Xóa profile
                try { driver.Close(); driver.Quit(); } catch { }
            }
            else
            {
                // Thread.Sleep(1800); //Giữ 1800 giây (3 phút) sẽ thoát trong bất kỳ trường hợp nào
                key_danplay("post", "hoanthanh_auto&id_pc=" + id_pc_run);
                try { if (luu_profile == "4") { string filePath_xoa = "Profile\\" + account_name; if (Directory.Exists(filePath_xoa)) { Directory.Delete(filePath_xoa, true); } } } catch { }  //Xóa profile
                try { driver.Close(); driver.Quit(); } catch { }
            }
            #endregion  

        }

    }
}

