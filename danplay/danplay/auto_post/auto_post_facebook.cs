﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using xNet;

namespace danplay
{
    class auto_post_facebook:danplay
    {
        private string type;
        private string list_via;
        private string id_pc_run;
        private string luu_profile;
        public auto_post_facebook(string type, string list_via, string id_pc_run, string luu_profile)
        {
            this.type = type;
            this.list_via = list_via;
            this.id_pc_run = id_pc_run;
            this.luu_profile = luu_profile;
        }

        #region GO_CHECK_POINT: go_check_poit
        void go_check_poit()
        {

            try
            {
                var get_theme_old = driver.FindElementByXPath("//*[@id=\"viewport\"]/div[3]/div/a");
                get_theme_old.Click(); Thread.Sleep(2000);
            }
            catch { }


            for (int c1 = 0; c1 < 20; c1++)
            {
                try
                {
                    var get_chekpoit = driver.FindElementByXPath("//*[@id=\"checkpointSubmitButton-actual-button\"]");
                    get_chekpoit.Click(); Thread.Sleep(2000);
                }
                catch
                {
                    Thread.Sleep(1000);
                    var get_chekpoit_1 = driver.FindElementsByXPath("//*[@id=\"checkpointSubmitButton-actual-button\"]");
                    if (get_chekpoit_1.Count < 1) { c1 = 20; }
                }
            }
        }
        #endregion

        #region get_groups_facebook
        string get_groups_facebook(string account_id, string account_login, string face_cookies)
        {
            string kq_Post_facebook = "";
            try
            {                
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                add_groups_facebook(face_cookies, account_id);
                driver.Url = "https://m.facebook.com/groups/?ref=bookmarks";
                driver.Navigate(); Thread.Sleep(4000);
                go_check_poit();
                for (int i = 0; i < 25; i++)
                {
                    int giay_load = 25 - i;                    
                    js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                    js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);"); Thread.Sleep(500);
                }
                string html_groups = driver.PageSource;                
                var html_groups_1 = Regex.Matches(html_groups, @"href=""/groups/.*?</div></div></a>", RegexOptions.Singleline);
                string data_1 = "";
                string save_data_nhom = "";
                string save_data_post = "-not";
                string nhom_sever_get = key_danplay("all", "get_nhom_facebook&id_face=" + account_login);                
                foreach (var nhom1 in html_groups_1)
                {
                    string[] chuoi_1 = nhom1.ToString().Split('/');
                    string[] chuoi_2 = nhom1.ToString().Split('>');
                    string[] chuoi_3 = chuoi_2[7].Split('<');
                    string[] chuoi_4 = chuoi_2[18].Split('<');                    

                    string nhom_ko_post_duoc = Regex.Match(nhom_sever_get, @"" + chuoi_1[2] + "-not").Value.Replace("-not", "");
                    if (nhom_ko_post_duoc != chuoi_1[2])
                    {
                        #region quét các nhóm mới, chưa lưu lên sever
                        string info_groups = GetData("https://www.facebook.com/groups/" + chuoi_1[2] + "/members/", face_cookies);
                        string thanh_vien = Regex.Match(info_groups, @"<span class=""_grt _50f8"">.*?(?=</span>)").Value.Replace("<span class=\"_grt _50f8\">", "");
                        thanh_vien = thanh_vien.Replace(@".", "").Trim();
                        thanh_vien = thanh_vien.Replace(@",", "").Trim();                        
                        if (thanh_vien != "")
                        {
                            int thanhvien = int.Parse(thanh_vien);                            
                            if (thanhvien > 30000)
                            {
                                string id_nhom = chuoi_1[2];
                                string ten_nhom = chuoi_3[0];
                                int mem_nhom = thanhvien;
                                var html_id_gr = GetData("https://www.facebook.com/groups/" + id_nhom + "/requests/", face_cookies);
                                var chuoi_duyet = Regex.Matches(html_id_gr, @"queue=pending", RegexOptions.Singleline);
                                int so_chuoi = chuoi_duyet.Count;
                                if (so_chuoi != 1)
                                {
                                    save_data_post = "-oke";
                                    data_1 = id_nhom + "|" + mem_nhom + "|" + ten_nhom;                                    
                                    key_danplay("all", "add_nhom_facebook&id_account=" + account_id + "&account=" + account_login + "&add_groups=" + data_1);                                    
                                }
                                else { save_data_post = "-not"; }
                            }
                            else { save_data_post = "-not"; }
                        }
                        #endregion
                    }
                    else { save_data_post = "-not"; }

                    save_data_nhom = save_data_nhom + chuoi_1[2] + save_data_post + "|";
                }
                if (!Directory.Exists("data_groups")) { Directory.CreateDirectory("data_groups"); }
                String filepath = Path.Combine(Environment.CurrentDirectory, "data_groups/" + account_login + ".txt");
                FileStream fs = new FileStream(filepath, FileMode.Create);//Tạo file mới
                StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);//fs là 1 FileStream              
                sWriter.WriteLine(save_data_nhom.Trim());
                sWriter.Flush();
                fs.Close();
                UploadFile("https://danplay.net/file_data/upload.php", "data_groups/" + account_login + ".txt");
                kq_Post_facebook = "oke";
            }
            catch { kq_Post_facebook = "error"; }            
            return kq_Post_facebook;
        }
        #endregion

        #region Post_groups_facebook
        string Post_groups_facebook(string id_nhom, string post_spam_data, string cookie, string account_id)
        {
            string kq_post = "";            
            try
            {
                #region post lần 1 index ảnh                
                driver.Url = "https://m.facebook.com/groups/" + id_nhom;
                driver.Navigate(); Thread.Sleep(5000);
                go_check_poit();
                var post_09 = driver.FindElementsByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                if (post_09.Count > 0)
                {
                    var post_11 = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                    post_11.Click(); Thread.Sleep(1000);
                }
                else
                {
                    string html_home1 = driver.PageSource;
                    string id_click_post = Regex.Match(html_home1, @"style.display = 'none';"".*?(?="">)").Value.Replace("style.display = 'none';\" id=\"", "");
                    if (id_click_post == "")
                    {
                        id_click_post = Regex.Match(html_home1, @"textbox.*?(?="">)").Value.Replace("textbox\" id=\"", "");
                    }
                    var post_10 = driver.FindElementByXPath("//*[@id=\"" + id_click_post + "\"]/div");
                    post_10.Click(); Thread.Sleep(1000);

                }                
                Thread.Sleep(3000);
                for (int ia = 0; ia < 10; ia++)
                {
                    try
                    {

                        try
                        {
                            var post_22_226 = driver.FindElementByXPath("//*[@id=\"composer-placeholder-textarea\"]");
                            post_22_226.SendKeys(post_spam_data); Thread.Sleep(5000); ia = 10;
                        }
                        catch { }

                        try
                        {
                            var post_22_225 = driver.FindElementByXPath("//*[@id=\"uniqid_1\"]");
                            post_22_225.SendKeys(post_spam_data); Thread.Sleep(5000); ia = 10;
                        }
                        catch { }

                    }
                    catch { Thread.Sleep(2000); }
                }

                #endregion                
                #region lặp lại lần 2 để lấy ảnh                      
                driver.Url = "https://m.facebook.com/groups/" + id_nhom;
                driver.Navigate(); Thread.Sleep(5000);

                for (int l2 = 0; l2 < 10; l2++)
                {
                    try
                    {


                        try
                        {
                            var post_11_001 = driver.FindElementByXPath("//*[@id=\"MRoot\"]/div/div[3]/div[1]/table/tbody/tr/td[2]/button");
                            post_11_001.Click(); Thread.Sleep(3000); l2 = 10;
                        }
                        catch { }


                        try
                        {
                            string html_home1_001 = driver.PageSource;
                            string id_click_post_001 = Regex.Match(html_home1_001, @"style.display = 'none';"".*?(?="">)").Value.Replace("style.display = 'none';\" id=\"", "");
                            if (id_click_post_001 == "")
                            {
                                id_click_post_001 = Regex.Match(html_home1_001, @"textbox.*?(?="">)").Value.Replace("textbox\" id=\"", "");
                            }
                            var post_10_001 = driver.FindElementByXPath("//*[@id=\"" + id_click_post_001 + "\"]/div");
                            post_10_001.Click(); Thread.Sleep(1000); l2 = 10;
                        }
                        catch { }



                    }
                    catch
                    {                        
                        Thread.Sleep(2000);
                    }

                }


                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        try
                        {
                            var post_22_225_002 = driver.FindElementByXPath("//*[@id=\"composer-placeholder-textarea\"]");
                            post_22_225_002.SendKeys(post_spam_data); Thread.Sleep(5000); i = 10;
                        }
                        catch { }

                        try
                        {
                            var post_22_225_001 = driver.FindElementByXPath("//*[@id=\"uniqid_1\"]");
                            post_22_225_001.SendKeys(post_spam_data); Thread.Sleep(5000); i = 10;
                        }
                        catch { }
                    }
                    catch
                    {                        
                        Thread.Sleep(2000);
                    }
                }

                #endregion

                for (int ip = 0; ip < 10; ip++)
                {
                    try
                    {                        

                        try
                        {
                            var post_13_1 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[3]/div/button");
                            post_13_1.Click(); Thread.Sleep(2000); ip = 10;
                        }
                        catch { }

                        try
                        {
                            var post_13 = driver.FindElementByXPath("//*[@id=\"composer-main-view-id\"]/div[1]/div/div[3]/div/button");
                            post_13.Click(); Thread.Sleep(2000); ip = 10;
                        }
                        catch { }


                    }
                    catch { Thread.Sleep(2000); }
                }


                kq_post = "oke";
            }
            catch { kq_post = "error"; }

            return kq_post;
        }
        #endregion

        public void start_auto_post_facebook()
        {


            #region account_id - account_name - account_pass - account_cookie - html_page - id_facebook - ghichu - kq_tao_yh                                               
            int tien_trinh = 0; string html_check_login = ""; string id_facebook = ""; string kq_tao_yh = ""; //Khởi tạo các giá trị ban đầu
            string html_page = ""; string account_id = ""; string account_name = ""; string account_pass = ""; string ghichu = "auto"; string account_limit = "";  string account_country = ""; string account_cookie = ""; int cookie_cu_ok = 0; //Các giá trị thường xuyên sữ dụng
            try
            {
                string profile_via = key_danplay("post", "list_acc_via&list_via=" + list_via + "&type=" + type + "&id_pc=" + id_pc_run); string[] arr_list = profile_via.Split('|');
                //MessageBox.Show(profile_via+"\nID PC: "+id_pc_run+"\nVIA: "+ list_via+"\ntype: "+type, "Tiến key trình: " + tien_trinh);
                account_id = arr_list[0]; account_name = arr_list[1]; account_pass = arr_list[2]; account_limit = arr_list[3]; account_country = arr_list[4]; account_cookie = arr_list[5];
                tien_trinh = 1;                
            } catch { tien_trinh = 99; }            

            if (tien_trinh == 1)
            {
                string kq_tao = null;
                try {kq_tao = profile_danplay_mau(account_name); } catch { tien_trinh = 99; }
                if (kq_tao == "oke") { tien_trinh = 1; } else { tien_trinh = 99; }
            }
            #endregion
            
            #region nếu tạo profile oke thì kiểm tra xem có đăng nhập sẳn không
            if (tien_trinh == 1)
            {
               try
                {
                    driver.Url = "https://facebook.com"; driver.Navigate(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                    if (id_facebook.Length > 7) { tien_trinh = 2; }
                } catch { tien_trinh = 99; }
            }
            #endregion

            try {
                #region sữ dụng cookie để login nếu nó có tồn tại giá trị ở sever
                if (tien_trinh == 1 && account_cookie != "not" && id_facebook.Length < 7)
                {
                    var temp = account_cookie.Split(';');
                    foreach (var item in temp)
                    {
                        var temp2 = item.Split('=');
                        if (temp2.Count() > 1)
                        { driver.Manage().Cookies.AddCookie(new Cookie(temp2[0], temp2[1])); }
                    }
                    driver.Navigate().Refresh(); Thread.Sleep(3000);
                    html_check_login = driver.PageSource;
                    id_facebook = Regex.Match(html_check_login, @"(ACCOUNT_ID"":"").*?(?="")").Value.Replace("ACCOUNT_ID\":\"", "");
                    if (id_facebook.Length > 7) { tien_trinh = 2; cookie_cu_ok = 1; }
                    if (driver.Url.Contains("checkpoint")) { tien_trinh = 4; }
                    ghichu = "login cookie";
                }
                #endregion

                #region nếu cookie không còn login đc thì đăng nhập bằng id và pass
                if (tien_trinh == 1 && id_facebook.Length < 7)
                {
                    string kq_lg_fb = "";
                    if (account_cookie == "not") { kq_lg_fb = login_facebook(account_name, account_pass); } else { kq_lg_fb = login_facebook(account_name, account_pass, "https://www.facebook.com"); }
                    if (kq_lg_fb == "check") { tien_trinh = 4; }
                    if (kq_lg_fb == "login") { tien_trinh = 3; }
                    if (kq_lg_fb.Length > 7) { tien_trinh = 2; id_facebook = kq_lg_fb; }
                    ghichu = "login pass - " + ghichu;
                }
                #endregion

                #region login oke thì kiểm tra nhóm
                if (tien_trinh == 2)
                {
                    string data_cookie = null;
                    try
                    {
                        quyen_rieng_tu();
                        driver.Url = "https://m.facebook.com"; driver.Navigate();
                        key_danplay("change", "add_cookie_sever&id_account=" + account_id + "&cookie = " + data_cookie);
                    }
                    catch { }
                    string kq_nhom_1 = key_danplay("post", "check_add_nhom&id_acc=" + account_id);
                    if (kq_nhom_1 != "0")
                    {
                        if (account_cookie != "not" && cookie_cu_ok == 1) { get_groups_facebook(account_id, id_facebook, account_cookie); }
                        else
                        {
                            get_groups_facebook(account_id, id_facebook, data_cookie);
                        }

                    }                    
                    add_groups_facebook(data_cookie, account_id);
                    string danh_sach_nhom = key_danplay("post", "get_list_groups&id_face=" + account_id + "&limit=" + account_limit);
                    if (danh_sach_nhom.Length > 10)
                    {
                        var arr_nhom = danh_sach_nhom.Split('|');
                        foreach (var value_nhom in arr_nhom)
                        {
                            if (value_nhom != "")
                            {
                                string noidungpost = key_danplay("post", "conten_post_groups&country_post_facebook=" + account_country + "&id_nhom_post=" + value_nhom);
                                string kq_auto_post = Post_groups_facebook(value_nhom, noidungpost, data_cookie, account_id);
                                if (kq_auto_post == "oke")
                                {
                                    key_danplay("post", "them_ghi_chu&account_id=" + account_id + "&ghi_chu_moi=POST bài thành công");
                                }

                            }
                        }
                    }
                    else { key_danplay("post", "them_ghi_chu&account_id=" + account_id + "&ghi_chu_moi=Không có nhóm POST bài"); }
                    tien_trinh = 99;
                }
                #endregion

                #region login sai thông tin
                if (tien_trinh == 3)
                {
                    key_danplay("post", "thong_tin_sai&id_login=" + account_name + "&id_account=" + account_id);

                    #region xóa thư mục
                    string xoa_profile_sai = "Profile\\" + account_name;
                    try { driver.Close(); driver.Quit(); } catch { }
                    try { if (Directory.Exists(xoa_profile_sai)) { Directory.Delete(xoa_profile_sai, true); } } catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion

                #region login bị check
                if (tien_trinh == 4)
                {
                    string url_bi_check = driver.Url;
                    key_danplay("post", "login_check&id_account=" + account_id + "&url_check=" + url_bi_check);

                    #region đổi tên thư mục
                    try { driver.Close(); driver.Quit(); } catch { }
                    try
                    {
                        string thu_muc_cu = "Profile\\" + account_name;
                        string thu_muc_moi = "Profile\\" + id_facebook;
                        Directory.Move(thu_muc_cu, thu_muc_moi);
                    }
                    catch { }
                    #endregion
                    tien_trinh = 99;
                }
                #endregion
            }
            catch { tien_trinh = 99; }            

            #region báo kết quả hoàn thành việc
            if (tien_trinh == 99)
            {
                key_danplay("post", "hoanthanh_auto&id_pc=" + id_pc_run);
                try { if (luu_profile == "4") { string filePath_xoa = "Profile\\" + account_name; if (Directory.Exists(filePath_xoa)) { Directory.Delete(filePath_xoa, true); } } } catch { }  //Xóa profile
                try { driver.Close(); driver.Quit(); } catch { }
            }
            else
            {                
               // Thread.Sleep(1800); //Giữ 1800 giây (3 phút) sẽ thoát trong bất kỳ trường hợp nào
                key_danplay("post", "hoanthanh_auto&id_pc=" + id_pc_run);
                try { if (luu_profile == "4") { string filePath_xoa = "Profile\\" + account_name; if (Directory.Exists(filePath_xoa)) { Directory.Delete(filePath_xoa, true); } }  } catch { }  //Xóa profile
                try { driver.Close(); driver.Quit(); } catch { }
            }
            #endregion  

        }

    }
}
